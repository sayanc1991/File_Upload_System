#! /usr/bin/env python

from collections import deque
import sys
import re
import shlex
import subprocess
import json
divider = '------------------------------------------------------------------------------'
p = re.compile(r"[:]+(?![^<]*>)")

cmd = 'bjobs -UF '
cmd_proc = subprocess.Popen(shlex.split(cmd),stdin=subprocess.PIPE,stdout=subprocess.PIPE)
out, err =  cmd_proc.communicate()

segments = [segment for segment in out.split(divider)]

def parseTimeLine(line):
    if(line.find("<")!=-1):
        m = deque(p.finditer(line), maxlen=1).pop()
        elements = re.split('[,]+(?![^<]*>)',re.sub(' +',' ',line[m.end():]))
        obj = {'time':line[:m.start()]}
        if(len(elements)==1):
            if(len(elements[0].split('<'))==1):
                obj['message']=elements[0]
        else:
            for i in elements:
                j = i.split('<')
                if(len(j)>1):
                    obj[j[0].strip()]=re.sub('>','',j[1])
        return obj
    else:
        obj = {'time':line[:19]}
        for matchNum, match in enumerate(re.finditer(r"\s([A-Za-z]*):\s([^;]*);", line[20:])):
            if(len(match.groups())==2):
                obj[match.group(1)]=match.group(2)
        return obj

def parseJobLine(line):
    jobObj={}
    elements = re.split('[,]+(?![^<]*>)',line)
    for i in elements:
        j = i.split('<')
        if(len(j)>0):
            jobObj[j[0].strip()]=re.sub('>','',j[1])
    return jobObj


def parseSegment(x):
    lines = [line for line in segment.split('\n')]
    foundJob = False
    endOfData = False
    jobLine = ''
    timeLines= []
    for line in lines:
        if not foundJob:
            if(line.startswith('Job <')):
                jobLine = line
#                 print("GOT LINE "+line)
                foundJob=True
        else:
            if not endOfData:
                if line=='':
#                     print('EmptyLine'+line)
                    endOfData=True
                else:
                    timeLines.append(line)
    if(foundJob and endOfData):
        bjob = parseJobLine(jobLine)
        bjob['entries'] = []
        for timeLine in timeLines:
            bjob['entries'].append(parseTimeLine(timeLine))
        return bjob
    else:
        return None
    
# parseSegment(segments[1])
finalJson = []
for segment in segments:
    jsonObj = parseSegment(segment)
    if(jsonObj!=None):
        finalJson.append(jsonObj)

print(json.dumps(finalJson))
