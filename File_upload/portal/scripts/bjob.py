#! /usr/bin/env python

from collections import deque
import sys
import re
import shlex
import subprocess
import json

if(len(sys.argv)>1):
    
    cmd = 'bjobs -l '+sys.argv[1]
    cmd_proc = subprocess.Popen(shlex.split(cmd),stdin=subprocess.PIPE,stdout=subprocess.PIPE)
    out, err =  cmd_proc.communicate()


    newline = '''
                     '''
    line2=''':
 '''
    out = out.replace(newline,'')

    lines = [line for line in out.split('\n') if line.strip()]

    bjob={}

    reg_PENDING_REASONS = re.compile(r"PENDING REASONS:+(?![^<]*>)")
    reg_SCHEDULING_PARAMETERS = re.compile(r"SCHEDULING PARAMETERS:+(?![^<]*>)")

    index_pending_reasons=len(lines)
    index_scheduling_parameters=len(lines)

    for line in lines:
        if(reg_PENDING_REASONS.search(line) is not None):
            index_pending_reasons=lines.index(line)
        if(reg_SCHEDULING_PARAMETERS.search(line) is not None):
            index_scheduling_parameters=lines.index(line)


    pending_reasons=[]
    scheduling_parameters={}
    comments=[]
    p = re.compile(r"[:]+(?![^<]*>)")
    
    
    if(index_pending_reasons<index_scheduling_parameters):
        pending_reasons = lines[index_pending_reasons+1:index_scheduling_parameters]

    if(index_scheduling_parameters<len(lines)):
        for i in lines[index_scheduling_parameters+2:len(lines)]:
            j=list(filter((lambda x:x!=''),re.sub(' +',' ',i).split(' ')))
            scheduling_parameters[j[0]]={
                'r15s':j[1],
                'r1m':j[2],
                'r15m':j[3],
                'ut':j[4],
                'pg':j[5],
                'io':j[6],
                'ls':j[7],
                'it':j[8],
                'tmp':j[9],
                'swp':j[10],
                'mem':j[11]
            }

    bjob['scheduling_parameters'] = scheduling_parameters
    bjob['pending_reasons']= pending_reasons
    
    for line in lines[1:index_pending_reasons if(index_pending_reasons<index_scheduling_parameters) else index_scheduling_parameters]:
        m = deque(p.finditer(line), maxlen=1).pop()
        elements = re.split('[,]+(?![^<]*>)',re.sub(' +',' ',line[m.end():]))
        obj = {'time':line[:m.start()]}
        if(len(elements)==1):
            if(len(elements[0].split('<'))==1):
                obj['message']=elements[0]
        else:
            for i in elements:
                j = i.split('<')
                if(len(j)>0):
                    obj[j[0].strip()]=re.sub('>','',j[1])

        comments.append(obj)

    bjob['comments']=comments

    line = lines[1]
    m = deque(p.finditer(line), maxlen=1).pop()
    elements = re.split('[,]+(?![^<]*>)',re.sub(' +',' ',line[m.end():]))
    for i in elements:
        j = i.split('<')
        if(len(j)>0):
            bjob[j[0].strip()]=re.sub('>','',j[1])

    print(json.dumps(bjob))
    
else:
    print(json.dumps({}))
