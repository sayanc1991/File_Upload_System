#! /usr/bin/env python

import shlex
import subprocess
import json
awk_cmd = ''' awk '{print $1","$2","$3","$4","$5","$6","$7","$8","$9","$10","$11}' '''
grep_proc = subprocess.Popen(shlex.split(awk_cmd),stdin=subprocess.PIPE,stdout=subprocess.PIPE)
# CMLS-436 - Chandrakant, The grep has been added to filter out adminq in the output
subprocess.Popen(["bqueues | grep -v adminq"], stdout=grep_proc.stdin, shell=True)
out, err = grep_proc.communicate()
import json
#lines = ['QUEUE_NAME,PRIO,STATUS,MAX,JL/U,JL/P,JL/H,NJOBS,PEND,RUN,SUSP', 'xxlarge,20,Open:Active,-,-,-,-,0,0,0,0', 'xlarge,20,Open:Active,-,-,-,-,0,0,0,0', 'large,20,Open:Active,-,-,-,-,0,0,0,0', 'normal,20,Open:Active,-,-,-,-,0,0,0,0', 'adminq,20,Open:Active,-,-,-,-,0,0,0,0', '']
lines = out.split('\n')
def csvToJson(x):
    ele = x.split(',')
    if(len(ele)==11):
        out = {
            "QUEUE_NAME":ele[0],
            "PRIO":ele[1],
            "STATUS":ele[2],
            "MAX":ele[3],
            "JL/U":ele[4],
            "JL/P":ele[5],
            "JL/H":ele[6],
            "NJOBS":ele[7],
            "PEND":ele[8],
            "RUN":ele[9],
            "SUSP":ele[10],
        }
        return out
    else:
        return None

print(json.dumps(list(filter((lambda x:x!=None),map(csvToJson,lines[1:])))))
