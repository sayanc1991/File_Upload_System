#! /usr/bin/env python

import re
import shlex
import subprocess
import json
cmd = 'lshosts -w'
cmd_proc = subprocess.Popen(shlex.split(cmd),stdin=subprocess.PIPE,stdout=subprocess.PIPE)
out, err =  cmd_proc.communicate()
lines  = out.split('\n')

def getLines(x):
    obj = None
    tmp = re.sub(' +',' ',x).split(' ')
    if(len(tmp)>1):
        obj = {
            'HOST_NAME':tmp[0],
            'type':tmp[1],
            'model':tmp[2],
            'cpuf':tmp[3],
            'ncpus':tmp[4],
            'maxmem':tmp[5],
            'maxswp':tmp[6],
            'server':tmp[7],
            'RESOURCES':','.join(tmp[8:]).replace('(', '').replace(')', '').split(',')
        }
    return obj

print(json.dumps(list(filter((lambda x:x!=None),map(getLines,lines[1:])))))
