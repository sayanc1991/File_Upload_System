package com.cadence.cumulus.events.api;

import com.cadence.cumulus.model.AgentCommand;
import com.cadence.cumulus.model.AgentResponse;
import com.cadence.cumulus.utility.AppCallbackResponesMap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Service;
import org.springframework.web.context.request.async.DeferredResult;


@Service
public class APIEventsPublisher {

    private static final Logger log = LoggerFactory.getLogger(APIEventsPublisher.class);

    @Autowired
    private ApplicationEventPublisher publisher;

    @Autowired
    private AppCallbackResponesMap callbackResponseMap;

    public void pubhishEvent(AgentCommand agentCommand, DeferredResult<AgentResponse> response) {
        log.debug("APIEventsPublisher entered : " + agentCommand);
        log.debug("Request ID added into map for tracking when result comes back");
        if (response != null) {
            callbackResponseMap.callbackMap.put(agentCommand.getRequestId(), response);
        }
        publisher.publishEvent(agentCommand);
    }

}
