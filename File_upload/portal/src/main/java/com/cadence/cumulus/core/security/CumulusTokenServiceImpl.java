package com.cadence.cumulus.core.security;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;

import org.apache.commons.httpclient.HttpStatus;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.authentication.encoding.Md5PasswordEncoder;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import com.cadence.cumulus.common.Constants;
import com.cadence.cumulus.common.CumulusException;
import com.cadence.cumulus.core.CryptoService;
import com.cadence.cumulus.core.TokenService;
import com.cadence.cumulus.dto.TokenStatus;
import com.cadence.cumulus.model.CumulusGrantedAuthority;
import com.cadence.cumulus.model.User;
import com.cadence.cumulus.service.CumulusUserService;

/**
 * Token generation, verification, validity logic with respect to Cumulus Token format
 * 
 * @author anirban
 *
 */

@Component
public class CumulusTokenServiceImpl implements TokenService {

	private static final Logger LOG = LoggerFactory.getLogger(CumulusTokenServiceImpl.class);

	@Value("${token.secret.key}")
	private String secretKey;

	@Value("${token.expiry.days}")
	private Long days;

	@Value("${unauthorized.permission.msg}")
	private String unauthorizedMessage;

	@Value("${token.expiry.msg}")
	private String tokenExpired;

	@Value("${token.tampering.msg}")
	private String tokenTampered;
	
	@Value("${token.not.generated.msg}")
	private String tokenNotGenerated;

	@Value("${user.inactive}")
	private String userInactive;

	@Autowired
	private CumulusUserService userService;

	@Autowired
	private UserAuthorizationServiceImpl userAuthorization;

	@Autowired
	private CryptoService crypto;

	private Md5PasswordEncoder md5;

	@Value("${token.salt.length}")
	private int tokenSaltLength;

	@PostConstruct
	private void init() {
		this.md5 = new Md5PasswordEncoder();
	}
	
	private LocalDateTime calculateUserExpiryDate(Date createdOn) {
		//Date createdOn = user.getTokenCreatedOn();
		LocalDateTime createdOnTimestamp = createdOn.toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime();
		LocalDateTime expiryTimestamp = createdOnTimestamp.plusDays(days);
		return expiryTimestamp;
	}

	@Override
	public TokenStatus generateToken(String loginName) {
		// TODO Auto-generated method stub
		TokenStatus ts = new TokenStatus();
		ts.setValidity(false);
		String result = "";
		int code = 0;
		User user = userService.getUserByEmail(loginName);// email id from okta validation when login credentials provided 
		if(user == null) {
			user = userService.getUserByUserName(loginName);// username from browser session validate against database
		}
		if (user != null) {
			try {
				//String data = user.getId().toString() + "." + String.valueOf(System.currentTimeMillis());//RandomStringUtils.randomAlphanumeric(tokenSaltLength);
				String data = user.getUsername() + "." + String.valueOf(System.currentTimeMillis());
				LOG.info("Token data: {}", data);
				String token = result = crypto.encrypt(secretKey, data);
				LocalDateTime timeStampNow = LocalDateTime.now();
				Date now = Date.from(timeStampNow.atZone(ZoneId.systemDefault()).toInstant());
				String hmac = md5.encodePassword(token, secretKey);
				user.setTokenCreatedOn(now);
				user.setHash(hmac);
				userService.saveUser(user);
				LOG.info("New token: {} generated for User: {} created on: {}", token, loginName, now);
				ts.setValidity(true);
			} catch (CumulusException e) {
				// TODO Auto-generated catch block
				LOG.error("System error: {} Token generation failed for User: {}", loginName, e.getMessage());
				result = e.getMessage();
				code = HttpStatus.SC_INTERNAL_SERVER_ERROR;
			}
		} else {
			String message = result = "User: " + loginName + " is not assigned to Cadence® Cloud Orchestrator in Okta";
			LOG.debug(message);
			code = HttpStatus.SC_UNAUTHORIZED;
		}
		ts.setMessage(result);
		ts.setCode(code);
		return ts;
	}

	@Override
	public TokenStatus verifyToken(String token, String permission) {
		// TODO Auto-generated method stub
		TokenStatus ts = new TokenStatus();
		ts.setValidity(false);
		String result = "";
		int code = 0;
		try {
			LOG.info("Encrypted token: {}", token);
			String decrypted = crypto.decrypt(secretKey, token);
			LOG.info("Decrypted token: {}", decrypted);
			String tokenized[] = decrypted.split("\\.");
			String hmac = md5.encodePassword(token, secretKey);
			String loginName = tokenized[0];
			User user = userService.getUserByUserName(loginName);
			Boolean userFound = user != null;
			LOG.info("User Name: {} ServerHash: {} ClientHash: {}", user.getUsername(), user.getHash(), hmac);
			String userActive = user.getStatus();
			if(Constants.USER_ACTIVE.equals(userActive)) {
				if (userFound) {
					if(StringUtils.hasText(user.getHash())) {// user has generated a token before
						LOG.info("User: {} has previously generated a token", user.getUsername());
						if(user.getHash().equals(hmac)) {// valid token
							LOG.info("User: {} token present in records", user.getUsername());
							TokenStatus status = isTokenExpired(user.getEmail());
							if(!status.getValidity()) {
								LOG.info("User: {} token is yet to expire", user.getUsername());
								if(StringUtils.hasText(permission)) {
									List<GrantedAuthority> authList = userAuthorization.getUserAuthorityList(user);
									for (GrantedAuthority ga : authList) {
										CumulusGrantedAuthority cag = (CumulusGrantedAuthority) ga;
										if (cag.getAuthority().equals(permission)) {
											ts.setValidity(true);
											result = user.getUsername();
											LOG.info("Token of user with ID: {} validated for authorized operation: {}", user.getId(),
													permission);
											break;
										}
									}
									if (!ts.getValidity()) {
										// not permitted
										result = unauthorizedMessage;
										LOG.debug("User ID: {} unauthorized for: {}", user.getId(), permission);
										code = HttpStatus.SC_UNAUTHORIZED;
									}
								} else {
									ts.setValidity(true);
									result = user.getUsername();
								}
							} else {
								// token expired
								result = tokenExpired;
								Long expiryTimestamp  = Long.parseLong(status.getMessage());
								LOG.debug("User ID: {} token expired as expiry date: {}", user.getId(), new Date(expiryTimestamp));
								code = HttpStatus.SC_UNAUTHORIZED;
							}
						} else {// invalid token
							LOG.debug("Token doesn't match with the one on server");
							result = tokenTampered;
							code = HttpStatus.SC_UNPROCESSABLE_ENTITY;
						}
					} else {// user has never generated a token
						result = tokenNotGenerated;
						LOG.debug("User ID: {} has not generated any token", user.getId());
						code = HttpStatus.SC_FORBIDDEN;
					}
				} else {
					// absent user id
					LOG.debug("Token payload compromised: User details unavailable!");
					result = tokenTampered;
					code = HttpStatus.SC_UNPROCESSABLE_ENTITY;
				}
			}else {
				LOG.debug("User is Inactive");
				result = userInactive;
				code = HttpStatus.SC_UNPROCESSABLE_ENTITY;
			}
		} catch (NumberFormatException | CumulusException e) {
			LOG.debug("Token payload compromised: {}", e.getCause());
			result = tokenTampered;
			code = HttpStatus.SC_UNAUTHORIZED;
		} 
		ts.setMessage(result);
		ts.setCode(code);
		return ts;
	}

	@Override
	public TokenStatus isTokenExpired(String loginName) {
		// TODO Auto-generated method stub
		TokenStatus ts = new TokenStatus();
		User user = userService.getUserByEmail(loginName);
		Boolean valid = true;
		String message = "0";
		if(user != null & user.getTokenCreatedOn() != null) {
			LocalDateTime timeStampNow = LocalDateTime.now();
			LOG.info("User ID: {} token created on: {}", user.getId(), user.getTokenCreatedOn());
			LocalDateTime expiryTimestamp = calculateUserExpiryDate(user.getTokenCreatedOn());
			Long expiryInMilliseconds = expiryTimestamp.atZone(ZoneId.systemDefault()).toInstant().toEpochMilli();
			valid = timeStampNow.isAfter(expiryTimestamp);
			message = expiryInMilliseconds.toString();
			LOG.info("User ID: {} token expiring on: {}", user.getId(), expiryTimestamp);
		} 
		ts.setValidity(valid);
		ts.setMessage(message);
		return ts;
	}

}
