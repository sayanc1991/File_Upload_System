package com.cadence.cumulus.common;

public final class Constants {
	public static final String CONTENT_TYPE_KEY = "content-type";
	public static final String CONTENT_TYPE_JSON = "application/json";
	
	public static final String AUTH_KEY = "Authorization";
	public static final String OKTA_AUTH_PREFIX = "SSWS";

	public static final String USERNAME = "username";
	public static final String PASSWORD = "password";
	public static final String DATE_FORMAT = "yyyy-MM-dd hh:mm:ss";
	
	public static final String TCL = "tcl";
	public static final String SH = "sh";
	public static final String MK = "mk";
	
	public static final String NEWSUB ="newsub";
	
	public static final String OPEN_LAVA_USAGE = "openLavaUsage";
	public static final String START_TIME = "startTime";
	
	public static final String STREAM_LOG_PREFIX = "/stream/log.";	
	public static final String STREAM_EVENT_PREFIX = "/stream/event.";

	public static final String ERROR_CODE = " [Error Code: %s]";
	
	public static final String VERSION_FILE = "jenkins.txt";
	
	public static final String INFRA_QUEUE_FILE = "vm-sizes.json";
	public static final String INFRA_QUEUE_NAME = "Name";
	public static final String INFRA_QUEUE_CORE_COUNT = "CoreCount";
	public static final String INFRA_QUEUE_MEMORY = "Memory";
	public static final String INFRA_QUEUE_NORMAL = "normal";
	public static final String INFRA_QUEUE_LARGE = "large";
	public static final String INFRA_QUEUE_XLARGE = "xlarge";
	public static final String INFRA_QUEUE_XXLARGE = "xxlarge";
	
	public static final String USER_ACTIVE = "Active";
			
}
