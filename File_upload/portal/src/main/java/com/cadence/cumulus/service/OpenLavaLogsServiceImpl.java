package com.cadence.cumulus.service;

import com.cadence.cumulus.model.OpenLavaResourceEntity;
import com.cadence.cumulus.repository.mysql.OpenLavaUsageRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.ProcessBuilder.Redirect;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author Vikramaditya Singh
 */
@Service("openLavaLogsService")
public class OpenLavaLogsServiceImpl {

    //TODO: Shambhu - remove this comment once tested without applicationEvent
//public class ResourceUsageServiceImpl implements ApplicationListener<ApplicationEvent>
    private static final Logger log = LoggerFactory.getLogger(OpenLavaLogsServiceImpl.class);

    @Value("${cumulus.app.openLava}")
    private String openLavaPath;

    @Autowired
    OpenLavaUsageRepository openLavaUsageRepository;

    /*
     * (non-Javadoc)
     *
     * @see
     * org.springframework.context.ApplicationListener#onApplicationEvent(org.
     * springframework.context.ApplicationEvent) Starts up on ApplicationReady
     * event. It is used to save, update the job details in openLavaUsage table.
     */
    /*@Override
    public void onApplicationEvent(ApplicationEvent arg0) {
        log.debug("OpenLava Event : " + arg0);
        if (arg0 instanceof ApplicationReadyEvent) {

            log.debug("Inside tail open lava log" + openLavaPath);

            new Thread("ParentEvent") {
                public void run() {
                    try {
                        saveLsbEventsDetails();
                    } catch (IOException | InterruptedException e) {
                        log.error("Error occurred while saving lsb events : " + e);
                    }
                }
            }.start();
            new Thread("ParentAcct") {
                public void run() {
                    try {
                        saveLsbAcctDetails();
                    } catch (IOException | InterruptedException e) {
                        log.error("Error occurred while saving lsb acct events : " + e);
                    }
                }
            }.start();

        }

    }
    */

    @PostConstruct
    public void initOpenLavaLogsProcessing() {

        log.debug("initOpenLavaLogsProcessing entered.. openLava path - " + openLavaPath);

        log.debug("Initializing lsb.events thread...");
        new Thread("ParentEvent") {
            public void run() {
                try {
                    saveLsbEventsDetails();
                } catch (IOException | InterruptedException e) {
                    log.error("Error occurred while saving lsb events : " + e);
                }
            }
        }.start();

        log.debug("Initializing lsb.events thread...");
        new Thread("ParentAcct") {
            public void run() {
                try {
                    saveLsbAcctDetails();
                } catch (IOException | InterruptedException e) {
                    log.error("Error occurred while saving lsb acct events : " + e);
                }
            }
        }.start();

    }

    private void saveLsbAcctDetails() throws InterruptedException, IOException {

        Process processAcct = Runtime.getRuntime().exec("tail -f " + openLavaPath + "/lsb.acct");
        log.debug("saveLsbAcctDetails.. " + "tail -f " + openLavaPath + "/lsb.acct");
        InputStream inputStreamAcct = processAcct.getInputStream();
        BufferedReader bufferedReaderAcct = new BufferedReader(new InputStreamReader(inputStreamAcct));

        new Thread("Accts:") {
            public void run() {
                log.debug("Inside run Acct: ");
                String line = null;
                try {
                    while ((line = bufferedReaderAcct.readLine()) != null) {
                        String[] logVal = line.split(" ");
                        String firstVal = logVal[0].replace("\"", "");
                        if (firstVal.equals(ResourceUsageConstants.JOB_FINISH)) {

                            OpenLavaResourceEntity openLavaResourceEntity = openLavaUsageRepository
                                    .findByJobId(logVal[3]);
                            if (null != openLavaResourceEntity && (null == openLavaResourceEntity.getFinishTime() || "".equals(openLavaResourceEntity.getFinishTime()))) {
                                log.debug("Persisting finish job data : " + openLavaResourceEntity.getJobId());
                                Date finishTime = new Date(Long.parseLong(logVal[2] + "000"));
                                openLavaResourceEntity.setFinishTime(logVal[2] + "000");
                                DecimalFormat executionTwoDecimal = new DecimalFormat("#.00");
                                String executionTime = executionTwoDecimal.format(
                                        Double.valueOf(finishTime.getTime() - Long.valueOf(openLavaResourceEntity.getStartTime()))
                                                / (1000 * 60));
                                log.debug("Execution time :" + executionTime + " and finish time : "
                                        + openLavaResourceEntity.getFinishTime());
                                openLavaResourceEntity.setExecutionTime(executionTime);
                                openLavaUsageRepository.save(openLavaResourceEntity);
                                log.debug("Saved finish job data: " + logVal[3]);
                            }
                        } else {
                            log.debug("Skipping lines from Acct");
                        }
                    }
                } catch (Exception e) {
                    log.error("Exception occurred " + e);
                }
            }
        }.start();
        processAcct.waitFor();

    }

    private void saveLsbEventsDetails() throws IOException, InterruptedException {
        Process processEvents = Runtime.getRuntime().exec("tail -f " + openLavaPath + "/lsb.events");
        log.debug("saveLsbEventsDetails.. " + "tail -f " + openLavaPath + "/lsb.events");
        InputStream inputStreamEvents = processEvents.getInputStream();
        BufferedReader bufferedReaderEvents = new BufferedReader(new InputStreamReader(inputStreamEvents));
        new Thread("Events:") {
            public void run() {
                log.debug("Inside run Events: ");
                String line = null;
                try {
                    while ((line = bufferedReaderEvents.readLine()) != null) {
                        String[] logVal = line.split(" ");
                        String firstVal = logVal[0].replace("\"", "");
                        if (firstVal.equals(ResourceUsageConstants.JOB_NEW)) {
                            saveNewJob(logVal);
                        } else if (firstVal.equals(ResourceUsageConstants.JOB_START)) {
                            updateStartedJob(logVal);
                        } else {
                            log.debug("Skipping the line..");
                        }
                    }
                } catch (IOException e) {
                    log.error("Exception occurred " + e);
                }
            }

            private void updateStartedJob(String[] logVal) {

                OpenLavaResourceEntity openLavaResourceEntity = openLavaUsageRepository.findByJobId(logVal[3]);
                if (null != openLavaResourceEntity && ((null == openLavaResourceEntity.getCoresUsed() || "".equals(openLavaResourceEntity.getCoresUsed()))
                        || (null == openLavaResourceEntity.getCountOfHosts() || "".equals(openLavaResourceEntity.getCountOfHosts())))) {
                    log.debug("Updating existing job data : " + logVal[3]);
                    openLavaResourceEntity.setCoresUsed(logVal[8]);
                    String hosts[] = logVal[9].split(" ");
                    String countOfHosts = String.valueOf(hosts.length);
                    openLavaResourceEntity.setCountOfHosts(countOfHosts);
                    openLavaUsageRepository.save(openLavaResourceEntity);
                    log.debug("Updated existing job data : " + logVal[3]);
                }

            }

            private void saveNewJob(String[] logVal) {
                String userName = logVal[13].substring(1, logVal[13].length() - 1);
                if (null == openLavaUsageRepository.findByJobId(logVal[3])) {
                    log.debug("Persisting new job : " + logVal[3]);
                    Process processGroupVal;
                    String groupName = "";
                    OpenLavaResourceEntity openLavaResourceEntity = null;
                    String queue = logVal[28].substring(1, logVal[28].length() - 1);
                    Date startTime = new Date(Long.parseLong(logVal[7] + "000"));
                    openLavaResourceEntity = new OpenLavaResourceEntity(logVal[3], "", userName, queue, "", "", "", "", startTime.getTime() + "");
                    try {
                        List<String> commands = new ArrayList<>();
                        commands.add("groups");
                        commands.add(userName);
                        processGroupVal = new ProcessBuilder(commands).redirectErrorStream(true).inheritIO()
                                .redirectOutput(Redirect.PIPE).start();

                        InputStream inputStreamGroup = processGroupVal.getInputStream();
                        BufferedReader bufferedReaderGroup = new BufferedReader(
                                new InputStreamReader(inputStreamGroup));
                        while ((groupName = bufferedReaderGroup.readLine()) != null) {
                            String group[] = groupName.split(":");
                            if (group.length > 1) {
                                groupName = group[1].trim();
                            }
                            log.debug("Group Name found " + groupName);
                            openLavaResourceEntity.setGroup(groupName);
                        }
                        processGroupVal.waitFor();
                    } catch (IOException e) {
                        log.error("Exception occurred IO : " + e);
                    } catch (InterruptedException e) {
                        log.error("Exception occurred IE : " + e);
                    }
                    log.debug("Group is : " + groupName);

                    openLavaUsageRepository.save(openLavaResourceEntity);
                    log.debug("Saved new job : " + logVal[3]);
                }
            }
        }.start();
        processEvents.waitFor();
    }

}
