package com.cadence.cumulus.dto;

import java.io.OutputStream;

/*
 * Class to capture the result of calling an external command.
 * exitCode is the exit code of the external process
 * output is rge output of the external process
 */
public class ProcessResult {
	private int exitCode;
	private String output;
	
	public ProcessResult(int exitCode, String output) {
		this.exitCode = exitCode;
		this.output = output;
	}
	
	public int getExitCode() {
		return exitCode;
	}

	public void setExitCode(int exitCode) {
		this.exitCode = exitCode;
	}

	public String getOutput() {
		return output;
	}

	public void setOutput(String output) {
		this.output = output;
	}

}