package com.cadence.cumulus.cache.service;

import com.cadence.cumulus.cache.CacheKey;

/**
 * Generic interface to store and retrieve objects from the Cache.
 * The underlying implementation used for achieving the cache can differ 
 * @author zabeer
 *
 */
public interface CacheService {
	
	
	public void storeObject(CacheKey key, Object object);
	
	public Object getObject(CacheKey key);
	
	public void invalidateCache(CacheKey key);

}
