package com.cadence.cumulus;

import org.apache.commons.lang3.SystemUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.aspectj.EnableSpringConfigured;

import com.cadence.cumulus.service.OSFileManager;
import com.cadence.cumulus.service.impl.UnixFileManagerImpl;
import com.cadence.cumulus.service.impl.WindowsFileManagerImpl;

@Configuration
@EnableSpringConfigured
public class OSFileManagerConfig {
	
	private static final Logger log = LoggerFactory.getLogger(OSFileManagerConfig.class);
	
	@Bean
    public OSFileManager fm() {
		String os = System.getProperty("os.name");
    	log.info("OS: {}", os);
    	if(SystemUtils.IS_OS_WINDOWS) {
    		return new WindowsFileManagerImpl();
    	} else {
    		return new UnixFileManagerImpl();
    	}
    }
	
	
}
