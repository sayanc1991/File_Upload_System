package com.cadence.cumulus.service.impl;

import java.util.Map;

import org.apache.http.impl.client.CloseableHttpClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;

import com.cadence.cumulus.service.EmailService;
import com.sendgrid.SendGrid;
import com.sendgrid.SendGridException;

@Service
public class EmailServiceImpl implements EmailService {
    private static final Logger log = LoggerFactory.getLogger(EmailServiceImpl.class);

    @Value("${cumulus.notification.from.email}")
    private String fromAddress;

    @Autowired
    private TemplateEngine templateEngine;

    @Autowired
    private SendGrid sendGrid;
    
    @Autowired
    @Qualifier("sendGridHttpClient")
    private CloseableHttpClient sendGridHttpClient;

    @Override
    public void sendEmail(String template, Map<String, String> params, String toAddress, String subject) {
        sendGrid.setClient(sendGridHttpClient);

        log.debug("Sending email to: {}, with subject: {}", toAddress, subject);

        String body = null;
        if (template != null)  {
	        Context ctx = new Context();
	        for (String key : params.keySet()) {
	            ctx.setVariable(key, params.get(key));
	        }
	
	        body = templateEngine.process(template, ctx);
	        log.debug("Email body: {}", body);
        }

        SendGrid.Email email = new SendGrid.Email();
        email.setFrom(fromAddress);
        email.addTo(toAddress);

        email.setSubject(subject);
        if (body != null)  {
			email.setHtml(body);
        } else {
        	email.setHtml("&nbsp;");
        }

        try {
            SendGrid.Response response = sendGrid.send(email);
            log.debug("Response from sendgrid: {}", response.getMessage());
        } catch (SendGridException sge) {
            log.debug("Error sending email", sge);
        }
    }
}
