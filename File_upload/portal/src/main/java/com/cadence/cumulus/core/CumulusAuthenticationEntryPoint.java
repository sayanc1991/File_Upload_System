package com.cadence.cumulus.core;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.util.StringUtils;

public class CumulusAuthenticationEntryPoint implements AuthenticationEntryPoint {
    private static final Logger log = LoggerFactory.getLogger(CumulusAuthenticationEntryPoint.class);

    @Override
    public void commence(HttpServletRequest request, HttpServletResponse response, AuthenticationException authException) throws IOException, ServletException {
        Cookie cookie = new Cookie("cumulus-auth", null);
        String cookiePath = request.getContextPath();

        if (!StringUtils.hasLength(cookiePath)) {
            cookiePath = "/";
        }
        cookie.setPath(cookiePath);
        cookie.setMaxAge(0);
        response.addCookie(cookie);

        boolean isApiRequest = false;

        String acceptHeaders = request.getHeader("Accept");
        for (String acceptHeader : acceptHeaders.split(",")) {
            log.debug("Checking header: {}", acceptHeader);

            try {
                MediaType header = MediaType.valueOf(acceptHeader);
                if (header.equals(MediaType.APPLICATION_JSON) || header.toString().contains("text/event-stream")) {
                    log.debug("{} Request, sending unauthorized", acceptHeader);
                    isApiRequest = true;
                    break;
                }
            } catch (Exception e) {
                log.error("Error detecting MediaType:", e);
            }
        }

        if (isApiRequest) {
            log.debug("API Request, sending unauthorized");
            response.sendError(HttpStatus.UNAUTHORIZED.value(), "Unauthorized");
        } else {
            log.debug("Non-API Request, sending redirect");
            response.sendRedirect("/login");
        }
    }
}
