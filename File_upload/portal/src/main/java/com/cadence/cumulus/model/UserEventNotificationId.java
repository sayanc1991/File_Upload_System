package com.cadence.cumulus.model;

import java.io.Serializable;

import javax.persistence.Embeddable;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * Id class to represent composite primary key for UserEventNotification entity
 * 
 * @author zabeer
 *
 */
@Embeddable
public class UserEventNotificationId implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private User user;

	private StatusType statusType;

	private NotificationChannel notificationChannel;

	@ManyToOne(fetch=FetchType.LAZY)
	@JsonIgnore
	public StatusType getStatusType() {
		return statusType;
	}

	public void setStatusType(StatusType statusType) {
		this.statusType = statusType;
	}

	@ManyToOne(fetch=FetchType.LAZY)
	@JsonIgnore
	public NotificationChannel getNotificationChannel() {
		return notificationChannel;
	}

	public void setNotificationChannel(NotificationChannel notificationChannel) {
		this.notificationChannel = notificationChannel;
	}

	@ManyToOne(fetch=FetchType.LAZY)
	@JsonIgnore
	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((statusType == null) ? 0 : statusType.hashCode());
		result = prime * result + ((notificationChannel == null) ? 0 : notificationChannel.hashCode());
		result = prime * result + ((user == null) ? 0 : user.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		UserEventNotificationId other = (UserEventNotificationId) obj;
		if (statusType == null) {
			if (other.statusType != null)
				return false;
		} else if (!statusType.equals(other.statusType))
			return false;
		if (notificationChannel == null) {
			if (other.notificationChannel != null)
				return false;
		} else if (!notificationChannel.equals(other.notificationChannel))
			return false;
		if (user == null) {
			if (other.user != null)
				return false;
		} else if (!user.equals(other.user))
			return false;
		return true;
	}

}
