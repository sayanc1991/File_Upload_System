package com.cadence.cumulus.model;

import javax.persistence.AssociationOverride;
import javax.persistence.AssociationOverrides;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;

/**
 * 
 * @author zabeer
 *
 */
@JsonInclude(JsonInclude.Include.NON_EMPTY)
@Entity
@Table(name = "RolePermission")
@AssociationOverrides({
		@AssociationOverride(name = "rolePermissionId.permission", joinColumns = @JoinColumn(name = "PermissionId")),
		@AssociationOverride(name = "rolePermissionId.role", joinColumns = @JoinColumn(name = "RoleId")) })
public class RolePermission extends BaseEntity {

	private RolePermissionId rolePermissionId = new RolePermissionId();

	@EmbeddedId
	public RolePermissionId getRolePermissionId() {
		return rolePermissionId;
	}

	public void setRolePermissionId(RolePermissionId rolePermissionId) {
		this.rolePermissionId = rolePermissionId;
	}

	@Transient
	@JsonIgnore
	public Permission getPermission() {
		return rolePermissionId.getPermission();
	}

	public void setPermission(Permission permission) {
		rolePermissionId.setPermission(permission);
	}

	@Transient
	@JsonIgnore
	public Role getRole() {
		return rolePermissionId.getRole();
	}

	public void setRole(Role role) {
		rolePermissionId.setRole(role);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((rolePermissionId == null) ? 0 : rolePermissionId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		RolePermission other = (RolePermission) obj;
		if (rolePermissionId == null) {
			if (other.rolePermissionId != null)
				return false;
		} else if (!rolePermissionId.equals(other.rolePermissionId))
			return false;
		return true;
	}

}
