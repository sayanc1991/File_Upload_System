/**
 * 
 */
package com.cadence.cumulus.model;

import java.util.Date;
import java.util.Map;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.cadence.cumulus.model.liberate.events.LiberateEvent;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;

/**
 * @author ramsudheer
 *
 */
@Entity
@Table(name = "JobRun")
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class JobRun extends BaseEntity {

	private Long id;

	// Related to Job.id
	private Long jobId;
	private Job job;

	private Date submitTime;
	private Date startTime;
	private Date endTime;
	private String status;

	// Get the pid from agent on start and save it here?
	public long pid;

	private Set<LiberateEvent> liberateEvents;
	
	private Map<String, String> showOnBrowser;
	
	private Map<String, String> read;
	
	/*added for RunJob API Support*/
	@Transient
	private String jobName;

	/**
	 * @return the id
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "Id")
	public Long getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	@ManyToOne
	@JoinColumn(name = "JobId")
	@JsonIgnore
	public Job getJob() {
		return job;
	}

	public void setJob(Job job) {
		this.job = job;
	}

	@Transient
	public String getLiberatePath() {
		return job.getToolPath();
	}

	@Transient
	public String getToolName() {
		return job.getToolName();
	}

	/**
	 * @return the submitTime
	 */
	@Transient
	public Date getSubmitTime() {
		return submitTime;
	}

	/**
	 * @param submitTime
	 *            the submitTime to set
	 */
	public void setSubmitTime(Date submitTime) {
		this.submitTime = submitTime;
	}

	/**
	 * @return the startTime
	 */

	public Date getStartTime() {
		return startTime;
	}

	/**
	 * @param startTime
	 *            the startTime to set
	 */
	public void setStartTime(Date startTime) {
		this.startTime = startTime;
	}

	/**
	 * @return the endTime
	 */

	public Date getEndTime() {
		return endTime;
	}

	/**
	 * @param endTime
	 *            the endTime to set
	 */
	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}

	/**
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * @param status
	 *            the status to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * @return the pid
	 */
	@Transient
	public long getPid() {
		return pid;
	}

	/**
	 * @param pid
	 *            the pid to set
	 */
	public void setPid(long pid) {
		this.pid = pid;
	}

	@Transient
	public String getShebang() {
		return job.getShebang();
	}

	@Transient
	public String getJobName() {
		if(job != null) {
			return this.job.getJobName();
		}
		return this.jobName;
	}

	@Transient
	public Long getJIdFromJob() {

		return job.getId();
	}

	@Transient
	public Long getJobId() {
		if (null != this.jobId) {
			return this.jobId;
		}else if(this.job != null) {
			return this.job.getId();
		}
		return null;
		
	}

	public void setJobId(Long jobId) {
		this.jobId = jobId;
	}

	public void setCreatedDate(Date d) {
		this.setCreatedOn(d);

	}

	public void setLastUpdatedDate(Date d) {
		this.setLastModifiedOn(d);

	}

	/**
	 * @return the jobRuns
	 */
	@OneToMany(mappedBy = "jobRun", cascade = CascadeType.REMOVE, fetch = FetchType.EAGER)
	public Set<LiberateEvent> getLiberateEvents() {
		return liberateEvents;
	}

	/**
	 * @param jobRuns
	 *            the jobRuns to set
	 */
	public void setLiberateEvents(Set<LiberateEvent> liberateEvents) {
		this.liberateEvents = liberateEvents;
	}

	public void setJobName(String jobName) {
		this.jobName = jobName;
	}


	@Convert(converter = MapToStringConverter.class)
	@Column(name = "ReadByUser")
	public Map<String, String> isRead() {
		return read;
	}

	public void setRead(Map<String, String> read) {
		this.read = read;
	}

	@Convert(converter = MapToStringConverter.class)
	@Column(name = "ShowOnBrowser")
	public Map<String, String> getShowOnBrowser() {
		return showOnBrowser;
	}

	public void setShowOnBrowser(Map<String, String> showOnBrowser) {
		this.showOnBrowser = showOnBrowser;
	}
	
}
