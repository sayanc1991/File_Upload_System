package com.cadence.cumulus.common;

/**
 * @author sandeep
 * 
 * Custom Exception to include the details of operation performed and the associated resource
 *
 */
public class ServiceException extends Exception {
	private static final long serialVersionUID = -2248912639808057661L;
	
	String message="";
	String operation="";
	String resource="";
	
	/**
	 * @param message Message from Exception if not handled, descriptive message otherwise
	 * @param operation Operation performed - String constant
	 * @param resource Name of resource associated with operation
	 */
	public ServiceException(String message, String operation, String resource){
		super(message);
		this.message = message;
		this.operation = operation;
		this.resource = resource;
	}
	
	/**
	 * Operations where specific resource information is not available
	 * 
	 * @param message Message from Exception if not handled, descriptive message otherwise
	 * @param operation Operation performed - String constant
	 */
	public ServiceException(String message, String operation){
		super(message);
		this.message = message;
		this.operation = operation;
		this.resource = "";
	}
	
	public String getMessage() {
		return message;
	}
	
	public void setMessage(String message) {
		this.message = message;
	}
	
	public String getOperation() {
		return operation;
	}
	
	public void setOperation(String operation) {
		this.operation = operation;
	}
	
	public String getResource() {
		return resource;
	}
	
	public void setResource(String resource) {
		this.resource = resource;
	}
}
