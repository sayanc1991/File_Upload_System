package com.cadence.cumulus.controller;

import java.security.Principal;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.cadence.cumulus.model.OpenLavaResourceVO;
import com.cadence.cumulus.service.OpenLavaUsageService;

/**
 * @author Vikramaditya Singh
 *
 */
@RestController
@RequestMapping(value="resource")
public class ResourceUsageController {
	
	private static final Logger log = LoggerFactory.getLogger(ResourceUsageController.class);

	@Autowired
	OpenLavaUsageService openLavaUsageService;
	
	/**
	 * @param principal
	 * @param startTime
	 * @param finishTime
	 * @return
	 * Controller method to return list of OpenLavaResourceVO objects based on startTime and FinishTime
	 */
	@GetMapping("/usageByJob/{startTime}/{finishTime}")
	@ResponseStatus(HttpStatus.OK)
	public List<OpenLavaResourceVO> getUsageDetails(Principal principal, @PathVariable("startTime") String startTime, @PathVariable("finishTime") String finishTime){
		log.debug("Inside getUsageDetails ");
		DateFormat df = new SimpleDateFormat("EEE, dd MMM yyyy HH:mm:ss z");
		Date d = null;
		Calendar calendar = Calendar.getInstance();
		try {
			d = df.parse(startTime);
			calendar.setTime(d);
			calendar.set(Calendar.HOUR_OF_DAY, 23);
			calendar.set(Calendar.MINUTE, 59);
			calendar.set(Calendar.SECOND, 59);
			calendar.set(Calendar.MILLISECOND, 999);
		} catch (ParseException e) {
			log.error("Start time can not be parsed " + e);
		}
		List<OpenLavaResourceVO> list;
		list = openLavaUsageService.findUsageDetailsFromRange(calendar.getTimeInMillis(), Long.valueOf(finishTime));
		log.debug("Exiting getUsageDetails");
		return list;
	}
	
}
