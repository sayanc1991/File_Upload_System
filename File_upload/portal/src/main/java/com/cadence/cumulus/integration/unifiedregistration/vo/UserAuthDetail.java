package com.cadence.cumulus.integration.unifiedregistration.vo;

import java.util.List;

import com.cadence.cumulus.model.Permission;
import com.cadence.cumulus.model.Role;
import com.cadence.cumulus.model.User;

public class UserAuthDetail {

	private User user;

	private List<Role> roles;

	private List<Permission> permissions;

	private boolean isAdmin;

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public List<Role> getRoles() {
		return roles;
	}

	public void setRoles(List<Role> roles) {
		this.roles = roles;
	}

	public List<Permission> getPermissions() {
		return permissions;
	}

	public void setPermissions(List<Permission> permissions) {
		this.permissions = permissions;
	}

	public boolean isAdmin() {
		return isAdmin;
	}

	public void setAdmin(boolean isAdmin) {
		this.isAdmin = isAdmin;
	}

}
