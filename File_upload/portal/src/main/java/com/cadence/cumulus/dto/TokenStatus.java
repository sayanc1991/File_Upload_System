package com.cadence.cumulus.dto;

public class TokenStatus {
	
	private Boolean validity;
	private String message;
	private Integer code;
	public Boolean getValidity() {
		return validity;
	}
	public void setValidity(Boolean validity) {
		this.validity = validity;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public TokenStatus(Boolean validity, String message) {
		super();
		this.validity = validity;
		this.message = message;
	}
	public TokenStatus() {
		super();
	}
	public Integer getCode() {
		return code;
	}
	public void setCode(Integer code) {
		this.code = code;
	}
	@Override
	public String toString() {
		return "{validity=" + validity + ", message=" + message + ", code=" + code + "}";
	}
	
}
