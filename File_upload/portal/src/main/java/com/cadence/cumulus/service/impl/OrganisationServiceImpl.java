package com.cadence.cumulus.service.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.cadence.cumulus.common.CumulusException;
import com.cadence.cumulus.model.Organisation;
import com.cadence.cumulus.repository.mysql.OrganisationRepository;
import com.cadence.cumulus.service.OrganisationService;

@Component
@Transactional(propagation = Propagation.REQUIRED, rollbackFor = { Exception.class })
public class OrganisationServiceImpl implements OrganisationService {

    /**
     * Logger
     */
    private static final Logger log = LoggerFactory.getLogger(OrganisationServiceImpl.class);

    @Autowired
    private OrganisationRepository organisationRepository;

	@Override
	public Organisation findByDomain(String domainName) throws CumulusException {
		log.debug("Retrieving organisation for domain name: "+domainName);
        return organisationRepository.findByDomain(domainName);
	}
}