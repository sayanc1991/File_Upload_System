package com.cadence.cumulus.service;

import java.util.List;

import com.cadence.cumulus.common.CumulusException;
import com.cadence.cumulus.model.Countries;

public interface CountriesService {

	public List<Countries> getAllCountries() throws CumulusException;
	
	public Countries getByCountryCode(String countryCode) throws CumulusException;
}
