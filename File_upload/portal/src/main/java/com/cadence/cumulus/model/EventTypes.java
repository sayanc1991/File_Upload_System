package com.cadence.cumulus.model;

public enum EventTypes {

	Started, Progress, Completed, Disconnect, Failed;

}
