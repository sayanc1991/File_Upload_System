package com.cadence.cumulus.controller;

import com.cadence.cumulus.events.api.APIEventsPublisher;
import com.cadence.cumulus.model.AgentCommand;
import com.cadence.cumulus.model.AgentResponse;
import com.cadence.cumulus.model.CumulusUser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.request.async.DeferredResult;
import org.springframework.web.context.request.async.DeferredResult.DeferredResultHandler;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.*;

@RestController
@RequestMapping(value = "/bsub")
public class BsubController {

    /**
     * Logger
     */
    private static final Logger log = LoggerFactory.getLogger(JobRunController.class);


    //Shambhu: Adding event publisher to send commands to for execution
    @Autowired
    private APIEventsPublisher publisher;

    @GetMapping(value = "/{scriptName}")
    @ResponseStatus(HttpStatus.OK)
    public DeferredResult<String> getBqueues(@PathVariable String scriptName, HttpServletResponse response) throws IOException {
 /*       if (!handler.isConnected()) {
            log.debug("Agent not available");

            response.setContentType(MediaType.TEXT_PLAIN_VALUE);
            response.sendError(HttpStatus.SERVICE_UNAVAILABLE.value(), "Agent not available");
            return null;
        }
*/


        log.info("BsubController invoked....");


        //TODO: Shambhu - Remove this once troubleshooting is completed..
        Map<String, String> envMap = System.getenv();
        SortedMap<String, String> sortedEnvMap = new TreeMap<String, String>(envMap);
        Set<String> keySet = sortedEnvMap.keySet();
        for (String key : keySet) {
            String value = envMap.get(key);
            log.info("[" + key + "] " + value);
        }

        CumulusUser user = (CumulusUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        DeferredResult<String> actualResponse = new DeferredResult<>();

        DeferredResult<AgentResponse> deferredResponse = new DeferredResult<>();

        deferredResponse.setResultHandler(new DeferredResultHandler() {
            @Override
            public void handleResult(Object result) {
                actualResponse.setResult(getLastLineFromResponse(((AgentResponse) result).getData()));
            }

            private String getLastLineFromResponse(String data) {
                String[] strArr = data.split("\n");
                return strArr[strArr.length - 1];
            }
        });

        AgentCommand agentCommand = new AgentCommand(UUID.randomUUID().toString(), user.getLoginName(), "bsub", "/usr/bin/python", "/scripts", scriptName + ".py");


        publisher.pubhishEvent(agentCommand, deferredResponse);


        return actualResponse;
    }

}
