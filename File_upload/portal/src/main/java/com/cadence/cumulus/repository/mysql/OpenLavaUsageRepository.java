package com.cadence.cumulus.repository.mysql;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.cadence.cumulus.model.OpenLavaResourceEntity;


/**
 * @author Vikramaditya Singh
 *
 */
@Repository
public interface OpenLavaUsageRepository extends JpaRepository<OpenLavaResourceEntity, Long>{

	public OpenLavaResourceEntity findByStartTime(String startTime);
	
	public OpenLavaResourceEntity findByJobId(String jobId);
	
	@Query(value="select * from OpenLavaUsage where StartTime >= ?1 and StartTime <= ?2", nativeQuery= true)
	public List<OpenLavaResourceEntity> findByStartTimeAfterAndStartTimeBefore(Long startTime, Long finishTime);
	
}
