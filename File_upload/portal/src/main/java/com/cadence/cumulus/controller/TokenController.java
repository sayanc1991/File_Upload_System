package com.cadence.cumulus.controller;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.Principal;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.io.FilenameUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.cadence.cumulus.common.LoginException;
import com.cadence.cumulus.common.ServiceConstants;
import com.cadence.cumulus.core.TokenService;
import com.cadence.cumulus.dto.TokenStatus;
import com.cadence.cumulus.dto.TokenVO;
import com.cadence.cumulus.model.UserLogin;
import com.cadence.cumulus.service.OktaService;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * Controller for authenticating with login credentials and obtaining a token
 * for further api calls from CLI client
 * 
 * @author anirban
 *
 */
@RestController
@RequestMapping("/api/token")
public class TokenController {

	private static final Logger LOG = LoggerFactory.getLogger(TokenController.class);
	
	@Value("${cli.app.storage.location}")
	private String cliAppLocation;

	@Autowired
	private OktaService oktaService;

	@Autowired
	private TokenService tokenManager;
	
	@Autowired
	private ObjectMapper mapper;

	@PostMapping(value = "/request")
	public void login(HttpServletRequest request, HttpServletResponse response) throws IOException {
		HttpSession session = request.getSession();
		Boolean tokenPresent = (Boolean) session.getAttribute(ServiceConstants.TOKEN_PRESENT);
		if (tokenPresent) {
			String token = (String) session.getAttribute(ServiceConstants.X_AUTH_TOKEN);
			LOG.info("Relaying authorization token back to client");
			sendTokenizedResponse(response, "", token);
		} else {
			UserLogin userLogin = (UserLogin) session.getAttribute(ServiceConstants.USER_LOGIN);
			String loginName = null;
			if (StringUtils.hasText(userLogin.getPassword())) {// explicit login using user credentials 
				try {
					// validate against okta the user credentials
					loginName = oktaService.doLogin(userLogin);
					if (loginName == null) {// 401, unauthorized because no user specified
						sendErrorResponse(response, "No user specified");
						return;
					}
				} catch (LoginException e) {// 401, unauthorized because okta returned error
					LOG.error("User: {] failed Okta validation: {}", loginName, e);
					sendErrorResponse(response, e.getMessage());
					return;
				}
			} else {// login from browser session context
				loginName = userLogin.getUserName();
			}
			TokenStatus ts = tokenManager.generateToken(loginName);
			if (ts.getValidity()) {
				sendTokenizedResponse(response, loginName, ts.getMessage());
				LOG.info("User: {} authenticated with token", loginName);
			} else {// 401, unauthorized because user not assigned to application
				sendErrorResponse(response, "User: " + loginName + " unauthorized to use the application");
			}

		}
	}

	private void sendTokenizedResponse(HttpServletResponse response, String userName, String token) throws IOException {
		TokenStatus status = tokenManager.isTokenExpired(userName);
		TokenVO body = new TokenVO();
		body.setTokenVal(token);
		body.setExpiryDate(Long.parseLong(status.getMessage()));
		body.setExpired(status.getValidity());
		
		// send the token in X-AUTH-HEADER
		response.setHeader(ServiceConstants.X_AUTH_TOKEN, token);
		// response.setHeader(ServiceConstants.SESSION_ID, session.getId());
		response.setStatus(HttpStatus.CREATED.value());
		//response.setStatus(HttpStatus.OK.value());
		response.setContentType(MediaType.APPLICATION_JSON_VALUE);
		response.setCharacterEncoding("UTF-8");
		mapper.writeValue(response.getWriter(), body);
		
		LOG.info("Server -> Client Token sent: {}", token);
	}

	private void sendErrorResponse(HttpServletResponse response, String message) throws IOException {
		response.setContentType(MediaType.TEXT_PLAIN_VALUE);
		response.sendError(HttpStatus.UNAUTHORIZED.value(), message);
		LOG.warn(message);
	}
	
	@GetMapping("/status")
	public ResponseEntity<TokenVO> tokenStatus(HttpServletRequest request) {
		Principal principal = request.getUserPrincipal();
		String userName = principal.getName();
		TokenVO body = new TokenVO();
		if(!StringUtils.hasText(userName)) {
			userName = "";
		} 
		LOG.info("Getting token status of user with name: {}", userName);
		TokenStatus status = tokenManager.isTokenExpired(userName);
		body.setExpired(status.getValidity());
		body.setExpiryDate(Long.parseLong(status.getMessage()));
		LOG.info("User: {} token expired: {}", userName, status);
		return new ResponseEntity<>(body, HttpStatus.OK);
	}
	
	@GetMapping("/client")
	public ResponseEntity<?> downloadCLI(HttpServletRequest request) throws Exception {
		TokenStatus ts = new TokenStatus();
		ResponseEntity<?> response = null;
		HttpSession session = request.getSession();
		Boolean tokenPresent = (Boolean) session.getAttribute(ServiceConstants.TOKEN_PRESENT);
		if (tokenPresent) {
			String token = (String) session.getAttribute(ServiceConstants.X_AUTH_TOKEN);
			ts = tokenManager.verifyToken(token, "");
		} else {
			UserLogin userLogin = (UserLogin) session.getAttribute(ServiceConstants.USER_LOGIN);
			ts.setValidity(true);
			ts.setMessage(userLogin.getUserName());
		}
		if(ts.getValidity()) {
			Path cliAppPath = Paths.get(cliAppLocation);
			byte[] content = Files.readAllBytes(cliAppPath);
			Long contentLength = Long.parseLong(String.valueOf(content.length));
			String contentName = FilenameUtils.getName(cliAppLocation);
			HttpHeaders headers = new HttpHeaders();
			headers.setContentLength(contentLength);
			headers.setContentType(MediaType.APPLICATION_OCTET_STREAM);
			headers.setDate(System.currentTimeMillis());
			headers.setContentDispositionFormData("attachment", contentName);
			Resource body = new ByteArrayResource(content);
			response = ResponseEntity.ok().headers(headers).body(body);
			LOG.info("Requested resource download: {} of size: {}", contentName, contentLength);
		} else {
			HttpHeaders headers = new HttpHeaders();
			headers.setContentType(MediaType.TEXT_PLAIN);
			response = new ResponseEntity<String>(ts.getMessage(), headers, HttpStatus.valueOf(ts.getCode()));
			LOG.info("Requested resource download could not be served due to authentication error");
		}
		return response;
	}
	
}
