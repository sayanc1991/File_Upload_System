package com.cadence.cumulus.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.domain.Specifications;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import com.cadence.cumulus.common.CumulusException;
import com.cadence.cumulus.model.ChannelType;
import com.cadence.cumulus.model.CumulusUserNotificationSettings;
import com.cadence.cumulus.model.NotificationChannel;
import com.cadence.cumulus.model.Role_;
import com.cadence.cumulus.model.StatusType;
import com.cadence.cumulus.model.User;
import com.cadence.cumulus.model.UserEventNotification;
import com.cadence.cumulus.model.UserEventNotificationId;
import com.cadence.cumulus.model.UserRoleId_;
import com.cadence.cumulus.model.UserRole_;
import com.cadence.cumulus.model.User_;
import com.cadence.cumulus.repository.mysql.NotificationChannelRepository;
import com.cadence.cumulus.repository.mysql.StatusTypeRepository;
import com.cadence.cumulus.repository.mysql.UserEventNotificationRepository;
import com.cadence.cumulus.repository.mysql.UserRepository;
import com.cadence.cumulus.repository.mysql.util.SpecificationFactory;
import com.cadence.cumulus.service.CumulusUserService;

@Service
@Transactional(propagation = Propagation.REQUIRED, rollbackFor = { Exception.class })
public class CumulusUserServiceImpl implements CumulusUserService {

	// @Autowired
	// private CumulusUserNotificationSettingsRepository settingsRepository;

	@Autowired
	private UserRepository userRepository;

	@Autowired
	private StatusTypeRepository statusTypeRepository;

	@Autowired
	private NotificationChannelRepository notificationChannelRepository;

	@Autowired
	private UserEventNotificationRepository userEventNotificationRepository;
	
	private static final Logger LOG = LoggerFactory.getLogger(CumulusUserServiceImpl.class);

	@Override
	public CumulusUserNotificationSettings getNotificationSettings(String email) {
		User user = userRepository.findByEmail(email);
		return getNotificationSettings(user);
	}

	@Override
	/**
	 * It is assumed that the User object passed is a persistent object
	 */
	public CumulusUserNotificationSettings getNotificationSettings(User user) {
		Set<UserEventNotification> userEventNotificationSet = user.getUserEventNotificationSet();
		if (userEventNotificationSet == null || userEventNotificationSet.isEmpty()) {
			return null;
		} else {

			return getCumulusUserSettings(user, userEventNotificationSet);

		}
	}
	
	@Override
	public CumulusUserNotificationSettings getNotificationSettings(Long userId) {
		User user = userRepository.findOne(userId);
		user.getUserEventNotificationSet().size();
		Set<UserEventNotification> userEventNotificationSet = user.getUserEventNotificationSet();
		if (userEventNotificationSet == null || userEventNotificationSet.isEmpty()) {
			return null;
		} else {

			return getCumulusUserSettings(user, userEventNotificationSet);

		}
	}

	public CumulusUserNotificationSettings saveDefaultNotificationSettings(User user) {

		// setup one row each for the combination of event type and channels for
		// this user

		// fetch all event types
		List<StatusType> statusTypes = statusTypeRepository.findAll();
		// fetch all channel types
		List<NotificationChannel> notificationChannels = notificationChannelRepository.findAll();
		Set<UserEventNotification> userEventNotificationSet = new HashSet<>();
		for (StatusType statusType : statusTypes) {
			for (NotificationChannel notificationChannel : notificationChannels) {

				UserEventNotification userEventNotification = new UserEventNotification();
				UserEventNotificationId userEventNotificationId = new UserEventNotificationId();

				userEventNotificationId.setUser(user);
				userEventNotificationId.setStatusType(statusType);
				userEventNotificationId.setNotificationChannel(notificationChannel);

				userEventNotification.setUserEventNotificationId(userEventNotificationId);
				userEventNotification.setCreatedOn(new Date());
				userEventNotification.setLastModifiedOn(new Date());
				userEventNotification.setActiveFlag(false);

				userEventNotificationSet.add(userEventNotification);
			}
		}

		userEventNotificationRepository.save(userEventNotificationSet);
		return getCumulusUserSettings(user, userEventNotificationSet);

	}

	@Override
	public CumulusUserNotificationSettings updateNotificationSettings(CumulusUserNotificationSettings settings) {

		List<UserEventNotification> userEventNotifications = userEventNotificationRepository
				.findByUserEventNotificationId_User_Email(settings.getEmail());
		
		//CMLS-638 : update phone number in DB
		User user = userRepository.findByEmail(settings.getEmail());
		user.setPhone(settings.getPhone());
		userRepository.save(user);
		
		LOG.debug("Found : {} UserEventNotification rows", userEventNotifications.size());
		// update the active flag and lastModifiedOn
		
		for (UserEventNotification userEventNotification : userEventNotifications) {
			LOG.debug("userEventNotification.getStatusType().getStatusTypeName()" + userEventNotification.getStatusType().getStatusTypeName());
			if (ChannelType.Browser.name().equals(userEventNotification.getNotificationChannel().getChannelName())) {
				userEventNotification.setActiveFlag(
						settings.getBrowserSettings().get(userEventNotification.getStatusType().getStatusTypeName()));
				userEventNotification.setLastModifiedOn(new Date());

			} else if (ChannelType.SMS.name().equals(userEventNotification.getNotificationChannel().getChannelName())) {
				userEventNotification.setActiveFlag(
						settings.getSmsSettings().get(userEventNotification.getStatusType().getStatusTypeName()));
				userEventNotification.setLastModifiedOn(new Date());

			} else if (ChannelType.Email.name()
					.equals(userEventNotification.getNotificationChannel().getChannelName())) {
				userEventNotification.setActiveFlag(
						settings.getEmailSettings().get(userEventNotification.getStatusType().getStatusTypeName()));
				userEventNotification.setLastModifiedOn(new Date());

			}

		}

		// save changes to DB
		userEventNotificationRepository.save(userEventNotifications);
		return settings;
	}

	private CumulusUserNotificationSettings getCumulusUserSettings(User user,
			Set<UserEventNotification> userEventNotificationSet) {
		LOG.debug("Inside getCumulusUserSettings");
		// construct CumulusUserNotificationSettings object
		
		CumulusUserNotificationSettings cumulusUserNotificationSettings = new CumulusUserNotificationSettings();
		cumulusUserNotificationSettings.setFirstName(user.getFirstName());
		cumulusUserNotificationSettings.setLastName(user.getLastName());
		cumulusUserNotificationSettings.setEmail(user.getEmail());
		cumulusUserNotificationSettings.setPhone(user.getPhone());

		Map<String, Boolean> emailSettings = new HashMap<>();
		Map<String, Boolean> smsSettings = new HashMap<>();
		Map<String, Boolean> browserSettings = new HashMap<>();

		for (UserEventNotification userEventNotification : userEventNotificationSet) {
			if (ChannelType.Email.name().equals(userEventNotification.getNotificationChannel().getChannelName())) {
				emailSettings.put(userEventNotification.getStatusType().getStatusTypeName(),
						userEventNotification.isActiveFlag());
			} else if (ChannelType.SMS.name().equals(userEventNotification.getNotificationChannel().getChannelName())) {
				smsSettings.put(userEventNotification.getStatusType().getStatusTypeName(),
						userEventNotification.isActiveFlag());
			} else if (ChannelType.Browser.name()
					.equals(userEventNotification.getNotificationChannel().getChannelName())) {
				browserSettings.put(userEventNotification.getStatusType().getStatusTypeName(),
						userEventNotification.isActiveFlag());
			}
		}

		LOG.debug("Browser Settings: " + browserSettings.toString() + " " + browserSettings.get("started"));
		cumulusUserNotificationSettings.setBrowserSettings(browserSettings);
		cumulusUserNotificationSettings.setSmsSettings(smsSettings);
		cumulusUserNotificationSettings.setEmailSettings(emailSettings);
		
		LOG.debug("Exiting getCumulusUserSettings ");
		return cumulusUserNotificationSettings;
	}

	@Override
	public User saveUser(User user) {
		return userRepository.save(user);
	}

	@Override
	public void deleteUser(User user) {
		userRepository.delete(user);

	}

	@Override
	public User getUserByEmail(String userId) {
		return userRepository.findByEmail(userId);
	}

	@Override
	public List<User> searchUser(String firstName, String lastName, String emailID, String roleID)
			throws CumulusException {
		// validate atleast one search parameter is provided
		if (isEmpty(firstName) && isEmpty(lastName) && isEmpty(emailID) && isEmpty(roleID)) {
			throw new CumulusException("Please specify a search criteria");
		}

		// construct specification expressions and chain it as required
		SpecificationFactory<User> specFactory = new SpecificationFactory<User>();

		Specification<User> specification = null;
		if (!isEmpty(firstName)) {
			specification = Specifications.where(specFactory.equal(User_.firstName.getName(), firstName));
		}
		if (!isEmpty(lastName)) {
			Specification<User> lastNameSpec = Specifications
					.where(specFactory.equal(User_.lastName.getName(), lastName));
			specification = specification == null ? lastNameSpec
					: Specifications.where(specification).and(lastNameSpec);
		}
		if (!isEmpty(emailID)) {
			Specification<User> emailSpec = Specifications.where(specFactory.equal(User_.email.getName(), emailID));
			specification = specification == null ? emailSpec : Specifications.where(specification).and(emailSpec);
		}
		if (!isEmpty(roleID)) {
			// the roleID is represented by id attribute in User -> userRoleSet
			// -> userRoleId -> role -> id
			// so pass the userRoleSet collectioin attribute and the path
			// attributes to naviagate to the role id
			// to the SpecificationFactory method
			List<String> pathList = new ArrayList<>();
			pathList.add(UserRole_.userRoleId.getName());
			pathList.add(UserRoleId_.role.getName());
			pathList.add(Role_.id.getName());
			Specification<User> roleIdSpec = Specifications
					.where(specFactory.joinAndEqualWithChildAttributes(User_.userRoleSet.getName(), pathList, roleID));
			specification = specification == null ? roleIdSpec : Specifications.where(specification).and(roleIdSpec);
		}

		return userRepository.findAll(specification);

	}

	private boolean isEmpty(String str) {
		return StringUtils.isEmpty(StringUtils.trimWhitespace(str));
	}

	@Override
	public User getUserById(Long id) {
		return userRepository.findOne(id);
	}

	// CMLS-658
	@Override
	public User getUserByUserName(String userName) {
		// TODO Auto-generated method stub
		return userRepository.findByUsername(userName);
	}

}
