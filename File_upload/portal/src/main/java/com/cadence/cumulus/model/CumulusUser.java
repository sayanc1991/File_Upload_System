package com.cadence.cumulus.model;

import java.io.Serializable;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;

import org.springframework.security.core.CredentialsContainer;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.SpringSecurityCoreVersion;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.util.Assert;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class CumulusUser extends BaseEntity implements UserDetails, CredentialsContainer {
	private static final long serialVersionUID = -3137505892761289425L;

	private Long id;
	private String username;
	private String loginName;
	private String firstName;
	private String lastName;
	private CumulusUserNotificationSettings userSettings;

	private final Set<GrantedAuthority> authorities;

	public CumulusUser() {
		authorities = null;
	}

	public CumulusUser(String username, String firstName, String lastName, CumulusUserNotificationSettings userSettings, boolean enabled, boolean accountNonExpired,
			boolean credentialsNonExpired, boolean accountNonLocked,
			Collection<? extends GrantedAuthority> authorities) {
		this.username = username;
		this.firstName = firstName;
		this.lastName = lastName;
		this.loginName = username.split("@")[0];
		this.userSettings = userSettings;
		this.authorities = Collections.unmodifiableSet(sortAuthorities(authorities));
	}
	
	public CumulusUser(Long id, String username, String firstName, String lastName, CumulusUserNotificationSettings userSettings, boolean enabled, boolean accountNonExpired,
			boolean credentialsNonExpired, boolean accountNonLocked,
			Collection<? extends GrantedAuthority> authorities) {
		this.id=id;
		this.username = username;
		this.firstName = firstName;
		this.lastName = lastName;
		this.loginName = username.split("@")[0];
		this.userSettings = userSettings;
		this.authorities = Collections.unmodifiableSet(sortAuthorities(authorities));
	}

	public Long getId() {
		return id;
	}
	

	public String getFirstName() {
		return firstName;
	}

	public String getLastName() {
		return lastName;
	}
	
	public CumulusUserNotificationSettings getUserSettings() {
	    return userSettings;
	}

	@Override
	public void eraseCredentials() {
	}

	@Override
	public Set<GrantedAuthority> getAuthorities() {
		return authorities;
	}

	@Override
	public String getPassword() {
		return null;
	}

	@Override
	public String getUsername() {
		return username;
	}

	public String getLoginName() {
		return loginName;
	}

	@Override
	@JsonIgnore
	public boolean isAccountNonExpired() {
		return true;
	}

	@Override
    @JsonIgnore
	public boolean isAccountNonLocked() {
		return true;
	}

	@Override
    @JsonIgnore
	public boolean isCredentialsNonExpired() {
		return true;
	}

	@Override
    @JsonIgnore
	public boolean isEnabled() {
		return true;
	}

	private static SortedSet<GrantedAuthority> sortAuthorities(Collection<? extends GrantedAuthority> authorities) {
		Assert.notNull(authorities, "Cannot pass a null GrantedAuthority collection");
		// Ensure array iteration order is predictable (as per
		// UserDetails.getAuthorities() contract and SEC-717)
		SortedSet<GrantedAuthority> sortedAuthorities = new TreeSet<GrantedAuthority>(new AuthorityComparator());

		for (GrantedAuthority grantedAuthority : authorities) {
			Assert.notNull(grantedAuthority, "GrantedAuthority list cannot contain any null elements");
			sortedAuthorities.add(grantedAuthority);
		}

		return sortedAuthorities;
	}

	private static class AuthorityComparator implements Comparator<GrantedAuthority>, Serializable {
		private static final long serialVersionUID = SpringSecurityCoreVersion.SERIAL_VERSION_UID;

		public int compare(GrantedAuthority g1, GrantedAuthority g2) {
			// Neither should ever be null as each entry is checked before
			// adding it to
			// the set.
			// If the authority is null, it is a custom authority and should
			// precede
			// others.
			if (g2.getAuthority() == null) {
				return -1;
			}

			if (g1.getAuthority() == null) {
				return 1;
			}

			return g1.getAuthority().compareTo(g2.getAuthority());
		}
	}
}
