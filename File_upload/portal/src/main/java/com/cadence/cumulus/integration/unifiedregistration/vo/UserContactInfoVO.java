package com.cadence.cumulus.integration.unifiedregistration.vo;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class UserContactInfoVO {

	private String Id;
	private String Email;
	private String FirstName;
	private String LastName;
	private String AccountId;
	private String Parent_Account__c;
	private String Country__c;
	private String Street__c;
	private String City__c;
	private String State_Region__c;
	private String Postal_Code__c;
	private String App_Code__c;
	private String Phone;

	public String getId() {
		return Id;
	}
	public void setId(String id) {
		Id = id;
	}
	public String getEmail() {
		return Email;
	}
	public void setEmail(String email) {
		Email = email;
	}
	public String getFirstName() {
		return FirstName;
	}
	public void setFirstName(String firstName) {
		FirstName = firstName;
	}
	public String getLastName() {
		return LastName;
	}
	public void setLastName(String lastName) {
		LastName = lastName;
	}
	public String getAccountId() {
		return AccountId;
	}
	public void setAccountId(String accountId) {
		AccountId = accountId;
	}
	public String getParent_Account__c() {
		return Parent_Account__c;
	}
	public void setParent_Account__c(String parent_Account__c) {
		Parent_Account__c = parent_Account__c;
	}
	public String getCountry__c() {
		return Country__c;
	}
	public void setCountry__c(String country__c) {
		Country__c = country__c;
	}
	public String getStreet__c() {
		return Street__c;
	}
	public void setStreet__c(String street__c) {
		Street__c = street__c;
	}
	public String getCity__c() {
		return City__c;
	}
	public void setCity__c(String city__c) {
		City__c = city__c;
	}
	public String getState_Region__c() {
		return State_Region__c;
	}
	public void setState_Region__c(String state_Region__c) {
		State_Region__c = state_Region__c;
	}
	public String getPostal_Code__c() {
		return Postal_Code__c;
	}
	public void setPostal_Code__c(String postal_Code__c) {
		Postal_Code__c = postal_Code__c;
	}
	public String getApp_Code__c() {
		return App_Code__c;
	}
	public void setApp_Code__c(String app_Code__c) {
		App_Code__c = app_Code__c;
	}
	public String getPhone() {
		return Phone;
	}
	public void setPhone(String phone) {
		Phone = phone;
	}
	
	@Override
	public String toString() {
		return "UserContactInfo [Id="+Id+ ", Email=" + Email + ", FirstName=" + FirstName + ", LastName=" + LastName
				+ ", AccountId=" + AccountId + ", Parent_Account__c=" + Parent_Account__c + ", Country__c=" + Country__c
				+ ", Street__c=" + Street__c + ", City__c=" + City__c + ", State_Region__c=" + State_Region__c
				+ ", Postal_Code__c=" + Postal_Code__c + ", App_Code__c=" + App_Code__c + ", Phone=" + Phone + "]";
	}
}