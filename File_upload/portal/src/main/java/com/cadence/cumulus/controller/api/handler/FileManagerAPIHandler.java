package com.cadence.cumulus.controller.api.handler;

import java.io.IOException;

import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import com.cadence.cumulus.model.Result;
import com.cadence.cumulus.model.User;
import com.cadence.cumulus.utility.FileSystemUtil;

/**
 * 
 * @author shirish
 *
 */
// dependency injection to use spring managed bean FileSystemUtil in accordance to CMLS-658, method signatures are same
@Component
public class FileManagerAPIHandler {
	/**
	 * Logger
	 */
	private static final Logger log = LoggerFactory
			.getLogger(FileManagerAPIHandler.class);
	
	@Autowired
	private FileSystemUtil fsUtil;

	/**
	 * This is a helper method for the File Upload API.
	 * 
	 * @param files
	 * @param destination
	 * @param user
	 * @param response
	 * @return
	 * @throws IOException
	 */
	public Result uploadFile(MultipartFile[] files, String destination,
			User user, HttpServletResponse response) {
		log.debug("Request to upload {} files to {}", files.length, destination);
		Result r = new Result();
		r.setSuccess(true);
		r.setError(null);
		for (MultipartFile file : files) {
			boolean isuploaded = fsUtil.writeFileToFS(file,
					destination, user.getUsername());
			if (!isuploaded) {
				r.setSuccess(false);
				r.setError(true);
			}
		}

		/*
		 * Arrays.stream(files).forEach(file -> {
		 * log.debug("Uploading file : {}", file.getOriginalFilename());
		 * 
		 * File dest = new File(destination, file.getOriginalFilename());
		 * 
		 * InputStream input = null; OutputStream output = null;
		 * 
		 * try { input = file.getInputStream(); output = new
		 * FileOutputStream(dest);
		 * 
		 * byte[] buffer = new byte[8192]; int length = 0; while ((length =
		 * input.read(buffer)) > 0) { output.write(buffer, 0, length); }
		 * 
		 * // log.debug("Setting permissions"); //
		 * permissionService.setOwnershipInfo(dest.getAbsolutePath(), //
		 * userName);
		 * 
		 * log.debug("File uploaded"); } catch (Throwable t) {
		 * log.debug("Unable to upload {}", file.getOriginalFilename());
		 * log.error("Error:", t); try { response.sendError(
		 * HttpStatus.INTERNAL_SERVER_ERROR.value(), "write error"); } catch
		 * (IOException e) { log.error("Error sending response:", e); } }
		 * finally { try { input.close(); } catch (IOException e) {
		 * log.error("Error:", e); } try { output.close(); } catch (IOException
		 * e) { log.error("Error:", e); } } });
		 */

		log.debug("All files uploaded");
		return r;

	}

}
