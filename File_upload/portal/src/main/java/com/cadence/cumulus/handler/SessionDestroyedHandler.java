package com.cadence.cumulus.handler;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.Enumeration;

import javax.annotation.PostConstruct;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.event.EventListener;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.session.SessionDestroyedEvent;
import org.springframework.security.web.session.HttpSessionDestroyedEvent;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import com.cadence.cumulus.common.ServiceConstants;
import com.cadence.cumulus.model.CumulusUser;
import com.cadence.cumulus.service.PublisherService;

@Component
public class SessionDestroyedHandler {
    private static final Logger log = LoggerFactory.getLogger(SessionDestroyedHandler.class);
    
    private DateTimeFormatter formatter;
    
    @PostConstruct
    private void init() {
    	this.formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss:SSS");
    }

    @EventListener(SessionDestroyedEvent.class)
    public void sessionDestroyed(HttpSessionDestroyedEvent event) {
    	log.debug("Session destroyed from: {}", event.getSource());
        HttpSession session = event.getSession();
        String sessionId = session.getId();
        // CMLS-658 display user of associated destroyed session along with inactivity
        LocalDateTime created = LocalDateTime.ofInstant(Instant.ofEpochMilli(session.getCreationTime()), ZoneId.systemDefault());
        LocalDateTime lastAccessed = LocalDateTime.ofInstant(Instant.ofEpochMilli(session.getLastAccessedTime()), ZoneId.systemDefault());
        LocalDateTime now = LocalDateTime.now();
        log.debug("Session: {} destroyed, created: {} last access: {} inactive for: {} {}, inactivity allowed: {} {}, current time: {}", 
        		sessionId, created.format(formatter), lastAccessed.format(formatter), lastAccessed.until(now, ChronoUnit.SECONDS), ChronoUnit.SECONDS,
        		session.getMaxInactiveInterval(), ChronoUnit.SECONDS, now.format(formatter));
        /*log.debug("Session: {} destroyed, created: {} last access: {}, inactive real: {} {}, inactivie ideal: {} {}, current time: {} {}", 
        		sessionId, session.getCreationTime(), session.getLastAccessedTime(), 
        		(session.getLastAccessedTime() - now.atZone(ZoneId.systemDefault()).toInstant().toEpochMilli()) / 1000l, 
        		session.getMaxInactiveInterval(), ChronoUnit.SECONDS, System.currentTimeMillis());*/
        String userName = (String) session.getAttribute(ServiceConstants.USERNAME);
        if(StringUtils.hasText(userName)) {
        	// from cli client
            log.debug("Username from session: {}", session.getAttribute(ServiceConstants.USERNAME));
        } else {
        	// from browser client
            for (SecurityContext ctx : event.getSecurityContexts()) {
                userName = ((CumulusUser) ctx.getAuthentication().getPrincipal()).getUsername().split("@")[0];
                log.debug("User name in destroyed session: [{}]", userName);

                break;
            }
        }

        Enumeration<String> sessionAttributes = session.getAttributeNames();
        while (sessionAttributes.hasMoreElements()) {
            String name = sessionAttributes.nextElement();
            if (name.startsWith(session.getId() + ".publisher.")) {
                log.debug("Found publisher {}", name);
                PublisherService publisher = (PublisherService) session.getAttribute(name);
                if (publisher != null) {
                    log.debug("Killing publihser");
                    publisher.setAlive(false);
                }
            }
        }
    }
}
