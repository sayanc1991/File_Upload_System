package com.cadence.cumulus.model;

import java.io.Serializable;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.Iterator;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.SpringSecurityCoreVersion;
import org.springframework.util.Assert;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_EMPTY)
@Entity
@Table(name = "User")
public class User extends BaseEntity {

	private Long id;
	private String userName;
	@Transient
	private String loginName;
	private String firstName;
	private String lastName;
	private String email;
	private String phone;
	private String status;

	private String department;

	private String project;

	private String contactId;// contactId in UR
	
	private Date tokenCreatedOn;// CMLS-658
	private String hash;// CMLS-658

	@Transient
	private Countries country;
	@Transient
	private String street;
	@Transient
	private String city;
	@Transient
	private String stateRegion;
	@Transient
	private String postalCode;

	@Transient
	private CumulusUserNotificationSettings userSettings;

	@Transient
	private final Set<GrantedAuthority> authorities;

	private Set<UserRole> userRoleSet;
	private Set<UserEventNotification> userEventNotificationSet;

	public User() {
		authorities = null;
	}

	public User(String username, String firstName, String lastName, CumulusUserNotificationSettings userSettings,
			boolean enabled, boolean accountNonExpired, boolean credentialsNonExpired, boolean accountNonLocked,
			Collection<? extends GrantedAuthority> authorities) {
		this.setUsername(username);
		this.setFirstName(firstName);
		this.setLastName(lastName);
		this.loginName = username.split("@")[0];
		this.setUserSettings(userSettings);
		this.authorities = Collections.unmodifiableSet(sortAuthorities(authorities));
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "Id")
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getFirstName() {
		return firstName;
	}

	public String getLastName() {
		return lastName;
	}

	@Transient
	public CumulusUserNotificationSettings getUserSettings() {
		return userSettings;
	}

	private static SortedSet<GrantedAuthority> sortAuthorities(Collection<? extends GrantedAuthority> authorities) {
		Assert.notNull(authorities, "Cannot pass a null GrantedAuthority collection");
		// Ensure array iteration order is predictable (as per
		// UserDetails.getAuthorities() contract and SEC-717)
		SortedSet<GrantedAuthority> sortedAuthorities = new TreeSet<GrantedAuthority>(new AuthorityComparator());

		for (GrantedAuthority grantedAuthority : authorities) {
			Assert.notNull(grantedAuthority, "GrantedAuthority list cannot contain any null elements");
			sortedAuthorities.add(grantedAuthority);
		}

		return sortedAuthorities;
	}

	@OneToMany(mappedBy = "userRoleId.user", cascade = CascadeType.ALL, fetch = FetchType.EAGER, orphanRemoval = true)
	@JsonIgnore
	public Set<UserRole> getUserRoleSet() {
		return userRoleSet;
	}

	@OneToMany(mappedBy = "userEventNotificationId.user", fetch = FetchType.LAZY)
	@JsonIgnore
	public Set<UserEventNotification> getUserEventNotificationSet() {
		return userEventNotificationSet;
	}

	public void setUserEventNotificationSet(Set<UserEventNotification> userEventNotificationSet) {
		this.userEventNotificationSet = userEventNotificationSet;
	}

	public void setUserRoleSet(Set<UserRole> userRoleSet) {
		if (this.userRoleSet != null) {
			// remove objects which are not present in the passed on set
			Iterator<UserRole> it = this.userRoleSet.iterator();
			while (it.hasNext()) {
				UserRole ur = it.next();
				if (!userRoleSet.contains(ur)) {
					it.remove();
				}
			}
			this.userRoleSet.addAll(userRoleSet);
		} else {
			this.userRoleSet = userRoleSet;
		}
	}

	public String getUsername() {
		return userName;
	}

	public void setUsername(String username) {
		this.userName = username;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getDepartment() {
		return department;
	}

	public void setDepartment(String department) {
		this.department = department;
	}

	public String getProject() {
		return project;
	}

	public void setProject(String project) {
		this.project = project;
	}

	public String getContactId() {
		return contactId;
	}

	public void setContactId(String contactId) {
		this.contactId = contactId;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	@Transient
	public String getUserId() {
		return userName;
	}

	public void setUserId(String userId) {
		this.userName = userId;
	}

	public void setUserSettings(CumulusUserNotificationSettings userSettings) {
		this.userSettings = userSettings;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	private static class AuthorityComparator implements Comparator<GrantedAuthority>, Serializable {
		private static final long serialVersionUID = SpringSecurityCoreVersion.SERIAL_VERSION_UID;

		public int compare(GrantedAuthority g1, GrantedAuthority g2) {
			// Neither should ever be null as each entry is checked before
			// adding it to
			// the set.
			// If the authority is null, it is a custom authority and should
			// precede
			// others.
			if (g2.getAuthority() == null) {
				return -1;
			}

			if (g1.getAuthority() == null) {
				return 1;
			}

			return g1.getAuthority().compareTo(g2.getAuthority());
		}
	}

	@Transient
	public Countries getCountry() {
		return country;
	}

	public void setCountry(Countries country) {
		this.country = country;
	}

	@Transient
	public String getStreet() {
		return street;
	}

	public void setStreet(String street) {
		this.street = street;
	}

	@Transient
	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	@Transient
	public String getStateRegion() {
		return stateRegion;
	}

	public void setStateRegion(String stateRegion) {
		this.stateRegion = stateRegion;
	}

	@Transient
	public String getPostalCode() {
		return postalCode;
	}

	public void setPostalCode(String postalCode) {
		this.postalCode = postalCode;
	}
	
	public String getHash() {
		return hash;
	}

	public void setHash(String hash) {
		this.hash = hash;
	}

	public Date getTokenCreatedOn() {
		return tokenCreatedOn;
	}

	public void setTokenCreatedOn(Date tokenCreatedOn) {
		this.tokenCreatedOn = tokenCreatedOn;
	}
}
