/**
 * 
 */
package com.cadence.cumulus.service.impl;

import java.io.IOException;
import java.util.List;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;
import org.springframework.web.context.request.async.DeferredResult;

import com.cadence.cumulus.common.CumulusException;
import com.cadence.cumulus.handler.AgentHandler;
import com.cadence.cumulus.model.AgentResponse;
import com.cadence.cumulus.model.CumulusUser;
import com.cadence.cumulus.model.Job;
import com.cadence.cumulus.model.JobRun;
import com.cadence.cumulus.repository.mysql.JobRepository;
import com.cadence.cumulus.service.JobService;

/**
 * @author ramsudheer
 *
 */
@Component
@Transactional(propagation = Propagation.REQUIRED, rollbackFor = { Exception.class })
public class JobServiceImpl implements JobService {

	private static final Logger log = LoggerFactory
			.getLogger(JobServiceImpl.class);

	@Autowired
	private JobRepository jobRepository;

	@Autowired
	private AgentHandler handler;
	
	/*
	 * (non-Javadoc)
	 * 
	 * @see com.cadence.cumulus.service.JobService#findById(java.lang.String)
	 */
	@Override
	public Job findById(String id) {
		Long jobId = Long.parseLong(id);
		log.debug("Fetching  job {}", id);
		Job job = jobRepository.findOne(jobId);

		// jobRuns also would be fetched along with parent
		// if (job != null) {
		// List<JobRun> jobRuns = jobRunRepository.findByJobId(jobId);
		// job.setJobRuns(jobRuns);
		// }

		return job;
	}

	@Override
	public List<Job> getJobsByUser(Long userId) {
		log.debug("Fetching all jobs");
		return jobRepository.findByCreatedBy(userId);
	}

	@Override
	public Job getJobById(Long id) {
		return jobRepository.findOne(id);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.cadence.cumulus.service.JobService#save(com.cadence.cumulus.model.
	 * Job)
	 */
	@Override
	public Job save(Job job) {
		log.debug("Creating job: {}", job);
		job = jobRepository.save(job);
		return job;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.cadence.cumulus.service.JobService#updateJob(com.cadence.cumulus.
	 * model.Job)
	 */
	@Override
	public Job updateJob(Job job) {
		log.debug("Updating job: {}", job);
		jobRepository.findOne(job.getId());
		return jobRepository.save(job);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.cadence.cumulus.service.JobService#deleteJob(java.lang.String)
	 */
	@Override
	public void deleteJob(String jobId) throws IOException, CumulusException {
		Long jId = Long.parseLong(jobId);
		
		Job jobDetails = getJobById(Long.parseLong(jobId));
		Set<JobRun> jobRuns = jobDetails.getJobRuns();
		if (!CollectionUtils.isEmpty(jobRuns)) {
			for (JobRun jobRun : jobRuns) {
				if ("progress".equalsIgnoreCase(jobRun.getStatus())) {
					CumulusUser user = (CumulusUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
					DeferredResult<AgentResponse> deferredResponse = new DeferredResult<>();
					handler.sendKillCommand(jobRun, user.getLoginName(), deferredResponse);

					if (!deferredResponse.hasResult()) {
						AgentResponse commandResponse = (AgentResponse) deferredResponse.getResult();
						if (commandResponse != null && commandResponse.isSuccess()) {
							log.debug("Can delete job now.");
						} else {
							throw new CumulusException("Job can not be deleted. Job runs are in progress");
						}
					}
				} else {
					log.debug("No job run in progress. Will delete job now.");
				}
			}
		}
		
		
		Job job = jobRepository.findOne(jId);
		log.debug("Deleting job: {}", job);
		jobRepository.delete(job);
	}

	/**
     * 
     */
	@Override
	public List<Job> findByJobNameForUser(String jobName, Long userId) {
		List<Job> jobList = jobRepository.findByJobNameAndCreatedBy(jobName,
				userId);
		return jobList;
	}

}
