/**
 * 
 */
package com.cadence.cumulus.common;

/**
 * @author ramsudheer
 *
 */
public enum JobStatus {
	
	InProgress("InProgress"), //
	Completed("Completed"), //
	Interrupted("Interrupted"), //
	Failed("Failed");
	
	private final String jobStatus;

	private JobStatus(String jobStatus) {
		this.jobStatus = jobStatus;
	}

	/**
	 * @return the jobStatus
	 */
	public final String getJobStatus() {
		return jobStatus;
	}

	public static JobStatus getJobStatus(String jobStatus) {
		for (JobStatus js : JobStatus.values()) {
			if (js.getJobStatus().equals(jobStatus))
				return js;
		}
		return null;
	}
}
