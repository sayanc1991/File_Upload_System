package com.cadence.cumulus.integration.unifiedregistration.vo;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class URResponse {

	private String logid;

	private String errorSummary;

	private String errorLink;

	private String errorId;

	private String errorCode;
	
	private String sfdcconid;

	public String getSfdcconid() {
		return sfdcconid;
	}

	public void setSfdcconid(String sfdcconid) {
		this.sfdcconid = sfdcconid;
	}

	public String getLogid() {
		return logid;
	}

	public void setLogid(String logid) {
		this.logid = logid;
	}

	public String getErrorSummary() {
		return errorSummary;
	}

	public void setErrorSummary(String errorSummary) {
		this.errorSummary = errorSummary;
	}

	public String getErrorLink() {
		return errorLink;
	}

	public void setErrorLink(String errorLink) {
		this.errorLink = errorLink;
	}

	public String getErrorId() {
		return errorId;
	}

	public void setErrorId(String errorId) {
		this.errorId = errorId;
	}

	public String getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}

	@Override
	public String toString() {
		return "URResponse [logid=" + logid + ", errorSummary=" + errorSummary + ", errorLink=" + errorLink
				+ ", errorId=" + errorId + ", errorCode=" + errorCode + ", sfdcconid=" + sfdcconid + "]";
	}
}
