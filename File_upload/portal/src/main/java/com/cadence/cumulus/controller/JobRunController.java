package com.cadence.cumulus.controller;

import com.cadence.cumulus.common.Constants;
import com.cadence.cumulus.common.CumulusException;
import com.cadence.cumulus.events.api.APIEventsPublisher;
import com.cadence.cumulus.events.app.AppEventsListener;
import com.cadence.cumulus.handler.AgentHandler;
import com.cadence.cumulus.model.*;
import com.cadence.cumulus.model.liberate.events.LiberateEvent;
import com.cadence.cumulus.model.liberate.events.LiberateStatus;
import com.cadence.cumulus.repository.mysql.JobRunRepository;
import com.cadence.cumulus.repository.mysql.LiberateEventRepository;
import com.cadence.cumulus.service.JobRunService;
import com.cadence.cumulus.service.JobService;
import com.cadence.cumulus.service.PublisherService;
import org.apache.commons.io.FilenameUtils;
import org.omg.CORBA.portable.ApplicationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.request.async.DeferredResult;
import org.springframework.web.servlet.mvc.method.annotation.SseEmitter;
import rx.RxReactiveStreams;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.ProcessBuilder.Redirect;
import java.security.Principal;
import java.util.*;

@RestController
@RequestMapping(value = "/jobrun")
public class JobRunController {
    /**
     * Logger
     */
    private static final Logger log = LoggerFactory.getLogger(JobRunController.class);

    private static final String JOBS = "jobs";

    @Autowired
    private AgentHandler handler_old;

    @Autowired
    private AppEventsListener handler;

    //Shambhu: Adding event publisher to send commands to for execution
    @Autowired
    private APIEventsPublisher publisher;

    @Autowired
    private JobRunService jobRunService;

    @Autowired
    private JobService jobService;

    @Autowired
    private LiberateEventRepository eventRepository;

    @Autowired
    private JobRunRepository jobRunRepository;
    /*
     * How many lines should be shown in the logs in UI. These many lines are
	 * tailed from the end of the stdout file.
     */
    private static final int LOG_LINE_LIMIT = 200;

    /**
     * End point to start the job
     *
     * @param job
     * @return
     * @throws ApplicationException
     * @throws IOException
     */
    @PostMapping
    @ResponseStatus(HttpStatus.OK)
    @PreAuthorize("hasAuthority('Run Jobs')")
    public JobRun runJob(@RequestBody JobRun jobRun, HttpServletResponse response)
            throws ApplicationException, IOException {
      /*  if (!handler.isConnected()) {
            log.debug("Agent not available");

            response.setContentType(MediaType.TEXT_PLAIN_VALUE);
            response.sendError(HttpStatus.SERVICE_UNAVAILABLE.value(), "Agent not available");
            return null;
        }*/

        log.debug("Checking if the job with id: {}, exists", jobRun.getJobId());
        Job job = jobService.findById("" + jobRun.getJobId());

        if (job == null) {
            response.sendError(HttpStatus.NOT_FOUND.value(), "Job not found");
        }

        log.debug("Run job {} ", jobRun);

        CumulusUser user = (CumulusUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        Date d = new Date();
        job.setLastRunOn(d);
        jobRun.setCreatedDate(d);
        jobRun.setLastUpdatedDate(d);
        jobRun.setStartTime(d);
        jobRun.setSubmitTime(d);
        // CMLS-586: updated the status name from not-started to pending
        jobRun.setStatus("pending");
        jobRun.setCreatedBy(user.getId());
        jobRun.setLastModifiedBy(user.getId());
        jobRun.setJob(job);
        jobRun = jobRunService.saveJobRun(jobRun);

        List<String> args = new ArrayList<>();
        args.add(job.getScriptPath());
        args.add(job.getClArguments());

        String fileExtn = FilenameUtils.getExtension(job.getScriptPath());
        log.debug("Script path is: {}, extension is: {}", job.getScriptPath(), fileExtn);

        if (fileExtn != null && !fileExtn.isEmpty()) {
            if (fileExtn.equalsIgnoreCase(Constants.TCL)) {
                log.debug("Sending command to start a job using TCL script");
            /*	handler.sendCommand(new AgentCommand(jobRun.getId(), user.getLoginName(), job.getInstanceType(),
                        job.getNumberOfNodes(), JOBS, Constants.NEWSUB, job.getScriptPath(), jobRun.getLiberatePath(),
						job.getToolName(), job.getClArguments()));*/
                AgentCommand agentCommand = new AgentCommand(jobRun.getId(), user.getLoginName(), job.getInstanceType(),
                        job.getNumberOfNodes(), JOBS, Constants.NEWSUB, job.getScriptPath(), jobRun.getLiberatePath(),
                        job.getToolName(), job.getClArguments());
                log.debug("JobRunController.. AgentCommand " + agentCommand);
                publisher.pubhishEvent(agentCommand, null);

            } else if (fileExtn.equalsIgnoreCase(Constants.SH)) {
                log.debug("Sending command to start a job using Shell Script");
/*                handler.sendCommand(new AgentCommand(jobRun.getId(), user.getLoginName(), job.getInstanceType(),
                        job.getNumberOfNodes(), JOBS, Constants.NEWSUB, job.getScriptPath(), jobRun.getLiberatePath(),
                        "sh", job.getClArguments()));*/
                AgentCommand agentCommand = new AgentCommand(jobRun.getId(), user.getLoginName(), job.getInstanceType(),
                        job.getNumberOfNodes(), JOBS, Constants.NEWSUB, job.getScriptPath(), jobRun.getLiberatePath(),
                        "sh", job.getClArguments());
                publisher.pubhishEvent(agentCommand, null);
            } else if (fileExtn.equalsIgnoreCase(Constants.MK)) {
                log.debug("Sending command to start a job using .MK file");
/*
                handler.sendCommand(new AgentCommand(jobRun.getId(), user.getLoginName(), job.getInstanceType(),
                        job.getNumberOfNodes(), JOBS, Constants.NEWSUB, job.getScriptPath(), jobRun.getLiberatePath(),
                        "make -f", job.getClArguments()));
*/
                AgentCommand agentCommand = new AgentCommand(jobRun.getId(), user.getLoginName(), job.getInstanceType(),
                        job.getNumberOfNodes(), JOBS, Constants.NEWSUB, job.getScriptPath(), jobRun.getLiberatePath(),
                        "make -f", job.getClArguments());
                publisher.pubhishEvent(agentCommand, null);

            } else if (job.getShebang() != null && !job.getShebang().equals("")) {
                log.debug("Sending command to start a job using interpreter found at line number 1");
/*                handler.sendCommand(new AgentCommand(jobRun.getId(), user.getLoginName(), job.getInstanceType(),
                        job.getNumberOfNodes(), JOBS, Constants.NEWSUB, job.getScriptPath(), jobRun.getLiberatePath(),
                        jobRun.getShebang(), job.getClArguments()));*/
                AgentCommand agentCommand = new AgentCommand(jobRun.getId(), user.getLoginName(), job.getInstanceType(),
                        job.getNumberOfNodes(), JOBS, Constants.NEWSUB, job.getScriptPath(), jobRun.getLiberatePath(),
                        jobRun.getShebang(), job.getClArguments());
                log.debug("JobRunController.. AgentCommand " + agentCommand);
                publisher.pubhishEvent(agentCommand, null);

            } else {
                log.debug("Invalid script, not sending any command");
                response.sendError(HttpStatus.NOT_FOUND.value(), "Invalid script");
            }
        } else if (job.getScriptPath().endsWith("Makefile")) {
            log.debug("Sending command to start a job using Makefile");
/*            handler.sendCommand(new AgentCommand(jobRun.getId(), user.getLoginName(), job.getInstanceType(),
                    job.getNumberOfNodes(), JOBS, Constants.NEWSUB, job.getScriptPath(), jobRun.getLiberatePath(),
                    "make -f", job.getClArguments()));*/
            AgentCommand agentCommand = new AgentCommand(jobRun.getId(), user.getLoginName(), job.getInstanceType(),
                    job.getNumberOfNodes(), JOBS, Constants.NEWSUB, job.getScriptPath(), jobRun.getLiberatePath(),
                    "make -f", job.getClArguments());
            publisher.pubhishEvent(agentCommand, null);

        } else {
            response.sendError(HttpStatus.NOT_FOUND.value(), "Invalid script");
        }

        return jobRun;
    }

    /*
     * DTO class for Run log lines. Cannot use the RunLog class which is
	 * essentially a Model class and hence needs to have a value for id. This
	 * was making sense because we were using the same to save in DB. We dont
	 * save logs in DB any more. The fields other than logLine dont make much
	 * sense now however still keeping them so that things that are/may be
	 * accessing these fields dont break.
     */
    class NewRunLog {
        private String id;
        private String runId;
        private String logLine;
        private Date timestamp;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getRunId() {
            return runId;
        }

        public void setRunId(String runId) {
            this.runId = runId;
        }

        public String getLogLine() {
            return logLine;
        }

        public void setLogLine(String logLine) {
            this.logLine = logLine;
        }

        public Date getTimestamp() {
            return timestamp;
        }

        public void setTimestamp(Date timestamp) {
            this.timestamp = timestamp;
        }

    }

    /*
     * Fetch last 200 lines from the stdout file of the job-run and send it to
	 * UI
     */
    @GetMapping("{jobRunId}/logs")
    @ResponseStatus(HttpStatus.OK)
    public List<NewRunLog> getLogs(@PathVariable String jobRunId, HttpServletResponse response, Principal principal)
            throws IOException, InterruptedException {
        String username = principal.getName().substring(0, principal.getName().indexOf("@"));
        log.debug("Fetching logs for: {}", "/home/" + username + "/cloud_orchestrator/runs/" + jobRunId + "/stdout");
        List<NewRunLog> runLogs = new ArrayList<NewRunLog>();
        List<String> commands = new ArrayList<String>();
        commands.add("sudo");
		commands.add("-u");
        commands.add(username);
        commands.add("/bin/bash");
		commands.add("-c");
		commands.add("tail -n " + LOG_LINE_LIMIT + " /home/" + username + "/cloud_orchestrator/runs/" + jobRunId + "/stdout");
		
        ProcessBuilder builder = new ProcessBuilder(commands).redirectErrorStream(true).inheritIO()
                .redirectOutput(Redirect.PIPE);
        Process process = builder.start();
        String line = null;
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(process.getInputStream()));
        while ((line = bufferedReader.readLine()) != null) {
            NewRunLog runLog = new NewRunLog();
            runLog.setLogLine(line);
            runLogs.add(runLog);
        }
        process.waitFor();
        if (runLogs.size() == 0) {
            response.setContentType(MediaType.TEXT_PLAIN_VALUE);
            response.setStatus(HttpStatus.NO_CONTENT.value());
            return null;
        }
        for (NewRunLog log : runLogs) {
            if (log.getLogLine().toLowerCase().contains("no such file or directory")) {
                response.setContentType(MediaType.TEXT_PLAIN_VALUE);
                response.setStatus(HttpStatus.NOT_FOUND.value());
                return null;
            }
        }
        return runLogs;
    }

    @GetMapping(value = "{jobRunId}/status")
    public ResponseEntity<ResponseVO> getStatus(@PathVariable String jobRunId, HttpServletResponse response) {
        log.debug("{jobRunId}/status entered.." + jobRunId);
        ResponseVO responseVO = new ResponseVO();
        String status = "";
        JobRun run = jobRunService.getJobRun(jobRunId);
        if (run == null) {
            response.setContentType(MediaType.TEXT_PLAIN_VALUE);
            response.setStatus(HttpStatus.NOT_FOUND.value());
            log.debug("Setting status as NOT FOUND");
            return null;
        }
        log.debug("job id is {}", run.getJobId());
        status = run.getStatus();
        log.debug("Job status for id {} is {} ", jobRunId, status);
        if (null != status && "" != status) {
            responseVO.setStatus(status);
            return ResponseEntity.ok(responseVO);
        }
        return ResponseEntity.ok(responseVO);

    }

    @GetMapping(value = "{jobRunId}/logs/stream", produces = "text/event-stream")
    @ResponseStatus(HttpStatus.OK)
    public SseEmitter getLogsStream(@PathVariable String jobRunId, HttpServletResponse response, HttpSession session,
                                    Principal principal) throws IOException {
        log.debug("JobRunController .. getLogsStream - logs/stream called for " + jobRunId);
        return getStream(jobRunId, "logs", response, session, principal);
    }

    @GetMapping(value = "{jobRunId}/events/stream", produces = "text/event-stream")
    @ResponseStatus(HttpStatus.OK)
    public SseEmitter getEventsStream(@PathVariable String jobRunId, HttpServletResponse response, HttpSession session,
                                      Principal principal) throws IOException {
        log.debug("JobRunController .. getLogsStream - events/stream called for " + jobRunId);
        return getStream(jobRunId, "events", response, session, principal);
    }

    private SseEmitter getStream(String jobRunId, String type, HttpServletResponse response, HttpSession session,
                                 Principal principal) throws IOException {

        JobRun run = jobRunService.getJobRun(jobRunId);

        if (run == null) {
            log.debug("Job Run: {}, not found. Retruning 404", jobRunId);

            response.setContentType(MediaType.TEXT_PLAIN_VALUE);
            response.setStatus(HttpStatus.NOT_FOUND.value());
            return null;
        }

        log.debug("Run status is: {}", run.getStatus());

        if (run.getStatus().equals(LiberateStatus.failed.value())
                || run.getStatus().equals(LiberateStatus.completed.value())
                || run.getStatus().equals(LiberateStatus.cancelled.value())) {
            log.debug("Job Run {} status is {}. Sending HTTP GONE (410)", jobRunId, run.getStatus());

            response.setContentType(MediaType.TEXT_PLAIN_VALUE);
            response.sendError(HttpStatus.GONE.value(), "Job run already completed");
            return null;
        }

        String userName = principal.getName().split("@")[0];

        String sessionPublisherKey = session.getId() + ".publisher." + type + "." + jobRunId;

        PublisherService oldPublisher = (PublisherService) session.getAttribute(sessionPublisherKey);
        if (oldPublisher != null) {
            log.debug("Found old publisher. Killing it.");
            oldPublisher.setAlive(false);
        }

        final PublisherService publisher;
        if (type.equals("logs")) {
            log.debug("Creating logs publisher");
            publisher = handler.getLogsPublisher(userName, session.getId(), jobRunId);
        } else {
            log.debug("Creating events publisher");
            publisher = handler.getEventsPublisher(userName, session.getId(), jobRunId);
        }

        if (publisher == null) {
            log.debug("Invalid type: {}. Sending HTTP BAD REQUEST (400)", type);

            response.sendError(HttpStatus.BAD_REQUEST.value(), "Invalid type: [" + type + "]");
            return null;
        }

        log.debug("Publisher created for {}", type);

        SseEmitter emitter = new SseEmitter();
        emitter.onCompletion(() -> {
            log.debug("{} Emitter Completed", type);
            publisher.setAlive(false);
            session.removeAttribute(sessionPublisherKey);
        });

        emitter.onTimeout(() -> {
            log.debug(type + " Emitter Timeout");
            publisher.setAlive(false);
            session.removeAttribute(sessionPublisherKey);
        });

        RxReactiveStreams.toObservable(publisher).subscribe(str -> {
            try {
                log.debug("Sending {}: {}", type, str);
                emitter.send(str);
            } catch (Exception e) {
                log.debug("Exception emitting {}", type);
                log.debug(type + " Exception: ", e);
                log.debug("Killing {} publisher", type);

                publisher.setAlive(false);
                emitter.completeWithError(e);
            }
        }, error -> {
            log.debug("Caught an error while sending {}", type);
            log.debug("Error: ", error);
            emitter.completeWithError(error);
        }, emitter::complete);

        log.debug("Setting new publisher in the session");
        session.setAttribute(sessionPublisherKey, publisher);

        log.debug("Returning {} emitter for {}@{} for {}", type, jobRunId, session.getId(), userName);
        return emitter;
    }

    @GetMapping(value = "{jobRunId}/events", produces = "application/json")
    @ResponseStatus(HttpStatus.OK)
    public Set<LiberateEvent> getEvents(@PathVariable String jobRunId, HttpServletResponse response) {
        JobRun jobRun = jobRunService.getJobRun(jobRunId);
        Set<LiberateEvent> events = jobRun.getLiberateEvents();

        if (CollectionUtils.isEmpty(events)) {
            response.setStatus(HttpStatus.NO_CONTENT.value());
        }

        return events;
    }

    @GetMapping("{jobRunId}/events/{eventId}")
    @ResponseStatus(HttpStatus.OK)
    public LiberateEvent getEvent(@PathVariable String jobRunId, @PathVariable String eventId,
                                  HttpServletResponse response) {
        LiberateEvent event = eventRepository.findOne(eventId);

        if (event == null) {
            response.setStatus(HttpStatus.NOT_FOUND.value());
        }

        return eventRepository.save(event);
    }

    @PostMapping("{jobRunId}/status/{status}")
    @ResponseStatus(HttpStatus.OK)
    public JobRun readEvent(@PathVariable String jobRunId, @PathVariable String status,
                            HttpServletResponse response) {
        log.debug("Inside readEvent. runId: {}", jobRunId);
        JobRun jobRun = jobRunRepository.findOne(Long.parseLong(jobRunId));

        if (jobRun == null) {
            response.setStatus(HttpStatus.NOT_FOUND.value());
        }

        Map<String, String> read = jobRun.isRead();
        read.put(status, "true");
        jobRun.setRead(read);
        ;
        log.debug("Exiting readEvent");
        return jobRunRepository.save(jobRun);
    }

    @PutMapping("{jobRunId}")
    @ResponseStatus(HttpStatus.OK)
    public JobRun killJob(@PathVariable String jobRunId, HttpServletResponse response, HttpSession session)
            throws IOException, CumulusException {
        log.debug("Kill request for {}", jobRunId);



        JobRun jobRun = jobRunService.getJobRun(jobRunId);

        if (jobRun == null) {
            log.debug("Job run {} not found, returning 404", jobRunId);
            response.setStatus(HttpStatus.NOT_FOUND.value());
        }

        CumulusUser user = (CumulusUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        Enumeration<String> sessionAttributes = session.getAttributeNames();
        while (sessionAttributes.hasMoreElements()) {
            String name = sessionAttributes.nextElement();
            if (name.startsWith(session.getId() + ".publisher.logs." + jobRunId)
                    || name.startsWith(session.getId() + ".publisher.events." + jobRunId)) {
                log.debug("[KillJob] Found publisher {}", name);
                PublisherService publisher = (PublisherService) session.getAttribute(name);
                if (publisher != null) {
                    log.debug("[KillJob] Killing publihser");
                    publisher.setAlive(false);
                }
            }
        }

        handler.removeEventsPublisher(user.getLoginName(), session.getId(), jobRunId);
        handler.removeLogsPublisher(user.getLoginName(), session.getId(), jobRunId);
        DeferredResult<AgentResponse> deferredResponse = new DeferredResult<>();
        //TODO: Shambhhu: Refactored code at this location.. so need to test this properly
        //handler.sendKillCommand(jobRun, user.getLoginName(), deferredResponse);
        sendKillCommand(jobRun, user.getLoginName(), deferredResponse);


        log.debug("Request sent to KillJob run");
        // jobRunService.deleteJobRun(jobRunId);
        return jobRun;
    }

    @DeleteMapping(value = "{jobRunId}")
    @ResponseStatus(HttpStatus.OK)
    public void deleteJob(@PathVariable String jobRunId, HttpServletResponse response)
            throws IOException, CumulusException {
        JobRun run = jobRunService.getJobRun(jobRunId);
        if (run == null) {
            response.setStatus(HttpStatus.NOT_FOUND.value());
        }
        if ("progress".equalsIgnoreCase(run.getStatus())) {
            throw new CumulusException("JobRun is not at end state");
        }
        jobRunService.deleteJobRun(run);
    }

    private void sendKillCommand(JobRun jobRun, String userName, DeferredResult<AgentResponse> response)
            throws IOException {
        AgentCommand command = new AgentCommand("" + jobRun.getId(), userName, "kill", null, null, null);
        log.debug("SendKill command initiated.." + command);
        publisher.pubhishEvent(command, response);
    }


}
