package com.cadence.cumulus.service.impl;

import com.cadence.cumulus.dto.BulkActionPayload;
import com.cadence.cumulus.dto.ProcessResult;
import com.cadence.cumulus.dto.SingleActionPayload;
import com.cadence.cumulus.service.OSFileManager;
import com.cadence.cumulus.utility.FileSystemUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowire;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.util.StringUtils;

import java.io.File;

/**
 * @author anirban
 */
@Configurable(autowire = Autowire.BY_TYPE, preConstruction = true, value = "fm")
public class UnixFileManagerImpl implements OSFileManager {

    private static final Logger log = LoggerFactory.getLogger(UnixFileManagerImpl.class);

    @Autowired
    private FileSystemUtil fsUtil;

    @Override
    public String doesUploadedResourceExists(SingleActionPayload metadata) throws Exception {// called by /api/info/**
    																						 // called by /api/save/**
        // TODO Auto-generated method stub
        log.debug("{} SingleActionPayload - basePath: {}, userName: {}, diskResourcePath: {}", "doesUploadedResourceExists", metadata.getBasePath(), metadata.getUserName(), metadata.getDiskResourcePath());
        String size = String.valueOf(0);
        /*ProcessResult processResult = fsUtil.runExternalCommand("sudo", "-u", metadata.getUserName(),
                "ls -l \"" + metadata.getAbsolutePath() + "\"" + " | cut -d \" \" -f5");*/
        ProcessResult processResult = fsUtil.runExternalCommand("sudo", "-u", metadata.getUserName(),
                "ls -l \"" + metadata.getDiskResourcePath() + "\"" + " | cut -d \" \" -f5");
        String output = processResult.getOutput();
        log.debug("Output Code For " + metadata.getDiskResourcePath() + "is =" + processResult.getExitCode() + "And Result ="
                + processResult.getOutput());
        if (output.matches("\\d*")) {
            size = output.trim();
            log.debug("Size is" + size);
        }
        return size;
    }

    @Override
    public String lastModifiedAt(SingleActionPayload metadata) throws Exception {// called by /api/info/**
        // TODO Auto-generated method stub
        String timeInMilliseconds = "0";
        log.debug("{} SingleActionPayload - basePath: {}, userName: {}, diskResourcePath: {}", "lastModifiedAt", metadata.getBasePath(), metadata.getUserName(), metadata.getDiskResourcePath());
        /*ProcessResult processResult = fsUtil.runExternalCommand("sudo", "-u", metadata.getUserName(), "stat -c %Y \"" + metadata.getAbsolutePath() + "\"");
        log.debug("user:" + metadata.getUserName() + ":" + "checking when " + metadata.getAbsolutePath() + " last modified; result=" + processResult.getExitCode());
        */
        ProcessResult processResult = fsUtil.runExternalCommand("sudo", "-u", metadata.getUserName(), "stat -c %Y \"" + metadata.getDiskResourcePath() + "\"");
        log.debug("user:" + metadata.getUserName() + ":" + "checking when " + metadata.getDiskResourcePath() + " last modified; result=" + processResult.getExitCode());
        if (processResult.getExitCode() == 0) {
            String timeInSeconds = processResult.getOutput().trim();
            Long seconds = Long.parseLong(timeInSeconds);
            Long milliseconds = seconds * 1000l;// controller expects all timestamp in milliseconds
            timeInMilliseconds = milliseconds.toString();
        }
        return timeInMilliseconds;
        //return processResult.getExitCode() == 0 ? processResult.getOutput().trim() : "0";
    }

    @Override
    public Boolean doesResourceExist(SingleActionPayload metadata) throws Exception {// called by /api/info/**
        // TODO Auto-generated method stub
        log.debug("{} SingleActionPayload - basePath: {}, userName: {}, diskResourcePath: {}", "doesResourceExist", metadata.getBasePath(), metadata.getUserName(), metadata.getDiskResourcePath());
        log.debug("Inside exists********" + metadata.getDiskResourcePath());
        //ProcessResult processResult = fsUtil.runExternalCommand("sudo", "-u", metadata.getUserName(), "ls -l \"" + metadata.getAbsolutePath() + "\"");
        ProcessResult processResult = fsUtil.runExternalCommand("sudo", "-u", metadata.getUserName(), "ls -l \"" + metadata.getDiskResourcePath() + "\"");
        log.debug("Exists = ls -l " + metadata.getDiskResourcePath() + "; exitcode=" + processResult.getExitCode());
        return processResult.getExitCode() == 0;
    }

    @Override
    public void createDirectoryResourceByUser(SingleActionPayload metadata) throws Exception {
        // TODO Auto-generated method stub
        log.debug("{} SingleActionPayload - basePath: {}, userName: {}, diskResourcePath: {}", "createDirectoryResourceByUser", metadata.getBasePath(), metadata.getUserName(), metadata.getDiskResourcePath());
        /*ProcessResult processResult = fsUtil.runExternalCommand("sudo", "-u", metadata.getUserName(), "mkdir -p \"" + metadata.getAbsolutePath() + "\"");
        log.debug("user:" + metadata.getUserName() + ":" + "creating directory " + metadata.getAbsolutePath() + "; exitcode=" + processResult.getExitCode());*/
        ProcessResult processResult = fsUtil.runExternalCommand("sudo", "-u", metadata.getUserName(), "mkdir -p \"" + metadata.getDiskResourcePath() + "\"");
        log.debug("user:" + metadata.getUserName() + ":" + "creating directory " + metadata.getDiskResourcePath() + "; exitcode=" + processResult.getExitCode());
    }

    @Override
    public void appendToFileResource(BulkActionPayload metadata) throws Exception {
        // TODO Auto-generated method stub
        log.debug("{} BulkActionPayload - basePath: {}, userName: {}, sourcePaths: {}, destinationPaths: {}, singleFileName: {}", "appendToFileResource", metadata.getBasePath(), metadata.getUserName(), metadata.getSourcePaths(), metadata.getDestinationPath(), metadata.getSingleFileName());
        ProcessResult processResult = fsUtil.runExternalCommand("sudo", "-u", metadata.getUserName(), "cat \"" + metadata.getSourcePaths().get(0) + "\" >> \"" + metadata.getDestinationPath() + "\"");
        log.debug("user:" + metadata.getUserName() + ":" + "Appending " + metadata.getSourcePaths().get(0) + " to " + metadata.getDestinationPath() + "; exitcode=" + processResult.getExitCode() + ";output->" + processResult.getOutput());
    }

    @Override
    public Integer moveResourceByUser(BulkActionPayload metadata) throws Exception {
        // TODO Auto-generated method stub
        log.debug("{} BulkActionPayload - basePath: {}, userName: {}, sourcePaths: {}, destinationPaths: {}, singleFileName: {}", "moveResourceByUser", metadata.getBasePath(), metadata.getUserName(), metadata.getSourcePaths(), metadata.getDestinationPath(), metadata.getSingleFileName());
        ProcessResult processResult = fsUtil.runExternalCommand("sudo", "-u", metadata.getUserName(), "mv \"" + metadata.getSourcePaths().get(0) + "\" \"" + metadata.getDestinationPath() + "\"");
        log.debug("user:" + metadata.getUserName() + ":" + "Moving :  " + metadata.getSourcePaths().get(0) + " to " + metadata.getDestinationPath() + "exitcode=" + processResult.getExitCode());
        return processResult.getExitCode();
    }

    @Override
    public void removeResourceByUser(SingleActionPayload metadata) throws Exception {
        // TODO Auto-generated method stub
        log.debug("{} SingleActionPayload - basePath: {}, userName: {}, diskResourcePath: {}", "removeResourceByUser", metadata.getBasePath(), metadata.getUserName(), metadata.getDiskResourcePath());
        log.debug("Inside rmdirByUser" + metadata.getDiskResourcePath());
        /*ProcessResult processResult = fsUtil.runExternalCommand("sudo", "-u", metadata.getUserName(), "rm -rf \"" + metadata.getAbsolutePath() + "\"");
        log.debug("RMDIR = user:" + metadata.getUserName() + ":" + "removed directory " + metadata.getAbsolutePath() + "; exitcode=" + processResult.getExitCode());
        */
        ProcessResult processResult = fsUtil.runExternalCommand("sudo", "-u", metadata.getUserName(), "rm -rf \"" + metadata.getDiskResourcePath() + "\"");
        log.debug("RMDIR = user:" + metadata.getUserName() + ":" + "removed directory " + metadata.getDiskResourcePath() + "; exitcode=" + processResult.getExitCode());
    }

    @Override
    public File createMockFileResource(BulkActionPayload metadata) {
        // TODO Auto-generated method stub
        // probable error - unverified
        //Path path = Paths.get(metadata.getBasePath(), metadata.getUserName(), metadata.getDestinationPath(), metadata.getSingleFileName());
        log.debug("{} BulkActionPayload - basePath: {}, userName: {}, sourcePaths: {}, destinationPaths: {}, singleFileName: {}", "createMockFileResource", metadata.getBasePath(), metadata.getUserName(), metadata.getSourcePaths(), metadata.getDestinationPath(), metadata.getSingleFileName());
        String filePath = (StringUtils.hasText(metadata.getBasePath()) ? metadata.getBasePath() : "");
        filePath = (StringUtils.hasText(filePath) ? filePath + File.separator : "") + (StringUtils.hasText(metadata.getUserName()) ? metadata.getUserName() : "");
        filePath = (StringUtils.hasText(filePath) ? filePath + File.separator : "") + (StringUtils.hasText(metadata.getDestinationPath()) ? metadata.getDestinationPath() : "");
        filePath = (StringUtils.hasText(filePath) ? filePath + File.separator : "") + (StringUtils.hasText(metadata.getSingleFileName()) ? metadata.getSingleFileName() : "");
        if (!filePath.startsWith(File.separator)) {
            filePath = File.separator + filePath;
        }
        File path = new File(filePath);
        log.debug("File mocked: {}", path);
        return path;
    }

    @Override
    public Boolean isResourceFolder(SingleActionPayload metadata) throws Exception {
        // TODO Auto-generated method stub
        log.debug("{} SingleActionPayload - basePath: {}, userName: {}, diskResourcePath: {}", "isResourceFolder", metadata.getBasePath(), metadata.getUserName(), metadata.getDiskResourcePath());

        //TODO - peform code cleanup here...
        //String resourcePathTemp = StringUtils.hasText(metadata.getDiskResourcePath()) ? metadata.getDiskResourcePath() : metadata.getBasePath() + metadata.getUserName();

        //Test case 1 - In case of empty .. /home/username

        //Test case 2 - if you specify the destination - relative path to home folder...

        //TODO - put validatio for NPE check.. Anirban.. Assuming it will not null
        //metadata.getBasePath() = /home/ - Sample value
        //metadata.getUsername() = shambhu - Sample

        String resourcePath = metadata.getBasePath() + metadata.getUserName() + File.separator + metadata.getDiskResourcePath();
        //String resourcePath = metadata.getAbsolutePath();
        log.debug("isResourceFolder -> ResourcePath value " + resourcePath);


        /*ProcessResult processResult = fsUtil.runExternalCommand("sudo", "-u", metadata.getUserName(), "/bin/bash", "-c", 
                "[ -d \"" + metadata.getAbsolutePath() + "\" ]");*/
        ProcessResult processResult = fsUtil.runExternalCommand("sudo", "-u", metadata.getUserName(), "/bin/bash", "-c", 
                "[ -d \"" + resourcePath + "\" ]");
        log.debug("user:" + metadata.getUserName() + ":" + "[ -d " + resourcePath + " ]"
                + "; exitcode=" + processResult.getExitCode());
        return processResult.getExitCode() == 0;
    }

    @Override
    public Boolean isResourceWritable(SingleActionPayload metadata) throws Exception {
        // TODO Auto-generated method stub
        log.debug("{} SingleActionPayload - basePath: {}, userName: {}, diskResourcePath: {}", "isResourceWritable", metadata.getBasePath(), metadata.getUserName(), metadata.getDiskResourcePath());
        //String resourcePath = StringUtils.hasText(metadata.getDiskResourcePath()) ? metadata.getDiskResourcePath() : metadata.getBasePath() + metadata.getUserName();
        String resourcePath = metadata.getBasePath() + metadata.getUserName() + File.separator + metadata.getDiskResourcePath();
        /*ProcessResult processResult = fsUtil.runExternalCommand("sudo", "-u", metadata.getUserName(),
                "[ -w \"" + metadata.getAbsolutePath() + "\" ]");*/
        ProcessResult processResult = fsUtil.runExternalCommand("sudo", "-u", metadata.getUserName(),
                "[ -w \"" + resourcePath + "\" ]");
        log.debug("user:" + metadata.getUserName() + ":" + "[ -w " + resourcePath + " ]" + "; exitcode=" + processResult.getExitCode());
        return processResult.getExitCode() == 0;
    }

    @Override
    public byte[] readResourceContents(SingleActionPayload metadata) throws Exception {
        // TODO Auto-generated method stub
        log.debug("{} SingleActionPayload - basePath: {}, userName: {}, diskResourcePath: {}", "readResourceContents", metadata.getBasePath(), metadata.getUserName(), metadata.getDiskResourcePath());
        // convert file to byte array
        /*ProcessResult processResult = fsUtil.runExternalCommand("sudo", "-u", metadata.getUserName(), "-c",
                "[ od -c \"" + metadata.getDiskResourcePath() + "\" ]");*/
        ProcessResult processResult = fsUtil.runExternalCommand("sudo", "-u", metadata.getUserName(), "cat \"" + metadata.getDiskResourcePath() + "\"");
        byte[] content = processResult.getOutput().getBytes();
        log.debug("user:" + metadata.getUserName() + ":" + "[ cat " + metadata.getDiskResourcePath() + " ]" + "; exitcode=" + processResult.getExitCode());
        return content;
    }

}
