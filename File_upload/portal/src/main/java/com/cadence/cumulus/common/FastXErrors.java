package com.cadence.cumulus.common;

import java.util.HashMap;
import java.util.Map;

/**
 * 
 * @author chandrakant
 *
 *         Messages for the various errors that are generated by the
 *         FastXController
 */
public class FastXErrors {
	public static Map<Integer, String> ERROR_MESSAGES = new HashMap<>();

	static {
		ERROR_MESSAGES.put(100, "Error: 100, Could not connect to FastX Server. Server might be down.");
		ERROR_MESSAGES.put(200, "Error: 200, Invalid response received from FastX Server.");
		ERROR_MESSAGES.put(300, "Error: 300, Unable to find a valid user session. Redirecting to error.");
		ERROR_MESSAGES.put(400, "Error: 400, Invalid XServer Session. Please FastX Server and License Server logs.");
		ERROR_MESSAGES.put(500, "Error: 500, Could not start XClient.");
	}
	
	public static final Integer FASTX_SERVER_DOWN_ERROR = 100;
	public static final Integer FASTX_SERVER_INVALID_RESPONSE = 200;
	public static final Integer FASTX_SERVER_PASSWORD_IS_NULL = 300;
	public static final Integer FASTX_INVALID_XSERVER_SESSION = 400;
	public static final Integer FASTX_COULD_NOT_START_XCLIENT = 500;
	
}
