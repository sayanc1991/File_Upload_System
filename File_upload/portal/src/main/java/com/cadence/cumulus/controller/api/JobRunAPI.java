package com.cadence.cumulus.controller.api;

import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.omg.CORBA.portable.ApplicationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.cadence.cumulus.common.LoginException;
import com.cadence.cumulus.controller.api.handler.BasicAuthenticationHandler;
import com.cadence.cumulus.controller.api.handler.JobRunAPIHandler;
import com.cadence.cumulus.core.security.UserAuthorizationServiceImpl;
import com.cadence.cumulus.model.CumulusUser;
import com.cadence.cumulus.model.JobRun;
import com.cadence.cumulus.model.JobRunRequest;
import com.cadence.cumulus.model.User;
import com.cadence.cumulus.model.UserLogin;
import com.cadence.cumulus.service.CumulusUserService;
import com.cadence.cumulus.service.OktaService;

/**
 * @author shirish
 *
 */
@RestController
@RequestMapping(value = "/api/jobrun")
public class JobRunAPI {
	/**
	 * Logger
	 */
	private static final Logger log = LoggerFactory.getLogger(JobRunAPI.class);

	/**
	 * JOB_RUN_API_AUTHZ Authority
	 */

	@Value("${job.run.api.authz}")
	private String JOB_RUN_API_AUTHZ;

	/**
	 * UNAUTHORIZED_PERMISSION_MSG
	 */
	@Value("${unauthorized.permission.msg}")
	private String UNAUTHORIZED_PERMISSION_MSG;

	@Autowired
	private JobRunAPIHandler jobRunAPIHandler;

	@Autowired
	private OktaService oktaService;

	@Autowired
	private UserAuthorizationServiceImpl userAuthorizationServiceImpl;

	@Autowired
	private CumulusUserService userService;

	/**
	 * End point to start the job
	 * 
	 * @param job
	 * @return
	 * @throws ApplicationException
	 * @throws IOException
	 */
	@PostMapping
	@ResponseStatus(HttpStatus.OK)
	public JobRun runJob(@RequestBody JobRunRequest jobRunRequest,
			HttpServletRequest request, HttpServletResponse response)
			throws ApplicationException, IOException {
		if (log.isDebugEnabled()) {
			log.debug("runJob called..");
		}

		UserLogin userLogin = BasicAuthenticationHandler.checkAuth(request);
		if (userLogin == null) {
			if (log.isDebugEnabled()) {
				log.debug("authorization header missing or incorrect..trying request body..");
			}
			userLogin = jobRunRequest.getUserlogin();
		}

		if (userLogin == null) {
			response.setContentType(MediaType.TEXT_PLAIN_VALUE);
			response.sendError(HttpStatus.UNAUTHORIZED.value(),
					"Missing User Login Details in request or Incorrect Authentication Header");
			return null;
		}

		String loginName = null;
		try {
			loginName = oktaService.doLogin(userLogin);
		} catch (LoginException e) {
			response.setContentType(MediaType.TEXT_PLAIN_VALUE);
			response.sendError(HttpStatus.UNAUTHORIZED.value(), e.getMessage());
			return null;
		}

		if (loginName == null) {
			sendErrorNoUserFound(response);
			return null;
		}
		User user = userService.getUserByEmail(loginName);
		if (user == null) {
			sendErrorNoUserFound(response);
			return null;
		}
		List<GrantedAuthority> authList = userAuthorizationServiceImpl
				.getUserAuthorityList(user);
		if (!(userAuthorizationServiceImpl.hasAuthority(JOB_RUN_API_AUTHZ,
				authList))) {
			response.setContentType(MediaType.TEXT_PLAIN_VALUE);
			response.sendError(HttpStatus.UNAUTHORIZED.value(),
					UNAUTHORIZED_PERMISSION_MSG);
			return null;
		}

		CumulusUser cmlsUser = new CumulusUser(user.getId(), user.getEmail(),
				null, null, null, false, false, false, false, authList);
		return jobRunAPIHandler.runJob(jobRunRequest.getJobRun(), cmlsUser,
				response);
	}

	private void sendErrorNoUserFound(HttpServletResponse response)
			throws IOException {
		response.setContentType(MediaType.TEXT_PLAIN_VALUE);
		response.sendError(HttpStatus.UNAUTHORIZED.value(),
				"Unable to find User.");
	}
}
