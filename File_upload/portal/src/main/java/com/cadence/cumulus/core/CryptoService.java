package com.cadence.cumulus.core;

import org.springframework.stereotype.Service;

import com.cadence.cumulus.common.CumulusException;

@Service
public interface CryptoService {

	public String encrypt(String key, String data) throws CumulusException;
	
	public String decrypt(String key, String data) throws CumulusException;
	
}
