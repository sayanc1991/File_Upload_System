package com.cadence.cumulus.integration.opendj.vo;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ContactInformation {

    private String emailAddress;
	private String telephoneNumber;

    public ContactInformation() {
    }
    
    public ContactInformation(String emailAddress, String telephoneNumber) {
    	this.emailAddress = emailAddress;
    	this.telephoneNumber = telephoneNumber;
    }

    public String getEmailAddress() {
		return emailAddress;
	}

	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}

	public String getTelephoneNumber() {
		return telephoneNumber;
	}

	public void setTelephoneNumber(String telephoneNumber) {
		this.telephoneNumber = telephoneNumber;
	}

	@Override
	public String toString() {
		return "ContactInformation [emailAddress=" + emailAddress + ", telephoneNumber=" + telephoneNumber + "]";
	}

}
