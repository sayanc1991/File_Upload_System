package com.cadence.cumulus.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.cadence.cumulus.model.Application;
import com.cadence.cumulus.model.Plugin;
import com.cadence.cumulus.repository.mysql.PluginRepository;
import com.cadence.cumulus.service.ApplicationService;

/**
 * 
 * @author shirish
 *
 */
@Service
@Transactional(propagation = Propagation.REQUIRED, rollbackFor = { Exception.class })
public class ApplicationServiceImpl implements ApplicationService {

	@Autowired
	private PluginRepository pluginRepository;

	@Override
	public List<Plugin> getPlugin() {
		return pluginRepository.findAll();
	}

	@Override
	public List<Plugin> getActivePlugin() {
		return pluginRepository.findByEnabled('Y');
	}

	@Override
	public Application getApplicationProfile() {
		Application app = new Application();
		app.setAppPluginList(getActivePlugin());
		return app;
	}

}
