package com.cadence.cumulus.events.api;

import com.cadence.cumulus.common.CommandHandlerConstants;
import com.cadence.cumulus.service.FileManagerServiceNew;
import com.cadence.cumulus.tailer.CumulusTailerListener;
import com.cadence.cumulus.tailer.StdoutTailer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.lang.ProcessBuilder.Redirect;

/**
 * NewTailer is a replacement for the tailer we initially had to monitor the
 * contents of a file and as and when the file is updated, call appropriate
 * methods of a listener. This class just does the job and can be made more
 * robust.
 *
 * @author chandrakant
 */
public class NewTailer implements Runnable {
    private String filename;
    private CumulusTailerListener cumulusTailerListener;
    private int sleeptime;
    private String username;
    private FileManagerServiceNew fileManager;
    // indicates the current status of parent process (bsub) whether its
    // stopped/killed
    private volatile boolean parentProcessStopped;
    private Logger log = LoggerFactory.getLogger(NewTailer.class);

    public NewTailer(String filename, CumulusTailerListener cumulusTailerListener, int sleeptime, String username,
                     boolean parentProcessStopped, FileManagerServiceNew fileManager) {
        this.filename = filename;
        this.cumulusTailerListener = cumulusTailerListener;
        this.sleeptime = sleeptime;
        this.username = username;
        this.parentProcessStopped = parentProcessStopped;
        this.fileManager = fileManager;
    }

    public void run() {
        /*
         * The tailer basically tails a file and when the file gets updated with
		 * new content it reads the new content and passes it to a listener to
		 * be handled.
		 */

        log.debug("NewTailer Started the tailer for {}, with sleeptime of {} and username {}", filename, sleeptime, username);
        // the tailer process needs small customization for stdout tailer as
        // there can be too many logs/lines compared to the event file
        if (cumulusTailerListener instanceof StdoutTailer) {
            log.debug("Stdout Tailer... condition entered");
            handleStdoutTailer();
        } else {
            // event tailer
            log.debug("Events Tailer... condition entered");
            handleEventsTailer();
        }

    }

    private void handleEventsTailer() {
        try {
            log.debug("Starting event tailer..");
            ProcessBuilder builder = new ProcessBuilder(new String[]{"sudo","-u", username, "/bin/bash","-c","tail -f " + filename})
                    .inheritIO().redirectOutput(Redirect.PIPE);
            log.debug("handleEventsTailer - Commands " + "sudo -u " +  username  + " tail -f " + filename );
            Process process = builder.start();
            // for auto closeable
            try (BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(process.getInputStream()))) {
                String line = null;
                log.debug("handleEventsTailer.... entered tailing event file output");
                while (true && !parentProcessStopped) {
                    log.debug("HandleEventsTailer - reading from event file.. loop");
                    line = bufferedReader.readLine();
                    log.debug("Fetched line from event file : {}", line);
                    if (line != null) {
                        cumulusTailerListener.handle(line);
                        if (line.contains("\"event_type\":\"disconnect\"")) {
                            log.debug("handleEventsTailer -Destroying forcibly...exiting...");
                            process.destroyForcibly();
                            //TODO: break the process...
                            break;
                        }
                    }

                    Thread.sleep(sleeptime);
                }
                log.debug("handleEventsTailer - Event tailing stopped..");
                log.debug("handleEventsTailer - event tailer process status: {} ", process.isAlive());
                log.debug("handleEventsTailer -Destroyed event tailer process");
            }
        } catch (Exception ex) {
            log.error("handleEventsTailer - Exception in event tailing", ex);
            cumulusTailerListener.handle(ex);
        }

    }

    private void handleStdoutTailer() {
        try {
            log.debug("handleStdoutTailer - Starting Stdout tailer..");
            log.debug("handleStdoutTailer - tailing file " + filename);
            ProcessBuilder builder = new ProcessBuilder(new String[]{"sudo","-u", username, "/bin/bash","-c","tail -f " + filename})
                    .inheritIO().redirectOutput(Redirect.PIPE);
            log.debug("handleStdoutTailer - command -> " + "sudo -u", username, "tail -f " + filename);
            Process process = builder.start();
            // try with resources for auto closing BufferedReader after use
            try (BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(process.getInputStream()))) {
                Long lastUpdatedTimeInSec = getLastUpdatedTimeForFile();
                // wait for the 1st line to be available in the log
                while (true && !parentProcessStopped) {
                    //log.debug("NewTailer - handleStdoutTailer - outside loop - 1");
                    //Vikramaditya: Checks if log file is updated, inserts newline character. Is required for
                    //scripts that doesn't generate newline character
                    Long currentLastUpdatedTimeInSec = getLastUpdatedTimeForFile();
                    while (true) {
                        //log.debug("NewTailer - handleStdoutTailer - inside loop - 2");
                        if (currentLastUpdatedTimeInSec > lastUpdatedTimeInSec) {
                            fileManager.appendToFile(username, filename,
                                    "\n");
                            break;
                        } else {
                            //log.debug("NewTailer - Sleeping before next check");
                            Thread.sleep(sleeptime);
                            currentLastUpdatedTimeInSec = getLastUpdatedTimeForFile();
                        }
                    }
                    String line = bufferedReader.readLine();
                    if (line != null) {
                        // we know the the log file has started getting
                        // populated,
                        // so break from this loop and start tail -200 process
                        log.debug("handleStdoutTailer - destroying the process.. ");
                        process.destroyForcibly();
                        break;
                    }
                }
            }

            log.debug("handleStdoutTailer moving forward");
            int counter = 1;
            String lineContent = "";
            // get the last updated time in sec
            Long lastUpdatedTimeInSec = getLastUpdatedTimeForFile();
            Long currentLastUpdatedTimeInSec = lastUpdatedTimeInSec;
            while (!parentProcessStopped) {

                // perform tail -200 if counter is 0 (i.e first time) or if the
                // current last updated time is greater than the earlier
                // captured time
                currentLastUpdatedTimeInSec = getLastUpdatedTimeForFile();
                //log.debug("NewTailer - currentLastUpdatedTimeInSec : " + currentLastUpdatedTimeInSec);
                //log.debug("NewTailer - lastUpdatedTimeInSec : {} , counter : {} ", lastUpdatedTimeInSec, counter);
                if (counter == 1 || currentLastUpdatedTimeInSec > lastUpdatedTimeInSec) {
                    //log.debug("NewTailer - Performing tail -200 operation # {} ", counter);
                    lineContent = tailLast200LogLines();
                    counter++;
                }
                lastUpdatedTimeInSec = currentLastUpdatedTimeInSec;
                //log.debug("NewTailer - sleeping before next tail -200, Counter = " + counter);
                Thread.sleep(sleeptime);

            }
            // check if the last fetched & sent lines have any indication of
            // process
            // killed/completed
            if (!lineContent.contains(CommandHandlerConstants.OPERATION_CANCELLED)
                    && !lineContent.contains(CommandHandlerConstants.OPERATION_COMPLETED) && !lineContent.contains(CommandHandlerConstants.OPERATION_FAILED)) {
                //log.debug("NewTailer - parent process stopped, sending the final 200 lines..");
                tailLast200LogLines();
            }
            log.debug("handleStdoutTailer - Stdout tailing stopped..");
        } catch (Exception ex) {
            log.error("handleStdoutTailer - Exception while tailing stdout", ex);
            cumulusTailerListener.handle(ex);
        }
    }

    private String tailLast200LogLines() throws Exception {
        ProcessBuilder builder = new ProcessBuilder(new String[]{"sudo","-u", username,"/bin/bash","-c", "tail -200 " + filename})
                .inheritIO().redirectOutput(Redirect.PIPE);
        Process process = builder.start();
        String lineContent = "";
        //log.debug("NewTailer - tailLast200LogLines entered.. command -> " + "su" + username + "-c" + "tail -200 " + filename);
        // auto close buffered reader after use
        try (BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(process.getInputStream()))) {
            StringBuilder lines = new StringBuilder("");
            bufferedReader.lines().forEach(l -> lines.append(l + "\n"));

            // send the lines the cumulusTailerListener if there is content
            lineContent = lines.toString();
            if (lineContent.length() > 0) {
                //log.debug("tailLast200LogLines - Line sent to cumulusTailerListener: ");
                cumulusTailerListener.handle(lines.toString());
            }
            //log.debug("NewTailer - tailLast200LogLines - exiting...");
            // destroy the current tail -200 process
            process.destroyForcibly();
        }
        return lineContent;
    }

    public boolean isParentProcessStopped() {
        return parentProcessStopped;
    }

    public void setParentProcessStopped(boolean parentProcessStopped) {
        this.parentProcessStopped = parentProcessStopped;
    }

    private Long getLastUpdatedTimeForFile() throws Exception {
        // find last updated time of the file in seconds
        //log.debug("NewTailer - getLastUpdatedTimeForFile invoked");
        ProcessBuilder builder = new ProcessBuilder(new String[]{"sudo","-u", username, "/bin/bash","-c","stat -c  %Y " + filename})
                .inheritIO().redirectOutput(Redirect.PIPE);
        //log.debug("NewTailer - getLastUpdatedTimeForFile - commands -> " + "su" + username + "-c" + "stat -c  %Y " + filename);
        Process process = builder.start();
        String lineContent = "";
        // auto close buffered reader after use
        try (BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(process.getInputStream()))) {
            StringBuilder lines = new StringBuilder("");
            bufferedReader.lines().forEach(l -> lines.append(l));

            lineContent = lines.toString();

            //log.debug("NewTailer - getLastUpdatedTimeForFile content.. " + lineContent);
            // destroy the process
            process.destroyForcibly();
        }
        return (lineContent.length() > 0) ? Long.parseLong(lineContent) : 0;
    }

}
