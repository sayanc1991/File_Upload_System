package com.cadence.cumulus.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.cadence.cumulus.common.AppMessages;
import com.cadence.cumulus.common.Constants;
import com.cadence.cumulus.common.CumulusException;
import com.cadence.cumulus.integration.IntegrationException;
import com.cadence.cumulus.integration.unifiedregistration.service.URIntegrationService;
import com.cadence.cumulus.integration.unifiedregistration.vo.UpdateUserContactInfoVO;
import com.cadence.cumulus.integration.unifiedregistration.vo.UpdateUserVO;
import com.cadence.cumulus.integration.unifiedregistration.vo.UserAuthDetail;
import com.cadence.cumulus.model.Organisation;
import com.cadence.cumulus.model.Permission;
import com.cadence.cumulus.model.Role;
import com.cadence.cumulus.model.RoleDetail;
import com.cadence.cumulus.service.OrganisationService;
import com.cadence.cumulus.service.UserManagementService;

/**
 * Controller with REST end points for all user management related API's
 * 
 * @author Zabeersultan.Farook
 *
 */
@RestController
@RequestMapping(value = "ums")
@PreAuthorize("hasAuthority('User Administration')")
public class UserManagementController {

	private static final Logger LOG = LoggerFactory.getLogger(UserManagementController.class);

	@Autowired
	private UserManagementService userManagementService;
	
	@Autowired
	private OrganisationService organisationService;
	
	@Autowired
	private URIntegrationService urIntegrationService;
	
	@Value("${ur.cumulus.create.newuser.type}")
	private String userType;
	
    private String IN_ACTIVE_USER = "Inactive";
    
    private String CADENCE_DOMAIN = "@cadence.com";
    
    private String INTERNAL_USER = "int";
    
	@Value("${ur.cumulus.create.newuser.appcode}")
	private String newUserAppcode;

	@RequestMapping(value = "/list", method = RequestMethod.GET)
	public List<UserAuthDetail> getUserList(@RequestParam("firstName") String firstName,
			@RequestParam("lastName") String lastName, @RequestParam("emailID") String emailID,
			@RequestParam("roleID") String roleID, HttpServletResponse response) throws IOException {
		LOG.debug("Within UserManagementController list User method");
		List<UserAuthDetail> userList = new ArrayList<>();
		try {
			userList = userManagementService.getCumulusUsers(firstName, lastName, emailID, roleID);
		} catch (CumulusException ce) {
			LOG.error("Exception while invoking getCumulusUsers method in UserManagementService", ce);
			response.setContentType(MediaType.TEXT_PLAIN_VALUE);
			response.sendError(HttpStatus.INTERNAL_SERVER_ERROR.value(), ce.getMessage());
			return null;
		}
		LOG.debug("Found : {} users for the search criteria", userList.size());
		return userList;

	}

	@RequestMapping(value = "/ursearch", method = RequestMethod.GET)
	public UserAuthDetail searchUserFromUR(@RequestParam("emailId") String emailId, HttpServletResponse response)
			throws IOException {
		LOG.debug("Within UserManagementController searchUserFromUR method");
		try {
			String domainName = emailId.substring(emailId.indexOf("@")+1, emailId.length());
			LOG.debug("search user domain: "+domainName);
			Organisation organisation = organisationService.findByDomain(domainName);
			if (organisation == null) {
				LOG.debug("User not found in registered domain.");
				response.setContentType(MediaType.TEXT_PLAIN_VALUE);
				// response.sendError(HttpStatus.INTERNAL_SERVER_ERROR.value(), "Domain not found");
				response.sendError(HttpStatus.INTERNAL_SERVER_ERROR.value(),
						AppMessages.USER_REGISTRATION_DOMAIN_NOT_FOUND.getMessage() + String.format(
								Constants.ERROR_CODE, AppMessages.USER_REGISTRATION_DOMAIN_NOT_FOUND.getCode()));
				return null;
			}
		} catch (CumulusException ce) {
			LOG.error("Exception while finding user domain in organisation.", ce);
			response.setContentType(MediaType.TEXT_PLAIN_VALUE);
			response.sendError(HttpStatus.INTERNAL_SERVER_ERROR.value(),
					AppMessages.USER_REGISTRATION_DOMAIN_NOT_FOUND.getMessage() + String.format(Constants.ERROR_CODE,
							AppMessages.USER_REGISTRATION_DOMAIN_NOT_FOUND.getCode())); // "Domain not found"
			return null;
		}
		UserAuthDetail userAuthDetail = null;
		try {
			userAuthDetail = userManagementService.getUserFromUnifiedRegistration(emailId);
		} catch (CumulusException ce) {
			LOG.error("Exception while invoking getUserFromUnifiedRegistration method in UserManagementService", ce);
			response.setContentType(MediaType.TEXT_PLAIN_VALUE);
			response.sendError(HttpStatus.INTERNAL_SERVER_ERROR.value(), ce.getMessage());
			return null;
		}
		LOG.debug("User found in UR : " + emailId);
		return userAuthDetail;

	}

	// NOTE : This API function merged with new user registration(UserProfileController->/create API).
	@RequestMapping(value = "/newuser", method = RequestMethod.POST)
	public UserAuthDetail addNewUser(@RequestBody UserAuthDetail userAuthDetail, HttpServletResponse response)
			throws IOException {
		UserAuthDetail responseObj = null;
		try {
			responseObj = userManagementService.addNewUser(userAuthDetail);
		} catch (CumulusException ce) {
			LOG.error("Exception while invoking addNewUser method in UserManagementService", ce);
			response.setContentType(MediaType.TEXT_PLAIN_VALUE);
			response.sendError(HttpStatus.INTERNAL_SERVER_ERROR.value(), ce.getMessage());
			return null;
		}
		return responseObj;
	}

	@RequestMapping(value = "/updateuser", method = RequestMethod.PUT)
	public UserAuthDetail updateUser(@RequestBody UserAuthDetail userAuthDetail, HttpServletResponse response)
			throws IOException {
		UserAuthDetail responseObj = null;
		try {
			responseObj = userManagementService.updateUser(userAuthDetail, true);
		} catch (CumulusException ce) {
			LOG.error("Exception while invoking updateUser method in UserManagementService", ce);
			response.setContentType(MediaType.TEXT_PLAIN_VALUE);
			response.sendError(HttpStatus.INTERNAL_SERVER_ERROR.value(), ce.getMessage());
			return null;
		}
		return responseObj;
	}

	@RequestMapping(value = "/removeuser/{emailId}/{contactId}", method = RequestMethod.DELETE)
	public void removeUser(@PathVariable("emailId") String emailId, @PathVariable("contactId") String contactId,
			HttpServletResponse response) throws IOException, CumulusException {

		UpdateUserVO updateUserVO = new UpdateUserVO();
		updateUserVO.setLoginid(emailId);
		UpdateUserContactInfoVO userContactInfoVO = new UpdateUserContactInfoVO();
		userContactInfoVO.setId(contactId);
		updateUserVO.setCon(userContactInfoVO);
		updateUserVO.setAppcode("Remove_"+newUserAppcode);
		if (emailId.endsWith(CADENCE_DOMAIN)) {
			updateUserVO.setUsertype(INTERNAL_USER);
		} else {
			updateUserVO.setUsertype(userType);
		}
		LOG.info("Start of removeUser : "+updateUserVO.toString());
		try {
			urIntegrationService.updateUserGroup(updateUserVO);
		} catch (IntegrationException ie) {
			LOG.error("Exception while invoking UR integration API", ie);
			// TODO : handle exception during 403 error.
			// throw new CumulusException("Removing user group failed, Please try again.");
			throw new CumulusException(AppMessages.USER_DELETE_FAILED_IN_UR.getMessage()
					+ String.format(Constants.ERROR_CODE, AppMessages.USER_DELETE_FAILED_IN_UR.getCode()));
		}

		// Call DB to update user status with In-Active.
		try {
			userManagementService.updateUser(emailId, IN_ACTIVE_USER);
		} catch (CumulusException ce) {
			LOG.error("Exception while invoking updateUser method in UserManagementService", ce);
			response.setContentType(MediaType.TEXT_PLAIN_VALUE);
			response.sendError(HttpStatus.INTERNAL_SERVER_ERROR.value(), ce.getMessage());
		}
	}

	/**
	 * This controller method is for populating drop down for Role
	 * 
	 * @param response
	 * @return
	 * @throws IOException
	 */
	@RequestMapping(value = "/roles", method = RequestMethod.GET)
	public List<Role> listAllRoles(HttpServletResponse response) throws IOException {
		List<Role> roleList = new ArrayList<>();
		try {
			Iterable<Role> roleIterable = userManagementService.getAvailableRoles();
			roleIterable.forEach(r -> {
				// clear permission collection as we just need the role
				// attributes for dropdown
				r.getRolePermissionSet().clear();
				roleList.add(r);
			});
		} catch (CumulusException ce) {
			LOG.error("Exception while invoking getAvailableRoles method in UserManagementService", ce);
			response.setContentType(MediaType.TEXT_PLAIN_VALUE);
			response.sendError(HttpStatus.INTERNAL_SERVER_ERROR.value(), ce.getMessage());
		}
		return roleList;
	}
	
	/**
	 * This controller method is for getting all the Roles along with Permissions
	 * 
	 * @param response
	 * @return
	 * @throws IOException
	 */
	@RequestMapping(value = "/roledetails", method = RequestMethod.GET)
	public List<RoleDetail> listAllRoleDetails(HttpServletResponse response) throws IOException {
		List<RoleDetail> roleDetailList = new ArrayList<>();
		try {
			roleDetailList = userManagementService.getAvailableRoleDetails();
			
		} catch (CumulusException ce) {
			LOG.error("Exception while invoking getAvailableRoles method in UserManagementService", ce);
			response.setContentType(MediaType.TEXT_PLAIN_VALUE);
			response.sendError(HttpStatus.INTERNAL_SERVER_ERROR.value(), ce.getMessage());
		}
		return roleDetailList;
	}

	/**
	 * This controller method is for populating drop down for permission
	 * 
	 * @param response
	 * @return
	 * @throws IOException
	 */
	@RequestMapping(value = "/permissions", method = RequestMethod.GET)
	public List<Permission> listAllPermissions(HttpServletResponse response) throws IOException {
		List<Permission> roleList = new ArrayList<>();
		try {
			roleList = userManagementService.getAvailablePermissions();
			

		} catch (CumulusException ce) {
			LOG.error("Exception while invoking getAvailablePermissions method in UserManagementService", ce);
			response.setContentType(MediaType.TEXT_PLAIN_VALUE);
			response.sendError(HttpStatus.INTERNAL_SERVER_ERROR.value(), ce.getMessage());
		}
		return roleList;
	}
	
	@RequestMapping(value = "/newrole", method = RequestMethod.POST)
	public RoleDetail addNewRole(@RequestBody RoleDetail role, HttpServletResponse response)
			throws IOException {
		RoleDetail responseObj = null;
		try {
			responseObj = userManagementService.addNewRole(role);
		} catch (CumulusException ce) {
			LOG.error("Exception while invoking addNewRole method in UserManagementService", ce);
			response.setContentType(MediaType.TEXT_PLAIN_VALUE);
			response.sendError(HttpStatus.INTERNAL_SERVER_ERROR.value(), ce.getMessage());
			return null;
		}
		return responseObj;
	}

	@RequestMapping(value = "/updaterole", method = RequestMethod.PUT)
	public RoleDetail updateRole(@RequestBody RoleDetail role, HttpServletResponse response)
			throws IOException {
		RoleDetail responseObj = null;
		try {
			responseObj = userManagementService.updateRole(role);
		} catch (CumulusException ce) {
			LOG.error("Exception while invoking updateRole method in UserManagementService", ce);
			response.setContentType(MediaType.TEXT_PLAIN_VALUE);
			response.sendError(HttpStatus.INTERNAL_SERVER_ERROR.value(), ce.getMessage());
			return null;
		}
		return responseObj;
	}

	@RequestMapping(value = "/removerole/{id}", method = RequestMethod.DELETE)
	public void removeRole(@PathVariable("id") String id, HttpServletResponse response) throws IOException {
		try {
			userManagementService.removeRole(id);
		} catch (CumulusException ce) {
			LOG.error("Exception while invoking updateUser method in UserManagementService", ce);
			response.setContentType(MediaType.TEXT_PLAIN_VALUE);
			response.sendError(HttpStatus.INTERNAL_SERVER_ERROR.value(), ce.getMessage());
		}
	}
}
