package com.cadence.cumulus.common;

import java.util.HashMap;

public class LDAPUNPWStore {
	
	private static LDAPUNPWStore hack = new LDAPUNPWStore();
	private static HashMap<String, String> unpwHash = new HashMap<String, String>();
	
	private LDAPUNPWStore() {
		
	}
	
	public static LDAPUNPWStore getInstance() {
		return hack;
	}
	
	public void storeUnpw(String user, String password) {
		unpwHash.put(user, password);
	}
	
	public String getPassForUser(String user) {
		return unpwHash.get(user);
	}

}
