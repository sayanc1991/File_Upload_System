package com.cadence.cumulus.service.impl;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.nio.file.attribute.FileTime;

import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.cadence.cumulus.dto.BulkActionPayload;
import com.cadence.cumulus.dto.SingleActionPayload;
import com.cadence.cumulus.service.OSFileManager;

/**
 * 
 * @author anirban
 *
 */

public class WindowsFileManagerImpl implements OSFileManager {

	private static final Logger log = LoggerFactory.getLogger(WindowsFileManagerImpl.class);
	
	@Override
	public String doesUploadedResourceExists(SingleActionPayload metadata) throws Exception {
		// TODO Auto-generated method stub
		String size = "0";
		Boolean status = false;
		Path path = Paths.get(metadata.getDiskResourcePath());
		if(Files.exists(path)) {
			status = true;
			size = String.valueOf(Files.size(path));
		}
		log.info("Resource: {} exists: {} size: {}", path, status, size);
		return size;
	}

	@Override
	public String lastModifiedAt(SingleActionPayload metadata) throws Exception {
		// TODO Auto-generated method stub
		Long time = 0l;
		Boolean status = false;
		Path path = Paths.get(metadata.getDiskResourcePath());
		if(Files.exists(path)) {
			status = true;
			FileTime timestamp = Files.getLastModifiedTime(path);
			time = timestamp.toMillis();
		}
		log.info("Resource: {} exists: {} lastModified: {}", path, status, FileTime.fromMillis(time));
		return String.valueOf(time);
	}

	@Override
	public Boolean doesResourceExist(SingleActionPayload metadata) {
		// TODO Auto-generated method stub
		Path path = Paths.get(metadata.getDiskResourcePath());
		Boolean status = Files.exists(path);
		log.info("Resource: {} exists: {}", path, status);
		return status;
	}

	@Override
	public void createDirectoryResourceByUser(SingleActionPayload metadata) throws Exception{
		// TODO Auto-generated method stub
		Path path = Paths.get(metadata.getDiskResourcePath());
		FileUtils.forceMkdir(path.toFile());
		log.info("Resource: {} created", path);
	}

	@Override
	public void appendToFileResource(BulkActionPayload metadata) throws Exception {
		// TODO Auto-generated method stub
		Path sourceFile = Paths.get(metadata.getSourcePaths().get(0));
		Path targetFile = Paths.get(metadata.getDestinationPath());
		byte[] bytes = FileUtils.readFileToByteArray(sourceFile.toFile());
		FileUtils.writeByteArrayToFile(targetFile.toFile(), bytes, true);
		log.info("Resource appended to: {}", targetFile);
	}

	@Override
	public Integer moveResourceByUser(BulkActionPayload metadata) throws Exception {
		// TODO Auto-generated method stub
		Path source = Paths.get(metadata.getSourcePaths().get(0));
		Path target = Paths.get(metadata.getDestinationPath());
		log.info("Moving from source: {} to target: {}", source, target);
		Path result = Files.move(source, source.resolveSibling(target.getFileName()), StandardCopyOption.REPLACE_EXISTING);
		Long size = result.toFile().length();
		log.info("move result: {}", result);
		return size.intValue();
	}

	@Override
	public void removeResourceByUser(SingleActionPayload metadata) throws Exception {
		// TODO Auto-generated method stub
		Path path = Paths.get(metadata.getDiskResourcePath());
		FileUtils.deleteQuietly(path.toFile());
		log.info("Resource deleted: {}", path);
	}
	
	@Override
	public File createMockFileResource(BulkActionPayload metadata) {
		// TODO Auto-generated method stub
		Path path = Paths.get(metadata.getBasePath(), metadata.getUserName(), metadata.getDestinationPath(), metadata.getSingleFileName());
		return path.toFile();
	}
	
	@Override
	public Boolean isResourceFolder(SingleActionPayload metadata) throws Exception {
		// TODO Auto-generated method stub
		Boolean status = false;
		Boolean directory = false;
		Path parent = Paths.get(metadata.getBasePath(), metadata.getUserName());
		if(!Files.exists(parent)) {
			parent = Files.createDirectories(parent);
		}
		Path path = Paths.get(parent.toString(), metadata.getDiskResourcePath());
		if(Files.exists(path)) {
			status = true;
			directory = Files.isDirectory(path);
		}
		log.info("Resource: {} exists: {} folder: {}", path, status, directory);
		return status;
	}

	@Override
	public Boolean isResourceWritable(SingleActionPayload metadata) {
		// TODO Auto-generated method stub
		Boolean status = false;
		Boolean writable = false;
		Path path = Paths.get(metadata.getBasePath(), metadata.getUserName(), metadata.getDiskResourcePath());
		if(Files.exists(path)) {
			status = true;
			writable = Files.isWritable(path);
		} 
		log.info("Resource: {} exists: {} writable: {}", path, status, writable);
		return status;
	}

	@Override
	public byte[] readResourceContents(SingleActionPayload metadata) throws Exception {
		// TODO Auto-generated method stub
		Path jarPath = Paths.get(metadata.getDiskResourcePath());
		byte[] content = new byte[0];
		Boolean exists = false;
		if(exists = Files.exists(jarPath)) {
			content = Files.readAllBytes(jarPath);
		} 
		log.info("Resource: {} exists: {} size: {}", jarPath, exists, content.length);
		return content;
	}


}
