package com.cadence.cumulus.model;

import java.util.HashMap;
import java.util.Map;

import org.springframework.util.StringUtils;

import com.cadence.cumulus.model.liberate.events.LiberateStatus;

public class CumulusUserNotificationSettings {

	private String firstName;

	private String lastName;

	private String email;

	private String phone;

	private Map<String, Boolean> emailSettings = new HashMap<>();

	private Map<String, Boolean> smsSettings = new HashMap<>();

	private Map<String, Boolean> browserSettings = new HashMap<>();
	
	public CumulusUserNotificationSettings() {
	}

	public boolean canNotifyEmail(LiberateStatus status) {
		return !StringUtils.isEmpty(email) && emailSettings.get(status.name()) != null
				&& emailSettings.get(status.name()) == true;
	}

	public boolean canNotifySms(LiberateStatus status) {
		return !StringUtils.isEmpty(phone) && smsSettings.get(status.name()) != null
				&& smsSettings.get(status.name()) == true;
	}

	public boolean canNotifyBrowser(LiberateStatus status) {
		return browserSettings.get(status.name()) != null && browserSettings.get(status.name()) == true;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public Map<String, Boolean> getEmailSettings() {
		return emailSettings;
	}

	public void setEmailSettings(Map<String, Boolean> emailSettings) {
		this.emailSettings = emailSettings;
	}

	public Map<String, Boolean> getSmsSettings() {
		return smsSettings;
	}

	public void setSmsSettings(Map<String, Boolean> smsSettings) {
		this.smsSettings = smsSettings;
	}

	public Map<String, Boolean> getBrowserSettings() {
		return browserSettings;
	}

	public void setBrowserSettings(Map<String, Boolean> browserSettings) {
		this.browserSettings = browserSettings;
	}

}
