package com.cadence.cumulus.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.cadence.cumulus.model.Permission;
import com.cadence.cumulus.model.Role;
import com.cadence.cumulus.repository.mysql.PermissionRepository;
import com.cadence.cumulus.repository.mysql.RoleRepository;
import com.cadence.cumulus.service.RolePermissionService;

/**
 * 
 * @author shirish
 *
 */
@Service
@Transactional(propagation = Propagation.REQUIRED, rollbackFor = { Exception.class })
public class RolePermissionServiceImpl implements RolePermissionService {

	@Autowired
	private RoleRepository roleRepository;
	
	@Autowired
	private PermissionRepository permissionRepository;

	@Override
	public Iterable<Role> findAllRole() {
		return roleRepository.findAll();
	}
	
	@Override
	public List<Role> findRoleByName(String name) {
		return roleRepository.findByNameIgnoreCase(name);
	}

	@Override
	public List<Permission> findAllPermission() {
		return permissionRepository.findAll();
	}

	@Override
	public void deleteRoleById(Long id) {
		roleRepository.delete(id);
	}

	@Override
	public Role saveRole(Role role) {
		return roleRepository.save(role);
		
	}

	@Override
	public Role findRole(Long id) {
		return roleRepository.findOne(id);
	}
	
	

	

}
