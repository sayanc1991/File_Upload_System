package com.cadence.cumulus.service.impl;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.cadence.cumulus.ProxyConfig;
import com.cadence.cumulus.common.LoginException;
import com.cadence.cumulus.model.UserLogin;
import com.cadence.cumulus.service.OktaService;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.okta.sdk.clients.AuthApiClient;
import com.okta.sdk.clients.FactorsApiClient;
import com.okta.sdk.framework.ApiClientConfiguration;
import com.okta.sdk.models.auth.AuthResult;
import com.okta.sdk.models.factors.Factor;
import com.okta.sdk.models.factors.FactorVerificationResponse;
import com.okta.sdk.models.factors.Verification;
import com.okta.sdk.models.users.User;
import com.okta.sdk.models.users.UserProfile;

/**
 * This class authenticates a user against Okta.
 * 
 * @author shirish
 *
 */
@Service
public class OktaServiceImpl implements OktaService {
	private static final Logger log = LoggerFactory
			.getLogger(OktaServiceImpl.class);

	@Value("${okta.site.url}")
	private String oktaSiteURL;

	@Value("${okta.api.key}")
	private String oktaAPIKey;

	@Autowired
	private ProxyConfig proxyConfig;

	private AuthApiClient authApiClient;
	private FactorsApiClient factorsApiClient;

	public OktaServiceImpl() {

	}

	/**
	 * Init for OktaFactorsAPIClient
	 */

	public void OktaFactorsAPIClient() {
		ApiClientConfiguration oktaSettings = new ApiClientConfiguration(
				oktaSiteURL, oktaAPIKey);
		FactorsApiClient factorsApiClient = new FactorsApiClient(oktaSettings);
		// return factorsApiClient;
		this.factorsApiClient = factorsApiClient;
	}

	/**
	 * init for OktaAuthAPIClient
	 */

	public void OktaAuthAPIClient() {
		System.out.println(this.oktaSiteURL);
		ApiClientConfiguration oktaSettings = new ApiClientConfiguration(
				oktaSiteURL, oktaAPIKey);
		AuthApiClient authClient = new AuthApiClient(oktaSettings);
		// return authClient;
		this.authApiClient = authClient;

	}

	/**
	 * MFA Authentication process
	 * 
	 * @param userLogin
	 * @throws LoginException
	 */
	/*public void doLoginWithMFA(final UserLogin userLogin) throws LoginException {
		if (proxyConfig.isEnabled()) {
			System.setProperty("https.proxyHost", proxyConfig.getHost());
			System.setProperty("https.proxyPort", "" + proxyConfig.getPort());
			System.setProperty("http.proxyHost", proxyConfig.getHost());
			System.setProperty("http.proxyPort", "" + proxyConfig.getPort());
		}
		log.debug("Proxy Set ");
		OktaFactorsAPIClient();
		OktaAuthAPIClient();
		String userId = this.doLogin(userLogin);
		String factorId = null;
		try {
			factorId = getUserFactor(userId);
			if (factorId == null) {
				throw new LoginException(
						"No Factor Authentication defined for User.");
			}

		} catch (IOException e) {
			log.error("Unable to retrieve User factor " + e.getMessage());
			throw new LoginException("Unable to retrieve User factor..");
		}
		try {
			boolean isFactorVerified = verifyFactor(userId, factorId,
					userLogin.getMfaToken());
			if (!isFactorVerified) {
				throw new LoginException("Unable to verify User factor..");
			}
		} catch (IOException e) {
			log.error("Unable to verify User factor " + e.getMessage());
			throw new LoginException("Unable to verify User factor..");
		}

	}*/

	/**
	 * Authentication with Okta
	 * 
	 * @param userLogin
	 * @return
	 * @throws LoginException
	 */
	public String doLogin(final UserLogin userLogin) throws LoginException {
		if (proxyConfig.isEnabled()) {
			System.setProperty("https.proxyHost", proxyConfig.getHost());
			System.setProperty("https.proxyPort", "" + proxyConfig.getPort());
			System.setProperty("http.proxyHost", proxyConfig.getHost());
			System.setProperty("http.proxyPort", "" + proxyConfig.getPort());
		}
		log.debug("Proxy Set ");
		OktaFactorsAPIClient();
		OktaAuthAPIClient();

		log.debug("authApiClient" + authApiClient);
		AuthResult result = null;
		try {
			result = authApiClient.authenticate(userLogin.getUserName(),
					userLogin.getPassword(), "");
		} catch (IOException e) {
			log.error("User Authentication Failed " + e.getMessage());
			throw new LoginException("User Authentication Failed ");
		}
		if (result.getStatus().equals("SUCCESS")) {
			log.info("User " + userLogin.getUserName() + " Authenticated..");
			@SuppressWarnings("unchecked")
			Map<String, Object> user = (Map<String, Object>) result
					.getEmbedded().get("user");
			
			final ObjectMapper mapper = new ObjectMapper();
			final User oktaUser = mapper.convertValue(user, User.class);
			UserProfile userProfile = oktaUser.getProfile();
			String loginName = userProfile.getLogin();
			log.info("User Id " + loginName + " Authenticated..");
			return loginName;
		} else {
			throw new LoginException("User Authentication Failed "
					+ result.getStatus());
		}

	}

	/**
	 * Verifies MFA Token
	 * 
	 * @param userId
	 * @param factorId
	 * @param mfaToken
	 * @return
	 * @throws IOException
	 */
	private boolean verifyFactor(String userId, String factorId, String mfaToken)
			throws IOException {
		Verification verification = new Verification();
		verification.setPassCode(mfaToken);
		FactorVerificationResponse vResponse = this.factorsApiClient
				.verifyFactor(userId, factorId, verification);
		log.info(vResponse.getFactorResult());
		log.info(vResponse.getFactorResultMessage());
		if (vResponse.getFactorResult().equals("SUCCESS")) {
			return true;
		}
		return false;
	}

	/**
	 * 
	 * Gets User factor Id
	 * 
	 * @param userId
	 * @return
	 * @throws IOException
	 */
	/*private String getUserFactor(final String userId) throws IOException {
		log.debug("userId Id - " + userId);
		if (userId != null) {
			List<Factor> factorList = this.factorsApiClient
					.getUserLifecycleFactors(userId);
			log.debug("Factor Id - " + factorList);
			for (Factor factor : factorList) {
				log.debug("Factor Id - " + factor.getProvider() + " "
						+ factor.getFactorType());
				if (factor.getProvider().equals(this.oktaMFAFactorProvider)
						&& factor.getFactorType()
								.equals(this.oktaMFAFactorType)) {
					log.debug("Factor Id - " + factor.getId());
					return factor.getId();
				}
			}
		}
		// If the user has no factors defined then the code will return null
		return null;

	}*/
	/*
	 * public static void main(String... s) throws IOException, LoginException {
	 * 
	 * 
	 * OktaServiceImpl spi = new OktaServiceImpl();
	 * 
	 * //spi.oktaAPIKey="00nLREEiVMFxvzSzIqWU4E6hgqS7mfZqRZPHgRWbje";
	 * //spi.oktaSiteURL="https://dev-913899-admin.oktapreview.com";
	 * 
	 * spi.oktaAPIKey="005UiV20hO16qQBiDFG-yZrIMePATfRipkXofLicII";
	 * spi.oktaSiteURL="https://cadence.oktapreview.com";
	 * spi.oktaMFAFactorProvider="GOOGLE";
	 * spi.oktaMFAFactorType="token:software:totp";
	 * 
	 * //spi.OktaAuthAPIClient(); //spi.OktaFactorsAPIClient();
	 * 
	 * UserLogin userLogin = new UserLogin();
	 * userLogin.setUserName("shirish@cadence.com");
	 * userLogin.setPassword("MiJ%F1L"); userLogin.setMfaToken("009345");
	 * 
	 * spi.doLoginWithMFA(userLogin); //
	 * authApiClient.authenticate("shirish@cadence.com", "Admin12345", "");
	 * 
	 * }
	 */
}
