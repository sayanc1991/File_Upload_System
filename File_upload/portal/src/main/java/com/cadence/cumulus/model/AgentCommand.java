package com.cadence.cumulus.model;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class AgentCommand {
    private String requestId;

    private String userName;

    private String command;

    private String runId;

    private String instanceType;

    private int numberOfAgents;

    private String execCommand;

    private String scriptPath;

    private String liberatePath;

    private String toolName;

    private String clArgs;

    private Object data;

    private FileOperationRequest fileOperationRequest;

    public AgentCommand() {
    }

    public AgentCommand(String requestId, Object data) {
        super();
        this.requestId = requestId;
        this.data = data;
    }

    public AgentCommand(String requestId, String userName, String command, String execCommand, String scriptPath, String clArgs) {
        super();
        this.requestId = requestId;
        this.userName = userName;
        this.command = command;
        this.execCommand = execCommand;
        this.scriptPath = scriptPath;
        this.clArgs = clArgs;
    }
    
    
    
    public AgentCommand(Long requestId, String userName, String instanceType, int numberOfAgents, String command, String execCommand, String scriptPath, String liberatePath, String toolName, String clArgs) {
        super();
        this.requestId = String.valueOf(requestId);
        this.userName = userName;
        this.instanceType = instanceType;
        this.numberOfAgents = numberOfAgents;
        this.command = command;
        this.execCommand = execCommand;
        this.scriptPath = scriptPath;
        this.liberatePath = liberatePath;
        this.toolName = toolName;
        this.clArgs = clArgs;
    }

    public AgentCommand(String requestId, String userName, String instanceType, int numberOfAgents, String command, String execCommand, String scriptPath, String liberatePath, String toolName, String clArgs) {
        super();
        this.requestId = requestId;
        this.userName = userName;
        this.instanceType = instanceType;
        this.numberOfAgents = numberOfAgents;
        this.command = command;
        this.execCommand = execCommand;
        this.scriptPath = scriptPath;
        this.liberatePath = liberatePath;
        this.toolName = toolName;
        this.clArgs = clArgs;
    }

    public String getRequestId() {
        return requestId;
    }

    public void setRequestId(String requestId) {
        this.requestId = requestId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getCommand() {
        return command;
    }

    public void setCommand(String command) {
        this.command = command;
    }

    public String getRunId() {
        return runId;
    }

    public void setRunId(String runId) {
        this.runId = runId;
    }

    public String getInstanceType() {
        return instanceType;
    }

    public void setInstanceType(String instanceType) {
        this.instanceType = instanceType;
    }

    public int getNumberOfAgents() {
        return numberOfAgents;
    }

    public void setNumberOfAgents(int numberOfAgents) {
        this.numberOfAgents = numberOfAgents;
    }

    public String getExecCommand() {
        return execCommand;
    }

    public void setExecCommand(String execCommand) {
        this.execCommand = execCommand;
    }

    public String getScriptPath() {
        return scriptPath;
    }

    public void setScriptPath(String scriptPath) {
        this.scriptPath = scriptPath;
    }

    public String getLiberatePath() {
        return liberatePath;
    }

    public void setLiberatePath(String liberatePath) {
        this.liberatePath = liberatePath;
    }

    public String getToolName() {
        return toolName;
    }

    public void setToolName(String toolName) {
        this.toolName = toolName;
    }

    public String getClArgs() {
        return clArgs;
    }

    public void setClArgs(String clArgs) {
        this.clArgs = clArgs;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }

    public FileOperationRequest getFileOperationRequest() {
        return fileOperationRequest;
    }

    public void setFileOperationRequest(FileOperationRequest fileOperationRequest) {
        this.fileOperationRequest = fileOperationRequest;
    }

    @Override
    public String toString() {
        String retVal = super.toString();
        try {
            retVal = new ObjectMapper().writeValueAsString(this);
        } catch (JsonProcessingException e) {
        }

        return retVal;
    }
}
