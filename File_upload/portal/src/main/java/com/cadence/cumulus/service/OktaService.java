package com.cadence.cumulus.service;

import com.cadence.cumulus.common.LoginException;
import com.cadence.cumulus.model.UserLogin;

public interface OktaService {
	
	public String doLogin(final UserLogin userLogin) throws LoginException;

}
