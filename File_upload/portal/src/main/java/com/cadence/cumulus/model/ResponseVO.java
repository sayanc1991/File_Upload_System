package com.cadence.cumulus.model;

/**
 * @author Vikramaditya Singh
 *
 */
public class ResponseVO {

	private String status;
	
	private String value;

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}
	
}
