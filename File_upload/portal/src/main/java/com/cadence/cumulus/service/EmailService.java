package com.cadence.cumulus.service;

import java.util.Map;

public interface EmailService {
    public void sendEmail(String template, Map<String, String> params, String toAddress, String subject);
}
