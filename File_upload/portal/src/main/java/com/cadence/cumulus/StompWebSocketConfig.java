package com.cadence.cumulus;
/**
 * @author ychandra
 * This configuration is required for streaming logs and events data via web sockets. 
 * This configuration uses SockJS and STOMP protocols to simplify the client-server interactions. 
 */
import org.springframework.context.annotation.Configuration;
import org.springframework.messaging.simp.config.MessageBrokerRegistry;
import org.springframework.web.socket.config.annotation.AbstractWebSocketMessageBrokerConfigurer;
import org.springframework.web.socket.config.annotation.EnableWebSocketMessageBroker;
import org.springframework.web.socket.config.annotation.StompEndpointRegistry;

@Configuration
@EnableWebSocketMessageBroker
public class StompWebSocketConfig extends AbstractWebSocketMessageBrokerConfigurer {

	@Override
	public void configureMessageBroker(MessageBrokerRegistry config) {
		//Since log and events are not very critical, in memory broker is sufficient.
		config.enableSimpleBroker("/stream/"); //topics or queues
		config.setApplicationDestinationPrefixes("/apps"); // application end point for clients to send messages.
	}

	@Override
	public void registerStompEndpoints(StompEndpointRegistry registry) {
		registry.addEndpoint("/socket").withSockJS();
	}
	
}