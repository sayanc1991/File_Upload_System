/**
 * 
 */
package com.cadence.cumulus.repository.mysql;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.cadence.cumulus.model.Job;

/**
 * @author ramsudheer
 *
 */

public interface JobRepository extends JpaRepository<Job, Long> {

	
	public List<Job> findByCreatedBy(Long userId);
	
	public List<Job> findByJobNameAndCreatedBy(String jobName, Long userId);
	

}
