package com.cadence.cumulus.integration.opendj.vo;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Name {

    private String givenName;
	private String familyName;

    public Name() {
    }

    public Name(String givenName, String familyName) {
    	this.givenName = givenName;
    	this.familyName = familyName;
    }

    public String getGivenName() {
		return givenName;
	}

	public void setGivenName(String givenName) {
		this.givenName = givenName;
	}

	public String getFamilyName() {
		return familyName;
	}

	public void setFamilyName(String familyName) {
		this.familyName = familyName;
	}

	@Override
	public String toString() {
		return "Name [givenName=" + givenName + ", familyName=" + familyName + "]";
	}
	
}
