package com.cadence.cumulus.integration.unifiedregistration.service;

import com.cadence.cumulus.integration.IntegrationException;
import com.cadence.cumulus.integration.unifiedregistration.vo.ChangePasswordVO;
import com.cadence.cumulus.integration.unifiedregistration.vo.CreateUserVO;
import com.cadence.cumulus.integration.unifiedregistration.vo.SecurityQARequest;
import com.cadence.cumulus.integration.unifiedregistration.vo.URResponse;
import com.cadence.cumulus.integration.unifiedregistration.vo.UpdateUserVO;
import com.cadence.cumulus.model.User;

/**
 * Interface to interact with Unified Registration to pull user data as well as
 * to add user to Cumulus. UR will in turn integrate with Okta and add user to
 * the respective Cumulus group passed
 * 
 * @author Zabeersultan.Farook
 *
 */
public interface URIntegrationService {
	
	/**
	 * Pulls the user information from UR based on the Email ID passed
	 * 
	 * @param emailId
	 *            - Emaild of the user record to be fetched
	 * @return User object representing the user from Okta if a match is found
	 *         for the passed in email id , else will return null
	 * @throws IntegrationException Exception if any while invoking the UR service         
	 */
	public User getUserDetails(String emailId) throws IntegrationException;

	/**
	 * Adds the user (represented by the contactId) to the Cumulus Instance in
	 * Okta (via UR) represented by the passed in cumulus GroupId
	 * 
	 * @param contactId
	 *            contactId representing the user in UR
	 * @param cumulusGroupId
	 *            Unique ID in Okta representing a cumulus instance
	 * @throws IntegrationException Exception if any while invoking the UR service            
	 */
	public void addUserToCumulusGroup(String contactId, String cumulusGroupId) throws IntegrationException;

	/**
	 * Updates user password in UR based on the ChangePasswordRequest object passed
	 * 
	 * @param changePasswordVO
	 *            - changePasswordVO object with user new/old/confirm password details to update in UR.
	 * @param tryCount
	 *            tryCount to handle a retry scenario in case of expired tokens
	 *            indicated by 403
	 * @return URResponse object with response obtained from UR, else will return null
	 * @throws IntegrationException Exception if any while invoking the UR service         
	 */
	public URResponse updateUserPassword(ChangePasswordVO changePasswordVO) throws IntegrationException;
	
	/**
	 * Update user group in UR based on the CreateUserVO object passed
	 * 
	 * @param UpdateUserVO
	 *            - updateUserVO object with user details to remove group in UR.
	 * @param tryCount
	 *            tryCount to handle a retry scenario in case of expired tokens
	 *            indicated by 403
	 * @return URResponse object with response obtained from UR, else will return null
	 * @throws IntegrationException Exception if any while invoking the UR service         
	 */
	public URResponse updateUserGroup(UpdateUserVO updateUserVO) throws IntegrationException;

	/**
	 * Updates user security question and answer in UR based on the SecurityQARequest object passed
	 * 
	 * @param securityQARequest
	 *            - SecurityQARequest object with user security question/answer details to update in UR.
	 * @param tryCount
	 *            tryCount to handle a retry scenario in case of expired tokens
	 *            indicated by 403
	 * @return URResponse object with response obtained from UR, else will return null
	 * @throws IntegrationException Exception if any while invoking the UR service         
	 */
	public URResponse resetSecurityQA(SecurityQARequest securityQARequest) throws IntegrationException;

	/**
	 * Create new user profile in UR based on the CreateUserVO object passed
	 * 
	 * @param CreateUserVO
	 *            - createUserVO object with new user details to create in UR.
	 * @param tryCount
	 *            tryCount to handle a retry scenario in case of expired tokens
	 *            indicated by 403
	 * @return URResponse object with response obtained from UR, else will return null
	 * @throws IntegrationException Exception if any while invoking the UR service         
	 */
	public URResponse createUser(CreateUserVO createUserVO) throws IntegrationException;

}
