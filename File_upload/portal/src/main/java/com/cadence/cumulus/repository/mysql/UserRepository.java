package com.cadence.cumulus.repository.mysql;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import com.cadence.cumulus.model.User;

@Repository
public interface UserRepository extends JpaRepository<User, Long>, JpaSpecificationExecutor<User> {

	public User findByEmail(String email);
	
	// CMLS-658
	public User findByUsername(String userName);
	
}
