package com.cadence.cumulus.core;

import org.springframework.stereotype.Service;

import com.cadence.cumulus.dto.TokenStatus;

@Service
public interface TokenService {
	
	public TokenStatus generateToken(String loginName);
	
	public TokenStatus isTokenExpired(String loginName);
	
	public TokenStatus verifyToken(String token, String permission);

}
