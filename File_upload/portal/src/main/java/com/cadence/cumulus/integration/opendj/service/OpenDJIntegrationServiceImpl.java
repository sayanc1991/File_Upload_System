package com.cadence.cumulus.integration.opendj.service;

import java.io.File;
import java.net.InetSocketAddress;
import java.net.Proxy;

import javax.annotation.PostConstruct;
import javax.net.ssl.SSLContext;
import javax.xml.bind.DatatypeConverter;

import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.http.client.SimpleClientHttpRequestFactory;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.cadence.cumulus.ProxyConfig;
import com.cadence.cumulus.integration.IntegrationException;
import com.cadence.cumulus.integration.opendj.vo.UpdatePassword;
import com.cadence.cumulus.integration.opendj.vo.User;
import com.cadence.cumulus.integration.opendj.vo.UserMountInformation;

@Service
public class OpenDJIntegrationServiceImpl implements OpenDJIntegrationService {

	private static final Logger LOG = LoggerFactory.getLogger(OpenDJIntegrationServiceImpl.class);

	@Autowired
	private ProxyConfig proxyConfig;

	private RestTemplate restTemplate;

	@Value("${opendj.users.baseUrl}")
	private String usersBaseURL;

	@Value("${opendj.admin.user}")
	private String adminUser;

	@Value("${opendj.admin.password}")
	private String adminPasswd;

	@Value("${opendj.server.cert.file}")
	private String certificateFile;

	@Value("${opendj.server.cert.password}")
	private String certificatePassword;

    @Value("${opendj.enabled}")
	private boolean isOpenDJEnabled;

    @Value("${opendj.users.auto.mountUrl}")
    private String USERS_AUTO_MOUNT_URL;
    
	@PostConstruct
	public void setup() {
		if (isOpenDJEnabled) {
		    SSLContext sslContext = null;
			try {
				sslContext = org.apache.http.ssl.SSLContexts.custom()
								.loadTrustMaterial(new File(certificateFile), certificatePassword.toCharArray())
				                .build();
			} catch (Exception e) {
				LOG.error("Exception while connecting to OpenDJ server: ", e);
			}
	
		    SSLConnectionSocketFactory csf = new SSLConnectionSocketFactory(sslContext);
	
		    CloseableHttpClient httpClient = HttpClients.custom()
		                    .setSSLSocketFactory(csf)
		                    .build();
	
		    HttpComponentsClientHttpRequestFactory requestFactory =
		                    new HttpComponentsClientHttpRequestFactory();
	
		    requestFactory.setHttpClient(httpClient);
		    restTemplate = new RestTemplate(requestFactory);
		} else {
			restTemplate = new RestTemplate();
			if (proxyConfig.isEnabled()) {
				SimpleClientHttpRequestFactory factory = new SimpleClientHttpRequestFactory();
				InetSocketAddress address = new InetSocketAddress(proxyConfig.getHost(), proxyConfig.getPort());
				Proxy proxy = new Proxy(Proxy.Type.HTTP, address);
				factory.setProxy(proxy);
				restTemplate.setRequestFactory(factory);
			}
		}
	}

	@Override
	public void addUser(User user) throws IntegrationException {
		LOG.debug("Adding Cumulus user to OpenDJ DS : ", user.getName());
		
		ResponseEntity<User> result = null;
		try {
			RestTemplate restTemplate = getRestTemplate();
			HttpHeaders headers = getHeader();
			HttpEntity<User> requestCreateEntity = new HttpEntity<>(user, headers);
			
			result = restTemplate.exchange(usersBaseURL+"?_action=create", HttpMethod.POST, requestCreateEntity, User.class);
		} catch (Exception e) {
			LOG.error("Exception while creating user in OpenDJ", e);
			throw new IntegrationException("Exception while creating user details in OpenDJ: "+e.getMessage());
		}
		
		// successfully created
		if (result != null && HttpStatus.CREATED.equals(result.getStatusCode())) {
			LOG.debug("response from OpenDJ for :" + usersBaseURL + "?_action=create :" + result.getBody());
		} else {
			LOG.error("Error response while calling OpenDJ DS API with end point " + usersBaseURL + "?_action=create " +(result != null
					? " Status :" + result.getStatusCodeValue() + " : response body : " + result.getBody() : ""));
			throw new IntegrationException("Error response while calling OpenDJ DS API with end point " + usersBaseURL + "?_action=create"
					+ (result != null
							? " Status :" + result.getStatusCodeValue() + " : response body : " + result.getBody()
							: ""));
		}

		LOG.debug("Completed adding user to OpenDJ Directory Service.");
	}

	@Override
	public void updateUser(UpdatePassword[] updatePassword, User user) throws IntegrationException {
		LOG.debug("Updating cumulus user to OpenDJ DS : " + user.getUserName());
		
		ResponseEntity<User> result = null;
		try {
			RestTemplate restTemplate = getRestTemplate();
			HttpHeaders headers = getHeader();
			HttpEntity<UpdatePassword[]> requestEntity = new HttpEntity<>(updatePassword, headers);
			// TODO : Below changes required if user attributes required to update in opendj.
			/*if (updatePassword != null) {
				requestEntity = new HttpEntity<>(updatePassword, headers);
			} else {
				requestEntity = new HttpEntity<>(headers);
			}*/
			
			result = restTemplate.exchange(usersBaseURL+"/"+user.getUserName(), HttpMethod.PATCH, requestEntity, User.class);
		} catch (Exception e) {
			LOG.error("Exception while updating user password in OpenDJ", e);
			throw new IntegrationException("Exception while updating user password in OpenDJ", e);
		}
		
		// successfully created
		LOG.info("Status code : " + result.getStatusCode());
		if (result != null && HttpStatus.OK.equals(result.getStatusCode())) {
			LOG.debug("response from OpenDJ for :" + usersBaseURL + "/" + user.getUserName() + result.getBody());
		} else {
			LOG.error("Error response while calling OpenDJ DS API with end point " + usersBaseURL + "/" + user.getUserName() +(result != null
					? " Status :" + result.getStatusCodeValue() + " : response body : " + result.getBody() : ""));
			throw new IntegrationException("Error response while calling OpenDJ DS API with end point " + usersBaseURL + "/" + user.getUserName()
					+ (result != null
							? " Status :" + result.getStatusCodeValue() + " : response body : " + result.getBody()
							: ""));
		}

		LOG.debug("Completed updating user password in OpenDJ Directory Service.");
	}

	@Override
	public void autoMount(UserMountInformation mountInformation) throws IntegrationException {
		LOG.debug("Mounting cumulus user in OpenDJ DS : ", mountInformation.getCn());

		ResponseEntity<UserMountInformation> result = null;
		try {
			RestTemplate restTemplate = getRestTemplate();
			HttpHeaders headers = getHeader();
			HttpEntity<UserMountInformation> requestCreateEntity = new HttpEntity<>(mountInformation, headers);

			result = restTemplate.exchange(USERS_AUTO_MOUNT_URL+"?_action=create", HttpMethod.POST, requestCreateEntity, UserMountInformation.class);
		} catch (Exception e) {
			LOG.error("Exception while updating user mount info in OpenDJ", e);
			throw new IntegrationException("Exception while updating user mount info in OpenDJ", e);
		}

		// successfully created
		if (result != null && HttpStatus.CREATED.equals(result.getStatusCode())) {
			LOG.debug("response from OpenDJ for :" + USERS_AUTO_MOUNT_URL + "?_action=create :" + result.getBody());
		} else {
			LOG.error("Error response while calling OpenDJ DS API with end point " + USERS_AUTO_MOUNT_URL + "?_action=create " +(result != null
					? " Status :" + result.getStatusCodeValue() + " : response body : " + result.getBody() : ""));
			throw new IntegrationException("Error response while calling OpenDJ DS API with end point " + USERS_AUTO_MOUNT_URL + "?_action=create"
					+ (result != null
							? " Status :" + result.getStatusCodeValue() + " : response body : " + result.getBody()
							: ""));
		}

		LOG.debug("Completed updating user mount information in OpenDJ Directory Service.");
	}

	// "_id": "test44"
	@Override
	public User findUser(String username) throws IntegrationException {
		LOG.debug("Finding Cumulus user to OpenDJ DS : " + username);
		
		ResponseEntity<User> result = null;
		try {
			RestTemplate restTemplate = getRestTemplate();
			HttpHeaders headers = getHeader();
			HttpEntity<User> requestUserEntity = new HttpEntity<>(headers);
			
			result = restTemplate.exchange(usersBaseURL+"/"+username, HttpMethod.GET, requestUserEntity, User.class);
		} catch (Exception e) {
			LOG.error("Exception while finding user in OpenDJ", e); // Exception while connecting to OpenDJ Directory Service.
			throw new IntegrationException("Exception while finding user details in OpenDJ. reason: "+e.getMessage());
		}
		
		if (result.getStatusCodeValue() != HttpStatus.OK.value()) {
			LOG.info("User not found in OpenDJ DS : "+username);
			throw new IntegrationException("User not found in OpenDJ : " + username + (result != null
					? " Status :" + result.getStatusCodeValue() + " : response body : " + result.getBody() : ""));
		}

		LOG.debug("Completed finding user in OpenDJ Directory Service.");
		return result.getBody();
	}
	
	private HttpHeaders getHeader() {
		String plainCreds = adminUser+":"+adminPasswd;
		byte[] plainCredsBytes = plainCreds.getBytes();
		String base64Creds = DatatypeConverter.printBase64Binary(plainCredsBytes);

		HttpHeaders headers = new HttpHeaders();
		headers.add("Authorization", "Basic " + base64Creds);
		headers.set("Accept", MediaType.APPLICATION_JSON_VALUE);
		headers.set("Content-type", MediaType.APPLICATION_JSON_VALUE);
		
		return headers;
	}

	private RestTemplate getRestTemplate() {
		return restTemplate;
	}
}