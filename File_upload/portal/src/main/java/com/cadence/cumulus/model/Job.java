/**
 * 
 */
package com.cadence.cumulus.model;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonInclude;

/**
 * @author ramsudheer
 *
 */
@Entity
@Table(name = "Job")
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class Job extends BaseEntity {

	private Long id;

	private String jobName;

	// Instance types are constants - based on discussion - For now going with
	// ENUM
	// Should this be associated with a Job Run?
	private String instanceType;

	// No history/version of job edits/updates will be maintained - So no place
	// hodler

	// Instance types are constants - based on discussion - For now going with
	// ENUM
	// Should this be associated with a Job Run?
	// public AWSInstanceTypes instanceType;

	// Assumption this will not change for each run
	private String clArguments = "";

	private String scriptPath;

	private int numberOfNodes;

	private Set<JobRun> jobRuns;

	private String toolName;
	private String toolPath;
	private String toolVersion;
	private Date lastRunOn;

	// Chandrakant: Workbench - read and process 1st line in shell script to
	// determine shell to use

	private String shebang;
	
	@Transient
	private String lastRunDate;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "Id")
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the jobName
	 */

	public String getJobName() {
		return jobName;
	}

	/**
	 * @param jobName
	 *            the jobName to set
	 */
	public void setJobName(String jobName) {
		this.jobName = jobName;
	}

	/**
	 * @return the instanceType
	 */

	public String getInstanceType() {
		return instanceType;
	}

	/**
	 * @param instanceType
	 *            the instanceType to set
	 */
	public void setInstanceType(String instanceType) {
		this.instanceType = instanceType;
	}

	/**
	 * @return the clArguments
	 */

	public String getClArguments() {
		return clArguments;
	}

	/**
	 * @param clArguments
	 *            the clArguments to set
	 */
	public void setClArguments(String clArguments) {
		this.clArguments = clArguments;
	}

	/**
	 * @return the scriptPath
	 */

	public String getScriptPath() {
		return scriptPath;
	}

	/**
	 * @param scriptPath
	 *            the scriptPath to set
	 */
	public void setScriptPath(String scriptPath) {
		this.scriptPath = scriptPath;
	}

	/*
	 * public String getUserName() { return userName; }
	 * 
	 * public void setUserName(String userName) { this.userName = userName;
	 * //this.setCreatedBy(userName); }
	 */

	/**
	 * @return the jobRuns
	 */
	@OneToMany(mappedBy = "job", cascade = CascadeType.REMOVE, fetch=FetchType.EAGER)
	public Set<JobRun> getJobRuns() {
		return jobRuns;
	}

	/**
	 * @param jobRuns
	 *            the jobRuns to set
	 */
	public void setJobRuns(Set<JobRun> jobRuns) {
		this.jobRuns = jobRuns;
	}

	public int getNumberOfNodes() {
		return numberOfNodes;
	}

	public void setNumberOfNodes(int numNodes) {
		this.numberOfNodes = numNodes;
	}

	public String getToolName() {
		return toolName;
	}

	public void setToolName(String toolName) {
		this.toolName = toolName;
	}

	public String getToolPath() {
		return toolPath;
	}

	public void setToolPath(String toolPath) {
		this.toolPath = toolPath;
	}

	public String getToolVersion() {
		return toolVersion;
	}

	public void setToolVersion(String toolVersion) {
		this.toolVersion = toolVersion;
	}

	// Chandrakant: Workbench - read and process 1st line in shell script to
	// determine shell to use

	public String getShebang() {
		return this.shebang;
	}

	public void setShebang(String shebang) {
		this.shebang = shebang;
	}

	public void setCreatedDate(Date d) {
		this.setCreatedOn(d);

	}

	public void setLastUpdatedDate(Date d) {
		this.setLastModifiedOn(d);

	}

	public Date getLastRunOn() {
		return lastRunOn;
	}

	public void setLastRunOn(Date lastRunOn) {
		this.lastRunOn = lastRunOn;
	}

	public void setLastRunDate(String lastRunDate) {
		this.lastRunDate = lastRunDate;
	}

	@Transient
	public String getLastRunDate() {
		String lastRunDate = "";
		SimpleDateFormat sdf = new SimpleDateFormat("MM-dd-yyyy HH:mm:ss");
		if(null != lastRunOn){
			lastRunDate = sdf.format(lastRunOn); 
		}
		return lastRunDate;
	}

	

}
