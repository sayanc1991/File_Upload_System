package com.cadence.cumulus.model.liberate.events;

public enum LiberateStatus {
    started("started"),
    failed("failed"),
    completed("completed"),
    pending("pending"),
	cancelled("cancelled");

    private String value;

    private LiberateStatus(String value) {
        this.value = value;
    }

    public String value() {
        return this.value;
    }
}
