package com.cadence.cumulus.service.impl;

import java.io.IOException;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.apache.commons.io.IOUtils;
import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.cadence.cumulus.common.Constants;
import com.cadence.cumulus.model.MachineInfo;
import com.cadence.cumulus.service.InfraQueueService;

@Component
public class InfraQueueServiceImpl implements InfraQueueService {

	@Value("${queueType.normal}")
	private String queueNormal;

	@Value("${queueType.large}")
	private String queuelarge;

	@Value("${queueType.xlarge}")
	private String queueXlarge;

	@Value("${queueType.xxlarge}")
	private String queueXxlarge;

	private static final Logger log = LoggerFactory.getLogger(InfraQueueServiceImpl.class);
	private static Map<String, MachineInfo> machinInfoMap = new LinkedHashMap<>();

	@PostConstruct
	private void getDetails() {
		MachineInfo machineInfo = null;

		try {
			String result = "";
			ClassLoader classLoader = getClass().getClassLoader();
			result = IOUtils.toString(classLoader.getResourceAsStream(Constants.INFRA_QUEUE_FILE));
			String jsonString = result.toString();
			JSONArray jsonarray = new JSONArray(jsonString);
			for (int i = 0; i < jsonarray.length(); i++) {
				JSONObject jsonobject = jsonarray.getJSONObject(i);
				machineInfo = new MachineInfo();
				String name = (String) jsonobject.getString(Constants.INFRA_QUEUE_NAME);
				Integer coreCount = (Integer) jsonobject.get(Constants.INFRA_QUEUE_CORE_COUNT);
				Double memory = null;
				if (jsonobject.get(Constants.INFRA_QUEUE_MEMORY) instanceof Integer) {
					memory = Double.valueOf(jsonobject.get(Constants.INFRA_QUEUE_MEMORY).toString());
				} else if (jsonobject.get(Constants.INFRA_QUEUE_MEMORY) instanceof Double) {
					memory = (Double) jsonobject.get(Constants.INFRA_QUEUE_MEMORY);
				}
				machineInfo.setName(name);
				machineInfo.setCoreCount(coreCount);
				machineInfo.setMemory(memory);
				machinInfoMap.put(machineInfo.getName(), machineInfo);
			}
			
		} catch (IOException e) {
			log.error("Exception occurred in while fetching Infra queue details " + e);
		}
	}

	@Override
	public Map<String, MachineInfo> fetchMachineInfo() {
		
		log.debug("Fetching Machine Info");
		Map<String, MachineInfo> machineQueueInfo = new HashMap<>();
		MachineInfo machineInfo = null;
		machineInfo = machinInfoMap.get(queueNormal);
		log.debug(machineInfo.getName());
		machineQueueInfo.put(Constants.INFRA_QUEUE_NORMAL, machineInfo);

		machineInfo = machinInfoMap.get(queuelarge);
		machineQueueInfo.put(Constants.INFRA_QUEUE_LARGE, machineInfo);

		machineInfo = machinInfoMap.get(queueXlarge);
		machineQueueInfo.put(Constants.INFRA_QUEUE_XLARGE, machineInfo);

		machineInfo = machinInfoMap.get(queueXxlarge);
		machineQueueInfo.put(Constants.INFRA_QUEUE_XXLARGE, machineInfo);
		
		log.debug("Fetched Machine Info");
		return machineQueueInfo;
	}

}
