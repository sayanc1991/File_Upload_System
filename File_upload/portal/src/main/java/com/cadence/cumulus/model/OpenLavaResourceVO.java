package com.cadence.cumulus.model;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @author Vikramaditya Singh
 *
 */
public class OpenLavaResourceVO {

	private String id;
	private String jobId;
	private String executionTime;
	private String userName;
	private String queue;
	private String coresUsed;
	private String group;
	private String countOfHosts;
	private String startTime;
	private String finishTime;
	
	public String getJobId() {
		return jobId;
	}
	public void setJobId(String jobId) {
		this.jobId = jobId;
	}
	public String getExecutionTime() {
		return executionTime;
	}
	public void setExecutionTime(String executionTime) {
		this.executionTime = executionTime;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getQueue() {
		return queue;
	}
	public void setQueue(String queue) {
		this.queue = queue;
	}
	public String getCoresUsed() {
		return coresUsed;
	}
	public void setCoresUsed(String coresUsed) {
		this.coresUsed = coresUsed;
	}

	public String getGroup() {
		return group;
	}
	public void setGroup(String group) {
		this.group = group;
	}
	public String getCountOfHosts() {
		return countOfHosts;
	}
	public void setCountOfHosts(String countOfHosts) {
		this.countOfHosts = countOfHosts;
	}
	public String getStartTime() {
		return startTime;
	}
	public void setStartTime(String startTime) {
		SimpleDateFormat sdf = new SimpleDateFormat("MMM-dd HH:mm");
		if(null != startTime && !"".equals(startTime)){
			this.startTime = sdf.format(new Date(Long.parseLong(startTime)));
		}else{
			this.startTime = "";
		}
	}
	public String getFinishTime() {
		return finishTime;
	}
	public void setFinishTime(String finishTime) {
		SimpleDateFormat sdf = new SimpleDateFormat("MMM-dd HH:mm");
		if(null != finishTime && !"".equals(finishTime)){
			this.finishTime = sdf.format(new Date(Long.parseLong(finishTime)));
		}else{
			this.finishTime = "";
		}
	}

	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	
}
