package com.cadence.cumulus.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;


import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_EMPTY)
@Entity
@Table(name="OpenLavaUsage")
public class OpenLavaResourceEntity implements Serializable{

	private static final long serialVersionUID = -2354287664577006617L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="Id")
	private Long id;
	
	@Column(name="JobId")
	private String jobId;
	
	@Column(name="ExecutionTime")
	private String executionTime;
	
	@Column(name="UserName")
	private String userName;
	
	@Column(name="Queue")
	private String queue;
	
	@Column(name="CoresUsed")
	private String coresUsed;
	
	@Column(name="GroupName")
	private String group;
	
	@Column(name="CountOfHosts")
	private String countOfHosts;
	
	@Column(name="FinishTime")
	private String finishTime;
	
	@Column(name="StartTime")
	private String startTime;
	
	public OpenLavaResourceEntity(){
		
	}
	
	public OpenLavaResourceEntity(String jobId, String executionTime, String userName, String queue, String coresUsed, String group, String countOfHosts, String finishTime, String startTime ){
		this.jobId = jobId != null ? jobId : "";
		this.executionTime = executionTime != null ? executionTime : "";
		this.userName = userName != null ? userName : "";
		this.queue = queue != null ? queue : "";
		this.coresUsed = coresUsed != null ? coresUsed : "";
		this.group = group != null ? group : "";
		this.countOfHosts = countOfHosts != null ? countOfHosts : "";
		this.finishTime = finishTime != null ? finishTime : null;
		this.startTime = startTime != null ? startTime : null;
		
	}
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	
	public String getStartTime() {
		return startTime;
	}
	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}
	
	public String getJobId() {
		return jobId;
	}
	public void setJobId(String jobId) {
		this.jobId = jobId;
	}
	
	public String getExecutionTime() {
		return executionTime;
	}
	public void setExecutionTime(String executionTime) {
		this.executionTime = executionTime;
	}
	
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	
	public String getQueue() {
		return queue;
	}
	public void setQueue(String queue) {
		this.queue = queue;
	}
	
	public String getCoresUsed() {
		return coresUsed;
	}
	public void setCoresUsed(String coresUsed) {
		this.coresUsed = coresUsed;
	}
	
	public String getGroup() {
		return group;
	}
	public void setGroup(String group) {
		this.group = group;
	}
	
	public String getCountOfHosts() {
		return countOfHosts;
	}
	public void setCountOfHosts(String countOfHosts) {
		this.countOfHosts = countOfHosts;
	}
	
	public String getFinishTime() {
		return finishTime;
	}
	public void setFinishTime(String finishTime) {
		this.finishTime = finishTime;
	}

}
