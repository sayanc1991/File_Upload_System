/**
 * 
 */
package com.cadence.cumulus.model;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonInclude;

/**
 * @author deepak
 *
 */
@Entity
@Table(name = "Countries")
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class Countries {

	private String countryName;
	private String countryCode;
	private Set<States> states;

	@OneToMany(mappedBy = "country", cascade = CascadeType.REMOVE, fetch=FetchType.EAGER)
	public Set<States> getStates() {
		return states;
	}

	public void setStates(Set<States> states) {
		this.states = states;
	}

	public String getCountryName() {
		return countryName;
	}

	public void setCountryName(String countryName) {
		this.countryName = countryName;
	}

	@Id
	@Column(name = "countryCode")
	public String getCountryCode() {
		return countryCode;
	}

	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}
}