package com.cadence.cumulus.service;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.TimeUnit;

import org.reactivestreams.Publisher;
import org.reactivestreams.Subscriber;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.task.TaskExecutor;

import com.cadence.cumulus.model.JobRun;

public class StatusPublisherService implements Publisher<String> {
    private static final Logger log = LoggerFactory.getLogger(StatusPublisherService.class);

    private TaskExecutor executor;

    private String type;

    private String stopString;

    private BlockingQueue<JobRun> statusQueue;
    
    private boolean alive = true;

	public StatusPublisherService(TaskExecutor executor, String type, BlockingQueue<JobRun> statusQueue) {
        this(executor, type, null, statusQueue);
    }
 
    public StatusPublisherService(TaskExecutor executor, String type, String stopString, BlockingQueue<JobRun> statusQueue) {
        this.executor = executor;
        this.type = type;
        this.stopString = stopString;
        this.statusQueue = statusQueue;
    }

    @Override
    public void subscribe(Subscriber<? super String> subscriber) {
    	log.debug("Inside StatusPublisherService");
        executor.execute(() -> {
            try {
                while (true) {
                    if (statusQueue == null) {
                        log.debug("No statusQueue available, breaking out");
                        subscriber.onComplete();
                        break;
                    }

                    JobRun jobRun = statusQueue.poll(50000, TimeUnit.MILLISECONDS);

                    if (!alive) {
                        log.debug("{} Not alive anymore. Breaking", type);
                        subscriber.onComplete();
                        break;
                    }

                    if (jobRun == null) {
                        log.debug("No {} for 50 seconds, sending ping", type);
                        subscriber.onNext("ping");
                    } else {
                        log.debug("Got response from {} statusQueue: {}", type, jobRun.toString());
                        try {
                            subscriber.onNext(jobRun.toString());
                        } catch (Exception e) {
                            log.debug("StatusPublisherService caught exception");
                            subscriber.onError(e);
                        }

                        if (stopString != null) {
                            if (stopString.contains(jobRun.getStatus())) {
                                log.debug("{} Completed, breaking out", type);
                                subscriber.onComplete();
                                break;
                            }
                        }
                    }
                }
                log.debug("Exiting StatusPublisherService");    
            } catch (InterruptedException e) {
                log.error(type + " Interrupted: ", e);
            }
        });
    }

    public boolean isAlive() {
    	log.debug("StatusPublisherService : Is alive " + alive);    
        return alive;
    }

    public void setAlive(boolean alive) {
        this.alive = alive;
        this.statusQueue = null;
    }
}
