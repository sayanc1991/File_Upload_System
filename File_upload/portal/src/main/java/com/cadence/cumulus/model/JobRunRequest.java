/**
 * 
 */
package com.cadence.cumulus.model;

public class JobRunRequest {

	private JobRun jobRun;
	private UserLogin userlogin;

	public JobRun getJobRun() {
		return jobRun;
	}

	public void setJobRun(JobRun jobRun) {
		this.jobRun = jobRun;
	}

	public UserLogin getUserlogin() {
		return userlogin;
	}

	public void setUserlogin(UserLogin userlogin) {
		this.userlogin = userlogin;
	}

	
}
