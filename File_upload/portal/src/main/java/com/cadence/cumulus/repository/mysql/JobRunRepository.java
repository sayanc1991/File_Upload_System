package com.cadence.cumulus.repository.mysql;


import org.springframework.data.jpa.repository.JpaRepository;

import com.cadence.cumulus.model.JobRun;


public interface JobRunRepository  extends JpaRepository<JobRun, Long> {

}
