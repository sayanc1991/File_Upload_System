package com.cadence.cumulus.common;

/**
 * @author sandeep
 * 
 */
public class ServiceConstants {

	// Service Responses
	public static final int SUCCESS = 0;
	public static final int FAILURE = -1;

	public static final int RESOURCE_NOT_FOUND = -1000;
	public static final int IO_EXCEPTION = -1001;
	public static final int INVALID_USER_ACCOUNT = -1002;

	// Common Message
	public static final String MSG_SUCCESS = "success";
	public static final String MSG_FAILURE = "failure";
	public static final String MSG_NOT_FOUND = "resource.not.found";
	public static final String MSG_INVALID_RESOURCE = "cannot.update.for.root";
	public static final String MSG_TARGET_EXISTS = "target.already.exists";

	// Service Operations
	public static final String OPR_GET_USER = "get.user.principal";
	public static final String OPR_GET_GROUP = "get.group.principal";
	public static final String OPR_GET_OWNERSHIP_INFO = "get.ownership.info";
	public static final String OPR_SET_OWNERSHIP_INFO = "set.ownership.info";
	public static final String OPR_GET_DIRECTORY = "get.directory.info";
	public static final String OPR_COPY_RESOURCE = "copy.resource";
	public static final String OPR_DELETE_RESOURCE = "delete.resource";
	public static final String OPR_MOVE_RESOURCE = "move.resource";

	public static final String OPR_SET_PERMISSION_INFO = "set.permission.info";

	public static final String ADMIN_ROLE = "Administrator";

	public static final String X_AUTH_TOKEN = "X-AUTH-TOKEN";
	public static final String ACCOUNT = "account";

	// CMLS-658
	public static final String SESSION_ID = "JSESSIONID";
	public static final String USER_LOGIN = "userLogin";
	public static final String CUMULUS_USER = "cumulusUser";
	public static final String USERNAME = "userName";
	public static final String TOKEN_PRESENT = "tokenPresent";
	public static final String USER_EMAIL = "userEmailId";
	
	public static final String OPR_CHECK_LICENSE_SERVER = "check.license.server";	

}
