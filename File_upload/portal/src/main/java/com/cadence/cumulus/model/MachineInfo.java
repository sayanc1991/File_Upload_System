package com.cadence.cumulus.model;

public class MachineInfo {
	
	private String name; 
	private Integer coreCount; 
	private Double memory;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Integer getCoreCount() {
		return coreCount;
	}
	public void setCoreCount(Integer coreCount) {
		this.coreCount = coreCount;
	}
	public Double getMemory() {
		return memory;
	}
	public void setMemory(Double memory) {
		this.memory = memory;
	}
	

}
