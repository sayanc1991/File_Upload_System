package com.cadence.cumulus.service;

import java.io.File;

import com.cadence.cumulus.dto.BulkActionPayload;
import com.cadence.cumulus.dto.SingleActionPayload;

/**
 * 
 * @author anirban
 *
 */

public interface OSFileManager  {
	
	public File createMockFileResource(BulkActionPayload metadata);
	
	public Boolean isResourceFolder(SingleActionPayload metadata) throws Exception;
	
	public Boolean isResourceWritable(SingleActionPayload metadata) throws Exception ;
	
	public  String doesUploadedResourceExists(SingleActionPayload metadata) throws Exception ;
	
	public  String lastModifiedAt(SingleActionPayload metadata) throws Exception;
	
	public  Boolean doesResourceExist(SingleActionPayload metadata) throws Exception;
	
	public  void createDirectoryResourceByUser(SingleActionPayload metadata) throws Exception;
	
	public  void appendToFileResource(BulkActionPayload metadata) throws Exception;
	
	public  Integer moveResourceByUser(BulkActionPayload metadata) throws Exception;
	
	public  void removeResourceByUser(SingleActionPayload metadata) throws Exception;
	
	public byte[] readResourceContents(SingleActionPayload metadata) throws Exception;
	
}
