package com.cadence.cumulus.core;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.DefaultRedirectStrategy;
import org.springframework.security.web.RedirectStrategy;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.util.Base64Utils;

import com.cadence.cumulus.model.CumulusUser;

public class CumulusSuccessLoginHandler implements AuthenticationSuccessHandler {
	private static final Logger log = LoggerFactory.getLogger(CumulusSuccessLoginHandler.class);

	private String redirectUrl;

	private RedirectStrategy redirectStrategy = new DefaultRedirectStrategy();

	@Override
	public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response, Authentication authentication) throws IOException, ServletException {
		log.debug("Login success");
		CumulusUser user = (CumulusUser) ((Authentication) authentication).getPrincipal();
		
		request.getSession().setAttribute("username", user.getUsername());

		log.debug("Setting cookie cumulus-auth");
		Cookie cumulusAuth = new Cookie("cumulus-auth", new String(Base64Utils.encode(user.toString().getBytes())));
		cumulusAuth.setHttpOnly(false);
		cumulusAuth.setMaxAge(2147483647);
		cumulusAuth.setVersion(1);
		cumulusAuth.setPath("/");

		response.addCookie(cumulusAuth);

		log.debug("Sending redirect to {}", redirectUrl);
		redirectStrategy.sendRedirect(request, response, redirectUrl);
	}

	public String getRedirectUrl() {
		return redirectUrl;
	}

	public void setRedirectUrl(String redirectUrl) {
		this.redirectUrl = redirectUrl;
	}
}
