package com.cadence.cumulus.integration.opendj.vo;

import java.util.Arrays;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class User {

    private ContactInformation contactInformation;
    private String displayName;
    private String[] schemas;
    private Name name;
    private String _id;
    private String userPassword;
	private long uidNumber;
    private String loginShell;
    private String homeDirectory;
    private long gidNumber;
    private String userName;
    
	public User() {
    }

    public ContactInformation getContactInformation() {
		return contactInformation;
	}

	public void setContactInformation(ContactInformation contactInformation) {
		this.contactInformation = contactInformation;
	}

	public String getDisplayName() {
		return displayName;
	}

	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}

	public String[] getSchemas() {
		return schemas;
	}

	public void setSchemas(String[] schemas) {
		this.schemas = schemas;
	}

	public Name getName() {
		return name;
	}

	public void setName(Name name) {
		this.name = name;
	}

	public String get_id() {
		return _id;
	}

	public void set_id(String _id) {
		this._id = _id;
	}

    public String getUserPassword() {
		return userPassword;
	}

	public void setUserPassword(String userPassword) {
		this.userPassword = userPassword;
	}

	public long getUidNumber() {
		return uidNumber;
	}

	public void setUidNumber(long uidNumber) {
		this.uidNumber = uidNumber;
	}

	public String getLoginShell() {
		return loginShell;
	}

	public void setLoginShell(String loginShell) {
		this.loginShell = loginShell;
	}

	public String getHomeDirectory() {
		return homeDirectory;
	}

	public void setHomeDirectory(String homeDirectory) {
		this.homeDirectory = homeDirectory;
	}

	public long getGidNumber() {
		return gidNumber;
	}

	public void setGidNumber(long gidNumber) {
		this.gidNumber = gidNumber;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	@Override
	public String toString() {
		return "User [contactInformation=" + contactInformation + ", displayName=" + displayName + ", schemas="
				+ Arrays.toString(schemas) + ", name=" + name + ", _id=" + _id + ", userPassword=" + userPassword
				+ ", uidNumber=" + uidNumber + ", loginShell=" + loginShell + ", homeDirectory=" + homeDirectory
				+ ", gidNumber=" + gidNumber + "]";
	}
}
