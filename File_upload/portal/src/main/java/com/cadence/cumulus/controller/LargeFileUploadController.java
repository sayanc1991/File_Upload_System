package com.cadence.cumulus.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.internal.StaticCredentialsProvider;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.ObjectListing;
import com.amazonaws.services.s3.model.S3ObjectSummary;
import com.cadence.cumulus.model.LargeFileCopyInput;

@RestController
@RequestMapping(value = "/fcc")
public class LargeFileUploadController {

	@Value("${amazon.s3.accesskey}")
	private String awsAccessKey;

	@Value("${amazon.s3.secret}")
	private String awsSecret;

	@Value("${amazon.s3.bucket}")
	private String s3Bucket;

	@Value("${amazon.s3.region}")
	private String s3region;

	@RequestMapping(value = "/filecopy", method = RequestMethod.POST)
	public String startCopyProcess(@RequestBody LargeFileCopyInput request, HttpServletRequest httpRequest) {
		System.out.println("startCopyProcess-"+httpRequest.getParameterMap());
		String sourceFile = request.getSourceFile();
		String targetFile = request.getTargetFile();
		System.out.println("sourceFile" + request.getSourceFile());
		System.out.println("targetFile" + request.getTargetFile());
		ProcessBuilder builder = new ProcessBuilder();

		List<String> commands = new ArrayList<>();
		commands.add("java");
		commands.add("-cp");
		commands.add("/opt/cumulus/cumulus-filecopy");
		commands.add("RunAWScli");
		commands.add("s3://" + s3Bucket + "/"+sourceFile);
		commands.add(targetFile);
		builder.command(commands);
		Process p = null;
		try {
			p = builder.start();
			System.out.println(p.hashCode());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return "";
	}

	@RequestMapping(value = "/listbucket", method = RequestMethod.POST)
	public List<S3ObjectSummary> listBucket() {

		System.out.println("list bucket " + s3region);
		BasicAWSCredentials awsCreds = new BasicAWSCredentials(awsAccessKey,
				awsSecret);
		AmazonS3 s3Client = AmazonS3ClientBuilder.standard().withRegion(s3region)
				.withCredentials(new StaticCredentialsProvider(awsCreds))
				.build();
		System.out.println("---- "
				+ com.amazonaws.regions.Region.getRegion(Regions.US_WEST_2));
		/*s3Client.setRegion(com.amazonaws.regions.Region
				.getRegion(Regions.US_WEST_2));*/
		ObjectListing listing = s3Client.listObjects(s3Bucket);
		List<S3ObjectSummary> summaries = listing.getObjectSummaries();
		System.out.println("summaries-" + summaries);
		System.out.println("bucket name"+listing.getBucketName());
		while (listing.isTruncated()) {
			listing = s3Client.listNextBatchOfObjects(listing);
			summaries.addAll(listing.getObjectSummaries());
			System.out.println(listing.getBucketName());
		}
		return summaries;

	}

	/*public static void main(String... s) {
		LargeFileUploadController lf = new LargeFileUploadController();
		lf.awsAccessKey = "AKIAJTSP5ILZAKG3BBIA";
		lf.awsSecret = "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";
		lf.awsBucket = "cumulus-pd";
		lf.listBucket();
	}*/

}
