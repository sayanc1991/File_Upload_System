package com.cadence.cumulus.repository.mysql;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.cadence.cumulus.model.Role;

/**
 * 
 * @author shirish
 *
 */

public interface RoleRepository extends JpaRepository<Role, Long> {
	
	public List<Role> findByNameIgnoreCase(String name);
	
	
}