package com.cadence.cumulus.events.app;

import com.cadence.cumulus.model.AgentResponse;
import com.cadence.cumulus.model.JobRun;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Component;

@Component
public class AppEventsPublisher {

    private static final Logger log = LoggerFactory.getLogger(AppEventsPublisher.class);

    @Autowired
    private ApplicationEventPublisher publisher;

    public void publishEvent(AgentResponse response) {
        log.debug("AppEventsPublisher - publishEvent called command - " + response.getCommand() + ", userid " + response.getUserName());
        publisher.publishEvent(response);
    }

    public void publishEvent(JobRun jobRun) {
        log.debug("AppEventsPublisher - publishEvent called command - Run command -> " + jobRun);
        publisher.publishEvent(jobRun);
    }


}
