
package com.cadence.cumulus.events.api;

import com.cadence.cumulus.common.ServiceException;
import com.cadence.cumulus.events.app.AppEventsPublisher;
import com.cadence.cumulus.model.AgentCommand;
import com.cadence.cumulus.model.AgentResponse;
import com.cadence.cumulus.model.FileOperationRequest;
import com.cadence.cumulus.model.Result;
import com.cadence.cumulus.service.FileManagerServiceNew;
import com.cadence.cumulus.service.LicenseAgentService;
import com.cadence.cumulus.tailer.EventsTailer;
import com.cadence.cumulus.tailer.StdoutTailer;
import com.cadence.cumulus.tailer.TailEmitterService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.io.FilenameUtils;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.event.EventListener;
import org.springframework.core.task.SimpleAsyncTaskExecutor;
import org.springframework.core.task.TaskExecutor;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;

import java.io.*;
import java.text.MessageFormat;
import java.util.*;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;


@Component
public class APIEventsListener {
    private static final Logger log = LoggerFactory.getLogger(APIEventsListener.class);
    private static final Logger jobLog = LoggerFactory.getLogger("job-log");

    @Value("${cds.lic.file}")
    private String cdsLicenseFile;

    @Value("${mmsim.home}")
    private String mmsimHome;

    @Value("${user.home.base}")
    private String userHomeBase;

    @Value("${lsf.envdir}")
    private String lsfEnvDir;

    @Value("${lsf.server.dir}")
    private String lsfServerDir;

    @Value("${lsf.bin.dir}")
    private String lsfBinDir;

    @Value("${lsf.lib.dir}")
    private String lsfLibDir;

    @Value("${man.path}")
    private String manPath;

    @Value("${lsf.server.hosts}")
    private String lsfServerHosts;

    @Value("${global.path}")
    private String globalPath;

    @Value("${scheduler.name}")
    private String schedulerName;

    @Value("${newsub.path:/grid/sfi/hpc/openlava/scripts/newsub/}")
    private String newsubPath;

    @Value("${cumulus.product}")
    private String productName;


    @Value("${cumulus.scripts.dir}")
    private String scriptsDirectory;

    private final String LIBERATE_PATH_FORMAT = "{0}/bin:{0}/tools.lnx86/bin:{0}/tools.lnx86/spectre/bin:{0}/tools.lnx86/spectre/tools.lnx86/bin:";

    private final ObjectMapper mapper = new ObjectMapper();

    private final Map<String, Process> runs = new HashMap<>();
    private final Map<String, String> runDirs = new HashMap<String, String>();
    // scriptTypeList should contain file extensions that will generate events.
    // added it so as to prevent creation of events file as 'tail -f' process doesn't get killed if file is empty and
    // after newsub process ends
    private Set<String> scriptType = new HashSet<String>();

    private boolean isKilledEvent = false;
    ;

    public APIEventsListener() {
        scriptType.add("tcl");
    }

    public APIEventsListener(BlockingQueue<AgentResponse> tailerQueue) {
        this.tailerQueue = tailerQueue;
        this.fileManager = fileManager;
        //Add file extensions that generates events
        scriptType.add("tcl");
    }

    @Autowired
    private FileManagerServiceNew fileManager;

    @Autowired
    private TemplateEngine newSubTemplateEngine;


    @Autowired
    private LicenseAgentService licenseAgentService;

    private Runnable heartbeatThread;


    @Autowired
    private AppEventsPublisher publisher;


    private TaskExecutor taskExecutor = new SimpleAsyncTaskExecutor("cumulus-");

    private BlockingQueue<AgentResponse> tailerQueue = new LinkedBlockingQueue();


    @Async
    @EventListener
    public void handleEvent(AgentCommand agentCommand) {
        log.debug("APIEventsListener....handleEvent invoked..." + agentCommand);


        try {
            switch (agentCommand.getCommand().toLowerCase()) {
                case "file-man":
                    FileOperationRequest request = agentCommand.getFileOperationRequest();

                    Result result = null;
                    log.debug("Targeting Action ----------------------------------------------------" + request.getAction());
                    switch (request.getAction()) {
                        case "createFolder":
                            result = fileManager.addFolder(request);
                            break;
                        case "compress":
                            result = fileManager.compress(request);
                            break;
                        case "copy":
                            result = fileManager.copy(request);
                            break;
                        case "changePermissions":
                            result = fileManager.changePermissions(request);
                            break;
                        case "move":
                            result = fileManager.move(request);
                            break;
                        case "remove":
                            result = fileManager.delete(request);
                            break;
                        case "editFile":
                            result = fileManager.editFile(request);
                            break;
                        case "savefile":
                            result = fileManager.saveFile(request);
                            break;
                        case "extract":
                            result = fileManager.extract(request);
                            break;
                        case "list":
                            JSONObject list = fileManager.list(request);
                            AgentResponse agentResponseResult = new AgentResponse(agentCommand.getRequestId(), agentCommand.getUserName(), agentCommand.getCommand(), list.toString());
                            log.debug("Publishing event to APPlication Events Handler..");
                            publisher.publishEvent(agentResponseResult);

                            return;
                        case "rename":
                            result = fileManager.rename(request);
                            break;
                        case "getContent":
                            String fileContents = fileManager.fileContents(request);
                            result = new Result(true, null);
                            result.setFileName(request.getItem());
                            result.setFileLength(String.valueOf(fileContents.length()));
                            result.setFileContent(fileContents.getBytes());
                            break;
                        default:
                            break;
                    }

                    if (result == null) {
                        result = new Result(false, "Unknown error");
                    }

                    log.debug("Result: {}", result.toString());
                    log.debug("Result size: {}", result.toString().length());

                    AgentResponse agentResponseResult = new AgentResponse(agentCommand.getRequestId(), agentCommand.getUserName(), agentCommand.getCommand(), result.toString());

                    log.debug("FileManager commands - Publishing event to APPlication Events Handler..");
                    publisher.publishEvent(agentResponseResult);

                    break;
                case "bsub":
                    checkLicenseAgent();
                    log.info("APIEventsListener - bsuub condition");
                    executeBsubCommand(agentCommand);
                    log.info("APIEventsListener - bsuub condition - COMPLETED_SUCCESFULLY");
                    break;
                case "jobs":
                    switch (agentCommand.getExecCommand().toLowerCase()) {
                        case "newsub":
                            checkLicenseAgent();
                            new Thread(new CommandRunner(agentCommand)).start();
                            log.debug("APIEventsListener - Job thread started and ready to send successResponse");
                            sendSuccessResponse(agentCommand);
                            break;
                    }
                case "kill":
                /*
                 * Killing of a job is done by running a script called killme which
            	 * is present inside the job directory. The killme script is present
            	 * inside a directory that starts with "jsControl"
            	 */
                    writeToLogs("Inside kill case {}", agentCommand.getRequestId());
                    writeToLogs("Displaying command details {} ", agentCommand.toString());

                    Process p = runs.get(agentCommand.getRequestId());
                    if (p != null) {
                        writeToLogs("Found a process for run id: {}. Killing the process", agentCommand.getRequestId());
                        if (p.isAlive()) {
                            writeToLogs("Process is alive, killing it...", null);

                            try {
                                writeToLogs("File path : ", runDirs.get(agentCommand.getRequestId()));
                                File f = new File(runDirs.get(agentCommand.getRequestId()));
                                writeToLogs("File is " + f.getPath());
                                writeToLogs("Current Dir : " + f.getAbsolutePath());

                                List<String[]> files = fileManager.ls(agentCommand.getUserName(), f.getAbsolutePath());
                                for (String[] ff : files) {
                                    String dirPath = f.getAbsolutePath() + File.separator + ff[ff.length - 1];
                                    if (fileManager.isDirectory(agentCommand.getUserName(), dirPath) && ff[ff.length - 1].startsWith("jsControl")) {
                                        writeToLogs("jsControl Dir: " + dirPath);
                                        List<String> commands = new ArrayList<String>();
                                        commands.add("sudo");
                                        commands.add("-u");
                                        commands.add(agentCommand.getUserName());
                                        commands.add("-E");
                                        commands.add(dirPath + File.separator + "killme");
                                        ProcessBuilder builder = new ProcessBuilder(commands).redirectErrorStream(true).inheritIO().redirectOutput(ProcessBuilder.Redirect.PIPE);
                                        Process process = builder.start();
                                        StringBuilder sb = new StringBuilder();
                                        new Thread() {
                                            public void run() {
                                                try {
                                                    InputStream in = process.getInputStream();
                                                    BufferedReader br = new BufferedReader(new InputStreamReader(in));
                                                    String line = null;
                                                    while ((line = br.readLine()) != null) {
                                                        sb.append(line + "\n");
                                                    }
                                                    in.close();
                                                } catch (Exception ex) {
                                                    writeToLogs("Exception occured: {}", ex.getMessage());
                                                    ex.printStackTrace();
                                                }
                                            }
                                        }.start();
                                        isKilledEvent = true;
                                        process.waitFor();
                                        writeToLogs("Process destroyed ", sb);
                                        writeToLogs("Process exit value  " + process.exitValue());
                                        runs.remove(agentCommand.getRequestId());
                                        runDirs.remove(agentCommand.getRequestId());

                                    }
                                }
                            } catch (Exception ex) {
                                log.debug("Exception occured: {}", ex.getMessage());
                                jobLog.debug("Exception occured: {}", ex.getMessage());
                                ex.printStackTrace();
                            }
                            //p.destroy();
                        }
                        sendSuccessResponse(agentCommand);
                    } else {
                        writeToLogs("No process found for run id: {}. Already killed or not running anymore", agentCommand.getRequestId());
                        sendFailureResponse(agentCommand, "404");
                    }

                    break;
                case "list-liberate":
                    log.debug("APIEventsListener - list-liberate condition entered ");

                    JSONObject liberateVersions = fileManager.listToolsAndVersions(null);
                    AgentResponse agentResponse = new AgentResponse(agentCommand.getRequestId(), agentCommand.getUserName(), agentCommand.getCommand(), liberateVersions.toString());
                    log.debug("APIEventsListener - check-tool-path condition - list-liberate condition Agent response object " + agentResponse);
                    publisher.publishEvent(agentResponse);

                    break;
                case "check-tool-path":
                    log.debug("APIEventsListener - check-tool-path condition entered ");
                    Boolean exeExists = fileManager.validatePath(null, (String) agentCommand.getData(), (String) agentCommand.getLiberatePath());

                    AgentResponse agentResponse1 = new AgentResponse(agentCommand.getRequestId(), agentCommand.getUserName(), agentCommand.getCommand(), exeExists.toString());
                    log.debug("APIEventsListener - check-tool-path condition - Check tool path condition Agent response object " + agentResponse1);
                    publisher.publishEvent(agentResponse1);
                    break;
                default:
                    log.debug("APIEventsListener - default case.. invalid command");
                    sendFailureResponse(agentCommand, "Invalid command");
                    break;
            }
        } catch (Exception e) {
            log.error("Operation failed:", e);
            //TODO: Shambhu... review this condition and throw proper exception
            //sendFailureResponse(command, session, e.getMessage());
        }
    }

    private void executeBsubCommand(AgentCommand command) {


        log.info("executeBsubCommand - > Scripts Directory", scriptsDirectory);


        log.info("executeBsubCommand - > " + System.getProperty("user.dir") + "/" + command.getScriptPath());

        File runDir = new File(scriptsDirectory + "/" + command.getScriptPath());


        BufferedReader stdout = null;
        Process p = null;


        writeToLogs("executeBsubCommand-> Executing bsub command -> " + command);

        List<String> commands = new ArrayList<>();
        commands.add("sudo");
		commands.add("-u");
        commands.add(command.getUserName());
        commands.add("-E");
        

        //commands.add(command.getExecCommand() + " " + command.getClArgs());
		commands.add(command.getExecCommand());
		commands.add(command.getClArgs());
        writeToLogs("executeBsubCommand -> Commands details -> " + commands);

        ProcessBuilder builder = new ProcessBuilder(commands).redirectErrorStream(true).inheritIO().redirectOutput(ProcessBuilder.Redirect.PIPE);
        builder.directory(runDir);

        try {
            p = builder.start();
            runs.put(command.getRequestId(), p);

            stdout = new BufferedReader(new InputStreamReader(p.getInputStream()));

            String s;
            StringBuffer sbuf = new StringBuffer();
            while ((s = stdout.readLine()) != null) {
                log.debug("Alive :{}, OUT: {}", p.isAlive(), s);
                jobLog.debug("Alive :{}, OUT: {}", p.isAlive(), s);
                sbuf.append(s).append("\n");
            }
            p.waitFor();

            AgentResponse response = new AgentResponse(command.getRequestId(), command.getUserName(), command.getCommand(), sbuf.toString());

            writeToLogs("executeBsub command - Publishing event to APPlication Events Handler.." + response);

            publisher.publishEvent(response);

        } catch (Exception e) {
            log.error("Error: ", e);
            jobLog.error("Error: ", e);
        } finally {
            try {
                if (stdout != null) {
                    stdout.close();
                }
            } catch (IOException e) {
                log.error("Error: ", e);
                jobLog.error("Error: ", e);
            }

        }
    }

    private void sendSuccessResponse(final AgentCommand command) throws IOException {
        writeToLogs("APIEventsListener -> sendSuccessResponse : " + command);

        AgentResponse agentResponse = new AgentResponse(command.getRequestId(), command.getUserName(), command.getCommand(), true);
        publisher.publishEvent(agentResponse);

    }

    private void sendFailureResponse(final AgentCommand command, String error) throws IOException {
        AgentResponse agentResponse = new AgentResponse(command.getRequestId(), command.getCommand(), error, false);
        publisher.publishEvent(agentResponse);

    }

    /**
     * @author chandrakant
     * Class to run a job through newsub. This class starts a new thread of execution and runs the job
     * through this thread. Alongwith starting the job on a new thread, it also starts off two new
     * threads, one to track the output logs of the job and another to track the event logs of the job.
     */
    public class CommandRunner implements Runnable {
        private final AgentCommand command;

        private final String cumulusUserHome;
        private final String runDirPath;
        private final String jobRunDir;
        private final String liberatePath;
        public static final String OPERATION_CANCELLED = "##OPERATION@@CANCELLED$$";
        public static final String OPERATION_COMPLETED = "##OPERATION@@COMPLETED$$";
        public static final String OPERATION_FAILED = "##OPERATION@@FAILED$$";


        public CommandRunner(AgentCommand request) {
            this.command = request;
            this.liberatePath = command.getLiberatePath();
            this.cumulusUserHome = userHomeBase + command.getUserName() + "/cloud_orchestrator";
            this.runDirPath = cumulusUserHome + "/runs/";
            this.jobRunDir = runDirPath + command.getRequestId();
        }

        @Override
        public void run() {
            writeToLogs("Preparing to run job");

            NewTailer stdOutTailer = null;
            NewTailer eventsTailer = null;

            try {
                writeToLogs("Creating runDir {}", jobRunDir);
                fileManager.mkdir(command.getUserName(), jobRunDir);
                File runDir = new File(jobRunDir);
                fileManager.chown(command.getUserName(), cumulusUserHome);
                // Not required as we are doing chown -R on the parent directory
                // fileManager.chown(command.getUserName(), runDirPath);
                // fileManager.chown(command.getUserName(), jobRunDir);

//				String arrVal[] = command.getScriptPath().split("/");
//				String fileName = arrVal[arrVal.length-1];
//				arrVal = fileName.split("\\.");
                String fileExtn = FilenameUtils.getExtension(command.getScriptPath());
                File eventsFile = null;
                boolean isInScriptTypeList = false;
                writeToLogs("File extension is : " + fileExtn + scriptType.contains(fileExtn));
                writeToLogs("scriptType " + scriptType);
                if (scriptType.contains(fileExtn)) {
                    isInScriptTypeList = true;
                    writeToLogs("Creating events file at {}/events", jobRunDir);
                    fileManager.touchFile(command.getUserName(), jobRunDir + File.separator + "events");
                    eventsFile = new File(runDir, "events");
                    fileManager.chown(command.getUserName(), jobRunDir + "/events");
                }


                writeToLogs("Creating stdout file at {}/stdout", jobRunDir);

                fileManager.touchFile(command.getUserName(), jobRunDir + File.separator + "stdout");
                File stdOutFile = new File(runDir, "stdout");
                fileManager.chown(command.getUserName(), jobRunDir + "/stdout");

                Context ctx = new Context();
                ctx.setVariable("runId", command.getRequestId());
                ctx.setVariable("instance", command.getInstanceType());
                ctx.setVariable("agentCount", command.getNumberOfAgents());
                ctx.setVariable("schedulerName", schedulerName);
                ctx.setVariable("runDirPath", jobRunDir);
                ctx.setVariable("commandPath", command.getToolName().toLowerCase());
                ctx.setVariable("scriptPath", command.getScriptPath());
                ctx.setVariable("commandLineArgs", command.getClArgs());
                ctx.setVariable("stdOutFile", jobRunDir + "/stdout");

                log.debug("For file name : {} {} {} ", command.getScriptPath(), command.getScriptPath(), command.getClArgs());
                jobLog.debug("For file name : {} {} {} ", command.getScriptPath(), command.getScriptPath(), command.getClArgs());
                String newSubFileContent = newSubTemplateEngine.process("newsub/newsub.txt", ctx);

                writeToLogs(
                        "############################################newsub.list start############################################");
                writeToLogs("", newSubFileContent);
                writeToLogs(
                        "#############################################newsub.list end#############################################");


                writeToLogs("Creating newsub template file");
                fileManager.touchFile(command.getUserName(), jobRunDir + File.separator + "newsub.list");


                fileManager.createFile(command.getUserName(), jobRunDir + File.separator + "newsub.list",
                        newSubFileContent);

                List<String> commands = new ArrayList<>();
                commands.add("sudo");
                commands.add("-u");
                commands.add(command.getUserName());
				commands.add("-E");
				commands.add("/bin/bash");
				commands.add("-c");
               
                commands.add(newsubPath + command.getExecCommand() + " -i " + jobRunDir + File.separator + "newsub.list");

                writeToLogs("Command: {} ", mapper.writeValueAsString(commands));

                final ProcessBuilder builder = new ProcessBuilder(commands).redirectErrorStream(true).inheritIO().redirectOutput(ProcessBuilder.Redirect.PIPE);

                Map<String, String> env = builder.environment();

                String newGlobalPath = globalPath + MessageFormat.format(LIBERATE_PATH_FORMAT, liberatePath);

                writeToLogs("Old Path: {}", env.get("PATH"));
                writeToLogs("New Path: {}", newGlobalPath);

                writeToLogs("Orig Environment: {}", mapper.writeValueAsString(env));

                env.put("CCLOUD_JOB_ID", command.getRequestId());
                if (isInScriptTypeList) {
                    env.put("CCLOUD_EVENT_FILE_PATH", eventsFile.getAbsolutePath());
                }
                env.put("CDS_LIC_FILE", cdsLicenseFile);
                env.put("ALTOSHOME", liberatePath);
                env.put("MMSIMHOME", liberatePath + mmsimHome);
                env.put("LSF_ENVDIR", lsfEnvDir);
                env.put("LSF_SERVERDIR", lsfServerDir);
                env.put("LSF_BINDIR", lsfBinDir);
                env.put("LSF_LIBDIR", lsfLibDir);
                env.put("LSF_SERVER_HOSTS", lsfServerHosts);
                env.put("MANPATH", manPath);
                env.put("PATH", newGlobalPath + env.get("PATH"));
                env.put("QUEUE_NAME", command.getInstanceType());

                writeToLogs("New Environment: {}", mapper.writeValueAsString(env));

                stdOutTailer = new NewTailer(stdOutFile.getAbsolutePath(), new StdoutTailer(tailerQueue, command), 500,
                        command.getUserName(), false, fileManager);
                writeToLogs("Product is : {}", APIEventsListener.this.productName);
                Thread stdoutTailerThread = null;
                Thread eventTailerThread = null;
                if (null != APIEventsListener.this.productName && !APIEventsListener.this.productName.equalsIgnoreCase("xcelium") && isInScriptTypeList) {
                    writeToLogs("##Create events tailer...##");
                    writeToLogs("Tailer queue Size " + tailerQueue.size());
                    eventsTailer = new NewTailer(eventsFile.getAbsolutePath(), new EventsTailer(tailerQueue, command), 10,
                            command.getUserName(), false, fileManager);
                    writeToLogs("events tailer created.. " + eventsTailer);
                    eventTailerThread = new Thread(eventsTailer);
                    writeToLogs("Events tailer starting..");
                    eventTailerThread.start();
                }

                writeToLogs("Before stdoutTailerThread creation...");
                stdoutTailerThread = new Thread(stdOutTailer);
                stdoutTailerThread.start();

                writeToLogs("Before starting TailerEmitter Service.. is Tailerqueue check null ? " + tailerQueue);

                taskExecutor.execute(new TailEmitterService(tailerQueue, publisher));

                final Process p = builder.start();
                writeToLogs("Request ID :: " + command.getRequestId());
                runs.put(command.getRequestId(), p);

                runDirs.put(command.getRequestId(), jobRunDir);

                // log.debug("RUNS details {} ", command.getRequestId());

                BufferedReader stdout = null;
                try {
                    stdout = new BufferedReader(new InputStreamReader(p.getInputStream()));
                    String line;
                    while ((line = stdout.readLine()) != null) {
                        writeToLogs("APIEventsLister: CommandRunner - MAINPROCESS-OUT: {}", line);
                    }
                } catch (Exception e) {
                    log.error("Error found :", e);
                    jobLog.error("Error found :", e);
                } finally {
                    try {
                        if (stdout != null) {
                            stdout.close();
                        }
                    } catch (Exception e) {
                        jobLog.error("Error: ", e);
                    }
                }

                p.waitFor();
                writeToLogs("APIEventsListener - Process status in agent : " + p.exitValue());
                writeToLogs("Is job killed: " + isKilledEvent);

                //Vikramaditya: Append newline: It is required if the script doesn't generated newline in log file
                if (p.exitValue() != 0 && isKilledEvent) {
                    writeToLogs("Appending new line");
                    fileManager.appendToFile(command.getUserName(), stdOutFile.getAbsolutePath(), "\n");
                    fileManager.appendToFile(command.getUserName(), stdOutFile.getAbsolutePath(),
                            OPERATION_CANCELLED + "\n\n");
                    fileManager.appendToFile(command.getUserName(), stdOutFile.getAbsolutePath(), "\n");
                    if (isInScriptTypeList) {
                        fileManager.appendToFile(command.getUserName(), eventsFile.getAbsolutePath(), "\n");
                    }
                    isKilledEvent = false;
                } else if (p.exitValue() != 0 && !isKilledEvent) {
                    writeToLogs("Appending new line - OPERATION_FAILED condition");
                    fileManager.appendToFile(command.getUserName(), stdOutFile.getAbsolutePath(), "\n");
                    fileManager.appendToFile(command.getUserName(), stdOutFile.getAbsolutePath(),
                            OPERATION_FAILED + "\n\n");
                    fileManager.appendToFile(command.getUserName(), stdOutFile.getAbsolutePath(), "\n");
                    if (isInScriptTypeList) {
                        fileManager.appendToFile(command.getUserName(), eventsFile.getAbsolutePath(), "\n");
                    }
                    isKilledEvent = false;
                } else {
                    writeToLogs("Appending new line - OPERATION_COMPLETED condition invoked ");
                    fileManager.appendToFile(command.getUserName(), stdOutFile.getAbsolutePath(), "\n");
                    fileManager.appendToFile(command.getUserName(), stdOutFile.getAbsolutePath(),
                            OPERATION_COMPLETED + "\n\n");
                    fileManager.appendToFile(command.getUserName(), stdOutFile.getAbsolutePath(), "\n");
                    if (isInScriptTypeList) {
                        fileManager.appendToFile(command.getUserName(), eventsFile.getAbsolutePath(), "\n");
                    }
                }
                //signal the child threads (event & stdout tailers) that the parent process stopped/killed so that
                //the child process can finish of the tailing
                writeToLogs("APIEventsListener - CommandRunner - Stopping the child process ");

                writeToLogs("Stopping the stdout trailer process");
                stdOutTailer.setParentProcessStopped(true);
                if (eventsTailer != null) {
                    //eventsTailer would be null for xcelium
                    writeToLogs("Stopping the events trailer process");
                    eventsTailer.setParentProcessStopped(true);
                }
                //wait for child threads spawned to complete
                writeToLogs("APIEventsListener - CommandRunner... Wait for child threads to complete...calling join method");

                stdoutTailerThread.join();
                if (eventTailerThread != null) {
                    eventTailerThread.join();
                }


                writeToLogs("APIEventsListener - CommandRunner - Child tailer threads completed successfully.. Run method ends");
                p.destroy();

            } catch (Exception e) {
                log.error("Error: ", e);
                jobLog.error("Error: ", e);
            } finally {
                runs.remove(command.getRequestId());
                runDirs.remove(command.getRequestId());
            }
        }
    }

    private void writeToLogs(String message, Object variable) {
        log.debug(message, variable);
        jobLog.debug(message, variable);
    }

    private void writeToLogs(String message) {
        log.debug(message);
        jobLog.debug(message);
    }

    /**
     * Check if the license server is running, wait if its restarting throw exception if its stopped or failed to restart
     *
     * @throws ServiceException
     */
    private void checkLicenseAgent() throws ServiceException {
        licenseAgentService.waitForLicenseAgent();
    }
}
