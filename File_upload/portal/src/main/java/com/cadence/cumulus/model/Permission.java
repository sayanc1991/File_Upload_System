/**
 * 
 */
package com.cadence.cumulus.model;

import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;

/**
 * 
 * @author shirish
 *
 */
@JsonInclude(JsonInclude.Include.NON_EMPTY)
@Entity
@Table(name = "Permission")
public class Permission extends BaseEntity {

	private Long id;

	private String name;

	private String description;

	private Set<RolePermission> rolePermissionSet;

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * 
	 * @return name
	 */

	public String getName() {
		return name;
	}

	/**
	 * 
	 * @param name
	 */
	public void setName(String name) {
		this.name = name;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "Id")
	public Long getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	@OneToMany(mappedBy = "rolePermissionId.permission", fetch = FetchType.LAZY)
	@JsonIgnore
	public Set<RolePermission> getRolePermissionSet() {
		return rolePermissionSet;
	}

	public void setRolePermissionSet(Set<RolePermission> rolePermissionSet) {
		this.rolePermissionSet = rolePermissionSet;
	}

	@Override
	public int hashCode() {
		return id.hashCode();
	}

	@Override
	public boolean equals(Object object) {
		boolean isEqual = false;

		if (object != null && object instanceof Permission) {
			isEqual = this.id.equals(((Permission) object).id);
		}
		return isEqual;
	}

}
