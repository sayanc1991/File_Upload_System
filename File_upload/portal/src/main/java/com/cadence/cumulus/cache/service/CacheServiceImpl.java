package com.cadence.cumulus.cache.service;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.cadence.cumulus.cache.CacheKey;

/**
 * 
 * @author Zabeersultan.Farook Implementation of the Cache Service using a java
 *         Map. This can be changed in the future to use Redis or Memcached when
 *         we have more cache use case
 * 
 * 
 */
@Service
public class CacheServiceImpl implements CacheService {

	private static final Logger LOG = LoggerFactory.getLogger(CacheServiceImpl.class);

	private Map<CacheKey, Object> map;
	
	@PostConstruct
	private void init() {
		this.map = new ConcurrentHashMap<>();
	}

	@Override
	public void storeObject(CacheKey key, Object object) {
		LOG.debug("Storing cache entry : " + key.getCacheObjectType() + ":" + key.getCacheObjectID());
		map.put(key, object);
	}

	@Override
	public Object getObject(CacheKey key) {
		LOG.debug("Fetching cache entry : " + key.getCacheObjectType() + ":" + key.getCacheObjectID());
		return map.get(key);

	}

	@Override
	public void invalidateCache(CacheKey key) {
		LOG.debug("Invalidating cache entry : " + key.getCacheObjectType() + ":" + key.getCacheObjectID());
		map.remove(key);

	}

}
