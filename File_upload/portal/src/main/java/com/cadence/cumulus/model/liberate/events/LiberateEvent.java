package com.cadence.cumulus.model.liberate.events;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;
import java.util.TimeZone;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.cadence.cumulus.model.BooleanToCharConverter;
import com.cadence.cumulus.model.JobRun;
import com.cadence.cumulus.model.MapToStringConverter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

@Entity
@Table(name = "JobRunEvent")
public class LiberateEvent {
	private String context;

	private String id;

	private JobRun jobRun;

	private String eventClass;

	private String eventType;

	private String observer;

	private int seq;

	private String source;

	private String ts;

	private Map<String, String> data;

	private Long userId;

	@Transient
	private String eventTime;

	public LiberateEvent() {
	}

	@Id
	@Column(name = "Id")
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getContext() {
		return context;
	}

	public void setContext(String context) {
		this.context = context;
	}

	@Transient
	public String getJobName() {
		return jobRun.getJobName();
	}

	@JsonProperty("eventClass")
	public String getEventClass() {
		return eventClass;
	}

	@JsonProperty("event_class")
	public void setEventClass(String eventClass) {
		this.eventClass = eventClass;
	}

	@JsonProperty("eventType")
	public String getEventType() {
		return eventType;
	}

	@JsonProperty("event_type")
	public void setEventType(String eventType) {
		this.eventType = eventType;
	}

	public String getObserver() {
		return observer;
	}

	public void setObserver(String observer) {
		this.observer = observer;
	}

	public int getSeq() {
		return seq;
	}

	public void setSeq(int seq) {
		this.seq = seq;
	}

	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}

	public String getTs() {
		return ts;
	}

	public void setTs(String ts) {
		this.ts = ts;
	}

	@Convert(converter = MapToStringConverter.class)
	public Map<String, String> getData() {
		return data;
	}

	public void setData(Map<String, String> data) {
		this.data = data;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	@ManyToOne
	@JoinColumn(name = "RunId")
	@JsonIgnore
	public JobRun getJobRun() {
		return jobRun;
	}

	public void setJobRun(JobRun jobRun) {
		this.jobRun = jobRun;
	}

	@Transient
	public String getEventTime() {
		String eventTime = "";
		SimpleDateFormat sdfFinal = new SimpleDateFormat("MMM-dd-yyyy HH:mm:ss");
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
		sdf.setTimeZone(TimeZone.getTimeZone("PDT"));
		if (null != ts && !"".equals(ts)) {
			try {
				Date time = sdf.parse(ts);
				eventTime = sdfFinal.format(time);
			} catch (ParseException e) {
			}
		}
		return eventTime;
	}

	public void setEventTime(String eventTime) {
		this.eventTime = eventTime;
	}

}
