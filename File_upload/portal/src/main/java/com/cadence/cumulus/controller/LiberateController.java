package com.cadence.cumulus.controller;

import com.cadence.cumulus.events.api.APIEventsPublisher;
import com.cadence.cumulus.model.AgentCommand;
import com.cadence.cumulus.model.AgentResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.async.DeferredResult;
import org.springframework.web.context.request.async.DeferredResult.DeferredResultHandler;

import java.io.IOException;
import java.util.UUID;

@RestController
@RequestMapping(value = "/liberate")
public class LiberateController {
    private static final Logger log = LoggerFactory.getLogger(LiberateController.class);


    //Shambhu: Adding event publisher to send commands to for execution
    @Autowired
    private APIEventsPublisher publisher;

    @GetMapping(value = "/listTools")
    public DeferredResult<String> listLiberateVersions() throws IOException {
        log.debug("Liberate Controller - Fetching list of liberate versions...");

        AgentCommand command = new AgentCommand();
        command.setCommand("list-liberate");
        command.setRequestId(UUID.randomUUID().toString());

        DeferredResult<String> result = new DeferredResult<>();
        DeferredResult<AgentResponse> response = new DeferredResult<>();
        response.setResultHandler(new DeferredResultHandler() {
            @Override
            public void handleResult(Object res) {
                AgentResponse agentResponse = (AgentResponse) res;
                result.setResult(agentResponse.getData());
            }
        });

        //handler.sendCommand(command, response);

        publisher.pubhishEvent(command, response);

        return result;
    }

    @GetMapping(value = "/checkPath")
    public DeferredResult<String> checkUserPath(@RequestParam("exePath") String exePath,
                                                @RequestParam("toolName") String toolName) throws IOException {

        log.debug("Liberate Controller - checkpath called..");
        AgentCommand command = new AgentCommand();
        command.setCommand("check-tool-path");
        command.setData(toolName);
        command.setLiberatePath(exePath);
        command.setRequestId(UUID.randomUUID().toString());

        DeferredResult<String> result = new DeferredResult<>();
        DeferredResult<AgentResponse> response = new DeferredResult<>();
        response.setResultHandler(new DeferredResultHandler() {
            @Override
            public void handleResult(Object res) {
                AgentResponse agentResponse = (AgentResponse) res;
                result.setResult(agentResponse.getData());
            }
        });

        //handler.sendCommand(command, response);

        publisher.pubhishEvent(command, response);

        return result;
    }
}
