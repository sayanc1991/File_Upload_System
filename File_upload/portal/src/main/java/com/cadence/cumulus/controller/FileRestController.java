package com.cadence.cumulus.controller;

import com.cadence.cumulus.events.api.APIEventsPublisher;
import com.cadence.cumulus.model.AgentCommand;
import com.cadence.cumulus.model.AgentResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.async.DeferredResult;

import java.util.UUID;

@RestController
public class FileRestController {

    private static final Logger log = LoggerFactory.getLogger(FileRestController.class);

    @Autowired
    private APIEventsPublisher publisher;

    @RequestMapping("/api/files")
    public DeferredResult<String> sendCommand() {

        log.debug("FileRestController entered...");

        AgentCommand agentCommand = new AgentCommand();
        agentCommand.setCommand("file-man");
        // TODO: Fix this java type issue
        //agentCommand.setFileOperationRequest();
        agentCommand.setRequestId(UUID.randomUUID().toString());

        log.debug("Build the AgentCommand object and Set the DeferredResultHandler");

        DeferredResult<String> result = new DeferredResult<>();
        DeferredResult<AgentResponse> response = new DeferredResult<>();

        response.setResultHandler(new DeferredResult.DeferredResultHandler() {
            @Override
            public void handleResult(Object arg0) {
                AgentResponse response = (AgentResponse) arg0;
                String resultStr = response.getData();
                result.setResult(resultStr);
                log.debug("FileRestController...DeferredResultHandler called...");
                log.debug("Result Data " + resultStr);
            }
        });

        log.debug("FileRestController.. publishing events to API Handler");
        publisher.pubhishEvent(agentCommand, response);

        log.debug("FileRestController.. returning result..");
        return result;

    }
}
