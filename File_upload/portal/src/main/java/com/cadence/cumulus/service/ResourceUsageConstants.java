package com.cadence.cumulus.service;

/**
 * @author Vikramaditya Singh
 *
 */
public interface ResourceUsageConstants {

	public static final String JOB_NEW = "JOB_NEW";
	public static final String JOB_START = "JOB_START";
	public static final String JOB_FINISH = "JOB_FINISH";

}
