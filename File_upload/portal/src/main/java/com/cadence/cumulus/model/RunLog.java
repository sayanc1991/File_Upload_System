package com.cadence.cumulus.model;

import java.util.Date;

public class RunLog {
	private String id;
	private String runId;
	private String logLine;
	private Date timestamp;

	public RunLog() {
	}

	public RunLog(String runId, String logLine) {
		this.runId = runId;
		this.logLine = logLine;
		this.timestamp = new Date();
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getRunId() {
		return runId;
	}

	public void setRunId(String runId) {
		this.runId = runId;
	}

	public String getLogLine() {
		return logLine;
	}

	public void setLogLine(String logLine) {
		this.logLine = logLine;
	}

	public Date getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(Date timestamp) {
		this.timestamp = timestamp;
	}

	@Override
	public int hashCode() {
		return id.hashCode();
	}
}
