/**
 * 
 */
package com.cadence.cumulus.repository.mysql;

import org.springframework.data.jpa.repository.JpaRepository;

import com.cadence.cumulus.model.Organisation;

/**
 * @author deepak
 *
 */
public interface OrganisationRepository extends JpaRepository<Organisation, Long> {

	public Organisation findByDomain(String domain);
}
