package com.cadence.cumulus.controller.api;

import java.io.IOException;

import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.cadence.cumulus.common.Constants;

/**
 * @author Shambhu Bhatt
 * Date: 10/31/2017
 * CMLS-796 Provide cumulus build information api
 */

@RestController
public class BuildInfoAPIController {

    private static final Logger log = LoggerFactory
            .getLogger(BuildInfoAPIController.class);


    //Vikramaditya Singh: CMLS-945 :Auto increment build number
    @RequestMapping("/api/build")
    public String getBuildInfo(){
        log.debug("getBuildInfo entered..build info details ");
        String result = "";
        String version[] ;
    	ClassLoader classLoader = getClass().getClassLoader();
    	try {
    	    result = IOUtils.toString(classLoader.getResourceAsStream(Constants.VERSION_FILE));
    	} catch (IOException e) {
    		log.error("Exception while reading version file " + e);
    	}
    	version = result.split("=");
        return "Build " + version[1];
    }
}
