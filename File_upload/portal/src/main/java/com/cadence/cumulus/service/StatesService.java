package com.cadence.cumulus.service;

import java.util.List;

import com.cadence.cumulus.common.CumulusException;
import com.cadence.cumulus.model.States;

public interface StatesService {

    public List<States> getStatesByCountryCode(String countryCode) throws CumulusException;
    
    public List<States> getAllStates() throws CumulusException;

    public States getStatesByStateCode(String stateCode) throws CumulusException;
}
