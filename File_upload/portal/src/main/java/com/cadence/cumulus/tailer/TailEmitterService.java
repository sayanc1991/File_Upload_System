package com.cadence.cumulus.tailer;

import com.cadence.cumulus.events.app.AppEventsPublisher;
import com.cadence.cumulus.model.AgentResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.BlockingQueue;


public class TailEmitterService implements Runnable {
    private static final Logger log = LoggerFactory.getLogger(TailEmitterService.class);


    private AppEventsPublisher publisher;


    private BlockingQueue<AgentResponse> queue;


    public TailEmitterService(BlockingQueue<AgentResponse> queue, AppEventsPublisher publisher) {
        this.queue = queue;
        this.publisher = publisher;
    }

    @Override
    public void run() {
        log.debug("TailEmitterService.. Consumer queue loop waiting..started");
        if (publisher == null) {
            log.debug("TailEmitterService... publisher is null..... first fix it..");
        }

        while (true) {
            try {
                AgentResponse response = queue.take();

                //log.debug("TailEmitterService - Run id: {}, Line: {}", response.getRequestId(), response.getData());

                //		log.debug("TailEmitterService - Run id: {}, Line: {}", response.getRequestId(), response.getCommand());

/*
                synchronized (session) {
					if (session.isOpen()) {
						session.sendMessage(new TextMessage(response.toString()));
					}
				}
				*/
                if (response != null) {
                    log.debug("TailEmitterService - TailHead removed. publishing event.. " + response.getCommand() + " " + response.getRequestId());
                    publisher.publishEvent(response);
                }


                //since we are sending multiple lines (last 200 lines) in case of stdout tailer (once the parent process is completed)
                //check  the end of job string in the multiple line content
                if (response != null && (response.getData().contains("##OPERATION@@COMPLETED$$")
                        || response.getData().contains("##OPERATION@@CANCELLED$$") || response.getData().contains("##OPERATION@@FAILED$$"))) {
                    log.debug("TailEmitterService - End of process. Stopping tails.. End flag received...");
                    // session.close();
                    break;
                }
            } catch (Exception e) {
                log.debug("TailEmitterService - Fatal error.. exiting.. loop");
                log.error("TailEmitterService Error:", e);
                e.printStackTrace();

                //TODO: Proper exception handling..
                break;
            }
        }

        log.debug("TailerEmitterService -- This is read producer consumer.. Mission accomplished...");
    }
}
