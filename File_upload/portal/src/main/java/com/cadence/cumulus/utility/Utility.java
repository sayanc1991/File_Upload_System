package com.cadence.cumulus.utility;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Service;
import org.springframework.util.AntPathMatcher;
import org.springframework.web.servlet.HandlerMapping;

import com.cadence.cumulus.model.OpenLavaResourceEntity;
import com.cadence.cumulus.model.OpenLavaResourceVO;

/**
 * @author Vikramaditya Singh
 * Utility 
 *
 */
@Service
public class Utility {

	/**
	 * @param openLavaResourceEntity
	 * @return
	 * method is used to convert entity object to VO object
	 */
	public OpenLavaResourceVO convertoOpenLavaResourceVO(OpenLavaResourceEntity openLavaResourceEntity){
		OpenLavaResourceVO openLavaResourcevo = new OpenLavaResourceVO();
		openLavaResourcevo.setCoresUsed(openLavaResourceEntity.getCoresUsed());
		openLavaResourcevo.setCountOfHosts(openLavaResourceEntity.getCountOfHosts());
		openLavaResourcevo.setExecutionTime(openLavaResourceEntity.getExecutionTime());
		openLavaResourcevo.setFinishTime(openLavaResourceEntity.getFinishTime());
		openLavaResourcevo.setGroup(openLavaResourceEntity.getGroup());
		openLavaResourcevo.setJobId(openLavaResourceEntity.getJobId());
		openLavaResourcevo.setQueue(openLavaResourceEntity.getQueue());
		openLavaResourcevo.setStartTime(openLavaResourceEntity.getStartTime());
		openLavaResourcevo.setUserName(openLavaResourceEntity.getUserName());
		
		return openLavaResourcevo;
	}
	
	/**
	 * Extract path from a controller mapping. /controllerUrl/** => return matched **
	 * @param request incoming request.
	 * @return extracted path
	 */
	public String extractPathFromPattern(final HttpServletRequest request){
	    String path = (String) request.getAttribute(
	            HandlerMapping.PATH_WITHIN_HANDLER_MAPPING_ATTRIBUTE);
	    String bestMatchPattern = (String ) request.getAttribute(HandlerMapping.BEST_MATCHING_PATTERN_ATTRIBUTE);
	    AntPathMatcher apm = new AntPathMatcher();
	    String finalPath = apm.extractPathWithinPattern(bestMatchPattern, path);
	    return finalPath;

	}
	
}
