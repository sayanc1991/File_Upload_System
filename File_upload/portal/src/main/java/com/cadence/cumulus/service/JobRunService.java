package com.cadence.cumulus.service;

import com.cadence.cumulus.model.JobRun;

public interface JobRunService {

    /**
     * Runs the selected Job
     * 
     * @param job
     * @return
     */
    public JobRun saveJobRun(JobRun jobRun);

    /**
     * Cancel the running job
     * 
     * @param job
     * @return
     */
    public void deleteJobRun(JobRun jobRun);

    public JobRun getJobRun(String jobRunId);
    
}
