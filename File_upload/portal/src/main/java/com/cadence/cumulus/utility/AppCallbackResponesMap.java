package com.cadence.cumulus.utility;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import com.cadence.cumulus.model.AgentResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.web.context.request.async.DeferredResult;

@Service
public class AppCallbackResponesMap {

    private static final Logger log = LoggerFactory.getLogger(AppCallbackResponesMap.class);

    public Map<String, DeferredResult<AgentResponse>> callbackMap = new ConcurrentHashMap<>();

}
