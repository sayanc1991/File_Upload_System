package com.cadence.cumulus.repository.mysql.util;

import java.util.List;

import javax.persistence.criteria.Join;
import javax.persistence.criteria.Path;

import org.springframework.data.jpa.domain.Specification;

/**
 * SpecificationFactory which provides Specification implementations for various
 * conditions by implementing the Specification functional interface
 * 
 * @author zabeer
 *
 * @param <T>
 *            The entity type on which the specification will be applied
 */
public final class SpecificationFactory<T> {

	/**
	 * like operation directly on an entity's attribute
	 * 
	 * @param attribute
	 * @param value
	 * @return
	 */
	public Specification<T> like(String attribute, String value) {
		return (root, query, cb) -> cb.like(root.get(attribute), "%" + value + "%");
	}

	/**
	 * equality operation directly on an entity's attribute
	 * 
	 * @param attribute
	 * @param value
	 * @return
	 */
	public Specification<T> equal(String attribute, String value) {
		return (root, query, cb) -> cb.equal(cb.upper(root.get(attribute)), value.toUpperCase());
	}

	/**
	 * equality operation on a attribute of a child collection
	 * 
	 * @param collectionAttribute
	 * @param attribute
	 * @param value
	 * @return
	 */
	public Specification<T> joinAndEqualWithChildAttributes(String collectionAttribute, List<String> pathAttributes,
			String value) {

		return (root, query, cb) -> {
			// build the join with the collection attribute
			Join<Object, Object> joinClause = root.join(collectionAttribute);
			// initialize a nw Path object with the above result
			Path<Object> path = joinClause;
			// perform path navigation for the path attributes in the same order
			for (String pathAttribute : pathAttributes) {
				path = path.get(pathAttribute);
			}
			// perform the equality operation
			return cb.equal(path, value);
		};
	}

}