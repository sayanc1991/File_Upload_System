package com.cadence.cumulus.integration.unifiedregistration.vo;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class CreateUserVO {

	private String loginid;
	private String fName;
	private String lName;
	private String appcode;
	private String usertype;
	private String pwd;
	private UserContactInfoVO con;

	public String getLoginid() {
		return loginid;
	}
	public void setLoginid(String loginid) {
		this.loginid = loginid;
	}
	public String getfName() {
		return fName;
	}
	public void setfName(String fName) {
		this.fName = fName;
	}
	public String getlName() {
		return lName;
	}
	public void setlName(String lName) {
		this.lName = lName;
	}
	public String getAppcode() {
		return appcode;
	}
	public void setAppcode(String appcode) {
		this.appcode = appcode;
	}
	public String getUsertype() {
		return usertype;
	}
	public void setUsertype(String usertype) {
		this.usertype = usertype;
	}
	public String getPwd() {
		return pwd;
	}
	public void setPwd(String pwd) {
		this.pwd = pwd;
	}
	public UserContactInfoVO getCon() {
		return con;
	}
	public void setCon(UserContactInfoVO con) {
		this.con = con;
	}

	@Override
	public String toString() {
		return "CreateUserRequest [loginid=" + loginid + ", fName=" + fName + ", lName=" + lName + ", appcode="
				+ appcode + ", usertype=" + usertype + ", pwd=" + pwd + ", con=" + con + "]";
	}
}