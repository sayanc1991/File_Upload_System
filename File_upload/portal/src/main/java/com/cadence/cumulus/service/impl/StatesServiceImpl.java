package com.cadence.cumulus.service.impl;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.cadence.cumulus.common.CumulusException;
import com.cadence.cumulus.model.States;
import com.cadence.cumulus.repository.mysql.StatesRepository;
import com.cadence.cumulus.service.StatesService;

@Component
@Transactional(propagation = Propagation.REQUIRED, rollbackFor = { Exception.class })
public class StatesServiceImpl implements StatesService {

    /**
     * Logger
     */
    private static final Logger log = LoggerFactory.getLogger(StatesServiceImpl.class);

    @Autowired
    private StatesRepository statesRepository;

    @Override
    public List<States> getStatesByCountryCode(String countryCode) throws CumulusException {
        log.debug("Retrieving states with country code: {}", countryCode);
        return statesRepository.findByCountryCode(countryCode);
    }

    @Override
    public List<States> getAllStates() throws CumulusException {
        log.debug("Retrieving all states.");
        return statesRepository.findAll();
    }

	@Override
	public States getStatesByStateCode(String stateCode) throws CumulusException {
		log.debug("Retrieving state for state code: "+stateCode);
        return statesRepository.findByStateCode(stateCode);
	}
}