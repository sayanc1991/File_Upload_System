package com.cadence.cumulus.common;

public class CommandHandlerConstants {

    public static final String OPERATION_CANCELLED = "##OPERATION@@CANCELLED$$";
    public static final String OPERATION_COMPLETED = "##OPERATION@@COMPLETED$$";
    public static final String OPERATION_FAILED = "##OPERATION@@FAILED$$";

}
