package com.cadence.cumulus.service;

import java.util.List;

import com.cadence.cumulus.common.CumulusException;
import com.cadence.cumulus.model.CumulusUserNotificationSettings;
import com.cadence.cumulus.model.User;

public interface CumulusUserService {
	public CumulusUserNotificationSettings getNotificationSettings(String email);
	
	public CumulusUserNotificationSettings getNotificationSettings(User user);

	
	public CumulusUserNotificationSettings updateNotificationSettings(CumulusUserNotificationSettings settings);
	
	public CumulusUserNotificationSettings saveDefaultNotificationSettings(User user);

	/**
	 * Saves User to DB
	 * 
	 * @param user
	 * @return
	 */
	public User saveUser(User user);

	public void deleteUser(User user);
	
	public User getUserById(Long id);

	public User getUserByEmail(String userId);

	/**
	 * Search user records based on different search parameters
	 * 
	 * @param firstName
	 * @param lastName
	 * @param emailID
	 * @param roleID
	 * @return
	 */
	public List<User> searchUser(String firstName, String lastName, String emailID, String roleID)
			throws CumulusException;

	public CumulusUserNotificationSettings getNotificationSettings(Long userId);
	
	// CMLS-658
	public User getUserByUserName(String loginName);
}
