package com.cadence.cumulus.integration.unifiedregistration.service;

import java.net.InetSocketAddress;
import java.net.Proxy;
import java.util.Arrays;

import javax.annotation.PostConstruct;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.SimpleClientHttpRequestFactory;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.cadence.cumulus.ProxyConfig;
import com.cadence.cumulus.cache.CacheKey;
import com.cadence.cumulus.cache.service.CacheService;
import com.cadence.cumulus.common.CumulusException;
import com.cadence.cumulus.integration.IntegrationException;
import com.cadence.cumulus.integration.unifiedregistration.vo.AuthenticationTokenVO;
import com.cadence.cumulus.integration.unifiedregistration.vo.ChangePasswordVO;
import com.cadence.cumulus.integration.unifiedregistration.vo.Contact;
import com.cadence.cumulus.integration.unifiedregistration.vo.CreateUserVO;
import com.cadence.cumulus.integration.unifiedregistration.vo.Record;
import com.cadence.cumulus.integration.unifiedregistration.vo.SecurityQARequest;
import com.cadence.cumulus.integration.unifiedregistration.vo.URResponse;
import com.cadence.cumulus.integration.unifiedregistration.vo.UpdateUserVO;
import com.cadence.cumulus.model.User;
import com.cadence.cumulus.service.CountriesService;
import com.cadence.cumulus.service.StatesService;
import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

@Service
public class URIntegrationServiceImpl implements URIntegrationService {

	private static final Logger LOG = LoggerFactory.getLogger(URIntegrationServiceImpl.class);

	private static final String CONTACT_QUERY = "SELECT id,Email,FirstName,LastName,Middle_Name__c,Login_Name__c,Country__c,State_Region__c,City__c,Street__c,Postal_Code__c,phone,Cellular_Phone__c,CreatedDate FROM Contact WHERE Email=':emailID'";

	@Autowired
	private CacheService cacheService;

	@Autowired
	private CountriesService countriesService;

	@Autowired
	private StatesService statesService;

	@Autowired
	private ProxyConfig proxyConfig;

	private RestTemplate restTemplate;

	@Value("${ur.loginURL}")
	private String loginURL;

	@Value("${ur.clientID}")
	private String clientID;

	@Value("${ur.clientSecret}")
	private String clientSecret;

	@Value("${ur.refreshToken}")
	private String refreshToken;

	@Value("${ur.query.request.endpoint}")
	private String queryEndPoint;

	@Value("${ur.query.change.password.request.endpoint}")
	private String changePasswordQueryEndPoint;

	@Value("${ur.query.reset.sqa.request.endpoint}")
	private String resetSqaQueryEndPoint;

	@Value("${ur.query.create.newuser.request.endpoint}")
	private String newUserQueryEndPoint;

	@Value("${ur.query.update.user.request.endpoint}")
	private String updateUserQueryEndPoint;

	private String loginQueryString;

	@PostConstruct
	public void setup() {
		StringBuilder sb = new StringBuilder();
		/*sb.append("&client_id=").append(clientID).append("&client_secret=").append(clientSecret).append("&username=")
				.append(userName).append("&password=").append(password).append("&grant_type=").append(grantPassword);*/
		sb.append("&grant_type=refresh_token").append("&client_id=").append(clientID)
				.append("&client_secret=").append(clientSecret).append("&refresh_token=").append(refreshToken);
		loginQueryString = sb.toString();
		restTemplate = new RestTemplate();
		// set proxy config RestTemplate if required
		if (proxyConfig.isEnabled()) {
			SimpleClientHttpRequestFactory factory = new SimpleClientHttpRequestFactory();
			InetSocketAddress address = new InetSocketAddress(proxyConfig.getHost(), proxyConfig.getPort());
			Proxy proxy = new Proxy(Proxy.Type.HTTP, address);
			factory.setProxy(proxy);
			restTemplate.setRequestFactory(factory);
		}
	}

	@Override
	public User getUserDetails(String emailId) throws IntegrationException {
		// pass retry as false for the initial call
		return getUserDetails(emailId, false);

	}

	/**
	 * 
	 * @param emailId
	 * @param tryCount
	 *            tryCount to handle a retry scenario in case of expired tokens
	 *            indicated by 403
	 * @return
	 * @throws IntegrationException
	 */
	private User getUserDetails(String emailId, boolean retry) throws IntegrationException {
		ResponseEntity<String> response = null;
		try {
			String query = CONTACT_QUERY.replace(":emailID", emailId);
			LOG.debug("UR query : " + query);
			AuthenticationTokenVO authenticationToken = getAuthenticationToken();
			String endPointURL = authenticationToken.getInstance_url().concat(queryEndPoint).concat(query);
			LOG.debug("endpoint URL" + endPointURL);
			RestTemplate restTemplate = getRestTemplate();
			HttpHeaders headers = new HttpHeaders();
			LOG.debug("authorization " + "Bearer " + authenticationToken.getAccess_token() );
			// headers.add("Authorization", "Bearer " + authenticationToken.getAccess_token());
			headers.add("Authorization", "OAuth " + authenticationToken.getAccess_token());
			headers.setAccept(Arrays.asList(org.springframework.http.MediaType.APPLICATION_JSON));
			LOG.debug("Headers "+ headers);
			HttpEntity<String> entity = new HttpEntity<>(headers);
			LOG.info("UR with end point :" + queryEndPoint);

			response = restTemplate.exchange(endPointURL, HttpMethod.GET, entity, String.class);
			LOG.debug("UR response " + response);
			LOG.debug("UR Response - get body " + response.getBody());

		} catch (Exception e) {
			// logic in case of expired tokens. If there is a UNAUTHORIZED
			// response,
			// it could be to do with expired tokens, so clear the token cache
			// and perform a retry
			// whenever try count exceeds 1 it will break and come out with an
			// exception
			if (e.getMessage().contains(HttpStatus.UNAUTHORIZED.toString()) && !retry) {
				LOG.error(
						"401 returned for UR API probably due to invalid token. re-generating token and trying again..",
						e);
				// invalidate the UR token
				cacheService.invalidateCache(CacheKey.UR_TOKEN_KEY);
				// call the method again with an incremented try count
				return getUserDetails(emailId, true);
			} else {
				LOG.error("Exception while getting user details from UR", e);
				throw new IntegrationException("Exception while getting user details from UR", e);
			}
		}

		if (response != null && HttpStatus.Series.SUCCESSFUL == response.getStatusCode().series()) {
			LOG.debug("UR response for the API with end point " + queryEndPoint + ":" + response.getBody());

			return getUserObject(response);
		} else {
			LOG.error("Error response while calling UR API with end point " + queryEndPoint + (response != null
					? " Status :" + response.getStatusCodeValue() + " : response body : " + response.getBody() : ""));
			throw new IntegrationException("Error response while calling UR API with end point " + queryEndPoint
					+ (response != null
							? " Status :" + response.getStatusCodeValue() + " : response body : " + response.getBody()
							: ""));
		}

	}

	@Override
	public void addUserToCumulusGroup(String contactId, String cumulusGroupId) throws IntegrationException {
		// TODO This API is not yet available in UR to integrate from Cumulus
		throw new IntegrationException("Operation not supported");

	}

	private AuthenticationTokenVO getAuthenticationToken() throws IntegrationException {
		// lookup for the token in cache and if found return, otherwise obtain a
		// new token
		AuthenticationTokenVO authTokenVO = null;
		if (cacheService.getObject(CacheKey.UR_TOKEN_KEY) != null) {
			LOG.debug("UR auth token found from cache");
			return (AuthenticationTokenVO) cacheService.getObject(CacheKey.UR_TOKEN_KEY);
		}

		// obtain new token
		LOG.debug("Within UR authentication step");

		RestTemplate restTemplate = getRestTemplate();
		HttpHeaders headers = new HttpHeaders();

		headers.setContentType(org.springframework.http.MediaType.APPLICATION_FORM_URLENCODED);
		HttpEntity<String> entity = new HttpEntity<>(loginQueryString, headers);
		LOG.debug("UR authentication string " + loginQueryString);
		try {
			LOG.debug("Authenticating with UR");
			ResponseEntity<AuthenticationTokenVO> response = restTemplate.exchange(loginURL, HttpMethod.POST, entity,
					AuthenticationTokenVO.class);
			LOG.debug("UR Authentication response " + response);
			LOG.debug("UR Authentication response body " + response.getBody());
			if (HttpStatus.OK == response.getStatusCode()) {
				LOG.debug("Authentication success");
				authTokenVO = response.getBody();
				cacheService.storeObject(CacheKey.UR_TOKEN_KEY, authTokenVO);
			} else {
				LOG.error("Exception while authenticating with UR: status code" + response.getStatusCodeValue());
				throw new IntegrationException("Exception while authenticating with UR");
			}

		} catch (Exception e) {
			LOG.error("Exception while authenticating with UR", e);
			throw e;

		}
		return authTokenVO;
	}

	private User getUserObject(ResponseEntity<String> response) throws IntegrationException {

		// unmarshal the JSON response into Java object
		ObjectMapper mapper = new ObjectMapper();
		mapper.configure(MapperFeature.ACCEPT_CASE_INSENSITIVE_PROPERTIES, true);
		Contact contact = null;
		try {
			contact = mapper.readValue(response.getBody(), Contact.class);
		} catch (Exception e) {
			LOG.error("Exception while parsing UR response " + response.getBody(), e);
			throw new IntegrationException("Exception while parsing UR response", e);
		}
		User user = null;
		// check if records are present in the response
		if (contact.getTotalSize() > 0) {
			Record record = contact.getRecords().get(0);
			user = new User();
			user.setContactId(record.getId());
			user.setFirstName(record.getFirstName());
			user.setLastName(record.getLastName());
			user.setUserId(record.getEmail());
			user.setEmail(record.getEmail());
			user.setPhone(record.getPhone());
			try {
				user.setCountry(countriesService.getByCountryCode(record.getCountryC()));
				// user.setStateRegion(statesService.getStatesByStateCode(record.getStateRegionC()).getStateName());
				if (StringUtils.isNotBlank(record.getStateRegionC())) {
					user.setStateRegion(statesService.getStatesByStateCode(record.getStateRegionC()).getStateName());
				}
			} catch (CumulusException e) {
				LOG.debug("Exception while fetching state/country : " + e.getMessage());
			}
			user.setCity(record.getCityC());
			user.setStreet(record.getStreetC());
			user.setPostalCode(record.getPostalCodeC());
		}
		return user;
	}

	private RestTemplate getRestTemplate() {

		return restTemplate;
	}

	@Override
	public URResponse updateUserPassword(ChangePasswordVO changePasswordVO) throws IntegrationException {
		return updateUserPassword(changePasswordVO, false);
	}

	private URResponse updateUserPassword(ChangePasswordVO changePasswordVO, boolean retry) throws IntegrationException {
		ResponseEntity<URResponse> response = null;
		try {
			AuthenticationTokenVO authenticationToken = getAuthenticationToken();
			String endPointURL = authenticationToken.getInstance_url().concat(changePasswordQueryEndPoint);
			LOG.debug(endPointURL);
			RestTemplate restTemplate = getRestTemplate();
			HttpHeaders headers = new HttpHeaders();
			// headers.add("Authorization", "Bearer " + authenticationToken.getAccess_token());
			headers.add("Authorization", "OAuth " + authenticationToken.getAccess_token());
			headers.setAccept(Arrays.asList(org.springframework.http.MediaType.APPLICATION_JSON));
			HttpEntity<ChangePasswordVO> entity = new HttpEntity<>(changePasswordVO, headers);
			LOG.info("UR with end point :" + changePasswordQueryEndPoint);

			response = restTemplate.exchange(endPointURL, HttpMethod.POST, entity, URResponse.class);

		} catch (Exception e) {
			// logic in case of expired tokens. If there is a UNAUTHORIZED
			// response,
			// it could be to do with expired tokens, so clear the token cache
			// and perform a retry
			// whenever try count exceeds 1 it will break and come out with an
			// exception
			if (e.getMessage().contains(HttpStatus.UNAUTHORIZED.toString()) && !retry) {
				LOG.error(
						"401 returned for UR API probably due to invalid token. re-generating token and trying again..",
						e);
				// invalidate the UR token
				cacheService.invalidateCache(CacheKey.UR_TOKEN_KEY);
				// call the method again with an incremented try count
				return updateUserPassword(changePasswordVO, true);
			} else {
				LOG.error("Exception while changing user password in UR.", e);
				throw new IntegrationException("Exception while changing user password in UR : "+e.getMessage());
			}
		}

		if (response != null && HttpStatus.Series.SUCCESSFUL == response.getStatusCode().series()) {
			LOG.debug("UR response for the API with end point " + changePasswordQueryEndPoint + ":" + response.getBody());
			URResponse urResponse = response.getBody();
			if (urResponse.getErrorCode() != null) {
				String errorMsg = "Error while updating user password in UR with errorCode: "
						+ urResponse.getErrorCode() + " and errorMsg: " + urResponse.getErrorSummary();
				LOG.error(errorMsg);
				// throw new IntegrationException("Error while updating user password in UR with error msg: "+urResponse.getErrorSummary());
				throw new IntegrationException(urResponse.getErrorSummary());
			}
		} else {
			LOG.error("Error response while calling UR API with end point " + changePasswordQueryEndPoint + (response != null
					? " Status :" + response.getStatusCodeValue() + " : response body : " + response.getBody() : ""));
			throw new IntegrationException("Error response while calling UR API with end point " + changePasswordQueryEndPoint
					+ (response != null
							? " Status :" + response.getStatusCodeValue() + " : response body : " + response.getBody()
							: ""));
		}

		return response.getBody();
	}

	@Override
	public URResponse updateUserGroup(UpdateUserVO updateUserVO) throws IntegrationException {
		return updateUserGroup(updateUserVO, false);
	}

	private URResponse updateUserGroup(UpdateUserVO updateUserVO, boolean retry) throws IntegrationException {
		ResponseEntity<URResponse> response = null;
		try {
			AuthenticationTokenVO authenticationToken = getAuthenticationToken();
			String endPointURL = authenticationToken.getInstance_url().concat(updateUserQueryEndPoint);
			LOG.debug(endPointURL);
			RestTemplate restTemplate = getRestTemplate();
			HttpHeaders headers = new HttpHeaders();
			// headers.add("Authorization", "Bearer " + authenticationToken.getAccess_token());
			headers.add("Authorization", "OAuth " + authenticationToken.getAccess_token());
			headers.setAccept(Arrays.asList(org.springframework.http.MediaType.APPLICATION_JSON));
			HttpEntity<UpdateUserVO> entity = new HttpEntity<>(updateUserVO, headers);
			LOG.info("UR with end point :" + updateUserQueryEndPoint);

			response = restTemplate.exchange(endPointURL, HttpMethod.POST, entity, URResponse.class);

		} catch (Exception e) {
			// logic in case of expired tokens. If there is a UNAUTHORIZED
			// response,
			// it could be to do with expired tokens, so clear the token cache
			// and perform a retry
			// whenever try count exceeds 1 it will break and come out with an
			// exception
			if (e.getMessage().contains(HttpStatus.UNAUTHORIZED.toString()) && !retry) {
				LOG.error(
						"401 returned for UR API probably due to invalid token. re-generating token and trying again..",
						e);
				// invalidate the UR token
				cacheService.invalidateCache(CacheKey.UR_TOKEN_KEY);
				// call the method again with an incremented try count
				return updateUserGroup(updateUserVO, true);
			} else {
				LOG.error("Exception while removing user group in UR.", e);
				throw new IntegrationException("Exception while removing user group in UR : "+e.getMessage());
			}
		}

		if (response != null && HttpStatus.Series.SUCCESSFUL == response.getStatusCode().series()) {
			LOG.debug("UR response for the API with end point " + updateUserQueryEndPoint + ":" + response.getBody());
			URResponse urResponse = response.getBody();
			if (urResponse.getErrorCode() != null) {
				String errorMsg = "Error while removing user group in UR with errorCode: "
						+ urResponse.getErrorCode() + " and errorMsg: " + urResponse.getErrorSummary();
				LOG.error(errorMsg);
				// throw new IntegrationException("Error while updating user password in UR with error msg: "+urResponse.getErrorSummary());
				throw new IntegrationException(urResponse.getErrorSummary());
			}
		} else {
			LOG.error("Error response while calling UR API with end point " + updateUserQueryEndPoint + (response != null
					? " Status :" + response.getStatusCodeValue() + " : response body : " + response.getBody() : ""));
			throw new IntegrationException("Error response while calling UR API with end point " + updateUserQueryEndPoint
					+ (response != null
							? " Status :" + response.getStatusCodeValue() + " : response body : " + response.getBody()
							: ""));
		}

		return response.getBody();
	}
	
	@Override
	public URResponse createUser(CreateUserVO createUserVO) throws IntegrationException {
		return createUser(createUserVO, false);
	}

	private URResponse createUser(CreateUserVO createUserVO, boolean retry) throws IntegrationException {
		ResponseEntity<URResponse> response = null;
		try {
			AuthenticationTokenVO authenticationToken = getAuthenticationToken();
			String endPointURL = authenticationToken.getInstance_url().concat(newUserQueryEndPoint);
			LOG.debug(endPointURL);
			RestTemplate restTemplate = getRestTemplate();
			HttpHeaders headers = new HttpHeaders();
			// headers.add("Authorization", "Bearer " + authenticationToken.getAccess_token());
			headers.add("Authorization", "OAuth " + authenticationToken.getAccess_token());
			headers.setAccept(Arrays.asList(org.springframework.http.MediaType.APPLICATION_JSON));
			HttpEntity<CreateUserVO> entity = new HttpEntity<>(createUserVO, headers);
			LOG.info("UR with end point :" + newUserQueryEndPoint);

			response = restTemplate.exchange(endPointURL, HttpMethod.POST, entity, URResponse.class);

		} catch (Exception e) {
			// logic in case of expired tokens. If there is a UNAUTHORIZED
			// response,
			// it could be to do with expired tokens, so clear the token cache
			// and perform a retry
			// whenever try count exceeds 1 it will break and come out with an
			// exception
			if (e.getMessage().contains(HttpStatus.UNAUTHORIZED.toString()) && !retry) {
				LOG.error(
						"401 returned for UR API probably due to invalid token. re-generating token and trying again..",
						e);
				// invalidate the UR token
				cacheService.invalidateCache(CacheKey.UR_TOKEN_KEY);
				// call the method again with an incremented try count
				return createUser(createUserVO, true);
			} else {
				LOG.error("Exception while creating new user in UR.", e);
				throw new IntegrationException("Exception while creating new user in UR : "+e.getMessage());
			}
		}

		if (response != null && HttpStatus.Series.SUCCESSFUL == response.getStatusCode().series()) {
			LOG.debug("UR response for the API with end point " + newUserQueryEndPoint + ":" + response.getBody());
			URResponse urResponse = response.getBody();
			if (urResponse.getErrorCode() != null) {
				String errorMsg = "Error while creating new user in UR with errorCode: "
						+ urResponse.getErrorCode() + " and errorMsg: " + urResponse.getErrorSummary();
				LOG.error(errorMsg);
				// throw new IntegrationException("Error while updating user password in UR with error msg: "+urResponse.getErrorSummary());
				throw new IntegrationException(urResponse.getErrorSummary());
			}
		} else {
			LOG.error("Error response while calling UR API with end point " + newUserQueryEndPoint + (response != null
					? " Status :" + response.getStatusCodeValue() + " : response body : " + response.getBody() : ""));
			throw new IntegrationException("Error response while calling UR API with end point " + newUserQueryEndPoint
					+ (response != null
							? " Status :" + response.getStatusCodeValue() + " : response body : " + response.getBody()
							: ""));
		}

		return response.getBody();
	}

	@Override
	public URResponse resetSecurityQA(SecurityQARequest securityQARequest) throws IntegrationException {
		return resetSecurityQA(securityQARequest, false);
	}

	private URResponse resetSecurityQA(SecurityQARequest securityQARequest, boolean retry) throws IntegrationException {
		ResponseEntity<URResponse> response = null;
		try {
			AuthenticationTokenVO authenticationToken = getAuthenticationToken();
			String endPointURL = authenticationToken.getInstance_url().concat(resetSqaQueryEndPoint);
			LOG.debug(endPointURL);
			RestTemplate restTemplate = getRestTemplate();
			HttpHeaders headers = new HttpHeaders();
			// headers.add("Authorization", "Bearer " + authenticationToken.getAccess_token());
			headers.add("Authorization", "OAuth " + authenticationToken.getAccess_token());
			headers.setAccept(Arrays.asList(org.springframework.http.MediaType.APPLICATION_JSON));
			HttpEntity<SecurityQARequest> entity = new HttpEntity<>(securityQARequest, headers);
			LOG.info("UR with end point :" + resetSqaQueryEndPoint);

			response = restTemplate.exchange(endPointURL, HttpMethod.POST, entity, URResponse.class);

		} catch (Exception e) {
			// logic in case of expired tokens. If there is a UNAUTHORIZED
			// response,
			// it could be to do with expired tokens, so clear the token cache
			// and perform a retry
			// whenever try count exceeds 1 it will break and come out with an
			// exception
			if (e.getMessage().contains(HttpStatus.UNAUTHORIZED.toString()) && !retry) {
				LOG.error("401 returned for UR API probably due to invalid token. re-generating token and trying again.",e);
				// invalidate the UR token
				cacheService.invalidateCache(CacheKey.UR_TOKEN_KEY);
				// call the method again with an incremented try count
				return resetSecurityQA(securityQARequest, true);
			} else {
				LOG.error("Exception while resetting user security question/answer in UR.", e);
				throw new IntegrationException("Exception while resetting user security question/answer in UR.", e);
			}
		}

		if (response != null && HttpStatus.Series.SUCCESSFUL == response.getStatusCode().series()) {
			LOG.debug("UR response for the API with end point " + resetSqaQueryEndPoint + ":" + response.getBody());
			URResponse urResponse = response.getBody();
			if (urResponse.getErrorCode() != null) {
				String errorMsg = "Error while updating user security question/answer in UR with errorCode: "
						+ urResponse.getErrorCode() + " and errorMsg: " + urResponse.getErrorSummary();
				LOG.error(errorMsg);
				throw new IntegrationException(errorMsg);
			}
		} else {
			LOG.error("Error response while calling UR API with end point " + resetSqaQueryEndPoint + (response != null
					? " Status :" + response.getStatusCodeValue() + " : response body : " + response.getBody() : ""));
			throw new IntegrationException("Error response while calling UR API with end point " + resetSqaQueryEndPoint
					+ (response != null
							? " Status :" + response.getStatusCodeValue() + " : response body : " + response.getBody()
							: ""));
		}

		return response.getBody();
	}

}
