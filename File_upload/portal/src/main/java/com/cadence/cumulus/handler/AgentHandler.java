package com.cadence.cumulus.handler;

import java.io.IOException;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.ApplicationEventPublisherAware;
import org.springframework.core.task.TaskExecutor;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Service;
import org.springframework.web.context.request.async.DeferredResult;
import org.springframework.web.socket.CloseStatus;
import org.springframework.web.socket.TextMessage;
import org.springframework.web.socket.WebSocketSession;
import org.springframework.web.socket.handler.AbstractWebSocketHandler;

import com.cadence.cumulus.common.Constants;
import com.cadence.cumulus.model.AgentCommand;
import com.cadence.cumulus.model.AgentResponse;
import com.cadence.cumulus.model.CumulusUserNotificationSettings;
import com.cadence.cumulus.model.JobRun;
import com.cadence.cumulus.model.RunLog;
import com.cadence.cumulus.model.liberate.events.LiberateEvent;
import com.cadence.cumulus.model.liberate.events.LiberateEvents;
import com.cadence.cumulus.model.liberate.events.LiberateStatus;
import com.cadence.cumulus.repository.mysql.LiberateEventRepository;
import com.cadence.cumulus.service.CumulusUserService;
import com.cadence.cumulus.service.JobRunService;
import com.cadence.cumulus.service.PublisherService;
import com.cadence.cumulus.service.StatusPublisherService;
import com.fasterxml.jackson.databind.ObjectMapper;

@Service
public class AgentHandler extends AbstractWebSocketHandler implements ApplicationEventPublisherAware {
	private static final Logger log = LoggerFactory.getLogger(AgentHandler.class);

	@Autowired
	private LiberateEventRepository eventRepository;

	@Autowired
	private JobRunService jobRunService;

	@Autowired
	private TaskExecutor logsExecutor;

	@Autowired
	private TaskExecutor eventsExecutor;

	@Autowired
	private TaskExecutor globalEventsExecutor;
	
	@Autowired
    private CumulusUserService userService;
	
	private ApplicationEventPublisher publisher;

	private WebSocketSession session;

	private Map<String, DeferredResult<AgentResponse>> responses = new HashMap<>();

	private Map<String, Map<String, Map<String, BlockingQueue<AgentResponse>>>> jobLogsQueue = new HashMap<>();

	private Map<String, Map<String, Map<String, BlockingQueue<AgentResponse>>>> jobEventsQueue = new HashMap<>();

	private Map<String, Map<String, BlockingQueue<JobRun>>> globalEventsQueue = new HashMap<>();

	private Map<Long, Boolean> statusNotificationSuccessMap = new HashMap<>();
	/*
	 * Data structure to hold the logs for each job-id till we have enough lines
	 * to push into the database and to the ui
	 * 
	 * private Map<String, ArrayList<String>> stdoutLogsMap = new HashMap<>();
	 * 
	 * 
	 * This is the number of lines that we would be buffering before pushing the
	 * whole batch to the UI and the DB. This is done because otherwise DyanmoDB
	 * is not able to handle the high throoughput.
	 * 
	 * private static final Integer STDOUT_LINE_BUFFER_LIMIT = 20;
	 */

	@Autowired
	private SimpMessagingTemplate simpMessagingTemplate;

	@Override
	public void afterConnectionEstablished(WebSocketSession session) throws Exception {
		log.info("Client connected");

		this.session = session;
	}

	@Override
	public void afterConnectionClosed(WebSocketSession session, CloseStatus status) throws Exception {
		log.info("Client disconnected");

		this.session = null;
	}

	@Override
	protected void handleTextMessage(WebSocketSession session, TextMessage message) throws Exception {
		try {
			final ObjectMapper mapper = new ObjectMapper();
			JobRun run = null;
			AgentResponse agentResponse = mapper.readValue(message.getPayload(), AgentResponse.class);
//				run = jobRunService.getJobRun(agentResponse.getRequestId());

			switch (agentResponse.getCommand()) {
			case "heartbeat":
				log.debug("Heartbeat signal received.");
				break;
			case "jobs-stdout":
				processStdoutLog(agentResponse);
				// Currently only commenting out the code so that we can revert
				// back if needed.
				/*
				 * RunLog line = new RunLog(agentResponse.getRequestId(),
				 * agentResponse.getData()); line = runLogRepository.save(line);
				 * String strLine = mapper.writeValueAsString(line);
				 * agentResponse.setData(strLine);
				 * log.debug("Saved {} line to DynamoDB", strLine);
				 * 
				 * //saving job run status when job execution completes if
				 * (line.getLogLine().contains("##OPERATION@@COMPLETED$$")) {
				 * log.debug("Saving run status {}",
				 * agentResponse.getRequestId()); run.setStatus("completed");
				 * run.setLastUpdatedDate(new Date());
				 * jobRunService.saveJobRun(run); }
				 *//**
					 * Send log line to client(s)
					 *//*
					 * log.debug("Sending the event to the topic {}{}, {}"
					 * ,Constants.STREAM_LOG_PREFIX,line.getRunId() , strLine);
					 * this.simpMessagingTemplate.convertAndSend(Constants.
					 * STREAM_LOG_PREFIX+ line.getRunId(), strLine);
					 * 
					 * if (jobLogsQueue.get(agentResponse.getUserName()) !=
					 * null) { Collections.unmodifiableMap(jobLogsQueue.get(
					 * agentResponse.getUserName())).forEach((sessionId,
					 * jobRuns) -> { if (jobRuns != null) {
					 * jobRuns.forEach((runId, queue) -> { if (queue != null) {
					 * try { log.debug("Adding line {} to logs queue",
					 * agentResponse.toString()); queue.put(agentResponse); }
					 * catch (InterruptedException e) { // TODO Auto-generated
					 * catch block e.printStackTrace(); } } }); } });
					 * 
					 * if
					 * (line.getLogLine().contains("##OPERATION@@COMPLETED$$"))
					 * { jobLogsQueue.get(agentResponse.getUserName()).forEach((
					 * sessionId, jobRuns) -> { if (jobRuns != null) { log.
					 * debug("Logs completed. Removing logs queue for run id: {}"
					 * , agentResponse.getRequestId());
					 * jobRuns.remove(agentResponse.getRequestId());
					 * run.setStatus("completed"); run.setLastUpdatedDate(new
					 * Date()); jobRunService.saveJobRun(run); } }); } }
					 */
				break;
			case "jobs-events":
				run = jobRunService.getJobRun(agentResponse.getRequestId());
//				Job job = jobService.findById("" + run.getJobId());
				log.debug("Inside jobs-events " + run.getJobId() + " Status : " + run.getStatus());
				
//				CumulusUserNotificationSettings settings = userService.getNotificationSettings(job.getCreatedBy());

				LiberateEvent liberateEvent = mapper.readValue(agentResponse.getData(), LiberateEvent.class);
				liberateEvent.setUserId(run.getCreatedBy());
				liberateEvent.setJobRun(run);
				
				log.debug("Liberate Event type " + liberateEvent.getEventType());
				// Vikramaditya: JobRun status update using Event is not necessary. 
				// The code below will be on observation
				/*if (run.getStatus().equals(LiberateEvents.cancelled)) {
					liberateEvent.setEventType(LiberateEvents.cancelled.toString());
				} else if (!run.getStatus().equals(liberateEvent.getEventType())
						&& LiberateEvents.valueOf(liberateEvent.getEventType()) != LiberateEvents.disconnect) {
					if (!(LiberateEvents.valueOf(liberateEvent.getEventType()) == LiberateEvents.failed
							&& liberateEvent.getEventClass().equals("Liberate.Cell.Failed"))) {
						// do not update status if the event class is
						// Liberate.Cell.Failed and event type is failed
						run.setStatus(liberateEvent.getEventType());
						run.setLastUpdatedDate(new Date());

						log.debug("Updating job run status to {}", liberateEvent.getEventType());

						jobRunService.saveJobRun(run);
					}
				}*/

				liberateEvent = eventRepository.save(liberateEvent);
				//TODO
				//need to handle this as part of user settings and notification
				String strLiberateEvent = mapper.writeValueAsString(liberateEvent);
				agentResponse.setData(strLiberateEvent);

				log.debug("Saved {} event to DB", strLiberateEvent);

				/**
				 * Send the events to web client(s)
				 */
				log.debug("Sending the event to the topic {}{}, {}", Constants.STREAM_EVENT_PREFIX, run.getId(),
						strLiberateEvent);
				this.simpMessagingTemplate.convertAndSend(Constants.STREAM_EVENT_PREFIX + run.getId(),
						strLiberateEvent);

				if (jobEventsQueue.get(agentResponse.getUserName()) != null) {
					Collections.unmodifiableMap(jobEventsQueue.get(agentResponse.getUserName()))
							.forEach((sessionId, jobRuns) -> {
								if (jobRuns != null) {
									jobRuns.forEach((runId, queue) -> {
										if (queue != null) {
											try {
												log.debug("Adding event [{}] to events queue",
														agentResponse.toString());
												queue.put(agentResponse);
											} catch (InterruptedException e) {
												// TODO Auto-generated catch
												// block
												e.printStackTrace();
											}
										}
									});
								}
							});

					if (liberateEvent.getEventType().equals(LiberateEvents.disconnect.value())) {
						jobEventsQueue.get(agentResponse.getUserName()).forEach((sessionId, jobRuns) -> {
							if (jobRuns != null) {
								log.debug("Run disconnected. Removing events queue for run id: {}",
										agentResponse.getRequestId());
								jobRuns.remove(agentResponse.getRequestId());
							}
						});
					}
				}

				break;
			case "kill":
				run = jobRunService.getJobRun(agentResponse.getRequestId());
				if (agentResponse.isSuccess() && run != null) {
					log.debug("Job cancelled for run id: {}", agentResponse.getRequestId());
				}
			default:
				// This line has been commented as any way the there won't be
				// any job with this request id
				// run = jobRunService.getJobRun(agentResponse.getRequestId());
				DeferredResult<AgentResponse> response = responses.get(agentResponse.getRequestId());
				log.debug("Response exists: {}", response != null);
				if (response != null) {
					responses.remove(agentResponse.getRequestId());
					response.setResult(agentResponse);

					log.info("Response set...");
				}
				break;
			}
		} catch (Exception e) {
			log.error("Error:", e);
		}
	}

	public void sendCommand(AgentCommand command, DeferredResult<AgentResponse> response) throws IOException {
		if (response != null) {
			responses.put(command.getRequestId(), response);
		}

		synchronized (session) {
			session.sendMessage(new TextMessage(command.toString()));
		}
	}

	public void sendCommand(AgentCommand command) throws IOException {
		sendCommand(command, null);
	}

	public void sendKillCommand(JobRun jobRun, String userName, DeferredResult<AgentResponse> response)
			throws IOException {
		AgentCommand command = new AgentCommand("" + jobRun.getId(), userName, "kill", null, null, null);
		responses.put(command.getRequestId(), response);

		synchronized (session) {
			session.sendMessage(new TextMessage(command.toString()));
		}
	}

	public boolean isConnected() {
		return true;
	}

	public PublisherService getLogsPublisher(String userName, String sessionId, String jobRunId) {
		Map<String, Map<String, BlockingQueue<AgentResponse>>> sessionMap = jobLogsQueue.get(userName);
		if (sessionMap == null) {
			sessionMap = new HashMap<>();
			jobLogsQueue.put(userName, sessionMap);
		}

		Map<String, BlockingQueue<AgentResponse>> jobRuns = sessionMap.get(sessionId);
		if (jobRuns == null) {
			jobRuns = new HashMap<>();
			sessionMap.put(sessionId, jobRuns);
		}

		BlockingQueue<AgentResponse> queue = new LinkedBlockingQueue<>();
		jobRuns.put(jobRunId, queue);

		return new PublisherService(logsExecutor, "logs", "##OPERATION@@COMPLETED$$,##OPERATION@@CANCELLED$$,##OPERATION@@FAILED$$", queue);
	}

	public void removeLogsPublisher(String userName, String sessionId, String jobRunId) {
		Map<String, Map<String, BlockingQueue<AgentResponse>>> sessionMap = jobLogsQueue.get(userName);
		if (sessionMap == null) {
			log.debug("No sessions for {}. Returning", userName);
			return;
		}

		Map<String, BlockingQueue<AgentResponse>> jobRuns = sessionMap.get(sessionId);
		if (jobRuns == null) {
			log.debug("No job runs for session {}. Returning", sessionId);
			return;
		}

		if (jobRuns.get(jobRunId) != null) {
			log.debug("Removing logs queue for {}", jobRunId);
			jobRuns.remove(jobRunId);
		}
	}

	public PublisherService getEventsPublisher(String userName, String sessionId, String jobRunId) {
		Map<String, Map<String, BlockingQueue<AgentResponse>>> sessionMap = jobEventsQueue.get(userName);
		if (sessionMap == null) {
			sessionMap = new HashMap<>();
			jobEventsQueue.put(userName, sessionMap);
		}

		Map<String, BlockingQueue<AgentResponse>> jobRuns = sessionMap.get(sessionId);
		if (jobRuns == null) {
			jobRuns = new HashMap<>();
			sessionMap.put(sessionId, jobRuns);
		}

		BlockingQueue<AgentResponse> queue = new LinkedBlockingQueue<>();
		jobRuns.put(jobRunId, queue);

		return new PublisherService(eventsExecutor, "events", "disconnect", queue);
	}

	public void removeEventsPublisher(String userName, String sessionId, String jobRunId) {
		Map<String, Map<String, BlockingQueue<AgentResponse>>> sessionMap = jobEventsQueue.get(userName);
		if (sessionMap == null) {
			log.debug("No sessions for {}. Returning", userName);
			return;
		}

		Map<String, BlockingQueue<AgentResponse>> jobRuns = sessionMap.get(sessionId);
		if (jobRuns == null) {
			log.debug("No job runs for session {}. Returning", sessionId);
			return;
		}

		if (jobRuns.get(jobRunId) != null) {
			log.debug("Removing events queue for {}", jobRunId);
			jobRuns.remove(jobRunId);
		}
	}

	/**
	 * @param userName
	 * @param sessionId
	 * @return
	 * This method reads data from globalEventsQueue, updates it and sends back to SseController.
	 */
	public StatusPublisherService getGlobalEventsPublisher(String userName, String sessionId) {
		log.debug("Inside getGlobalEventsPublisher");
		Map<String, BlockingQueue<JobRun>> sessionMap = globalEventsQueue.get(userName);
		if (sessionMap == null) {
			sessionMap = new HashMap<>();
			globalEventsQueue.put(userName, sessionMap);
		}

		BlockingQueue<JobRun> queue = sessionMap.get(sessionId);
		if (queue == null) {
			log.debug("Queue is null");
			queue = new LinkedBlockingQueue<>();
			sessionMap.put(sessionId, queue);
		}
		log.debug("Exiting getGlobalEventsPublisher");
		return new StatusPublisherService(globalEventsExecutor, "global events","completed,failed,cancelled", queue);
	}

	@Override
	public void setApplicationEventPublisher(ApplicationEventPublisher publisher) {
		this.publisher = publisher;
	}

	/*
	 * Method to process the stdout logs that are sent from the agent. Simply
	 * Send that to the UI.
	 */
	
	/**
	 * @param response
	 * @throws Exception
	 * CMLS-647 | trigger notifications based on Job status
	 */
	private void processStdoutLog(AgentResponse response) throws Exception {
		final ObjectMapper mapper = new ObjectMapper();
		String userName = response.getUserName();
		RunLog line = new RunLog(response.getRequestId(), response.getData());
		// line = runLogRepository.save(line);
		String strLine = mapper.writeValueAsString(line);
		response.setData(strLine);
		if (response.getData().contains("##OPERATION@@COMPLETED$$")) {
			JobRun run = jobRunService.getJobRun(response.getRequestId());
			if (!run.getStatus().equals(LiberateStatus.completed.toString())) {
				log.debug("Saving run status {}", response.getRequestId());
				CumulusUserNotificationSettings settings = userService.getNotificationSettings(run.getCreatedBy());
				run.setStatus("completed");
				run.setLastUpdatedDate(new Date());
				Map<String, String> browserNotificationMap = setBrowserNotification(run, LiberateStatus.completed.toString(),
						settings.canNotifyBrowser(LiberateStatus.valueOf(run.getStatus())));
				run.setShowOnBrowser(browserNotificationMap);
				Map<String, String> isRead = run.isRead();
				isRead.put(LiberateStatus.completed.toString(), "false");
				run.setRead(isRead);
				jobRunService.saveJobRun(run);
				log.debug("Publishing events for JobRun Status : Completed "
						+ settings.canNotifyBrowser(LiberateStatus.valueOf(run.getStatus())));
				statusNotificationSuccessMap.remove(run.getId());
				log.debug("Completed: Can notify browser " + settings.canNotifyBrowser(LiberateStatus.valueOf(run.getStatus())));
				if (settings.canNotifyBrowser(LiberateStatus.valueOf(run.getStatus()))) {
					log.debug("Completed: Notifying browser");
					sendBrowserNotifications(run, userName);
				}
				publisher.publishEvent(run);
			}
		}else if(response.getData().contains("##OPERATION@@FAILED$$")){
			JobRun run = jobRunService.getJobRun(response.getRequestId());
			if (!run.getStatus().equals(LiberateStatus.failed.toString())) {
				log.debug("Saving run status {}", response.getRequestId());
				CumulusUserNotificationSettings settings = userService.getNotificationSettings(run.getCreatedBy());
				run.setStatus("failed");
				run.setLastUpdatedDate(new Date());
				Map<String, String> browserNotificationMap = setBrowserNotification(run, LiberateStatus.failed.toString(),
						settings.canNotifyBrowser(LiberateStatus.valueOf(run.getStatus())));
				run.setShowOnBrowser(browserNotificationMap);
				Map<String, String> isRead = run.isRead();
				isRead.put(LiberateStatus.failed.toString(), "false");
				run.setRead(isRead);
				jobRunService.saveJobRun(run);
				log.debug("Publishing events for JobRun Status : Failed "
						+ settings.canNotifyBrowser(LiberateStatus.valueOf(run.getStatus())));
				statusNotificationSuccessMap.remove(run.getId());
				log.debug("failed: Can notify browser " + settings.canNotifyBrowser(LiberateStatus.valueOf(run.getStatus())));
				if (settings.canNotifyBrowser(LiberateStatus.valueOf(run.getStatus()))) {
					log.debug("failed: Notifying browser");
					sendBrowserNotifications(run, userName);
				}
				publisher.publishEvent(run);
			}
		}else if (response.getData().contains("##OPERATION@@CANCELLED$$")) {
			JobRun run = jobRunService.getJobRun(response.getRequestId());
			if (!run.getStatus().equals(LiberateStatus.cancelled.toString())) {
				log.debug("Saving run status {}", response.getRequestId());
				CumulusUserNotificationSettings settings = userService.getNotificationSettings(run.getCreatedBy());
				run.setStatus("cancelled");
				run.setLastUpdatedDate(new Date());
				Map<String, String> browserNotificationMap = setBrowserNotification(run, LiberateStatus.cancelled.toString(),
						settings.canNotifyBrowser(LiberateStatus.valueOf(run.getStatus())));
				run.setShowOnBrowser(browserNotificationMap);
				Map<String, String> isRead = run.isRead();
				isRead.put(LiberateStatus.cancelled.toString(), "false");
				run.setRead(isRead);
				jobRunService.saveJobRun(run);
				log.debug("Publishing events for JobRun Status : Cancelled "
						+ settings.canNotifyBrowser(LiberateStatus.valueOf(run.getStatus())));
				statusNotificationSuccessMap.remove(run.getId());
				log.debug("cancelled: Can notify browser " + settings.canNotifyBrowser(LiberateStatus.valueOf(run.getStatus())));
				if (settings.canNotifyBrowser(LiberateStatus.valueOf(run.getStatus()))) {
					log.debug("cancelled: Notifying browser");
					sendBrowserNotifications(run, userName);
				}
				publisher.publishEvent(run);
			}
		}else{
			// statusNotificationSuccessMap: Is updated with every new job run and is cleared after the job ends.
			
			JobRun run = jobRunService.getJobRun(response.getRequestId());
			CumulusUserNotificationSettings settings = userService.getNotificationSettings(run.getCreatedBy());
			log.debug("Run status = " + run.getStatus());
			if(statusNotificationSuccessMap.isEmpty() && !run.getStatus().equals(LiberateStatus.started.toString())){
				log.debug("statusNotificationSuccessMap " + statusNotificationSuccessMap.isEmpty());
				statusNotificationSuccessMap.put(run.getId(), true);
				run.setStatus("started");
				run.setLastUpdatedDate(new Date());
				Map<String, String> browserNotificationMap = setBrowserNotification(run,LiberateStatus.started.toString(), settings.canNotifyBrowser(LiberateStatus.valueOf(run.getStatus())));
				run.setShowOnBrowser(browserNotificationMap);
				Map<String, String> isRead = run.isRead();
				isRead.put(LiberateStatus.started.toString(),"false");
				run.setRead(isRead);
				jobRunService.saveJobRun(run);
				log.debug("started: Can notify browser " + settings.canNotifyBrowser(LiberateStatus.valueOf(run.getStatus())));
				if(settings.canNotifyBrowser(LiberateStatus.valueOf(run.getStatus()))){
					log.debug("started: Notifying browser");
					sendBrowserNotifications(run, userName);
				}
				publisher.publishEvent(run);
			}else if(!statusNotificationSuccessMap.containsKey(run.getId())  && !run.getStatus().equals(LiberateStatus.started.toString())){
				log.debug("statusNotificationSuccessMap " + statusNotificationSuccessMap.containsKey(run.getId()) );
				statusNotificationSuccessMap.put(run.getId(), true);
				run.setStatus("started");
				run.setLastUpdatedDate(new Date());
				Map<String, String> browserNotificationMap = setBrowserNotification(run,LiberateStatus.started.toString(), settings.canNotifyBrowser(LiberateStatus.valueOf(run.getStatus())));
				run.setShowOnBrowser(browserNotificationMap);
				Map<String, String> isRead = run.isRead();
				isRead.put(LiberateStatus.started.toString(),"false");
				run.setRead(isRead);
				jobRunService.saveJobRun(run);
				log.debug("started: Can notify browser " + settings.canNotifyBrowser(LiberateStatus.valueOf(run.getStatus())));
				if(settings.canNotifyBrowser(LiberateStatus.valueOf(run.getStatus()))){
					log.debug("started: Notifying browser");
					sendBrowserNotifications(run, userName);
				}
				publisher.publishEvent(run);
			}
			
		}
		log.debug("Sending log to topic {} {}, {}", Constants.STREAM_LOG_PREFIX, line.getRunId(), strLine);
		this.simpMessagingTemplate.convertAndSend(Constants.STREAM_LOG_PREFIX + line.getRunId(), strLine);
	}

	/**
	 * @param run
	 * @param status
	 * @param val
	 * @return
	 * The method updates browserNotification map (here, 'map') with key as Job Status and value as 'true'
	 */
	private Map<String, String> setBrowserNotification(JobRun run, String status, Boolean val) {
		Map<String, String> map = run.getShowOnBrowser();
		log.debug("Inside setBrowserNotification");
		if ( map.isEmpty() || null != map) {
			log.debug("Map is empty " + val);
			map.put(status,String.valueOf(val));
		}
		log.debug("Exiting setBrowserNotification");
		return map;
	}
	
	/**
	 * @param jobRun
	 * @param userName
	 * The method updates globalEventQueue.
	 */
	private void sendBrowserNotifications(JobRun jobRun, String userName){
		log.debug("Inside sendBrowserNotification " + globalEventsQueue.toString());
		if (globalEventsQueue.get(userName) != null) {
			Collections.unmodifiableMap(globalEventsQueue.get(userName))
					.forEach((sessionId, queue) -> {
						if (queue != null) {
							try {
								log.debug("Adding status [{}] to global status queue", jobRun.toString());
								queue.put(jobRun);
							} catch (InterruptedException e) {
								e.printStackTrace();
							}
						}
					});
		}
		log.debug("Exiting sendBrowserNotification");
	}
	// Not needed anymore as we are not saving logs into the DB anymore. See
	// above method.
	// This is for batch saving of logs
	/*
	 * We need to buffer lines that are returned in the agent response till we
	 * have enough lines that would be sent to dynamodb in a batch. This is not
	 * a foolproof method to buffer lines. A better way to do the same would be
	 * to buffer bytes rather than lines because individual lines could be
	 * really large and if such large lines are sent to DynamoDB in a batch, the
	 * issue of DynamoDB throughput would not be resolved.
	 * 
	 * We need to figure out a good number of STDOUT_LINE_BUFFER_LIMIT that
	 * would result in no DynamoDB exception for most jobs.
	 */
	/*
	 * private void processStdoutLog(AgentResponse response) throws Exception {
	 * // Put the log in the log map and if the stack is full for a given // job
	 * id then flush it to DB and send to UI if
	 * (!stdoutLogsMap.keySet().contains(response.getRequestId())) {
	 * stdoutLogsMap.put(response.getRequestId(), new ArrayList<String>()); }
	 * stdoutLogsMap.get(response.getRequestId()).add(response.getData()); if
	 * (stdoutLogsMap.get(response.getRequestId()).size() >=
	 * STDOUT_LINE_BUFFER_LIMIT ||
	 * response.getData().contains("##OPERATION@@COMPLETED$$") ||
	 * response.getData().contains("##OPERATION@@KILLED$$")) { final
	 * ObjectMapper mapper = new ObjectMapper(); final JobRun run =
	 * jobRunService.getJobRun(response.getRequestId()); String data =
	 * String.join("\n", stdoutLogsMap.get(response.getRequestId())); RunLog
	 * line = new RunLog(response.getRequestId(), data);
	 * 
	 * line = runLogRepository.save(line); String strLine =
	 * mapper.writeValueAsString(line); response.setData(strLine);
	 * log.debug("Saved {} line to DynamoDB", strLine);
	 * 
	 * //saving job run status when job execution completes if
	 * (response.getData().contains("##OPERATION@@COMPLETED$$")) {
	 * log.debug("Saving run status {}", response.getRequestId());
	 * run.setStatus("completed"); run.setLastUpdatedDate(new Date());
	 * jobRunService.saveJobRun(run); }
	 *//**
		 * Send log line to client(s)
		 *//*
		 * log.debug("Sending the event to the topic {}{}, {}",Constants.
		 * STREAM_LOG_PREFIX,line.getRunId() , strLine);
		 * this.simpMessagingTemplate.convertAndSend(Constants.
		 * STREAM_LOG_PREFIX+ line.getRunId(), strLine);
		 * 
		 * if (jobLogsQueue.get(response.getUserName()) != null) {
		 * Collections.unmodifiableMap(jobLogsQueue.get(response.getUserName()))
		 * .forEach((sessionId, jobRuns) -> { if (jobRuns != null) {
		 * jobRuns.forEach((runId, queue) -> { if (queue != null) { try {
		 * log.debug("Adding line {} to logs queue", response.toString());
		 * queue.put(response); } catch (InterruptedException e) { // TODO
		 * Auto-generated catch block e.printStackTrace(); } } }); } });
		 * 
		 * if (line.getLogLine().contains("##OPERATION@@COMPLETED$$")) {
		 * jobLogsQueue.get(response.getUserName()).forEach((sessionId, jobRuns)
		 * -> { if (jobRuns != null) {
		 * log.debug("Logs completed. Removing logs queue for run id: {}",
		 * response.getRequestId()); jobRuns.remove(response.getRequestId());
		 * run.setStatus("completed"); run.setLastUpdatedDate(new Date());
		 * jobRunService.saveJobRun(run); } }); } }
		 * stdoutLogsMap.get(response.getRequestId()).clear(); } }
		 */
}
