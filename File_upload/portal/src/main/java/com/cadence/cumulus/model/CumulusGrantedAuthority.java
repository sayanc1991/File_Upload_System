package com.cadence.cumulus.model;

import org.springframework.security.core.GrantedAuthority;

public class CumulusGrantedAuthority implements GrantedAuthority {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -7565477954058197716L;

	private String role;
	
	private Long id;

	@Override
	public String getAuthority() {
		// TODO Auto-generated method stub
		return role;
	}

	public Long getId() {
		return id;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CumulusGrantedAuthority other = (CumulusGrantedAuthority) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "{role=" + role + ", id=" + id + "}";
	}

	public CumulusGrantedAuthority(Long id, String role) {
		super();
		this.role = role;
		this.id = id;
	}
	
}
