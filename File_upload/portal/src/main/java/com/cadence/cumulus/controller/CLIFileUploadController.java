package com.cadence.cumulus.controller;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.math.BigInteger;
import java.net.URLDecoder;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.Date;
import java.util.UUID;

import javax.annotation.PostConstruct;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.SystemUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.cadence.cumulus.common.ServiceConstants;
import com.cadence.cumulus.core.TokenService;
import com.cadence.cumulus.dto.BulkActionPayload;
import com.cadence.cumulus.dto.DestinationValidation;
import com.cadence.cumulus.dto.FileInfo;
import com.cadence.cumulus.dto.SingleActionPayload;
import com.cadence.cumulus.dto.TokenStatus;
import com.cadence.cumulus.model.ResponseVO;
import com.cadence.cumulus.service.OSFileManager;
import com.cadence.cumulus.utility.Utility;



/**
 * Controller with REST API's required for the CLI client utility to perform
 * file/folder upload
 * 
 * @author anirban
 *
 */
@RestController
@RequestMapping(value = "/api/fm/upload")
public class CLIFileUploadController {

	private static final Logger LOG = LoggerFactory.getLogger(CLIFileUploadController.class);

	@Value("${cli.upload.file.extension}")
	private String uploadFileExt;

	@Value("${cli.upload.tmp.dir}")
	private String temporaryDirectory;

	@Autowired
	private OSFileManager fm;

	@Autowired
	private Utility util;

	@Autowired
	private TokenService tokenService;

	@Value("${file.upload.api.authz}")
	private String fileUploadPermission;

	@Value("${user.home.base}")
	private String userBase;

	@Value("${cli.session.inactivity.seconds}")
	private Integer inactivityInterval;

	@PostConstruct
	private void init() {
		if (SystemUtils.IS_OS_WINDOWS) {
			Path path = Paths.get(System.getProperty("user.home"));
			this.userBase = path.getParent().toString();
		}
		LOG.info("User home base: {}", userBase);
	}

	/**
	 * Validate whether the destination provided for uploading file is valid and the
	 * user has write access
	 * 
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@GetMapping(value = "/validate/**", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> validateDestinationOnServer(HttpServletRequest request) throws Exception {
		HttpSession session = request.getSession();
		String token = (String) session.getAttribute(ServiceConstants.X_AUTH_TOKEN);
		TokenStatus ts = tokenService.verifyToken(token, fileUploadPermission);
		HttpHeaders headers = new HttpHeaders();
		ResponseEntity<?> response = null;
		if (ts.getValidity()) {
			DestinationValidation body = new DestinationValidation();
			String destination = util.extractPathFromPattern(request);
			destination = URLDecoder.decode(destination, "UTF-8");
			SingleActionPayload metadata = new SingleActionPayload();
			metadata.setUserName(ts.getMessage());
			metadata.setDiskResourcePath(destination);
			metadata.setBasePath(userBase);
			
			body.setFolderName(destination);
			boolean validationSuccess = true;
			boolean canWrite = true;
			boolean isFolder = true;
			if (!fm.isResourceFolder(metadata)) {
				body.setValidationResult(false);
				body.setValidationError("Invalid destination path: " + destination + " directory does not exist in your home.");
				LOG.debug("Validation result: Error - Invalid path");
				validationSuccess = false;
				isFolder = false;
			}
			if (validationSuccess && (!fm.isResourceWritable(metadata))) {
				body.setValidationResult(false);
				body.setValidationError("Permission denied: You are not allowed to write in directory: " + destination);
				LOG.debug("Validation result: Destination folder does not have write permission for the user");
				canWrite = false;
			}
			if (canWrite && isFolder) {
				body.setValidationResult(true);
				session = request.getSession(true);
				session.setMaxInactiveInterval(inactivityInterval);
				session.setAttribute(ServiceConstants.USERNAME, ts.getMessage());
				session.setAttribute(ServiceConstants.SESSION_ID, session.getId());
				headers.set(ServiceConstants.USERNAME, ts.getMessage());
				headers.set(ServiceConstants.SESSION_ID, session.getId());
				
				LOG.info("Session: {} created for user: {} at: {}", session.getId(), ts.getMessage(),
						new Date(session.getCreationTime()));
			}
			LOG.debug("Validation result: {}", body.toString());
			response = new ResponseEntity<DestinationValidation>(body, headers, HttpStatus.OK);
			
			return response;
		} else {
			response =   handleValidationResponse(ts);
		}
		return response;
	}

	@PostMapping(path = "/save/**", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> saveFileChunk(HttpServletRequest request, @RequestParam Long size,
			@RequestParam Long lastSize) throws Exception {
		TokenStatus body = new TokenStatus();
		String relativeFilePath = util.extractPathFromPattern(request);
		String userName = (String) request.getParameterValues(ServiceConstants.USERNAME)[0];
		String osAdjustedFileName = FilenameUtils.getName(relativeFilePath);
		osAdjustedFileName =  URLDecoder.decode(osAdjustedFileName, "UTF-8");
		String fileName = FilenameUtils.separatorsToSystem(osAdjustedFileName);
		String osAdjustedPath = FilenameUtils.getPath(relativeFilePath);
		osAdjustedPath = URLDecoder.decode(osAdjustedPath, "UTF-8");
		String destination = FilenameUtils.separatorsToSystem(osAdjustedPath);
		LOG.debug("Save File Destination" + destination);

		BulkActionPayload upload = new BulkActionPayload();
		upload.setBasePath(userBase);
		upload.setUserName(userName);
		upload.setDestinationPath(StringUtils.hasText(destination) ? destination : "");
		upload.setSingleFileName(fileName + uploadFileExt);
		File uploadFile = fm.createMockFileResource(upload);

		SingleActionPayload uploadSizeMetadata = new SingleActionPayload();
		uploadSizeMetadata.setUserName(userName);
		uploadSizeMetadata.setDiskResourcePath(uploadFile.getAbsolutePath());
		BigInteger uploadSize = new BigInteger(fm.doesUploadedResourceExists(uploadSizeMetadata));

		BigInteger zeroSize = new BigInteger("0");
		LOG.debug("IN SAVE FILE last size=" + lastSize + "uploadSize" + uploadSize + "zerosize" + zeroSize);
		if (uploadSize.equals(zeroSize) && lastSize > 0) {
			LOG.debug("Do nothing as File is cancelled");
		} else {
			LOG.debug("Inside else in savefile");
			parseRequestBody(request.getInputStream(), userName, fileName, size, destination,
					uploadFile.getAbsolutePath());
		}
		body.setCode(HttpStatus.NO_CONTENT.value());
		body.setMessage("");
		ResponseEntity<?> response = handleGenericResponse(body);
		return response;
	}

	@GetMapping(path = "/info/**", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> uploadedFileStats(HttpServletRequest request, @RequestParam Long lastModified)
			throws Exception {
		ResponseEntity<?> response = null;
		String relativeFilePath = util.extractPathFromPattern(request);
		String userName = (String) request.getParameterValues(ServiceConstants.USERNAME)[0];

		String osAdjustedFileName = FilenameUtils.getName(relativeFilePath);
		osAdjustedFileName =  URLDecoder.decode(osAdjustedFileName, "UTF-8");
		String fileName = FilenameUtils.separatorsToSystem(osAdjustedFileName);
		LOG.info("Converting file name from: {} to: {}", osAdjustedFileName, fileName);
		String osAdjustedPath = FilenameUtils.getPath(relativeFilePath);
		osAdjustedPath = URLDecoder.decode(osAdjustedPath, "UTF-8");
		String destination = FilenameUtils.separatorsToSystem(osAdjustedPath);
		LOG.info("Converting destination path from: {} to: {}", osAdjustedPath, destination);

		LOG.info("Getting information of file: {} from destination path: {}", fileName, destination);

		Boolean clientModifiedDateCheck = true;
		Boolean outputLastModifiedSame = false;
		BulkActionPayload bulk = new BulkActionPayload();
		bulk.setUserName(userName);
		bulk.setDestinationPath(StringUtils.hasText(destination) ? destination : "");
		bulk.setSingleFileName(fileName);
		bulk.setBasePath(userBase);
		File outputFile = fm.createMockFileResource(bulk);

		SingleActionPayload output = new SingleActionPayload();
		output.setDiskResourcePath(outputFile.getAbsolutePath());
		output.setUserName(userName);
		BigInteger size = new BigInteger(fm.doesUploadedResourceExists(output));

		bulk.setSingleFileName(fileName + uploadFileExt);
		File uploadFile = fm.createMockFileResource(bulk);

		SingleActionPayload upload = new SingleActionPayload();
		upload.setUserName(userName);
		upload.setDiskResourcePath(uploadFile.getAbsolutePath());
		BigInteger uploadSize = new BigInteger(fm.doesUploadedResourceExists(upload));

		// check Moidified Time For Already Exists
		if (clientModifiedDateCheck && fm.doesResourceExist(output)) {
			String lastModifiedAt = fm.lastModifiedAt(output);
			LOG.debug("Last modified time for file " + outputFile.getAbsolutePath() + " is " + lastModified);
			long lastMotifiedInSec = 0;
			try {
				lastMotifiedInSec = Long.parseLong(lastModifiedAt);
			} catch (NumberFormatException e) {
				LOG.debug("Failed to parse last modified time for - " + lastModified);
			}
			LOG.debug("Last modified time for file " + outputFile.getAbsolutePath() + " after parsing "
					+ lastMotifiedInSec + "lastModifiedDate" + lastModified);
			if (!(lastMotifiedInSec /** 1000 */
			< lastModified)) {
				outputLastModifiedSame = true;
				LOG.debug("{} < {} : {}", lastMotifiedInSec * 1000, lastModified, outputLastModifiedSame);
			}

		}
		LOG.debug("UploadFileSize" + uploadSize + "OutputFileSize" + size);
		FileInfo body = new FileInfo();
		body.setOutputFile(outputFile.getName());
		body.setOutputFileSize(size);
		body.setOutputLastModifiedSame(outputLastModifiedSame);
		body.setUploadFile(uploadFile.getName());
		body.setUploadFileSize(uploadSize);
		response = new ResponseEntity<FileInfo>(body, HttpStatus.PARTIAL_CONTENT);
		return response;
	}

	@GetMapping(path = "/process/{action}/**", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> processFileOperation(HttpServletRequest request, @PathVariable String action)
			throws Exception {
		TokenStatus body = new TokenStatus();
		String relativeFilePath = util.extractPathFromPattern(request);
		String userName = (String) request.getParameterValues(ServiceConstants.USERNAME)[0];
		relativeFilePath =  URLDecoder.decode(relativeFilePath, "UTF-8");
		String osAdjustedFileName = FilenameUtils.getName(relativeFilePath);
		String fileName = FilenameUtils.separatorsToSystem(osAdjustedFileName);
		LOG.info("Converting file name from: {} to: {}", osAdjustedFileName, fileName);
		String osAdjustedPath = FilenameUtils.getPath(relativeFilePath);
		osAdjustedPath = URLDecoder.decode(osAdjustedPath, "UTF-8");
		String destination = FilenameUtils.separatorsToSystem(osAdjustedPath);
		LOG.info("Converting destination path from: {} to: {}", osAdjustedPath, destination);

		BulkActionPayload bulk = new BulkActionPayload();
		bulk.setDestinationPath(StringUtils.hasText(destination) ? destination : "");
		bulk.setSingleFileName(fileName);
		bulk.setUserName(userName);
		bulk.setBasePath(userBase);
		File targetFile = fm.createMockFileResource(bulk);

		bulk.setSingleFileName(fileName + uploadFileExt);
		File sourceFile = fm.createMockFileResource(bulk);

		SingleActionPayload remove = new SingleActionPayload();
		remove.setUserName(userName);
		remove.setDiskResourcePath(sourceFile.getAbsolutePath());

		if (action.equalsIgnoreCase("rename")) {
			BulkActionPayload move = new BulkActionPayload();
			move.setUserName(userName);
			move.setDestinationPath(targetFile.getAbsolutePath());
			move.setSourcePaths(Arrays.asList(sourceFile.getAbsolutePath()));
			fm.moveResourceByUser(move);
		}
		if (action.equalsIgnoreCase("delete")) {
			fm.removeResourceByUser(remove);
		}
		LOG.debug("SourcePath=" + sourceFile.getAbsolutePath());
		// Double Check If file exists or not and retry 3 times
		for (int i = 0; i < 3; i++) {
			if (fm.doesResourceExist(remove)) {
				LOG.debug("Inside Exist of File" + sourceFile.getAbsolutePath() + "Retry=" + i);
				fm.removeResourceByUser(remove);
			} else {
				LOG.debug("In Break of Retry=" + i + "for File" + sourceFile.getAbsolutePath());
				break;
			}
		}
		body.setCode(HttpStatus.NO_CONTENT.value());
		body.setMessage("");
		ResponseEntity<?> response = handleGenericResponse(body);
		return response;
	}
	
	@GetMapping(path = "/complete")
	public ResponseEntity<?> completeUpload(HttpServletRequest request)
			throws Exception {
		Boolean body = true;
		HttpStatus status = HttpStatus.ACCEPTED;
		HttpSession session = request.getSession();
		String userName = (String) request.getParameterValues(ServiceConstants.USERNAME)[0];
		try {
			session.invalidate();
			LOG.debug("Invalidating session: {} as user: {} has completed all upload operations in this session", session.getId(), userName);
		} catch (IllegalStateException e) {
			// might never get called
			LOG.debug("Session: {} for user: {} failed invalidation: {}", session.getId(), userName, e);
			body = false;
			status = HttpStatus.CONFLICT;
		}
		ResponseEntity<Boolean> response = new ResponseEntity<Boolean>(body, status);
		
		return response;
	}

	private ResponseEntity<ResponseVO> handleValidationResponse(TokenStatus status) throws IOException {
		ResponseVO body = new ResponseVO();
		body.setStatus(HttpStatus.valueOf(status.getCode()).name());
		body.setValue(status.getMessage());
		ResponseEntity<ResponseVO> response = new ResponseEntity<ResponseVO>(body, HttpStatus.NON_AUTHORITATIVE_INFORMATION);
		return response;
	}
	private ResponseEntity<ResponseVO> handleGenericResponse(TokenStatus status) throws IOException {
		ResponseVO body = new ResponseVO();
		body.setStatus(HttpStatus.valueOf(status.getCode()).name());
		body.setValue(status.getMessage());
		ResponseEntity<ResponseVO> response = ResponseEntity.status(status.getCode()).body(body);
		return response;
	}
	
	private void parseRequestBody(InputStream filecontent, String userName, String filename, Long size,
			String destination, String outputFile) throws Exception {
		OutputStream out = null;
		try {
			byte[] bytes = IOUtils.toByteArray(filecontent, size);
			LOG.debug("size=" + bytes.length);
			String tmpFilename = UUID.randomUUID().toString();
			BulkActionPayload bulk = new BulkActionPayload();
			bulk.setDestinationPath(temporaryDirectory);
			bulk.setSingleFileName(tmpFilename);
			bulk.setUserName("");// userName
			bulk.setBasePath("");// userBase
			File bufferFile = fm.createMockFileResource(bulk);
			FileUtils.writeByteArrayToFile(bufferFile, bytes);
			try {
				// added below line because userBase is not being set before this step as
				// expected through previous implementation
				bulk.setBasePath(userBase);
				bulk.setDestinationPath(destination);
				bulk.setSingleFileName(filename);
				bulk.setUserName(userName);
				File file = fm.createMockFileResource(bulk);
				String parentPath = file.getAbsoluteFile().getParent();
				LOG.debug("Parent Path" + parentPath);
				SingleActionPayload parent = new SingleActionPayload();
				parent.setUserName(userName);
				parent.setDiskResourcePath(parentPath);
				if (!fm.doesResourceExist(parent)) {
					LOG.debug("Creating Path" + parentPath);
					fm.createDirectoryResourceByUser(parent);
				}
				bulk.setSourcePaths(Arrays.asList(bufferFile.getAbsolutePath()));
				bulk.setDestinationPath(outputFile);
				fm.appendToFileResource(bulk);
			} catch (Exception ex) {
				LOG.debug("Unable to Merge  File: {} Error: {}", bufferFile, ex);
				throw ex;
			} finally {
				try {
					LOG.debug("Deleting File=" + bufferFile);
					FileUtils.deleteQuietly(bufferFile);
				} catch (Exception e) {
					LOG.debug("Could not delete tmp file used for uploading file. Reason: {}", e.getMessage());
				}
			}
		} catch (FileNotFoundException fne) {
			LOG.debug(
					"You either did not specify a file to upload or are trying to upload a file to a protected or nonexistent location. ERROR: {}",
					fne);
		} finally {
			if (out != null) {
				out.close();
			}
			if (filecontent != null) {
				filecontent.close();
			}
		}
	}

}
