package com.cadence.cumulus.model;

import org.json.JSONObject;
import org.springframework.util.StringUtils;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class AgentResponse {
    private String requestId;

    private String command;

    private String userName;

    private String data;

    private boolean success;

    public AgentResponse() {
    }

    public AgentResponse(String requestId, String userName, String command, boolean success) {
        this.requestId = requestId;
        this.userName = userName;
        this.command = command;
        this.success = success;
    }

    public AgentResponse(String requestId, String userName, String command, String data) {
        this.requestId = requestId;
        this.userName = userName;
        this.command = command;
        this.data = data;
        this.success = true;
    }

    public AgentResponse(String requestId, String userName, String command, String data, boolean success) {
        this.requestId = requestId;
        this.userName = userName;
        this.command = command;
        this.data = data;
        this.success = success;
    }

    public String getRequestId() {
        return requestId;
    }

    public void setRequestId(String requestId) {
        this.requestId = requestId;
    }

    public String getCommand() {
        return command;
    }

    public void setCommand(String command) {
        this.command = command;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    @Override
    public String toString() {
        try {
            return new ObjectMapper().writeValueAsString(this);
        } catch (JsonProcessingException e) {
            JSONObject thisObject = new JSONObject();
            thisObject.put("requestId", this.requestId);
            thisObject.put("userName", userName);
            thisObject.put("command", this.command);
            thisObject.put("success", this.success);

            if (!StringUtils.isEmpty(data)) {
                thisObject.put("data:", this.data);
            }

            return thisObject.toString();
        }
    }
}
