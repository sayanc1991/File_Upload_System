package com.cadence.cumulus.model;

import java.util.Date;

import javax.persistence.AssociationOverride;
import javax.persistence.AssociationOverrides;
import javax.persistence.Convert;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;

/**
 * Entity which maps the various event types and notification channels
 * 
 * @author zabeer
 *
 */
@JsonInclude(JsonInclude.Include.NON_EMPTY)
@Entity
@Table(name = "UserEventNotification")
@AssociationOverrides({
		@AssociationOverride(name = "userEventNotificationId.user", joinColumns = @JoinColumn(name = "UserId")),
		@AssociationOverride(name = "userEventNotificationId.statusType", joinColumns = @JoinColumn(name = "EventTypeId")),
		@AssociationOverride(name = "userEventNotificationId.notificationChannel", joinColumns = @JoinColumn(name = "NotificationChannelId")) })
public class UserEventNotification {

	private UserEventNotificationId userEventNotificationId = new UserEventNotificationId();

	private Date createdOn;

	private Date lastModifiedOn;

	private boolean activeFlag;

	@EmbeddedId
	public UserEventNotificationId getUserEventNotificationId() {
		return userEventNotificationId;
	}

	public void setUserEventNotificationId(UserEventNotificationId userEventNotificationId) {
		this.userEventNotificationId = userEventNotificationId;
	}

	@Transient
	@JsonIgnore
	public User getUser() {
		return userEventNotificationId.getUser();
	}

	public void setUser(User user) {
		userEventNotificationId.setUser(user);
	}

	@Transient
	@JsonIgnore
	public StatusType getStatusType() {
		return userEventNotificationId.getStatusType();
	}

	public void setStatusType(StatusType eventType) {
		userEventNotificationId.setStatusType(eventType);
	}

	@Transient
	@JsonIgnore
	public NotificationChannel getNotificationChannel() {
		return userEventNotificationId.getNotificationChannel();
	}

	public void setNotificationChannel(NotificationChannel notificationChannel) {
		userEventNotificationId.setNotificationChannel(notificationChannel);
	}

	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public Date getLastModifiedOn() {
		return lastModifiedOn;
	}

	public void setLastModifiedOn(Date lastModifiedOn) {
		this.lastModifiedOn = lastModifiedOn;
	}

	@Convert(converter = BooleanToCharConverter.class)
	public boolean isActiveFlag() {
		return activeFlag;
	}

	public void setActiveFlag(boolean activeFlag) {
		this.activeFlag = activeFlag;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((userEventNotificationId == null) ? 0 : userEventNotificationId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		UserEventNotification other = (UserEventNotification) obj;
		if (userEventNotificationId == null) {
			if (other.userEventNotificationId != null)
				return false;
		} else if (!userEventNotificationId.equals(other.userEventNotificationId))
			return false;
		return true;
	}

}
