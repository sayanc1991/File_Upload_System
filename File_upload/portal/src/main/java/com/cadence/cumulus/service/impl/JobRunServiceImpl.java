package com.cadence.cumulus.service.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.cadence.cumulus.model.JobRun;
import com.cadence.cumulus.repository.mysql.JobRunRepository;
import com.cadence.cumulus.service.JobRunService;

@Component
@Transactional(propagation = Propagation.REQUIRED, rollbackFor = { Exception.class })
public class JobRunServiceImpl implements JobRunService {

    /**
     * Logger
     */
    private static final Logger log = LoggerFactory.getLogger(JobRunServiceImpl.class);

    @Autowired
    private JobRunRepository jobRunRepository;
    
    @Override
    public JobRun saveJobRun(JobRun jobRun) {
        log.debug("Creating jobRun: {}", jobRun);
        return jobRunRepository.save(jobRun);
    }

    @Override
    public void deleteJobRun(JobRun jobRun) {
        jobRunRepository.delete(jobRun);
        log.debug("Deleted job run: "+ jobRun);
    }

    @Override
    public JobRun getJobRun(String jobRunId) {
        log.debug("Retrieving job run with id: {}", jobRunId);

        return jobRunRepository.findOne(Long.parseLong(jobRunId));
    }

    
}
