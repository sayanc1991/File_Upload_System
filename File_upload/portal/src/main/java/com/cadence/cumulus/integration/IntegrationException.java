package com.cadence.cumulus.integration;

/**
 * Exception class to represent exception thrown from the integration layer with UR/OpenDJ etc
 * @author Zabeersultan.Farook
 *
 */
public class IntegrationException extends Exception {

	
	private static final long serialVersionUID = 1L;
	private final String message;

	public IntegrationException(String message, Throwable throwable) {
		super(message, throwable);
		this.message = message;

	}

	public IntegrationException(String message) {
		super(message);
		this.message = message;

	}

	@Override
	public String getMessage() {
		return message;
	}

	@Override
	public String toString() {
		return message;

	}

}
