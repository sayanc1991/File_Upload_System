package com.cadence.cumulus.service;

import java.util.List;

import com.cadence.cumulus.common.CumulusException;
import com.cadence.cumulus.integration.unifiedregistration.vo.UserAuthDetail;
import com.cadence.cumulus.model.Permission;
import com.cadence.cumulus.model.Role;
import com.cadence.cumulus.model.RoleDetail;

/**
 * Service interface hosting all API"s associated with User Management. This would
 * be consumed by the corresponding REST Controller
 * 
 * @author Zabeersultan.Farook
 *
 */
public interface UserManagementService {
	
	/**
	 * Query user information from UR based on the email id passed
	 * @param emailID emailID based on which the user needs to be searched from UR
	 * @return User details from UR for the given email id. Null will be returned if user not found
	 * @throws CumulusException Any exception communicating with UR
	 */
	public UserAuthDetail getUserFromUnifiedRegistration(String emailID) throws CumulusException;
	
	/**
	 * Search users in Cumulus based on attributes 
	 * @param firstName
	 * @param lastName
	 * @param emailID
	 * @param roleID
	 * @return List of users in Cumulus matching the search criteria
	 * @throws CumulusException any exception while performing the search
	 */
	public List<UserAuthDetail> getCumulusUsers(String firstName, String lastName, String emailID, String roleID) throws CumulusException;
	
	/**
	 * Add the new user to Cumulus. This user should be already present in UR/Okta. This will validate if user is present 
	 * in UR and add the user details to Cumulus DB. 
	 * @param userAuthDetail Object with the details of the user and the roles required for the user
	 * @return Updated UserAuthDetail information
	 * @throws CumulusException Exception if any while adding the user to Cumulus
	 */
	public UserAuthDetail addNewUser(UserAuthDetail userAuthDetail) throws CumulusException;
	
	/**
	 * Removes the user from Cumulus and removes cumulus permission in Okta through UR
	 * @param emailId
	 * @param contactId
	 * @throws CumulusException
	 */
	public void removeUser(String emailId, String contactId) throws CumulusException;
	
	/**
	 * Service used to update user in Cumulus. As of now only project, department and roles can ONLY be modified for existing users
	 * @param userAuthDetail Object with the details of the user and the roles required for the user
	 * @param updateStatus flag to update user status.
	 * @return Updated UserAuthDetail information
	 * @throws CumulusException Exception if any while updating the user in Cumulus
	 */
	public UserAuthDetail updateUser(UserAuthDetail userAuthDetail, boolean updateStatus) throws CumulusException;
	
	/**
	 * Service used to update user status in Cumulus.
	 * @param emailId to fetch user details and update status to In-Active.
	 * @param status to update user.
	 * @return Updated User information
	 * @throws CumulusException Exception if any while updating the user in Cumulus
	 */
	public UserAuthDetail updateUser(String emailId, String status) throws CumulusException;

	/**
	 * Returns all available roles configured in the system
	 * @return
	 * @throws CumulusException
	 */
	public Iterable<Role> getAvailableRoles() throws CumulusException;
	
	/**
	 * Returns all available permissions configured in the system
	 * @return
	 * @throws CumulusException
	 */
	public List<Permission> getAvailablePermissions() throws CumulusException;
	
	/**
	 * Add new Role in Cumulus with assigned permissions 
	 * @param role
	 * @return
	 * @throws CumulusException
	 */
	public RoleDetail addNewRole(RoleDetail role) throws CumulusException;
	
	/**
	 * Removes the Role from the system 
	 * @param id
	 * @throws CumulusException
	 */
	public void removeRole(String id) throws CumulusException;
	
	/**
	 * Updates the Role
	 * @param role
	 * @return
	 * @throws CumulusException
	 */
	public RoleDetail updateRole(RoleDetail role) throws CumulusException;
	
	
	/**
	 * Returns all available roles along with details configured in the system
	 * @return
	 * @throws CumulusException
	 */
	public List<RoleDetail> getAvailableRoleDetails() throws CumulusException;
	

}
