package com.cadence.cumulus.controller.api;

import static org.springframework.http.MediaType.TEXT_PLAIN_VALUE;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.ProcessBuilder.Redirect;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import javax.activation.MimetypesFileTypeMap;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.cadence.cumulus.common.LoginException;
import com.cadence.cumulus.common.ServiceConstants;
import com.cadence.cumulus.controller.FileManagerController.ProcessResult;
import com.cadence.cumulus.controller.api.handler.BasicAuthenticationHandler;
import com.cadence.cumulus.controller.api.handler.FileManagerAPIHandler;
import com.cadence.cumulus.core.TokenService;
import com.cadence.cumulus.core.security.UserAuthorizationServiceImpl;
import com.cadence.cumulus.dto.TokenStatus;
import com.cadence.cumulus.model.Result;
import com.cadence.cumulus.model.User;
import com.cadence.cumulus.model.UserLogin;
import com.cadence.cumulus.service.CumulusUserService;
import com.cadence.cumulus.service.OktaService;

@RestController
public class FileManagerAPI {

    /**
     * Logger
     */
    private static final Logger log = LoggerFactory
            .getLogger(FileManagerAPI.class);

    @Value("${user.home.base}")
    private String HOME_FOLDER;

    @Autowired
    private OktaService oktaService;

    @Autowired
    private FileManagerAPIHandler fileManagerAPIHandler;

    @Autowired
    private UserAuthorizationServiceImpl userAuthorizationServiceImpl;

    @Autowired
    private CumulusUserService userService;

    private static String TMP_DIRECTORY = "/tmp/";

    /**
     * JOB_RUN_API_AUTHZ Authority
     */

    @Value("${file.upload.api.authz}")
    private String FILE_UPLOAD_API_AUTHZ;
    
    @Value("${file.download.api.authz}")
    private String FILE_DOWNLOAD_API_AUTHZ;

    @Autowired
    private TokenService tokenService;


    /**
     * UNAUTHORIZED_PERMISSION_MSG
     */
    @Value("${unauthorized.permission.msg}")
    private String UNAUTHORIZED_PERMISSION_MSG;

    /**
     * API to upload a file to users home directory.
     *

     * @param files
     * @param response
     * @return
     * @throws IOException
     */
    @RequestMapping(value = "/api/upload", method = RequestMethod.POST)
    public Result uploadFile(
            @RequestPart(name = "userLogin", required = false) UserLogin uLogin,
            @RequestPart("file") MultipartFile[] files,
            HttpServletRequest request, HttpServletResponse response)
            throws IOException {
        if (log.isDebugEnabled()) {
            log.debug("uploadFile called..");
        }
        UserLogin userLogin = BasicAuthenticationHandler.checkAuth(request);
        if (userLogin == null) {
            if (log.isDebugEnabled()) {
                log.debug("authorization header missing or incorrect..trying request body..");
            }
            userLogin = uLogin;
        }

        if (userLogin == null) {
            response.setContentType(MediaType.TEXT_PLAIN_VALUE);
            response.sendError(HttpStatus.UNAUTHORIZED.value(),
                    "Missing User Login Details in request or incorrect Authentication Header");
            return null;
        }

        String loginName = null;
        try {
            loginName = oktaService.doLogin(userLogin);
        } catch (LoginException e) {
            response.setContentType(MediaType.TEXT_PLAIN_VALUE);
            response.sendError(HttpStatus.UNAUTHORIZED.value(), e.getMessage());
            return null;
        }
        if (loginName == null) {
            sendErrorNoUserFound(response);
            return null;
        }
        User user = userService.getUserByEmail(loginName);
        if (user == null) {
            sendErrorNoUserFound(response);
            return null;
        }
        List<GrantedAuthority> authList = userAuthorizationServiceImpl
                .getUserAuthorityList(user);
        if (!(userAuthorizationServiceImpl.hasAuthority(FILE_UPLOAD_API_AUTHZ,
                authList))) {
            response.setContentType(MediaType.TEXT_PLAIN_VALUE);
            response.sendError(HttpStatus.UNAUTHORIZED.value(),
                    UNAUTHORIZED_PERMISSION_MSG);
            return null;
        }

        String finalDestination = HOME_FOLDER + user.getUsername();
        log.debug("Request to upload {} files to {}", files.length,
                finalDestination);
        return this.fileManagerAPIHandler.uploadFile(files, finalDestination, user,
                response);

    }


    // added by Brijesh JIRA ID:CMLS-493

    /*
     * Check if a path exists and is valid.
     */
    private boolean exists(String username, String filePath) throws Exception {
        //File work = new File(filePath);
        //return work.exists();
        log.debug("Inside exists********" + filePath);
        ProcessResult processResult = runExternalCommand("sudo", "-u", username, "ls -l \"" + filePath + "\"");
        log.debug("Exists = ls -l " + filePath + "; exitcode=" + processResult.getExitCode());
        return processResult.getExitCode() == 0;
    }


    /*
     * Get the length of the file in bytes.
     */
    private String getLength(String username, String filePath) throws Exception {
        //ProcessResult processResult = runExternalCommand("su", "-", username, "-c", "du -b \"" + filePath + "\"");
        ProcessResult processResult = runExternalCommand("sudo", "-u", username, "stat -c '%s' \"" + filePath + "\"");
        //String len=processResult.getOutput().split("\\s+")[0].trim();
        //ProcessResult processResult = runExternalCommand("su", "-", username, "-c", "ls -l \"" + filePath + " | cut -d \" \" -f5"+ "\"");
        String len = processResult.getOutput().trim();
        log.debug("Length is" + len);
        return len;
    }

    /*
     * Does the user have read permissions on the file.
     */
    private boolean canRead(String username, String filePath) throws Exception {
        // File fileToCheck = new File(filePath);
        // return fileToCheck.exists() ? true : false;
        ///*
        ProcessResult processResult = runExternalCommand("sudo", "-u", username, "[ -r \"" + filePath + "\" ]");
        log.debug("[ -r " + filePath + " ]" + "; exitcode=" + processResult.getExitCode());
        return processResult.getExitCode() == 0;
        //*/
    }

    /*
     * Run external commands passed as an array of strings and return the output and
     * exit code for the external command.
     * We are running the commands by creating a script because filenames with space
     * are not treated well. Enclosing the filenames in quotes is also not helping
     * as ProcessBuilder takes care of that only if we pass the entire name with space
     * as an element of the array. We cant do that because we are using su and the
     * param after -c should be a single string with the whole command that needs to
     * be executed.
     */
    private static ProcessResult runExternalCommand(String... commands) throws Exception {
        String tmpScriptName = TMP_DIRECTORY + UUID.randomUUID().toString() + ".sh";
        File f = new File(tmpScriptName);
        FileOutputStream fout = new FileOutputStream(f);
        fout.write(("#!/bin/bash\nset -e\n" + commands[commands.length - 1]).getBytes());
        fout.close();
        f.setExecutable(true, false);
        commands[commands.length - 1] = tmpScriptName;
        ProcessBuilder builder = new ProcessBuilder(commands).redirectErrorStream(true).inheritIO().redirectOutput(Redirect.PIPE);
        Process p = builder.start();
        String line = null;
        String output = "";
        BufferedReader reader = new BufferedReader(new InputStreamReader(p.getInputStream()));
        while ((line = reader.readLine()) != null) {
            output += line;
        }
        reader.close();
        int exitCode = p.waitFor();
        f.delete();
        log.debug("Ran executable : {}", f.getAbsolutePath());
        return new ProcessResult(exitCode, output);
    }

    /**
     * Download API
     * Accepts base64encoded Filename from the requests and sends file content in the response if the user has permissions.
     * Doest not accept actual file because it rest API cannot accept file extension so always use base64encoded value
     *
     * @param request
     * @param response
     * @param fileName base64 encoded value
     * @throws IOException
     */
    @RequestMapping(path = "/api/download/**", method = RequestMethod.GET)
    public void downloadFile(HttpServletRequest request, HttpServletResponse response) throws IOException {
        String fileName = "";
        InputStream input = null;
        BufferedOutputStream output = null;

        String token = (String) request.getHeader(ServiceConstants.X_AUTH_TOKEN);
        log.debug("Token from header:  {}", token);
        TokenStatus ts = tokenService.verifyToken(token, FILE_DOWNLOAD_API_AUTHZ);
        if (ts.getValidity()) {

            // get the relative path from request
            String[] stringArr = request.getRequestURI().split("download/");
            if (stringArr.length > 0) {
                fileName = stringArr[1];
            }
            log.debug("Relative path and file name  ---------- {}", fileName);

            try {

            	// anirban - see latest commit message for this file
              
                String strFile = HOME_FOLDER + ts.getMessage() + File.separator + fileName;

                log.debug("Request to download {} for {}", strFile, ts.getMessage());

                File file = new File(strFile);

                if (!exists(ts.getMessage(), strFile)) {
                    log.debug("File {} doesn't exist.", strFile);

                    response.setContentType(TEXT_PLAIN_VALUE);
                    response.setStatus(HttpStatus.NOT_FOUND.value());

                    return;
                }
                if (!canRead(ts.getMessage(), strFile)) {
                    log.debug("Cannot read {}.", strFile);

                    response.setContentType(TEXT_PLAIN_VALUE);
                    response.setStatus(HttpStatus.FORBIDDEN.value());

                    return;
                }

                MimetypesFileTypeMap mimeTypesMap = new MimetypesFileTypeMap();
                // This code tries figuring out the mime type from the file name.
                // Some files were opening up in the browser rather than showing a download dialog.
                // This was because scripts without shebang would be treated as normal text files
                // if we try reading the mime type from file contents(which is the right way to do it)
                // So rather than using the getMimeType() method we are using the following:
                String mimeTypeFromName = mimeTypesMap.getContentType(fileName);
                if (TEXT_PLAIN_VALUE.equals(mimeTypeFromName)) {
                    mimeTypeFromName = MediaType.APPLICATION_OCTET_STREAM_VALUE;
                }
                response.setHeader("Content-Type", mimeTypeFromName);

                response.setHeader("Content-Length", getLength(ts.getMessage(), strFile));
                response.setHeader("Content-Disposition", "inline; filename=\"" + file.getName() + "\"");
                response.addHeader("Access-Control-Allow-Origin", "*");
                response.addHeader("Access-Control-Allow-Methods", "GET,POST");
                response.addHeader("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");

                log.debug("Starting download...");

                List<String> commands = new ArrayList<String>();
                commands.add("sudo");
                commands.add("-u");
                commands.add(ts.getMessage());
                commands.add("cat \"" + strFile + "\"");

			/*
             * Create the executable script to do the job
			 */

                String tmpScriptName = TMP_DIRECTORY + UUID.randomUUID().toString() + ".sh";
                File f = new File(tmpScriptName);
                FileOutputStream fout = new FileOutputStream(f);
                fout.write(("#!/bin/bash\nset -e\n" + commands.get(commands.size() - 1)).getBytes());
                fout.close();
                f.setExecutable(true, false);
                commands.set(commands.size() - 1, tmpScriptName);

                log.debug("Final command..{}", commands);

                ProcessBuilder builder = new ProcessBuilder(commands).redirectErrorStream(true).inheritIO().redirectOutput(Redirect.PIPE);
                Process process = builder.start();
                input = process.getInputStream();
                output = new BufferedOutputStream(response.getOutputStream());

                byte[] buffer = new byte[8192];
                for (int length = 0; (length = input.read(buffer)) > 0; ) {
                    output.write(buffer, 0, length);
                }
                output.flush();
                process.waitFor();
                f.delete();

                log.debug("Download of {} complete", strFile);
            } catch (Throwable t) {
                log.error("Error in sending download: ", t);
            } finally {
                if (output != null) {
                    try {
                        output.close();
                    } catch (IOException logOrIgnore) {
                    }
                }
                if (input != null) {
                    try {
                        input.close();
                    } catch (IOException logOrIgnore) {
                    }
                }
            }
        } else {
            log.debug("Error :  {}", ts.getMessage());
            handleErrorResponse(response, ts.getCode(), ts.getMessage());
        }

    }


    private void sendErrorNoUserFound(HttpServletResponse response)
            throws IOException {
        response.setContentType(MediaType.TEXT_PLAIN_VALUE);
        response.sendError(HttpStatus.UNAUTHORIZED.value(),
                "Unable to find User.");
    }


    /***
     * This method list all files and sub directories under given directory path
     * it will first authenticate the user and then list the file
     * @param request
     * @param response
     *
     */
    @RequestMapping(path = "/api/listfiles/**", method = RequestMethod.GET)
    public String listFiles(HttpServletRequest request, HttpServletResponse response) throws IOException {

        log.debug("listFiles executing started");

        String directoryName = "";
        InputStream input = null;
        String output = "";
        String token = (String) request.getHeader(ServiceConstants.X_AUTH_TOKEN);
        log.debug("Token from header:  {}", token);
        TokenStatus ts = tokenService.verifyToken(token, FILE_DOWNLOAD_API_AUTHZ);
        log.debug("Token Status " + ts);
        if (ts.getValidity()) {

            String[] stringArr = request.getRequestURI().split("listfiles/");
            if (stringArr.length == 2) {
                directoryName = stringArr[1];
            }
            log.debug("directory name:  {}", directoryName);

            try {


                String directoryPath = HOME_FOLDER + ts.getMessage() + File.separator + directoryName;

                List<String> commands = new ArrayList<String>();
                commands.add("sudo");
                commands.add("-u");
                commands.add(ts.getMessage());
                commands.add("ls -d $PWD/**");

			/*
             * Create the executable script to do the job
			 */

                String tmpScriptName = TMP_DIRECTORY + UUID.randomUUID().toString() + ".sh";
                File f = new File(tmpScriptName);
                FileOutputStream fout = new FileOutputStream(f);
                fout.write(("#!/bin/bash\nset -e\n" + "cd " + directoryPath + "\n" + commands.get(commands.size() - 1)).getBytes());
                fout.close();
                f.setExecutable(true, false);
                commands.set(commands.size() - 1, tmpScriptName);

                log.debug("Final command to run..{}", commands);

                ProcessBuilder builder = new ProcessBuilder(commands).redirectErrorStream(true).inheritIO().redirectOutput(Redirect.PIPE);
                Process process = builder.start();
                input = process.getInputStream();

                process.waitFor();
                BufferedReader buf = new BufferedReader(new InputStreamReader(
                        input));
                String line = "";


                while ((line = buf.readLine()) != null) {
                    output += line + "\n";
                }

                if (output.contains("No such file or directory") || output.contains("Not a directory")) {
                    response.setContentType(TEXT_PLAIN_VALUE);
                    response.setStatus(HttpStatus.NOT_FOUND.value());
                    output = "";
                }

                log.debug("command output.............{}", output);


            } catch (Throwable t) {
                log.error("Error while listing file:  {}", t);
            } finally {

                if (input != null) {
                    try {
                        input.close();
                    } catch (IOException e) {
                        log.error("Error in while processing command", e);
                    }
                }
            }
        } else {
            log.debug("Error :  {}", ts.getMessage());
            handleErrorResponse(response, ts.getCode(), ts.getMessage());
        }
        return output;
    }

    private void handleErrorResponse(HttpServletResponse response, int code, String message) throws IOException {
        response.setContentType(MediaType.TEXT_PLAIN_VALUE);
        response.sendError(code, message);
        log.error(message);
    }
	
	
	/*
	 * @RequestMapping(value = "/api/upload", method = RequestMethod.POST)
	 * public @ResponseBody Response<String> uploadCommons(
	 * MultipartHttpServletRequest request) {
	 * 
	 * System.out.println("File Upload.. "+request);
	 * System.out.println("mpFileMap1" + request.getMultiFileMap());
	 * 
	 * 
	 * try { boolean isMultipart =
	 * ServletFileUpload.isMultipartContent(request); if (!isMultipart) { //
	 * Inform user about invalid request Response<String> responseObject = new
	 * Response<String>(false, "Not a multipart request.", ""); return
	 * responseObject; }
	 * 
	 * //Map<String, MultipartFile> mpFileMap = request.getFileMap();
	 * 
	 * MultiValueMap<String, MultipartFile> mvMap = request.getMultiFileMap();
	 * 
	 * 
	 * System.out.println("mvMap" + mvMap); for (String fileName :
	 * mvMap.keySet()) { System.out.println("uploading Multipart key " +
	 * fileName); List<MultipartFile> mpFileList = mvMap.get(fileName);
	 * System.out.println("file List Size " + mpFileList.size()); MultipartFile
	 * mpFile = mpFileList.get(0);
	 * System.out.println("uploading Multipart file  " +
	 * mpFile.getOriginalFilename()); InputStream stream =
	 * mpFile.getInputStream(); OutputStream out = new
	 * FileOutputStream(mpFile.getOriginalFilename());
	 * System.out.println("Creating File " + mpFile.getOriginalFilename());
	 * IOUtils.copy(stream, out); stream.close(); out.close(); } } catch
	 * (IOException e) { return new Response<String>(false,
	 * "Internal server IO error", e.toString()); }
	 * 
	 * return new Response<String>(true, "Success", ""); }
	 * 
	 * @RequestMapping(value = "/api/uploadxx", method = RequestMethod.POST)
	 * public @ResponseBody Response<String> upload(HttpServletRequest request)
	 * { System.out.println("File Upload.." +
	 * ServletFileUpload.isMultipartContent(request)); try { boolean isMultipart
	 * = ServletFileUpload.isMultipartContent(request); if (!isMultipart) { //
	 * Inform user about invalid request Response<String> responseObject = new
	 * Response<String>(false, "Not a multipart request.", ""); return
	 * responseObject; }
	 * 
	 * // Create a new file upload handler ServletFileUpload upload = new
	 * ServletFileUpload();
	 * 
	 * // Parse the request FileItemIterator iter =
	 * upload.getItemIterator(request); System.out.println("iter.hasNext()" +
	 * iter.hasNext()); while (iter.hasNext()) { FileItemStream item =
	 * iter.next(); String name = item.getFieldName(); InputStream stream =
	 * item.openStream(); if (!item.isFormField()) { String filename =
	 * item.getName(); // Process the input stream OutputStream out = new
	 * FileOutputStream(filename); System.out.println("Creating File " +
	 * filename); IOUtils.copy(stream, out); stream.close(); out.close(); } } }
	 * catch (FileUploadException e) { return new Response<String>(false,
	 * "File upload error", e.toString()); } catch (IOException e) { return new
	 * Response<String>(false, "Internal server IO error", e.toString()); }
	 * 
	 * return new Response<String>(true, "Success", ""); }
	 */

}
