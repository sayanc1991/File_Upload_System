/**
 * 
 */
package com.cadence.cumulus.common;

import com.fasterxml.jackson.annotation.JsonInclude;

/**
 * @author ramsudheer
 *
 */
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class ApiResponse {
	
	/**
	 * Http status code.
	 */
	private int code;

	/**
	 * message.
	 */
	private String message;
	/**
	 * 
	 * @param code
	 * @param message
	 */
	public ApiResponse(int code, String message) {
		this.code = code;
		this.message = message;
	}

	/**
	 * Default constructor.
	 */
	public ApiResponse() {

	}

	/**
	 * @return the code
	 */
	public final int getCode() {
		return code;
	}

	/**
	 * @param code
	 *            the code to set
	 */
	public final void setCode(int code) {
		this.code = code;
	}

	/**
	 * @return the message
	 */
	public final String getMessage() {
		return message;
	}

	/**
	 * @param message
	 *            the message to set
	 */
	public final void setMessage(String message) {
		this.message = message;
	}
}
