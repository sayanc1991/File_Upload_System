package com.cadence.cumulus.model;

public class UserLoginResult extends BaseEntity {
	private int HTTPStatus;
	private String oktaUserId;
	private String factorId;
	private String errorSummary;

	public String getOktaUserId() {
		return oktaUserId;
	}

	public void setOktaUserId(String oktaUserId) {
		this.oktaUserId = oktaUserId;
	}

	public String getFactorId() {
		return factorId;
	}

	public void setFactorId(String factorId) {
		this.factorId = factorId;
	}

	public int getHTTPStatus() {
		return HTTPStatus;
	}

	public void setHTTPStatus(int hTTPStatus) {
		HTTPStatus = hTTPStatus;
	}

	public String getErrorSummary() {
		return errorSummary;
	}

	public void setErrorSummary(String errorSummary) {
		this.errorSummary = errorSummary;
	}

}
