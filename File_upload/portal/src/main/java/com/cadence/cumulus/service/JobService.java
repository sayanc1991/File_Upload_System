/**
 * 
 */
package com.cadence.cumulus.service;

import java.io.IOException;
import java.util.List;

import com.cadence.cumulus.common.CumulusException;
import com.cadence.cumulus.model.Job;

/**
 * @author ramsudheer
 *
 */
public interface JobService {
	
	/**
	 * Saves the Job
	 * @param job
	 * @return
	 */
	public Job save(Job job);
	/**
	 * Updates the existing Job
	 * @param job
	 * @return
	 */
	public Job updateJob(Job job);
	/**
	 * Returns the Job
	 * @param id
	 * @return
	 */
	public Job findById(String id);
	
	
	/**
	 * Returns all jobs created by the user 
	 * @param userId
	 * @return
	 */
	public List<Job> getJobsByUser(Long userId);
	
	public Job getJobById(Long id);
	
	/**
	 * Deletes the Job
	 * @param jobId
	 */
	public void deleteJob(String jobId) throws IOException, CumulusException;
	
	
	/**
	 * Returns a specific job created by the user, which matches the job name
	 * @param jobName
	 * @param createdUser
	 * @return
	 */
	public List<Job> findByJobNameForUser(String jobName, Long userId);
	
	
}
