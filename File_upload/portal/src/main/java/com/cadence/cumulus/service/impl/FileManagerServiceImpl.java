package com.cadence.cumulus.service.impl;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.attribute.BasicFileAttributes;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.FileUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.cadence.cumulus.common.Constants;
import com.cadence.cumulus.service.FileManagerService;
import com.cadence.cumulus.utility.FileSystemUtil;

@Component
public class FileManagerServiceImpl implements FileManagerService {

    private static final Logger log = LoggerFactory.getLogger(FileManagerServiceImpl.class);

    // for now hard coding the base uri values, need to read this values
    // dynamically
    // private String REPOSITORY_BASE_URL = "";
    private String USER_HOME_BASE = System.getProperty("file-agent-home-base", "/home/");
    private String REPOSITORY_BASE_PATH = USER_HOME_BASE;

    @Autowired
    private FileSystemUtil fsUtil;
    
   @Override
    public JSONObject list(String path) {

        try {
            boolean onlyFolders = false;
            log.debug("list path: {} onlyFolders {}", path, onlyFolders);

            List<JSONObject> resultList = new ArrayList<JSONObject>();
            try (DirectoryStream<Path> directoryStream = Files.newDirectoryStream(Paths.get(REPOSITORY_BASE_PATH, path))) {
                SimpleDateFormat dt = new SimpleDateFormat(Constants.DATE_FORMAT);
                for (Path pathObj : directoryStream) {
                    if (!Files.isHidden(pathObj)) {
                        BasicFileAttributes attrs = Files.readAttributes(pathObj, BasicFileAttributes.class);

                        if (onlyFolders && !attrs.isDirectory()) {
                            continue;
                        }
                        JSONObject el = new JSONObject();
                        el.put("name", pathObj.getFileName().toString());
                        el.put("rights", fsUtil.getPermissions(pathObj));
                        el.put("date", dt.format(new Date(attrs.lastModifiedTime().toMillis())));
                        el.put("size", attrs.size());
                        el.put("type", attrs.isDirectory() ? "dir" : "file");
                        resultList.add(el);
                    }
                }
            } catch (IOException ex) {
            }

            return new JSONObject().put("result", resultList);
        } catch (Exception e) {
            log.error("list", e);
            return error(e.getMessage());
        }

    }
    
    @Override
    public JSONObject rename(JSONObject params) {
        try {
            String path = params.getString("item");
            String newpath = params.getString("newItemPath");
            log.debug("rename from: {} to: {}", path, newpath);

            File srcFile = new File(REPOSITORY_BASE_PATH, path);
            File destFile = new File(REPOSITORY_BASE_PATH, newpath);
            if (srcFile.isFile()) {
                FileUtils.moveFile(srcFile, destFile);
            } else {
                FileUtils.moveDirectory(srcFile, destFile);
            }
            return success(params);
        } catch (Exception e) {
            log.error("rename", e);
            return error(e.getMessage());
        }

    }
    
    @Override
    public JSONObject copy(JSONObject params) {

        try {
            // String path = params.getString("path");
            JSONArray items = params.getJSONArray("items");
            String newPath = params.getString("newPath");
            boolean singleFile = params.has("singleFilename");
            newPath = singleFile ? newPath + "/" + params.getString("singleFilename") : newPath;
            for (int i = 0; i < items.length(); i++) {
                Object item = items.get(i);
                log.debug("copy from: {} to: {}", item, newPath);
                File srcFile = new File(REPOSITORY_BASE_PATH, (String) item);
                File dest = new File(REPOSITORY_BASE_PATH, newPath);
                if (srcFile.isFile()) {
                    if (!singleFile) {
                        FileUtils.copyFileToDirectory(srcFile, dest, true);
                    } else {
                        FileUtils.copyFile(srcFile, dest);
                    }
                } else {
                    FileUtils.copyDirectory(srcFile, dest);
                }
            }
            return success(params);
        } catch (Exception e) {
            log.error("copy", e);
            return error(e.getMessage());
        }

    }
    
    @Override
    public JSONObject move(JSONObject params) {

        try {
            JSONArray items = params.getJSONArray("items");
            String newpath = params.getString("newPath");
            for (int i = 0; i < items.length(); i++) {
                Object item = items.get(i);
                log.debug("move from: {} to: {}", item, newpath);
                File srcFile = new File(REPOSITORY_BASE_PATH, (String) item);
                File destFile = new File(REPOSITORY_BASE_PATH, newpath + "/");
                if (srcFile.isFile()) {
                    FileUtils.moveFileToDirectory(srcFile, destFile, true);
                } else {
                    FileUtils.moveDirectoryToDirectory(srcFile, destFile, false);
                }
            }
            return success(params);
        } catch (Exception e) {
            log.error("move", e);
            return error(e.getMessage());
        }

    }
    
    @Override
    public JSONObject delete(JSONObject params) {
        try {
            JSONArray items = params.getJSONArray("items");
            for (int i = 0; i < items.length(); i++) {
                Object item = items.get(i);
                log.debug("delete {}", item);
                File srcFile = new File(REPOSITORY_BASE_PATH, (String) item);
                if (!FileUtils.deleteQuietly(srcFile)) {
                    throw new Exception("Can't delete: " + srcFile.getAbsolutePath());
                }
            }
            return success(params);
        } catch (Exception e) {
            log.error("delete", e);
            return error(e.getMessage());
        }

    }
    
    @Override
    public JSONObject editFile(JSONObject params) {
        // get content
        try {
            String path = params.getString("path");
            log.debug("editFile path: {}", path);

            File srcFile = new File(REPOSITORY_BASE_PATH, path);
            String content = FileUtils.readFileToString(srcFile);

            return new JSONObject().put("result", content);
        } catch (Exception e) {
            log.error("editFile", e);
            return error(e.getMessage());
        }
    }
    
    @Override
    public JSONObject addFolder(JSONObject params) {

        try {
            String path = params.getString("newPath");
            // String name = params.getString("name");
            log.debug("addFolder path: {} name: {}", path);

            File newDir = new File(REPOSITORY_BASE_PATH, path);
            if (!newDir.mkdir()) {
                throw new Exception("Can't create directory: " + newDir.getAbsolutePath());
            }
            return success(params);
        } catch (Exception e) {
            log.error("addFolder", e);
            return error(e.getMessage());
        }

    }

    @Override
    public void downloadFiles(JSONObject params, HttpServletResponse response) {

        String path = params.getString("path");

        File file = new File(REPOSITORY_BASE_PATH, path);

        // someone just copy pasted spanish comments alognwith the code xD
        // se imageName non ha estensione o non è immagine sballa! ;)
        // response.setHeader("Content-Type",
        // getServletContext().getMimeType(imageName));
        response.setHeader("Content-Type", file.getName());
        response.setHeader("Content-Length", String.valueOf(file.length()));
        response.setHeader("Content-Disposition", "inline; filename=\"" + file.getName() + "\"");
        response.addHeader("Access-Control-Allow-Origin", "*");
        response.addHeader("Access-Control-Allow-Methods", "GET,POST");
        response.addHeader("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");

        FileInputStream input = null;
        BufferedOutputStream output = null;
        try {

            input = new FileInputStream(file);
            output = new BufferedOutputStream(response.getOutputStream());
            byte[] buffer = new byte[8192];
            for (int length = 0; (length = input.read(buffer)) > 0;) {
                output.write(buffer, 0, length);
            }
        } catch (Throwable t) {
        } finally {
            if (output != null) {
                try {
                    output.close();
                } catch (IOException logOrIgnore) {
                }
            }
            if (input != null) {
                try {
                    input.close();
                } catch (IOException logOrIgnore) {
                }
            }
        }

    }

    @Override
    public void downloadMultipleFiles(JSONObject params, HttpServletResponse response) {

        // String path = params.getString("path");
        
         /** JSONArray items = params.getJSONArray("items"); String newPath =
         * params.getString("newPath"); boolean singleFile =
         * params.has("singleFilename"); newPath = singleFile ? newPath + "/" +
         * params.getString("singleFilename") : newPath;*/
         

        JSONArray itemValues = params.getJSONArray("items");
        String toFileName = params.getString("toFilename");

        
        /* * String path = request.getParameter("path");
         * 
         * String itemValues[]=request.getParameterValues("items[]"); String
         * toFileName=request.getParameter("toFilename"); String action =
         * request.getParameter("action");*/
         
        // Set the content type based to zip

        response.setContentType("Content-type: text/zip");
        response.setHeader("Content-Disposition", "attachment; filename=" + toFileName);

        // List of files to be downloaded
        List<File> files = new ArrayList<File>();

        for (int i = 0; i < itemValues.length(); i++) {
            Object item = itemValues.get(i);

            files.add(new File(REPOSITORY_BASE_PATH, item.toString()));
        }

        try {

            ServletOutputStream out = response.getOutputStream();
            ZipOutputStream zos = new ZipOutputStream(new BufferedOutputStream(out));

            for (File file : files) {

                log.debug("Adding " + file.getName());
                zos.putNextEntry(new ZipEntry(file.getName()));

                // Get the file
                FileInputStream fis = null;
                try {
                    fis = new FileInputStream(file);

                } catch (FileNotFoundException fnfe) {
                    // If the file does not exists, write an error entry instead
                    // of
                    // file
                    // contents
                    zos.write(("ERRORld not find file " + file.getName()).getBytes());
                    zos.closeEntry();
                    System.out.println("Couldfind file " + file.getAbsolutePath());
                    continue;
                }

                BufferedInputStream fif = new BufferedInputStream(fis);

                // Write the contents of the file
                int data = 0;
                while ((data = fif.read()) != -1) {
                    zos.write(data);
                }
                fif.close();

                zos.closeEntry();
                System.out.println("Finishedng file " + file.getName());
            }

            zos.close();

        } catch (Exception e) {
            log.error("Un Handel exception" + e);
        }

    }

    @Override
    public JSONObject compress(JSONObject params) {

        try {

            String destination = params.getString("destination"); // "/public_html/extracted-files"
            String compressedFilename = params.getString("compressedFilename"); // /public_html/compressed.zip"
            JSONArray items = params.getJSONArray("items");

            String fileName = REPOSITORY_BASE_PATH + "/" + destination + "/" + compressedFilename;

            FileOutputStream fos = new FileOutputStream(new SimpleDateFormat("'" + fileName + "_'yyyyMMddhhmm'.zip'").format(new Date()));
            ZipOutputStream zos = new ZipOutputStream(fos);

            for (int i = 0; i < items.length(); i++) {
                Object item = items.get(i);
                addDirToZipArchive(zos, new File(REPOSITORY_BASE_PATH, item.toString()), null);
            }
            zos.flush();
            fos.flush();
            zos.close();
            fos.close();
            log.debug("ALl good");
            return success(params);
            // return success(params);
        } catch (Exception e) {
            log.error("compress", e);
            return error(e.getMessage());
        }

    }

    private JSONObject error(String msg) {
        JSONObject result = new JSONObject();
        try {
            result.put("success", false);
            result.put("error", msg);
        } catch (JSONException e) {
            log.error("Un-Expected exception while construction error message " + e);
        }
        return new JSONObject().put("result", result);
    }

    private JSONObject success(JSONObject params) {
        try {
            JSONObject result = new JSONObject();
            result.put("success", true);
            result.put("error", (Object) null);
            return new JSONObject().put("result", result);
        } catch (JSONException e) {
            log.error("Un-Expected exception while construction success message " + e);
        }
        return null;
    }

    private void addDirToZipArchive(ZipOutputStream zos, File fileToZip, String parrentDirectoryName) throws Exception {
        if (fileToZip == null || !fileToZip.exists()) {
            return;
        }

        String zipEntryName = fileToZip.getName();
        if (parrentDirectoryName != null && !parrentDirectoryName.isEmpty()) {
            zipEntryName = parrentDirectoryName + "/" + fileToZip.getName();
        }

        if (fileToZip.isDirectory()) {
            log.debug("+" + zipEntryName);
            for (File file : fileToZip.listFiles()) {
                addDirToZipArchive(zos, file, zipEntryName);
            }
        } else {
            log.debug("   " + zipEntryName);
            byte[] buffer = new byte[1024];
            FileInputStream fis = new FileInputStream(fileToZip);
            zos.putNextEntry(new ZipEntry(zipEntryName));
            int length;
            while ((length = fis.read(buffer)) > 0) {
                zos.write(buffer, 0, length);
            }
            zos.closeEntry();
            fis.close();
        }
    }

}
