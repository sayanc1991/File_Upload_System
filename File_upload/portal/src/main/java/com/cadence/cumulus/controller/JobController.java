/**
 * 
 */
package com.cadence.cumulus.controller;

import java.io.IOException;
import java.security.Principal;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.cadence.cumulus.common.ApiResponse;
import com.cadence.cumulus.common.CumulusException;
import com.cadence.cumulus.common.JobWrapper;

import com.cadence.cumulus.model.CumulusUser;
import com.cadence.cumulus.model.Job;
import com.cadence.cumulus.service.JobService;

/**
 * @author ramsudheer
 *
 */
@RestController
@RequestMapping(value = "/jobs")
public class JobController {

	/**
	 * Logger
	 */
	private static final Logger log = LoggerFactory.getLogger(JobController.class);

	@Autowired
	private JobService jobService;
	

	

	
	/**
	 * End point to create the Job
	 * 
	 * @param job
	 * @return
	 * @throws CumulusException
	 */
	@PostMapping
	@ResponseStatus(HttpStatus.CREATED)
	public Job createJob(@RequestBody Job job, Principal principal) throws CumulusException {
		if (principal != null) {
			log.debug("user is {}", principal.getName());
		}
		if(StringUtils.isBlank(job.getJobName())){
			throw new CumulusException("Invalid Job Name");
		}

		Date d = new Date();
		job.setCreatedDate(d);
		job.setLastUpdatedDate(d);
		
		
		CumulusUser cumulusUser = (CumulusUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		job.setCreatedBy(cumulusUser.getId());
		job.setLastModifiedBy(cumulusUser.getId());
		
		
		if (log.isDebugEnabled()) {
			log.debug("Creating job ", job);
		}
		// check for duplicate job name
		List<Job> existingJobs = jobService.getJobsByUser(cumulusUser.getId());
		if (!CollectionUtils.isEmpty(existingJobs)) {
			for (Job ejob : existingJobs) {
				if (job.getJobName().equalsIgnoreCase(ejob.getJobName())) {
					throw new CumulusException("A Job is already exists with given Job name");
				}
			}
		}
		return jobService.save(job);
	}

	/**
	 * End point to update the existing job
	 * 
	 * @param updatedJob
	 * @return
	 * @throws CumulusException
	 */
	// @RequestMapping(method = RequestMethod.PUT, consumes =
	// MediaType.APPLICATION_JSON_VALUE)
	@PutMapping("/{jobId}")
	@ResponseStatus(HttpStatus.OK)
	public Job updateJob(@PathVariable String jobId, @RequestBody Job updatedJob) throws CumulusException {
		if (log.isDebugEnabled()) {
			log.debug("Creating job ", updatedJob);
		}
		
		Job job = jobService.findById(jobId);
		if (job == null) {
			throw new CumulusException("Invalid job");
		}
		
		job.setJobName(updatedJob.getJobName());
		job.setInstanceType(updatedJob.getInstanceType());
		job.setClArguments(updatedJob.getClArguments());
		job.setScriptPath(updatedJob.getScriptPath());
		job.setNumberOfNodes(updatedJob.getNumberOfNodes());
		job.setToolName(updatedJob.getToolName());
		job.setToolPath(updatedJob.getToolPath());
		job.setToolVersion(updatedJob.getToolVersion());
		job.setLastUpdatedDate(new Date());
		// check for duplicate job name
		CumulusUser cumulusUser = (CumulusUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		job.setLastModifiedBy(cumulusUser.getId());
		List<Job> existingJobs = jobService.getJobsByUser(cumulusUser.getId());
		if (!CollectionUtils.isEmpty(existingJobs)) {
			for (Job ejob : existingJobs) {
				if (job.getJobName().equalsIgnoreCase(ejob.getJobName())) {
					if (ejob.getId() != job.getId()) {
						throw new CumulusException("A Job is already exists with given Job name");
					}
				}
			}
		}
		return jobService.updateJob(job);
	}

	/**
	 * End point to fetch by job id
	 * 
	 * @param jobId
	 * @param response
	 * @return
	 * @throws CumulusException
	 */
	@GetMapping(value = "{jobId}")
	//@ResponseStatus(HttpStatus.OK)
	public @ResponseBody Job getJobById(@PathVariable("jobId") String jobId, Principal principal, HttpServletResponse response)
			throws CumulusException {
		if (principal != null) {
			log.debug("user is {}", principal.getName());
		}

		if (log.isDebugEnabled()) {
			log.debug("Retriveing job {}", jobId);
		}
		Job job = jobService.findById(jobId);
		if (job == null) {
			response.setStatus(HttpStatus.NO_CONTENT.value());
			return null;
		}
		return job;
	}
	/**
	 * End point to fetch all jobs
	 * 
	 * @param response
	 * @return
	 * @throws CumulusException
	 */
	@GetMapping
	public @ResponseBody JobWrapper getAllJobs(Principal principal, HttpServletResponse response) throws CumulusException {
		CumulusUser user = (CumulusUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		if (log.isDebugEnabled()) {
			
			log.debug("Retrieving all jobs for " + user.getId()+"-"+user.getFirstName());
		}
		
		List<Job> jobs = jobService.getJobsByUser(user.getId());
		if (CollectionUtils.isEmpty(jobs)) {
			response.setStatus(HttpStatus.NO_CONTENT.value());
			return null;
		}

		return new JobWrapper().setJobs(jobs);
	}



	/**
	 * End point to delete job
	 * 
	 * @param jobId
	 * @param response
	 * @return
	 * @throws CumulusException
	 * @throws IOException 
	 */
	// @RequestMapping(value="{jobId}",method = RequestMethod.DELETE)
	@DeleteMapping(value = "{jobId}")
	@ResponseStatus(HttpStatus.OK)
	public ApiResponse deleteJobById(@PathVariable("jobId") String jobId, HttpServletResponse response)
			throws CumulusException, IOException {


		log.debug("Deleting job {}", jobId);

		// Delete all job runs before deleting the job
		
		jobService.deleteJob(jobId);
		return new ApiResponse(HttpStatus.OK.value(), HttpStatus.OK.getReasonPhrase());
	}
}
