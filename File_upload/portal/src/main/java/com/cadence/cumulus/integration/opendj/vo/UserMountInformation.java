package com.cadence.cumulus.integration.opendj.vo;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class UserMountInformation {

    private String[] schemas;
    private String _id;
    private String automountkey;
	private String nismapentry;
    private String cn;
    private String nismapname;
    private String automountinformation;
    
	public String[] getSchemas() {
		return schemas;
	}

	public void setSchemas(String[] schemas) {
		this.schemas = schemas;
	}

	public String get_id() {
		return _id;
	}

	public void set_id(String _id) {
		this._id = _id;
	}

	public String getAutomountkey() {
		return automountkey;
	}

	public void setAutomountkey(String automountkey) {
		this.automountkey = automountkey;
	}

	public String getNismapentry() {
		return nismapentry;
	}

	public void setNismapentry(String nismapentry) {
		this.nismapentry = nismapentry;
	}

	public String getNismapname() {
		return nismapname;
	}

	public void setNismapname(String nismapname) {
		this.nismapname = nismapname;
	}

	public String getAutomountinformation() {
		return automountinformation;
	}

	public void setAutomountinformation(String automountinformation) {
		this.automountinformation = automountinformation;
	}

	public UserMountInformation() {
    }

	public String getCn() {
		return cn;
	}

	public void setCn(String cn) {
		this.cn = cn;
	}

}
