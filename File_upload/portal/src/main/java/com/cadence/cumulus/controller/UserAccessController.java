package com.cadence.cumulus.controller;

import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.cadence.cumulus.common.CumulusException;
import com.cadence.cumulus.core.security.UserAuthorizationServiceImpl;
import com.cadence.cumulus.model.CumulusUser;
import com.cadence.cumulus.service.CumulusUserService;

@RestController
@RequestMapping(value = "/user")
public class UserAccessController {
	@Autowired
	private CumulusUserService userService;

	@Autowired
	private UserAuthorizationServiceImpl userAuthzService;

	/**
	 * Logger
	 */
	private static final Logger log = LoggerFactory
			.getLogger(UserAccessController.class);

	@PostMapping(value = "/auth/term")
	@PreAuthorize("hasAuthority('Terminal Access')")
	public @ResponseBody CumulusUser hasPermissionTerminal()
			throws CumulusException, IOException {
		log.debug("user is authorized for Terminal..");
		return (CumulusUser) SecurityContextHolder.getContext()
				.getAuthentication().getPrincipal();
	}

}
