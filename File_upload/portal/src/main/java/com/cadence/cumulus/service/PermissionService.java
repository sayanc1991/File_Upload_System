package com.cadence.cumulus.service;

import java.io.File;
import java.io.IOException;
import java.nio.file.DirectoryStream;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.LinkOption;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.attribute.GroupPrincipal;
import java.nio.file.attribute.PosixFileAttributeView;
import java.nio.file.attribute.PosixFileAttributes;
import java.nio.file.attribute.PosixFilePermission;
import java.nio.file.attribute.UserPrincipal;
import java.nio.file.attribute.UserPrincipalLookupService;
import java.util.HashSet;
import java.util.Set;

import org.springframework.stereotype.Service;

import com.cadence.cumulus.common.ServiceConstants;
import com.cadence.cumulus.common.ServiceException;
import com.cadence.cumulus.model.ResourceOwnershipInfo;

/**
 * 
 * PermissionService deprecated since non root user cannot access the other users files directly. 
 * Instead use FileManager or UnixFileManager or write your own
 *
 */
@Deprecated
@Service
public class PermissionService {
    /**
     * @param userName
     *            Unix user account name
     * @return UserPrincipal object for the user
     * @throws ServiceException
     */
    public UserPrincipal getUser(String userName) throws ServiceException {
        UserPrincipal user = null;
        UserPrincipalLookupService lookupService = FileSystems.getDefault().getUserPrincipalLookupService();
        try {
            user = lookupService.lookupPrincipalByName(userName);
        } catch (IOException ie) {
            // TODO : printStackTrace may not be required - Only during initial
            // dev
            ie.printStackTrace();
            throw new ServiceException(ie.getMessage(), ServiceConstants.OPR_GET_USER, userName);
        }
        return user;
    }

    /**
     * @param resource
     *            Absolute path name of resource whose group info is required
     * @return GroupPrincipal
     * @throws ServiceException
     */
    public GroupPrincipal getGroup(String resource) throws ServiceException {
        File fileResource = new File(resource);
        GroupPrincipal group = null;
        if (!fileResource.exists()) {
            throw new ServiceException(ServiceConstants.MSG_NOT_FOUND, ServiceConstants.OPR_GET_GROUP, resource);
        }
        try {
            group = Files.readAttributes(fileResource.toPath(), PosixFileAttributes.class, LinkOption.NOFOLLOW_LINKS).group();
        } catch (IOException ie) {
            // TODO : printStackTrace : for initial dev debug
            ie.printStackTrace();
            throw new ServiceException(ie.getMessage(), ServiceConstants.OPR_GET_GROUP, resource);
        }
        return group;
    }

    /**
     * @param resource
     *            file or folder name
     * @return UserPrincipal and GroupPrincipal info
     * @throws ServiceException
     */
    public ResourceOwnershipInfo getOwnershipInfo(String resource) throws ServiceException {
        File resourceFile = new File(resource);
        ResourceOwnershipInfo owner = new ResourceOwnershipInfo();
        if (!resourceFile.exists()) {
            throw new ServiceException(ServiceConstants.MSG_NOT_FOUND, ServiceConstants.OPR_GET_OWNERSHIP_INFO, resource);
        }
        Path path = Paths.get(resourceFile.getAbsolutePath());
        try {
            owner.setUser(Files.getOwner(path, LinkOption.NOFOLLOW_LINKS));
            owner.setGroup(Files.readAttributes(resourceFile.toPath(), PosixFileAttributes.class, LinkOption.NOFOLLOW_LINKS).group());
        } catch (IOException ie) {
            // TODO : printStackTrace : for initial dev debug
            ie.printStackTrace();
            throw new ServiceException(ie.getMessage(), ServiceConstants.OPR_GET_OWNERSHIP_INFO, resource);
        }
        return owner;
    }

    /**
     * @param resource
     *            file or folder name
     * @param owner
     *            Ownership info to be set recursively
     * @throws ServiceException
     */
    public void setOwnershipInfo(Path resource, ResourceOwnershipInfo owner) throws ServiceException {
        try {
            Files.setOwner(resource, owner.getUser());
            Files.getFileAttributeView(resource, PosixFileAttributeView.class, LinkOption.NOFOLLOW_LINKS).setGroup(owner.getGroup());
        } catch (IOException ie) {
            // TODO : printStackTrace : for initial dev debug
            ie.printStackTrace();
            throw new ServiceException(ie.getMessage(), ServiceConstants.OPR_SET_OWNERSHIP_INFO, resource.getFileName().toString());

        }
        if (Files.isDirectory(resource, LinkOption.NOFOLLOW_LINKS)) {
            DirectoryStream<Path> directoryStream = null;
            try {
                directoryStream = Files.newDirectoryStream(Paths.get(resource.toString()));
            } catch (IOException ie) {
                // TODO : printStackTrace : for initial dev debug
                ie.printStackTrace();
                throw new ServiceException(ie.getMessage(), ServiceConstants.OPR_SET_OWNERSHIP_INFO, resource.getFileName().toString());
            }
            for (Path path : directoryStream) {
                setOwnershipInfo(path, owner);
            }

            if (directoryStream != null) {
                try {
                    directoryStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    /**
     * @param resource
     *            file or folder name
     * @param owner
     *            Ownership info to be set recursively
     * @throws ServiceException
     */
    public void setOwnershipInfo(String resourceName, ResourceOwnershipInfo owner) throws ServiceException {
        File resourceFile = new File(resourceName);
        if (!resourceFile.exists()) {
            throw new ServiceException(ServiceConstants.MSG_NOT_FOUND, ServiceConstants.OPR_GET_OWNERSHIP_INFO, resourceName);
        }
        Path resource = Paths.get(resourceFile.getAbsolutePath());
        this.setOwnershipInfo(resource, owner);
    }

    /**
     * @param resourceName
     *            Name of file/folder
     * @param userName
     *            Unix user account name - Group taken from parent folder
     */
    public void setOwnershipInfo(String resourceName, String userName) throws ServiceException {
        File resourceFile = new File(resourceName);
        if (resourceName.trim().equals("/")) {
            throw new ServiceException(ServiceConstants.MSG_INVALID_RESOURCE, ServiceConstants.OPR_SET_OWNERSHIP_INFO, resourceName);
        }
        if (!resourceFile.exists()) {
            throw new ServiceException(ServiceConstants.MSG_NOT_FOUND, ServiceConstants.OPR_GET_OWNERSHIP_INFO, resourceName);
        }
        String parentFolder = resourceFile.getParent();
        String fileName = resourceFile.getName();
        setOwnershipInfo(Paths.get(parentFolder, fileName), new ResourceOwnershipInfo(getUser(userName), getGroup(parentFolder)));
    }

    /**
     * @param resourceName
     *            Name of file/folder
     * @param userName
     *            Unix user account name - Group taken from passed home Folder
     */
    public void setOwnershipInfo(String resourceName, String userName, String homeFolder) throws ServiceException {
        File resourceFile = new File(resourceName);
        if (resourceName.trim().equals("/")) {
            throw new ServiceException(ServiceConstants.MSG_INVALID_RESOURCE, ServiceConstants.OPR_SET_OWNERSHIP_INFO, resourceName);
        }
        if (!resourceFile.exists()) {
            throw new ServiceException(ServiceConstants.MSG_NOT_FOUND, ServiceConstants.OPR_GET_OWNERSHIP_INFO, resourceName);
        }
        String parentFolder = resourceFile.getParent();
        String fileName = resourceFile.getName();
        setOwnershipInfo(Paths.get(parentFolder, fileName), new ResourceOwnershipInfo(getUser(userName), getGroup(homeFolder)));
    }

    // /**
    // * @param resourceName
    // * Name of file/folder(Complete path)
    // */
    // public void setPermission(String resourceName) throws ServiceException {
    // File resourceFile = new File(resourceName);
    // Set<PosixFilePermission> perms = new HashSet<>();
    //
    // if (resourceName.trim().equals("/")) {
    // throw new ServiceException(ServiceConstants.MSG_INVALID_RESOURCE,
    // ServiceConstants.OPR_SET_OWNERSHIP_INFO,
    // resourceName);
    // }
    // if (!resourceFile.exists()) {
    // throw new ServiceException(ServiceConstants.MSG_NOT_FOUND,
    // ServiceConstants.OPR_GET_OWNERSHIP_INFO,
    // resourceName);
    // }
    //
    // if (resourceFile.isDirectory()) {
    // perms.add(PosixFilePermission.OWNER_READ);
    // perms.add(PosixFilePermission.OWNER_WRITE);
    // perms.add(PosixFilePermission.OWNER_EXECUTE);
    // } else {
    // perms.add(PosixFilePermission.OWNER_READ);
    // perms.add(PosixFilePermission.OWNER_WRITE);
    // }
    //
    // try {
    // Files.setPosixFilePermissions(resourceFile.toPath(),
    // (resourceFile.isDirectory()?getDefaultFolderPermissions():getDefaultFilePermissions()));
    // } catch (IOException ie) {
    // throw new ServiceException(ie.getMessage(),
    // ServiceConstants.OPR_SET_PERMISSION_INFO,
    // resourceFile.getName().toString());
    // }
    // }

    public void setPermission(File resourceFile, Set<PosixFilePermission> permissions) throws ServiceException {
        // Do not perform on root folder
        if (resourceFile.toString().equals("/")) {
            throw new ServiceException(ServiceConstants.MSG_INVALID_RESOURCE, ServiceConstants.OPR_SET_OWNERSHIP_INFO, resourceFile.toString());
        }
        // Resource should exist
        if (!resourceFile.exists()) {
            throw new ServiceException(ServiceConstants.MSG_NOT_FOUND, ServiceConstants.OPR_GET_OWNERSHIP_INFO, resourceFile.toString());
        }
        try {
            Files.setPosixFilePermissions(resourceFile.toPath(), permissions);
        } catch (Exception e) {
            throw new ServiceException(e.getMessage(), ServiceConstants.OPR_SET_PERMISSION_INFO);
        }
    }

    /**
     * @return Default file rights to be assigned to a folder
     */
    public Set<PosixFilePermission> getDefaultFolderPermissions() {
        Set<PosixFilePermission> folderPermissions = new HashSet<PosixFilePermission>();
        folderPermissions.add(PosixFilePermission.OWNER_READ);
        folderPermissions.add(PosixFilePermission.OWNER_WRITE);
        folderPermissions.add(PosixFilePermission.OTHERS_EXECUTE);
        return folderPermissions;
    }

    /**
     * @return Default file rights to be assigned to a new file created
     *         (compress)
     */
    public Set<PosixFilePermission> getDefaultFilePermissions() {
        Set<PosixFilePermission> folderPermissions = new HashSet<PosixFilePermission>();
        folderPermissions.add(PosixFilePermission.OWNER_READ);
        folderPermissions.add(PosixFilePermission.OWNER_WRITE);
        folderPermissions.add(PosixFilePermission.GROUP_READ);
        folderPermissions.add(PosixFilePermission.GROUP_WRITE);
        folderPermissions.add(PosixFilePermission.OTHERS_READ);
        return folderPermissions;
    }

    /**
     * @param folder
     *            Folder URI
     * @return
     */
    public Set<PosixFilePermission> getFolderPermissions(String folder) {
        try {
            return Files.getPosixFilePermissions(Paths.get(folder));
        } catch (Exception e) {
            return getDefaultFolderPermissions();
        }
    }
}
