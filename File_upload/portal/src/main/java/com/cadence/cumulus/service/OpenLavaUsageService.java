package com.cadence.cumulus.service;

import java.util.List;

import com.cadence.cumulus.model.OpenLavaResourceVO;

/**
 * @author Vikramaditya Singh
 *
 */
public interface OpenLavaUsageService {
	
	public List<OpenLavaResourceVO> findUsageDetailsFromRange(Long startTime, Long finishTime);
	
}
