package com.cadence.cumulus.service;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.cadence.cumulus.common.ServiceConstants;
import com.cadence.cumulus.common.ServiceException;

/**
 *
 * @author ychandra
 *
 */
@Service
public class LicenseAgentService {

    private static final Logger log = LoggerFactory.getLogger(LicenseAgentService.class);

    @Value("${license.server.check}")
    private Boolean LICENSE_SERVER_CHECK;


    @Value("${license.server.avail.cmd}")
    private String LICENSE_SERVER_AVAILABLE_COMMAND;


    @Value("${license.returnval.pattern}")
    private String LICENSE_RETURN_VAL_PATTERN;

    @Value("${license.returnval.name}")
    private String LICENSE_RETURN_VAL_NAME;


    /**
     * Wait for license server to restart for number of minutes returned by the server_available.sh script.
     * If the server doesn't comes back in 5 minutes this method exits.
     *
     * @throws ServiceException
     */
    public void waitForLicenseAgent() throws ServiceException {

        if (!LICENSE_SERVER_CHECK) {
            log.debug("License Server Check is disabled");
            return;
        }

        try {
            short status = getLicenseAgentServerStatus();
            if (status == 0) {
                log.debug("License Server is running..");
            } else if (status > 5) {
                log.error("Server is not running/failed to restart.. status code :" + status);
                throw new ServiceException(ServiceConstants.MSG_FAILURE,ServiceConstants.OPR_CHECK_LICENSE_SERVER, ""+status);
            } else {
                log.debug("License Server is rebooting.. Sleep for {}  minutes",status);
                Thread.sleep(1000 * 60 * status);
                status = getLicenseAgentServerStatus();
                if (status == 0 || status > 5) {
                    log.debug("License Server is running..");
                } else {
                    log.error("Server is not running/failed to restart.. status code :" + status);
                    throw new ServiceException(ServiceConstants.MSG_FAILURE,ServiceConstants.OPR_CHECK_LICENSE_SERVER, ""+status);
                }
            }

        } catch (InterruptedException e) {
            log.error("Error: ", e);
        }

    }

    /**
     *
     * Execute server_available.sh script on license server to see if server is available.
     * eg. "Timestamp is 1494295441
     * Elapsed Minutes=1
     * Return Value is 4"
     * If it returns between 1-5 then server will be available in that many minutes.
     * If it returns a 6 then manage_server.sh has not been able to start the server.
     * 7 if there is any error executing the script
     * @return
     */
    private short getLicenseAgentServerStatus() {
        BufferedReader stdout = null;
        Process p = null;
        try {

            // execute a command using ssh cmls@aw05ls01 /bin/server_available.sh
            List<String> commands = new ArrayList<>();
            for (String command : LICENSE_SERVER_AVAILABLE_COMMAND.split("~~")) {
                commands.add(command);
            }

			 /* Create a Runtime process using ProcessBuilder and read the output

			 */
            ProcessBuilder builder = new ProcessBuilder(commands).redirectErrorStream(true);
            p = builder.start();
            stdout = new BufferedReader(new InputStreamReader(p.getInputStream()));
            String s;
            StringBuffer sbuf = new StringBuffer();
            while ((s = stdout.readLine()) != null) {
                log.debug("Alive :{}, OUT: {}", p.isAlive(), s);
                sbuf.append(s).append("\n");
            }
            String status = sbuf.toString();

            log.debug("License Server Agent Response: {}",status);

            // check if the output has contains the expected pattern. "(.*)(Return Value is )(\\d+?)"
            Pattern r = Pattern.compile(LICENSE_RETURN_VAL_PATTERN,
                    Pattern.CASE_INSENSITIVE | Pattern.DOTALL | Pattern.MULTILINE);
            String returnVal = null;
            String returnValTime = null;
            Matcher m = r.matcher(status);

            // extract return value
            if (m.matches() && m.groupCount() > 2) {
                returnVal = m.group(2);
                returnValTime = m.group(3);
            }

            if (returnVal != null && returnValTime != null && returnVal.contains(LICENSE_RETURN_VAL_NAME)) { //"Return Value is "
                return Short.parseShort(returnValTime);
            }

        } catch (Exception e) {
            log.error("Error: ", e);
        } finally {
            try {
                if (stdout != null) {
                    stdout.close();
                }
            } catch (IOException e) {
                log.error("Error: ", e);
            }

        }
        // error or other cases
        return 7;
    }
}
