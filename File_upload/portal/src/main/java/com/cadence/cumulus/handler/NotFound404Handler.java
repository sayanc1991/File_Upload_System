package com.cadence.cumulus.handler;

import static org.springframework.http.HttpStatus.NOT_FOUND;

import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.web.AbstractErrorController;
import org.springframework.boot.autoconfigure.web.ErrorAttributes;
import org.springframework.boot.autoconfigure.web.ErrorViewResolver;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/error")
public class NotFound404Handler extends AbstractErrorController {
    private static final Logger log = LoggerFactory.getLogger(NotFound404Handler.class);

    @Value("${cumulus.base.url}")
    public String baseUrl;

    @Autowired
    public NotFound404Handler(ErrorAttributes errorAttributes, List<ErrorViewResolver> errorViewResolvers) {
        super(errorAttributes, errorViewResolvers);
    }

    @Override
    public String getErrorPath() {
        return "/error";
    }

    @RequestMapping
    public String handle(HttpServletRequest request, HttpServletResponse response) throws IOException {
        String originalRequestPath = (String) request.getAttribute("javax.servlet.error.request_uri");
        log.debug("Original request path: {}", originalRequestPath);
        
        HttpStatus status = getStatus(request);
        log.debug("Status is {}", status);

        if (status == NOT_FOUND) {
            if (originalRequestPath.startsWith("/")) {
                originalRequestPath = originalRequestPath.substring(1, originalRequestPath.length());
            }

            response.sendRedirect(baseUrl + "?referer=" + originalRequestPath.replaceAll("/", "."));
        }else{
    		return new JSONObject(getErrorAttributes(request, true)).toString();
        }
        return "";
    }
}
