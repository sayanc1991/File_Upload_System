package com.cadence.cumulus.controller;

import java.io.IOException;
import java.security.SecureRandom;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.TimeZone;

import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.cadence.cumulus.common.AppMessages;
import com.cadence.cumulus.common.Constants;
import com.cadence.cumulus.common.CumulusException;
import com.cadence.cumulus.integration.IntegrationException;
import com.cadence.cumulus.integration.unifiedregistration.service.URIntegrationService;
import com.cadence.cumulus.integration.unifiedregistration.vo.ChangePasswordRequest;
import com.cadence.cumulus.integration.unifiedregistration.vo.ChangePasswordVO;
import com.cadence.cumulus.integration.unifiedregistration.vo.CreateUserVO;
import com.cadence.cumulus.integration.unifiedregistration.vo.SecurityQARequest;
import com.cadence.cumulus.integration.unifiedregistration.vo.URResponse;
import com.cadence.cumulus.integration.unifiedregistration.vo.UpdateUserContactInfoVO;
import com.cadence.cumulus.integration.unifiedregistration.vo.UpdateUserVO;
import com.cadence.cumulus.integration.unifiedregistration.vo.UserAuthDetail;
import com.cadence.cumulus.integration.unifiedregistration.vo.UserContactInfoVO;
import com.cadence.cumulus.model.Countries;
import com.cadence.cumulus.model.CumulusUser;
import com.cadence.cumulus.model.CumulusUserNotificationSettings;
import com.cadence.cumulus.model.States;
import com.cadence.cumulus.model.User;
import com.cadence.cumulus.service.CountriesService;
import com.cadence.cumulus.service.CumulusUserService;
import com.cadence.cumulus.service.EmailService;
import com.cadence.cumulus.service.StatesService;
import com.cadence.cumulus.service.UserManagementService;

@RestController
@RequestMapping(value = "/user")
public class UserProfileController {
    @Autowired
    private CumulusUserService userService;

	@Autowired
	private URIntegrationService urIntegrationService;

	@Value("${ur.cumulus.create.newuser.appcode}")
	private String newUserAppcode;

	@Value("${ur.cumulus.create.newuser.type}")
	private String newUserType;

	@Value("${ur.cumulus.create.newuser.accountId}")
	private String accountId;

	@Value("${ur.cumulus.create.newuser.parent.accountId}")
	private String parentAccountId;
	
    @Value("${cumulus.user.registration.template}")
    private String NEW_USER_REGISTRATION_TEMPLATE;

    @Value("${cumulus.user.registration.subject}")
    private String NEW_USER_REGISTRATION_SUBJECT;
    
    @Value("${cumulus.url}")
    private String CUMULUS_URL;

    @Value("${ur.query.change.password.url}")
    private String CHANGE_PASSWORD_URL;

    @Value("${cadence.logo}")
    private String CADENCE_LOGO;

	@Autowired
	private UserManagementService userManagementService;
	
    @Autowired
    private EmailService emailService;

    @Autowired
    private CountriesService countriesService;
    
    @Autowired
    private StatesService statesService;
    
	private static final Random RANDOM = new SecureRandom();
	
	public static final int PASSWORD_LENGTH = 8;

	private String CADENCE_DOMAIN = "@cadence.com";
    
    private String INTERNAL_USER = "int";

    /**
     * Logger
     */
    private static final Logger log = LoggerFactory.getLogger(UserProfileController.class);

    @GetMapping(value = "/profile")
    public @ResponseBody CumulusUser getUserProfile() throws CumulusException {
        log.debug("Fetching user profile");

        return (CumulusUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
    }

    @GetMapping(value = "/settings")
    @ResponseStatus(HttpStatus.OK)
    public @ResponseBody CumulusUserNotificationSettings getUserSettings(HttpServletResponse response) {
        CumulusUser user = (CumulusUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        CumulusUserNotificationSettings settings = userService.getNotificationSettings(user.getUsername());

        if (settings == null) {
            response.setStatus(HttpStatus.NO_CONTENT.value());
        }

        return settings;
    }

    @PutMapping(value = "/settings")
    @ResponseStatus(HttpStatus.OK)
    public CumulusUserNotificationSettings updateUserSettings(@RequestBody CumulusUserNotificationSettings notificationSettings, HttpServletResponse response) {
        log.debug("Attempting to update notification settings");

        if (StringUtils.isEmpty(notificationSettings.getEmail())) {
            log.debug("No email address provided. Sending bad request");

            response.setStatus(HttpStatus.BAD_REQUEST.value());
            return null;
        }

        CumulusUser user = (CumulusUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        if (!user.getUsername().equalsIgnoreCase(notificationSettings.getEmail().trim())) {
            log.debug("Logged in user email ({}) and update email address ({}) don't match. Sending forbidden", user.getUsername(), notificationSettings.getEmail());

            response.setStatus(HttpStatus.FORBIDDEN.value());
            return null;
        }

        log.debug("Updating notification settings");


        return userService.updateNotificationSettings(notificationSettings);
    }

	@RequestMapping(value = "/changePassword", method = RequestMethod.POST)
	public URResponse changePassword(@RequestBody ChangePasswordRequest changePasswordRequest, 
			HttpServletResponse response) throws IOException, CumulusException {
		URResponse urResponse = new URResponse();
		CumulusUser cumulusUser = (CumulusUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

		String email = cumulusUser.getUserSettings().getEmail();
		String newpwd = changePasswordRequest.getNewpwd();
		String oldpwd = changePasswordRequest.getOldpwd();
		String confirmPwd = changePasswordRequest.getConfirmPwd();

		if (StringUtils.isBlank(newpwd) || StringUtils.isBlank(oldpwd) || StringUtils.isBlank(confirmPwd)) {
			// throw new CumulusException("User password should not be empty.");
			throw new CumulusException(AppMessages.USER_CHANGE_PASSWORD_FIELDS_EMPTY.getMessage()
					+ String.format(Constants.ERROR_CODE, AppMessages.USER_CHANGE_PASSWORD_FIELDS_EMPTY.getCode()));
		}
		
		if (!newpwd.equals(confirmPwd)) {
			// throw new CumulusException("User new password and confirm password should be same.");
			throw new CumulusException(AppMessages.USER_CHANGE_PASSWORD_INVALID.getMessage()
					+ String.format(Constants.ERROR_CODE, AppMessages.USER_CHANGE_PASSWORD_INVALID.getCode()));
		}

		if (StringUtils.containsWhitespace(newpwd) || StringUtils.containsWhitespace(oldpwd)) {
			// throw new CumulusException("User password should not contain a white space character.");
			throw new CumulusException(AppMessages.USER_CHANGE_PASSWORD_INVALID_VALUE.getMessage()
					+ String.format(Constants.ERROR_CODE, AppMessages.USER_CHANGE_PASSWORD_INVALID_VALUE.getCode()));
		}

		User user;
		try {
			user = urIntegrationService.getUserDetails(email); // 
		} catch (IntegrationException ie) {
			log.error("Exception while invoking UR integration API", ie);
			// TODO : handle exception during 403 error.
			throw new CumulusException(ie.getMessage());
		}
		// if user not found from UR , throw exception
		if (user == null) {
			// throw new CumulusException("User " + email + " not found in Unified Registration");
			throw new CumulusException(String.format(AppMessages.USER_CHANGE_PASSWORD_NOT_FOUND.getMessage(), email)
					+ String.format(Constants.ERROR_CODE, AppMessages.USER_CHANGE_PASSWORD_NOT_FOUND.getCode()));
		}

		log.info("contactId obtained from UR : "+user.getContactId());
		if (StringUtils.isEmpty(user.getContactId())) {
			throw new CumulusException("User contactId not found in Unified Registration.");
		} else {
			changePasswordRequest.setContactId(user.getContactId());
		}
		log.info("Cumulus user contact Id : "+user.getContactId());

		changePasswordRequest.setUsername(email); // 
		
		ChangePasswordVO changePasswordVO = new ChangePasswordVO();
		
		BeanUtils.copyProperties(changePasswordRequest, changePasswordVO);

		try {
			urResponse = urIntegrationService.updateUserPassword(changePasswordVO);
		} catch (IntegrationException ie) {
			log.error("Exception while invoking UR integration API", ie);
			// TODO : handle exception during 403 error.
			// throw new CumulusException("Updating user password failed, Please try again."); // ie.getMessage()
			throw new CumulusException(AppMessages.USER_CHANGE_PASSWORD_FAIL.getMessage()
					+ String.format(Constants.ERROR_CODE, AppMessages.USER_CHANGE_PASSWORD_FAIL.getCode()));
		}
		
		return urResponse;
	}

	/**
	 * This controller method is for populating drop down for Countries
	 * 
	 * @param response
	 * @return
	 * @throws IOException
	 */
	@RequestMapping(value = "/countries", method = RequestMethod.GET)
	public List<Countries> listAllCountires(HttpServletResponse response) throws IOException {
		List<Countries> countryList = new ArrayList<>();
		try {
			countryList = countriesService.getAllCountries();
			log.info("Completed retrieving all countries.");
		} catch (CumulusException ce) {
			log.error("Exception while invoking getAllCountries method in CountriesService", ce);
			response.setContentType(MediaType.TEXT_PLAIN_VALUE);
			response.sendError(HttpStatus.INTERNAL_SERVER_ERROR.value(), ce.getMessage());
		}
		return countryList;
	}
	
	/**
	 * This controller method is for populating drop down for states for selected country.
	 * 
	 * @param response
	 * @return
	 * @throws IOException
	 */
	/*@RequestMapping(value = "/state/{countryCode}", method = RequestMethod.POST)
	public List<States> listAllCountires(@PathVariable("countryCode") String countryCode, HttpServletResponse response) throws IOException {*/
	@RequestMapping(value = "/state", method = RequestMethod.POST)
	public List<States> listAllCountires(@RequestBody Countries countries, HttpServletResponse response) throws IOException {
		List<States> statesList = new ArrayList<>();
		try {
			log.info("Started retrieving all states for the country : "+countries.getCountryCode());
			statesList = statesService.getStatesByCountryCode(countries.getCountryCode());
			log.info("Completed retrieving all states for the country : "+countries.getCountryCode());
		} catch (CumulusException ce) {
			log.error("Exception while invoking getStatesByCountryCode method in StatesService", ce);
			response.setContentType(MediaType.TEXT_PLAIN_VALUE);
			response.sendError(HttpStatus.INTERNAL_SERVER_ERROR.value(), ce.getMessage());
		}
		return statesList;
	}
	
	@RequestMapping(value = "/create", method = RequestMethod.POST)
	public UserAuthDetail create(@RequestBody UserAuthDetail userAuthDetail, 
			HttpServletResponse response) throws IOException, CumulusException {
		URResponse urResponse = null;
		CumulusUser loggedInUser = (CumulusUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		String loggedInUserEmail = loggedInUser.getUserSettings().getEmail();
		log.debug("userRequest : "+userAuthDetail.toString());
		User userReq = userAuthDetail.getUser();
		if (StringUtils.isBlank(userReq.getEmail()) || StringUtils.isBlank(userReq.getFirstName())
				|| StringUtils.isBlank(userReq.getLastName())) {
			throw new CumulusException("Please enter mandatory user details.");
		}
		log.debug("user contact Id : "+userReq.getContactId());
		
		// Registering user in cadence.
		if (userReq.getContactId() == null || "".equals(userReq.getContactId())) {
			if (StringUtils.isBlank(userReq.getCountry().getCountryName())
					|| StringUtils.isBlank(userReq.getStreet()) 
					|| StringUtils.isBlank(userReq.getCity())
					|| StringUtils.isBlank(userReq.getPostalCode()) 
					|| StringUtils.isBlank(userReq.getPhone())) {
				throw new CumulusException("Please enter mandatory user details.");
			}

			// Added validation to avoid internal user creation(i.e @cadence.com)
			if (userReq.getEmail().endsWith(CADENCE_DOMAIN)) {
				throw new CumulusException(AppMessages.USER_REGISTRATION_CREATE_INTERNAL_USER.getMessage()
						+ String.format(Constants.ERROR_CODE, AppMessages.USER_REGISTRATION_CREATE_INTERNAL_USER.getCode()));
			}
			
			urResponse = new URResponse();
			CreateUserVO createUserVO = new CreateUserVO();
			String genPassword = copyProperties(userReq, createUserVO);
			log.info("createUser : "+createUserVO.toString());

			try {
				urResponse = urIntegrationService.createUser(createUserVO);
			} catch (IntegrationException ie) {
				log.error("Exception while invoking UR integration API", ie);
				// TODO : handle exception during 403 error.
				// throw new CumulusException("Creating user in UR failed, Please try again.");
				throw new CumulusException(AppMessages.USER_REGISTRATION_CREATE_ERROR.getMessage()
						+ String.format(Constants.ERROR_CODE, AppMessages.USER_REGISTRATION_CREATE_ERROR.getCode()));
			}
			
			String userEmail = userReq.getEmail();
	        Map<String, String> params = new HashMap<>();
			params.put("firstName", userReq.getFirstName());
	        params.put("url", CHANGE_PASSWORD_URL);
	        // params.put("imgSrc", CUMULUS_URL + CADENCE_LOGO);
	        params.put("cadenceUrl", CUMULUS_URL);
			params.put("email", userEmail);
			params.put("tempPassword", genPassword);
			params.put("adminEmail", loggedInUserEmail);

	        log.info("Notifying {}, via email about new user registration in cadence ", userEmail);
			emailService.sendEmail(NEW_USER_REGISTRATION_TEMPLATE, params, userEmail, NEW_USER_REGISTRATION_SUBJECT);
			userAuthDetail.getUser().setContactId(urResponse.getSfdcconid());
		} else {
			// TODO : Update UR API required to activate user in Okta.
			// Below code used to activate user in UR.
			UpdateUserVO updateUserVO = new UpdateUserVO();
			updateUserVO.setLoginid(userReq.getEmail());
			UpdateUserContactInfoVO userContactInfoVO = new UpdateUserContactInfoVO();
			userContactInfoVO.setId(userReq.getContactId());
			updateUserVO.setCon(userContactInfoVO);
			updateUserVO.setAppcode("Add_"+newUserAppcode);
			updateUserVO.setUsertype(newUserType);
			if (userReq.getEmail().endsWith(CADENCE_DOMAIN)) {
				updateUserVO.setUsertype(INTERNAL_USER);
			} else {
				updateUserVO.setUsertype(newUserType);
			}
			log.info("Start of re-adding user : "+updateUserVO.toString());
			try {
				urIntegrationService.updateUserGroup(updateUserVO);
			} catch (IntegrationException ie) {
				log.error("Exception while invoking UR integration API", ie);
				// TODO : handle exception during 403 error.
				// throw new CumulusException("Re-adding user to group failed, Please try again.");
				throw new CumulusException(AppMessages.USER_REGISTRATION_UPDATE_ERROR.getMessage()
						+ String.format(Constants.ERROR_CODE, AppMessages.USER_REGISTRATION_UPDATE_ERROR.getCode()));
			}
		}

		UserAuthDetail responseObj = userManagementService.addNewUser(userAuthDetail);

		return responseObj;
	}
	
	private String copyProperties(User userReq, CreateUserVO createUserVO) {
		createUserVO.setLoginid(userReq.getEmail());
		createUserVO.setfName(userReq.getFirstName());
		createUserVO.setlName(userReq.getLastName());
		UserContactInfoVO contactInfo = new UserContactInfoVO();
		contactInfo.setEmail(userReq.getEmail());
		contactInfo.setFirstName(userReq.getFirstName());
		contactInfo.setLastName(userReq.getLastName());
		contactInfo.setAccountId(accountId);
		contactInfo.setParent_Account__c(parentAccountId);
		contactInfo.setCountry__c(userReq.getCountry().getCountryCode());
		contactInfo.setStreet__c(userReq.getStreet());
		contactInfo.setCity__c(userReq.getCity());
		contactInfo.setState_Region__c(userReq.getStateRegion());
		contactInfo.setPostal_Code__c(userReq.getPostalCode());
		contactInfo.setApp_Code__c(newUserAppcode);
		contactInfo.setPhone(userReq.getPhone());
		createUserVO.setCon(contactInfo);
		createUserVO.setAppcode(newUserAppcode);
		createUserVO.setUsertype(newUserType);
		String genPasswd = autoGeneratePwdLogic(userReq.getEmail(), newUserAppcode);
		createUserVO.setPwd(genPasswd);
		return genPasswd;
	}

	@RequestMapping(value = "/resetSQA", method = RequestMethod.POST)
	public URResponse resetSQA(@RequestBody SecurityQARequest securityQARequest) throws IOException, CumulusException {
		URResponse urResponse = new URResponse();
		CumulusUser cumulusUser = (CumulusUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

		String email = cumulusUser.getUserSettings().getEmail();
		String password = securityQARequest.getPwd();
		String question = securityQARequest.getQuestion();
		String answer = securityQARequest.getAns();
		
		if (StringUtils.isBlank(password) || StringUtils.isBlank(question) || StringUtils.isBlank(answer)) {
			// throw new CumulusException("User password / security question / security answer should not be empty.");
			throw new CumulusException(AppMessages.USER_RESET_SECURITY_QA_FIELDS_EMPTY.getMessage()
					+ String.format(Constants.ERROR_CODE, AppMessages.USER_RESET_SECURITY_QA_FIELDS_EMPTY.getCode()));
		}

		if (password.equalsIgnoreCase(question) || password.equalsIgnoreCase(answer)) {
			// throw new CumulusException("User password / security question / security answer must not have same values.");
			throw new CumulusException(AppMessages.USER_RESET_SECURITY_QA_INVALID_PASSWORD.getMessage() + String
					.format(Constants.ERROR_CODE, AppMessages.USER_RESET_SECURITY_QA_INVALID_PASSWORD.getCode()));
		}

		if (answer.equalsIgnoreCase(cumulusUser.getUsername()) || answer.equalsIgnoreCase(email)) {
			// throw new CumulusException("User answer should not match its Username.");
			throw new CumulusException(AppMessages.USER_RESET_SECURITY_QA_INVALID_ANSWER.getMessage()
					+ String.format(Constants.ERROR_CODE, AppMessages.USER_RESET_SECURITY_QA_INVALID_ANSWER.getCode()));
		}

		User user;
		try {
			user = urIntegrationService.getUserDetails(email); // 
		} catch (IntegrationException ie) {
			log.error("Exception while invoking UR integration API", ie);
			// TODO : handle exception during 403 error.
			throw new CumulusException(ie.getMessage());
		}
		// if user not found from UR , throw exception
		if (user == null) {
			// throw new CumulusException("User " + email + " not found in Unified Registration");
			throw new CumulusException(String.format(AppMessages.USER_RESET_SECURITY_QA_NOT_FOUND.getMessage(), email)
					+ String.format(Constants.ERROR_CODE, AppMessages.USER_RESET_SECURITY_QA_NOT_FOUND.getCode()));
		}

		log.info("contactId obtained from UR : "+user.getContactId());
		if (StringUtils.isEmpty(user.getContactId())) {
			throw new CumulusException("User contactId not found in Unified Registration.");
		} else {
			securityQARequest.setContactid(user.getContactId());
		}
		log.info("Cumulus user contact Id : "+user.getContactId());

		securityQARequest.setUsername(email); // 

		try {
			urResponse = urIntegrationService.resetSecurityQA(securityQARequest);
		} catch (IntegrationException ie) {
			log.error("Exception while invoking UR integration API", ie);
			// TODO : handle exception during 403 error.
			// throw new CumulusException("Updating user security question/answer failed, Please try again."); // ie.getMessage()
			throw new CumulusException(AppMessages.USER_RESET_SECURITY_QA_FAIL.getMessage()
					+ String.format(Constants.ERROR_CODE, AppMessages.USER_RESET_SECURITY_QA_FAIL.getCode()));
		}

		return urResponse;
	}

	/**
	 * Generate a random String suitable for use as a temporary password.
	 *
	 * @return String suitable for use as a temporary password
	 */
	private String generateRandomPassword() {
		String capital_chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
		String small_chars = "abcdefghijklmnopqrstuvwxyz";
		String numbers = "0123456789";
		String symbols = "!@#$%^&*_=+-/.?<>)";

		String letters = capital_chars + small_chars + numbers + symbols;

		String pw = "";
		for (int i = 0; i < PASSWORD_LENGTH; i++) {
			int index = (int) (RANDOM.nextDouble() * letters.length());
			pw += letters.substring(index, index + 1);
		}
		return pw;
	}

	public static String autoGeneratePwdLogic(String emailId, String appName) {
		String autGenPwd = "";
		DateFormat dateFormat = new SimpleDateFormat("HHmmssddMMyyyy");
		Calendar now = Calendar.getInstance(TimeZone.getDefault());
		String dt = dateFormat.format(now.getTime());

		if (emailId != null) {
			try {
				// split the email id
				String[] emailSep;
				emailSep = emailId.split("\\@");
				String splitEmail = emailSep[0].toLowerCase();

				splitEmail = splitEmail.replaceAll("[^a-zA-Z]", "");
				// System.debug('=======SPLIT replace all========' +splitEmail);
				Integer len = splitEmail.length();
				// System.debug('=========EMAIL LENGTH======' + len);
				String appInfo = null;
				if (appName != null && appName != "") {
					appInfo = appName.toUpperCase();
					if ("cdotcom".equalsIgnoreCase(appInfo))
						appInfo = "CD0T";
					if ("lms".equalsIgnoreCase(appInfo))
						appInfo = "LM5";
					if ("cos".equalsIgnoreCase(appInfo))
						appInfo = "C0S";
					if ("eot".equalsIgnoreCase(appInfo))
						appInfo = "E0T";
				}

				if (len == 0) {
					splitEmail = "a";
				} else if (len == 1) {
					splitEmail = splitEmail;
				} else if (len == 2) {
					splitEmail = splitEmail;
				} else if (len >= 3) {
					splitEmail = splitEmail.substring(0, 2);
					splitEmail = splitEmail;
				} /*
					 * else if(len >= 4){ splitEmail =
					 * splitEmail.substring(0,3); splitEmail = splitEmail; }
					 */

				if (splitEmail != null) {
					autGenPwd = splitEmail + dt + appInfo;
				}
			} catch (Exception exp) {
				log.info("====Error====" + exp);
			}
		}

		log.info("====autoGeneratePwdLogic====" + autGenPwd);
		return autGenPwd;
	}
}
