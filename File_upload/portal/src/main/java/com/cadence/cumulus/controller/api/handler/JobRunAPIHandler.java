package com.cadence.cumulus.controller.api.handler;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.FilenameUtils;
import org.omg.CORBA.portable.ApplicationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;

import com.cadence.cumulus.common.Constants;
import com.cadence.cumulus.handler.AgentHandler;
import com.cadence.cumulus.model.AgentCommand;
import com.cadence.cumulus.model.CumulusUser;
import com.cadence.cumulus.model.Job;
import com.cadence.cumulus.model.JobRun;
import com.cadence.cumulus.service.JobRunService;
import com.cadence.cumulus.service.JobService;

@Component
public class JobRunAPIHandler {

	@Autowired
	private AgentHandler handler;

	/**
	 * Logger
	 */
	private static final Logger log = LoggerFactory
			.getLogger(JobRunAPIHandler.class);

	@Autowired
	private JobService jobService;

	@Autowired
	private JobRunService jobRunService;

	private static final String JOBS = "jobs";

	public JobRun runJob(JobRun jobRun, CumulusUser user,
			HttpServletResponse response) throws ApplicationException,
			IOException {
		
		if (!handler.isConnected()) {
			log.debug("Agent not available");

			response.setContentType(MediaType.TEXT_PLAIN_VALUE);
			response.sendError(HttpStatus.SERVICE_UNAVAILABLE.value(),
					"Agent not available");
			return null;
		}
		Job job = null;
		/* Retrieving the Job on the basis of either jobId or JobName*/
		if(jobRun.getJobId() != null) {
			log.debug("Checking if the job with id: {}, exists", jobRun.getJobId());
			 job = jobService.getJobById(jobRun.getId());

		}else if(jobRun.getJobName() != null) {
			List<Job> jobList = jobService.findByJobNameForUser(jobRun.getJobName(), user.getId());
			
			if(jobList != null && jobList.size() > 1) {
				response.setContentType(MediaType.TEXT_PLAIN_VALUE);
				response.sendError(HttpStatus.NOT_FOUND.value(),
						"Multiple Jobs Found ");
				
				return null;
			}
			if(jobList != null && jobList.size() > 0) {
				job = jobList.get(0);
				jobRun.setJobId(job.getId());
				
			}
		}
		if (job == null) {
			response.sendError(HttpStatus.BAD_REQUEST.value(), "Job not Found");
			return null;
		}
		log.debug("Run job {} ", jobRun);
		
		/*CumulusUser user = (CumulusUser) SecurityContextHolder.getContext()
				.getAuthentication().getPrincipal();
				*/

		
		Date d = new Date();
		
		jobRun.setStatus("pending");
		jobRun.setCreatedDate(d);
		jobRun.setLastUpdatedDate(d);
		jobRun.setStartTime(d);
		jobRun.setSubmitTime(d);

		jobRun.setCreatedBy(user.getId());
		jobRun.setLastModifiedBy(user.getId());
		jobRun.setJob(job);
		jobRun = jobRunService.saveJobRun(jobRun);

		

		List<String> args = new ArrayList<>();
		args.add(job.getScriptPath());
		args.add(job.getClArguments());

		String fileExtn = FilenameUtils.getExtension(job.getScriptPath());
		log.debug("Script path is: {}, extension is: {}", job.getScriptPath(),
				fileExtn);

		if (fileExtn != null && !fileExtn.isEmpty()) {
			if (fileExtn.equalsIgnoreCase(Constants.TCL)) {
				log.debug("Sending command to start a job using TCL script");
				handler.sendCommand(new AgentCommand(jobRun.getId(), user
						.getLoginName(), job.getInstanceType(), job
						.getNumberOfNodes(), JOBS, Constants.NEWSUB, job
						.getScriptPath(), jobRun.getLiberatePath(), job
						.getToolName(), job.getClArguments()));
			} else if (fileExtn.equalsIgnoreCase(Constants.SH)) {
				log.debug("Sending command to start a job using Shell Script");
				handler.sendCommand(new AgentCommand(jobRun.getId(), user
						.getLoginName(), job.getInstanceType(), job
						.getNumberOfNodes(), JOBS, Constants.NEWSUB, job
						.getScriptPath(), jobRun.getLiberatePath(), "sh", job
						.getClArguments()));
			} else if (fileExtn.equalsIgnoreCase(Constants.MK)) {
				log.debug("Sending command to start a job using .MK file");
				handler.sendCommand(new AgentCommand(jobRun.getId(), user
						.getLoginName(), job.getInstanceType(), job
						.getNumberOfNodes(), JOBS, Constants.NEWSUB, job
						.getScriptPath(), jobRun.getLiberatePath(), "make -f",
						job.getClArguments()));
			} else if (job.getShebang() != null && !job.getShebang().equals("")) {
				log.debug("Sending command to start a job using interpreter found at line number 1");
				handler.sendCommand(new AgentCommand(jobRun.getId(), user
						.getLoginName(), job.getInstanceType(), job
						.getNumberOfNodes(), JOBS, Constants.NEWSUB, job
						.getScriptPath(), jobRun.getLiberatePath(), jobRun
						.getShebang(), job.getClArguments()));
			} else {
				log.debug("Invalid script, not sending any command");
				response.sendError(HttpStatus.NOT_FOUND.value(),
						"Invalid script");
			}
		} else if (job.getScriptPath().endsWith("Makefile")) {
			log.debug("Sending command to start a job using Makefile");
			handler.sendCommand(new AgentCommand(jobRun.getId(), user
					.getLoginName(), job.getInstanceType(), job
					.getNumberOfNodes(), JOBS, Constants.NEWSUB, job
					.getScriptPath(), jobRun.getLiberatePath(), "make -f", job
					.getClArguments()));
		} else {
			response.sendError(HttpStatus.NOT_FOUND.value(), "Invalid script");
		}

		return jobRun;
	}

}
