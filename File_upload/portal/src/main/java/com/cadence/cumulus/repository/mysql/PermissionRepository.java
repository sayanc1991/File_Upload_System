/**
 * 
 */
package com.cadence.cumulus.repository.mysql;

import org.springframework.data.jpa.repository.JpaRepository;

import com.cadence.cumulus.model.Permission;

/**
 * 
 * @author shirish
 *
 */

public interface PermissionRepository extends JpaRepository<Permission, Long> {

}
