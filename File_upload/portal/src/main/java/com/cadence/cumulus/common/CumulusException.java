package com.cadence.cumulus.common;

/**
 * Created by Gautam Sabba on 10/27/2016.
 */
public class CumulusException extends Exception {
    /**
     * 
     */
    private static final long serialVersionUID = -2791902149043891933L;

    public CumulusException() {
        super();
    }

    public CumulusException(String message) {
        super(message);
    }
}
