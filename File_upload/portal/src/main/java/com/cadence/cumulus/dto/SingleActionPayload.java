package com.cadence.cumulus.dto;

public class SingleActionPayload extends ActionPayload {

    private String diskResourcePath;

    public String getDiskResourcePath() {
        return diskResourcePath;
    }

    public void setDiskResourcePath(String diskResourcePath) {
        this.diskResourcePath = diskResourcePath;
    }

}
