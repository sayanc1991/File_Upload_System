/**
 * 
 */
package com.cadence.cumulus.repository.mysql;

import org.springframework.data.jpa.repository.JpaRepository;

import com.cadence.cumulus.model.Countries;

/**
 * @author deepak
 *
 */
public interface CountriesRepository extends JpaRepository<Countries, Long> {

	public Countries findByCountryCode(String countryCode);
}
