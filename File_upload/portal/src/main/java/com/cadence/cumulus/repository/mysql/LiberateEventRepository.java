package com.cadence.cumulus.repository.mysql;

import org.springframework.data.jpa.repository.JpaRepository;

import com.cadence.cumulus.model.liberate.events.LiberateEvent;


public interface LiberateEventRepository extends JpaRepository<LiberateEvent, String> {
}
