package com.cadence.cumulus.integration.opendj.vo;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Manager {

    private String displayName;
	private String _id;

    public Manager() {
    }

    public Manager(String _id, String displayName) {
    	this._id = _id;
    	this.displayName = displayName;
    }

    public String getDisplayName() {
		return displayName;
	}

	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}

	public String get_id() {
		return _id;
	}

	public void set_id(String _id) {
		this._id = _id;
	}

	@Override
	public String toString() {
		return "Manager [displayName=" + displayName + ", _id=" + _id + "]";
	}
	
}
