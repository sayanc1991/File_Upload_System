package com.cadence.cumulus.integration.nfs.service;

import java.io.File;
import java.net.InetSocketAddress;
import java.net.Proxy;

import javax.annotation.PostConstruct;
import javax.net.ssl.SSLContext;
import javax.xml.bind.DatatypeConverter;

import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.http.client.SimpleClientHttpRequestFactory;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.cadence.cumulus.ProxyConfig;
import com.cadence.cumulus.integration.IntegrationException;
import com.cadence.cumulus.integration.opendj.vo.User;

@Service
public class NFSIntegrationServiceImpl implements NFSIntegrationService {

	private static final Logger LOG = LoggerFactory.getLogger(NFSIntegrationServiceImpl.class);

	@Autowired
	private ProxyConfig proxyConfig;

	private RestTemplate restTemplate;

	@Value("${nfs.users.baseUrl}")
	private String usersBaseURL;

	@Value("${nfs.admin.user}")
	private String adminUser;

	@Value("${nfs.admin.password}")
	private String adminPasswd;

	// TODO : check property required or not
	@Value("${opendj.server.cert.file}")
	private String certificateFile;

	// TODO : check property required or not
	@Value("${opendj.server.cert.password}")
	private String certificatePassword;

	// TODO : check property required or not
    @Value("${opendj.enabled}")
	private boolean isOpenDJEnabled;

	@PostConstruct
	public void setup() {
		if (isOpenDJEnabled) {
		    SSLContext sslContext = null;
			try {
				sslContext = org.apache.http.ssl.SSLContexts.custom()
								.loadTrustMaterial(new File(certificateFile), certificatePassword.toCharArray())
				                .build();
			} catch (Exception e) {
				LOG.error("Exception while connecting to OpenDJ server: ", e);
			}
	
		    SSLConnectionSocketFactory csf = new SSLConnectionSocketFactory(sslContext);
	
		    CloseableHttpClient httpClient = HttpClients.custom()
		                    .setSSLSocketFactory(csf)
		                    .build();
	
		    HttpComponentsClientHttpRequestFactory requestFactory =
		                    new HttpComponentsClientHttpRequestFactory();
	
		    requestFactory.setHttpClient(httpClient);
		    restTemplate = new RestTemplate(requestFactory);
		} else {
			restTemplate = new RestTemplate();
			if (proxyConfig.isEnabled()) {
				SimpleClientHttpRequestFactory factory = new SimpleClientHttpRequestFactory();
				InetSocketAddress address = new InetSocketAddress(proxyConfig.getHost(), proxyConfig.getPort());
				Proxy proxy = new Proxy(Proxy.Type.HTTP, address);
				factory.setProxy(proxy);
				restTemplate.setRequestFactory(factory);
			}
		}
	}

	@Override
	public void createHomedir(User user) throws IntegrationException {
		LOG.debug("Mounting cumulus user in OpenDJ DS : ", user.getName());

		ResponseEntity<User> result = null;
		try {
			RestTemplate restTemplate = getRestTemplate();
			HttpHeaders headers = getHeader();
			HttpEntity<User> requestCreateEntity = new HttpEntity<>(user, headers);
			
			result = restTemplate.exchange(usersBaseURL+"?_action=create", HttpMethod.POST, requestCreateEntity, User.class);
		} catch (Exception e) {
			LOG.error("Exception while updating user mount info in OpenDJ", e);
			throw new IntegrationException("Exception while updating user mount info in OpenDJ", e);
		}

		// successfully created
		if (result != null && HttpStatus.CREATED.equals(result.getStatusCode())) {
			LOG.debug("response from OpenDJ for :" + usersBaseURL + "?_action=create :" + result.getBody());
		} else {
			LOG.error("Error response while calling OpenDJ DS API with end point " + usersBaseURL + "?_action=create " +(result != null
					? " Status :" + result.getStatusCodeValue() + " : response body : " + result.getBody() : ""));
			throw new IntegrationException("Error response while calling OpenDJ DS API with end point " + usersBaseURL + "?_action=create"
					+ (result != null
							? " Status :" + result.getStatusCodeValue() + " : response body : " + result.getBody()
							: ""));
		}

		LOG.debug("Completed updating user mount information in OpenDJ Directory Service.");
	}

	private HttpHeaders getHeader() {
		String plainCreds = adminUser+":"+adminPasswd;
		byte[] plainCredsBytes = plainCreds.getBytes();
		String base64Creds = DatatypeConverter.printBase64Binary(plainCredsBytes);

		HttpHeaders headers = new HttpHeaders();
		headers.add("Authorization", "Basic " + base64Creds);
		headers.set("Accept", MediaType.APPLICATION_JSON_VALUE);
		headers.set("Content-type", MediaType.APPLICATION_JSON_VALUE);
		
		return headers;
	}

	private RestTemplate getRestTemplate() {
		return restTemplate;
	}
}