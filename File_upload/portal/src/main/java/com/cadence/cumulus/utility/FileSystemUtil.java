package com.cadence.cumulus.utility;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.lang.ProcessBuilder.Redirect;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.attribute.PosixFileAttributeView;
import java.nio.file.attribute.PosixFileAttributes;
import java.nio.file.attribute.PosixFilePermission;
import java.nio.file.attribute.PosixFilePermissions;
import java.util.Arrays;
import java.util.Set;
import java.util.UUID;

import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import com.cadence.cumulus.dto.ProcessResult;


// converted to managed spring bean to serve as singleton where required in accordance to CMLS-658
@Component
public class FileSystemUtil {

	private static String TMP_DIRECTORY = "/tmp/";// "c:/tmp/"; // "/tmp/";
	/**
	 * Logger
	 */
	private static final Logger log = LoggerFactory
			.getLogger(FileSystemUtil.class);
	 
	public int mkdirByUser(String username, String path)
			throws Exception {
		// FileUtils.forceMkdir(new File(path));
		// return 0;
		ProcessResult processResult = runExternalCommand("sudo", "-u", username,
				"mkdir -p \"" + path + "\"");
		log.debug("user:" + username + ":" + "creating directory " + path
				+ "; exitcode=" + processResult.getExitCode());
		return processResult.getExitCode();
	}

	public int copyFileTo(String username, String sourcePath,
			String targetPath, String dirName) throws Exception {
		mkdirByUser(username, dirName);
		// FileUtils.copyFile(new File(sourcePath), new File(dirName + "/" +
		// targetPath));
		// return 0;
		ProcessResult processResult = runExternalCommand("sudo", "-u", username,
				"cp \"" + sourcePath + "\" \"" + dirName + "/"
						+ targetPath + "\"");
		log.debug("user:" + username + ":" + "copyFileTo : copying "
				+ sourcePath + " to " + targetPath + " to dir " + dirName
				+ "; exitcode=" + processResult.getExitCode());
		return processResult.getExitCode();
	}

	public int copyFileTo(String username, String sourcePath,
			String targetPath) throws Exception {
		ProcessResult processResult = runExternalCommand("sudo", "-u", username, "cp \"" + sourcePath + "\" \"" + targetPath +"\"");
		
		log.debug("user:" + username + ":" + "copyFileTo : copying "
				+ sourcePath + " to " + targetPath + "; exitcode="
				+ processResult.getExitCode()+";"+processResult.getOutput());
		return processResult.getExitCode();
	}
	
	public boolean writeFileToFS(MultipartFile file, String destination,
			String userName) {
		
		log.debug("Uploading file : {}", file.getOriginalFilename());

		File tmp_dest = new File(TMP_DIRECTORY, file.getOriginalFilename());

		InputStream input = null;
		OutputStream output = null;

		try {
			input = file.getInputStream();
			// writing the file in a tmp location.

			output = new FileOutputStream(tmp_dest);

			byte[] buffer = new byte[8192];
			int length = 0;
			while ((length = input.read(buffer)) > 0) {
				output.write(buffer, 0, length);
			}

			copyFileTo(userName, tmp_dest.getAbsolutePath(), destination + "/"
					+ file.getOriginalFilename());
			FileUtils.deleteQuietly(tmp_dest);

			// log.debug("Setting permissions");
			// permissionService.setOwnershipInfo(dest.getAbsolutePath(),
			// userName);

			log.debug("File uploaded");
		} catch (Throwable t) {
			log.debug("Unable to upload {}", file.getOriginalFilename());
			log.error("Error:", t);
			return false;
		} finally {
			try {
				input.close();
			} catch (IOException e) {
				log.error("Error:", e);
				return false;
			}
			try {
				output.close();
			} catch (IOException e) {
				log.error("Error:", e);
				return false;
			}
		}
		return true;

	}
	
	
	 /** Class to capture the result of calling an external command.
	 * exitCode is the exit code of the external process
	 * output is rge output of the external process*/
	 
	/*public static class ProcessResult {
		private int exitCode;
		private String output;
		
		public ProcessResult(int exitCode, String output) {
			this.exitCode = exitCode;
			this.output = output;
		}
		public int getExitCode() {
			return exitCode;
		}
		public void setExitCode(int exitCode) {
			this.exitCode = exitCode;
		}
		public String getOutput() {
			return output;
		}
		public void setOutput(String output) {
			this.output = output;
		}
	}*/
	
	public String getPermissions(Path path) throws IOException {
        PosixFileAttributeView fileAttributeView = Files.getFileAttributeView(path, PosixFileAttributeView.class);
        PosixFileAttributes readAttributes = fileAttributeView.readAttributes();
        Set<PosixFilePermission> permissions = readAttributes.permissions();
        return PosixFilePermissions.toString(permissions);
    }
	
	
	 /** Run external commands passed as an array of strings and return the output and
	 * exit code for the external command. We are running the commands by creating a
	 * script because filenames with space are not treated well. Enclosing the
	 * filenames in quotes is also not helping as ProcessBuilder takes care of that
	 * only if we pass the entire name with space as an element of the array. We
	 * cant do that because we are using su and the param after -c should be a
	 * single string with the whole command that needs to be executed.*/
	 
	public ProcessResult runExternalCommand(String... commands) throws Exception {
		String tmpScriptName = TMP_DIRECTORY + UUID.randomUUID().toString() + ".sh";
		File f = new File(tmpScriptName);
		Process p = commandRunnerHelper(f, commands);
		String line = null;
		String output = "";
		BufferedReader reader = new BufferedReader(new InputStreamReader(p.getInputStream()));
		while ((line = reader.readLine()) != null) {
			output += line;
		}
		reader.close();
		int exitCode = p.waitFor();
		f.delete();
		log.debug("Ran executable : {}", f.getAbsolutePath());
		return new ProcessResult(exitCode, output);
	}
	
	private Process commandRunnerHelper(File scriptFile, String... commands) throws IOException {
		FileOutputStream fout = new FileOutputStream(scriptFile);
		String scriptContent = "#!/bin/bash\nset -e\n" + commands[commands.length - 1]; 
		log.debug("Running script: {}", scriptContent);
		fout.write(scriptContent.getBytes());
		fout.close();
		scriptFile.setExecutable(true, false);
		commands[commands.length - 1] = scriptFile.getAbsolutePath(); // CMLS 994 use absolute file path
		log.debug("Commands passed to shell: {}", Arrays.toString(commands));
		ProcessBuilder builder = new ProcessBuilder(commands).redirectErrorStream(true).inheritIO()
				.redirectOutput(Redirect.PIPE);
		Process p = builder.start();
		return p;
	}
	/*
	 * Check if a path exists and is valid.
	 */
	private boolean exists(String username, String filePath) throws Exception {
	 
		log.debug("username:{}, filePath:{}" ,username, filePath);
		
		ProcessResult processResult = runExternalCommand("sudo", "-u", username, format("ls -l \"%s\"", filePath));
		 
		return processResult.getExitCode() == 0;
	}

	/**
	 * creates symbolic link by executing linux ln -s.
	 * @param username
	 * @param source
	 * @param target
	 * @return
	 * @throws Exception
	 */
	public boolean createSymbolicLink(String username, String source, String target) throws Exception {
		log.debug("entering", username, source, target);

		if (exists(username, target)) {
			log.debug("{} target already exists", source);
			return true;
		}
		ProcessResult processResult = runExternalCommand("sudo", "-u", username,
				format("ln -s \"%s\" \"%s\"", source,target));

		return processResult.getExitCode() == 0;
	}

	
	public String format(String format, Object... args){		
		return String.format(format, args);
	}	
	
}
