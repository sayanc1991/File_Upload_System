/**
 * 
 */
package com.cadence.cumulus.service;

import java.util.List;

import com.cadence.cumulus.model.Application;
import com.cadence.cumulus.model.Plugin;

/**
 * @author shirish
 *
 */
public interface ApplicationService {
	
	
	/**
	 * Gets List of Application Plugins
	 * @return
	 */
	public Application getApplicationProfile();
	
	/**
	 * Gets List of Application Plugins
	 * @return
	 */
	public List<Plugin> getPlugin();
	
	
	/**
	 * Gets List of Application Plugins which are currently active.
	 * @return
	 */
	public List<Plugin> getActivePlugin();
	
	
}
