package com.cadence.cumulus.integration.unifiedregistration.vo;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "attributes", "Id", "Email", "FirstName", "LastName", "Middle_Name__c", "Login_Name__c", "Phone",
		"Cellular_Phone__c", "CreatedDate" })
public class Record {

	@JsonProperty("attributes")
	private Attributes attributes;
	@JsonProperty("Id")
	private String id;
	@JsonProperty("Email")
	private String email;
	@JsonProperty("FirstName")
	private String firstName;
	@JsonProperty("LastName")
	private String lastName;
	@JsonProperty("Middle_Name__c")
	private Object middleNameC;
	@JsonProperty("Login_Name__c")
	private String loginNameC;
	@JsonProperty("Phone")
	private String phone;
	@JsonProperty("Country__c")
	private String countryC;
	@JsonProperty("State_Region__c")
	private String stateRegionC;
	@JsonProperty("City__c")
	private String cityC;
	@JsonProperty("Street__c")
	private String streetC;
	@JsonProperty("Postal_Code__c")
	private String postalCodeC;
	@JsonProperty("Cellular_Phone__c")
	private String cellularPhoneC;
	@JsonProperty("CreatedDate")
	private String createdDate;
	@JsonIgnore
	private Map<String, Object> additionalProperties = new HashMap<String, Object>();

	@JsonProperty("attributes")
	public Attributes getAttributes() {
		return attributes;
	}

	@JsonProperty("attributes")
	public void setAttributes(Attributes attributes) {
		this.attributes = attributes;
	}

	@JsonProperty("Id")
	public String getId() {
		return id;
	}

	@JsonProperty("Id")
	public void setId(String id) {
		this.id = id;
	}

	@JsonProperty("Email")
	public String getEmail() {
		return email;
	}

	@JsonProperty("Email")
	public void setEmail(String email) {
		this.email = email;
	}

	@JsonProperty("FirstName")
	public String getFirstName() {
		return firstName;
	}

	@JsonProperty("FirstName")
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	@JsonProperty("LastName")
	public String getLastName() {
		return lastName;
	}

	@JsonProperty("LastName")
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	@JsonProperty("Middle_Name__c")
	public Object getMiddleNameC() {
		return middleNameC;
	}

	@JsonProperty("Middle_Name__c")
	public void setMiddleNameC(Object middleNameC) {
		this.middleNameC = middleNameC;
	}

	@JsonProperty("Login_Name__c")
	public String getLoginNameC() {
		return loginNameC;
	}

	@JsonProperty("Login_Name__c")
	public void setLoginNameC(String loginNameC) {
		this.loginNameC = loginNameC;
	}

	@JsonProperty("Phone")
	public String getPhone() {
		return phone;
	}

	@JsonProperty("Phone")
	public void setPhone(String phone) {
		this.phone = phone;
	}

	@JsonProperty("Cellular_Phone__c")
	public String getCellularPhoneC() {
		return cellularPhoneC;
	}

	@JsonProperty("Cellular_Phone__c")
	public void setCellularPhoneC(String cellularPhoneC) {
		this.cellularPhoneC = cellularPhoneC;
	}

	@JsonProperty("CreatedDate")
	public String getCreatedDate() {
		return createdDate;
	}

	@JsonProperty("CreatedDate")
	public void setCreatedDate(String createdDate) {
		this.createdDate = createdDate;
	}

	@JsonAnyGetter
	public Map<String, Object> getAdditionalProperties() {
		return this.additionalProperties;
	}

	@JsonAnySetter
	public void setAdditionalProperty(String name, Object value) {
		this.additionalProperties.put(name, value);
	}

	@JsonProperty("Country__c")
	public String getCountryC() {
		return countryC;
	}

	@JsonProperty("Country__c")
	public void setCountryC(String countryC) {
		this.countryC = countryC;
	}

	@JsonProperty("State_Region__c")
	public String getStateRegionC() {
		return stateRegionC;
	}

	@JsonProperty("State_Region__c")
	public void setStateRegionC(String stateRegionC) {
		this.stateRegionC = stateRegionC;
	}

	@JsonProperty("City__c")
	public String getCityC() {
		return cityC;
	}

	@JsonProperty("City__c")
	public void setCityC(String cityC) {
		this.cityC = cityC;
	}

	@JsonProperty("Street__c")
	public String getStreetC() {
		return streetC;
	}

	@JsonProperty("Street__c")
	public void setStreetC(String streetC) {
		this.streetC = streetC;
	}

	@JsonProperty("Postal_Code__c")
	public String getPostalCodeC() {
		return postalCodeC;
	}

	@JsonProperty("Postal_Code__c")
	public void setPostalCodeC(String postalCodeC) {
		this.postalCodeC = postalCodeC;
	}
}
