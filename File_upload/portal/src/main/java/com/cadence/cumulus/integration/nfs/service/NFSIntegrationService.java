package com.cadence.cumulus.integration.nfs.service;

import com.cadence.cumulus.integration.IntegrationException;
import com.cadence.cumulus.integration.opendj.vo.User;

public interface NFSIntegrationService {
	
	/**
	 * Creates cumulus user home directory in Network File Servers.
	 * 
	 * 
	 * @param User user of the cumulus user (usually username,etc) ) to be created in NFS.
	 * @throws IntegrationException Exception if any while accessing NFS.
	 */
	public void createHomedir(User user) throws IntegrationException;

}
