package com.cadence.cumulus.core.security;

import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

import javax.annotation.PostConstruct;
import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.SecretKeySpec;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.util.Base64Utils;

import com.cadence.cumulus.common.CumulusException;
import com.cadence.cumulus.core.CryptoService;

@Component
public class AESServiceImpl implements CryptoService {
	
	private static final Logger LOG = LoggerFactory.getLogger(AESServiceImpl.class);
	
	private Cipher cipher;
	
	private String encryptionAlgorithm;
	
	@PostConstruct
	private void init() throws NoSuchAlgorithmException, NoSuchPaddingException {
		this.encryptionAlgorithm = "AES";
		this.cipher = Cipher.getInstance(encryptionAlgorithm);
	}

	@Override
	public String encrypt(String key, String data) throws CumulusException {
		// TODO Auto-generated method stub
		try {
			SecretKeySpec skeySpec = new SecretKeySpec(key.getBytes("UTF-8"), encryptionAlgorithm);
			cipher.init(Cipher.ENCRYPT_MODE, skeySpec);
			byte[] rawEncryption = cipher.doFinal(data.getBytes());
			String encodedEncryption = Base64Utils.encodeToString(rawEncryption);
			return encodedEncryption;
		} catch (UnsupportedEncodingException | InvalidKeyException | IllegalArgumentException | IllegalBlockSizeException | BadPaddingException e) {
			LOG.error("Encryption error: {} from: {}", e.getMessage(), e.getClass());
			throw new CumulusException(e.getMessage());
		}
	}

	@Override
	public String decrypt(String key, String data) throws CumulusException {
		// TODO Auto-generated method stub
		try {
			SecretKeySpec skeySpec = new SecretKeySpec(key.getBytes("UTF-8"), encryptionAlgorithm);
			cipher.init(Cipher.DECRYPT_MODE, skeySpec);
			byte[] rawEncryption = Base64Utils.decodeFromString(data);
			byte[] rawDecryption = cipher.doFinal(rawEncryption);
			String decoded = new String(rawDecryption);
			return decoded;
		} catch (UnsupportedEncodingException | InvalidKeyException | IllegalArgumentException | IllegalBlockSizeException | BadPaddingException e) {
			LOG.error("Decryption error: {} from: {}", e.getMessage(), e.getCause());
			throw new CumulusException(e.getMessage());
		}
	}

}
