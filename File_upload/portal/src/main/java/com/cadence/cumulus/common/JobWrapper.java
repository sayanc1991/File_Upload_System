/**
 * 
 */
package com.cadence.cumulus.common;

import com.cadence.cumulus.model.Job;
import com.fasterxml.jackson.annotation.JsonInclude;

import java.util.List;

/**
 * @author ramsudheer
 *
 */
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class JobWrapper {
	
	private List<Job> jobs;

	/**
	 * @return the jobs
	 */
	public List<Job> getJobs() {
		return jobs;
	}
	public JobWrapper setJobs(List<Job> jobs) {
		this.jobs = jobs;

		return this;
	}

	
}
