package com.cadence.cumulus.model;

import java.util.List;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class FileOperationRequest {
    private String requestId;

	private String action;

	private String path;

	private String item;

	private String newItemPath;
	
	private List<String> items;
	
	private String newPath;
	
	private String singleFilename;
	
	private String content;
	
	private String perms;
	
	private String permsCode;
	
	private boolean recursive;
	
	private String destination;
	
	private String compressedFilename;
	
	private String toFilename;

	private String userName;

	public FileOperationRequest() {
		// TODO Auto-generated constructor stub
	}

	public String getRequestId() {
        return requestId;
    }

    public void setRequestId(String requestId) {
        this.requestId = requestId;
    }

    public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	public String getItem() {
		return item;
	}

	public void setItem(String item) {
		this.item = item;
	}

	public String getNewItemPath() {
		return newItemPath;
	}

	public void setNewItemPath(String newItemPath) {
		this.newItemPath = newItemPath;
	}

	public List<String> getItems() {
		return items;
	}

	public void setItems(List<String> items) {
		this.items = items;
	}

	public String getNewPath() {
		return newPath;
	}

	public void setNewPath(String newPath) {
		this.newPath = newPath;
	}

	public String getSingleFilename() {
		return singleFilename;
	}

	public void setSingleFilename(String singleFilename) {
		this.singleFilename = singleFilename;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getPerms() {
		return perms;
	}

	public void setPerms(String perms) {
		this.perms = perms;
	}

	public String getPermsCode() {
		return permsCode;
	}

	public void setPermsCode(String permsCode) {
		this.permsCode = permsCode;
	}

	public boolean isRecursive() {
		return recursive;
	}

	public void setRecursive(boolean recursive) {
		this.recursive = recursive;
	}

	public String getDestination() {
		return destination;
	}

	public void setDestination(String destination) {
		this.destination = destination;
	}

	public String getCompressedFilename() {
		return compressedFilename;
	}

	public void setCompressedFilename(String compressedFilename) {
		this.compressedFilename = compressedFilename;
	}

	public String getToFilename() {
		return toFilename;
	}

	public void setToFilename(String toFilename) {
		this.toFilename = toFilename;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}
    @Override
    public String toString() {
        try {
            return new ObjectMapper().writeValueAsString(this);
        } catch (JsonProcessingException e) {
            // TODO Send json manually if the mapper fails
            return super.toString();
        }
    }
}
