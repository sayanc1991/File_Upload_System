package com.cadence.cumulus.service;

import com.cadence.cumulus.common.CumulusException;
import com.cadence.cumulus.model.Organisation;

public interface OrganisationService {

	public Organisation findByDomain(String domainName) throws CumulusException;
	
}
