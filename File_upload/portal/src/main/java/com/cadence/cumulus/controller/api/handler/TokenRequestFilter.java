package com.cadence.cumulus.controller.api.handler;

import java.io.IOException;
import java.io.PrintWriter;
import java.security.Principal;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.Collections;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.util.Pair;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;
import org.springframework.security.web.util.matcher.RequestMatcher;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import com.cadence.cumulus.common.ServiceConstants;
import com.cadence.cumulus.model.ResponseVO;
import com.cadence.cumulus.model.UserLogin;
import com.fasterxml.jackson.databind.ObjectMapper;

@Component
public class TokenRequestFilter extends HandlerInterceptorAdapter {

	private static final Logger LOG = LoggerFactory.getLogger(TokenRequestFilter.class);

	@Autowired
	private ObjectMapper mapper;

	@Value("${token.tampering.msg}")
	private String tokenTampered;
	
	@Value("${cli.session.expired.msg}")
	private String sessionExpiredMessage;
	
	@Value("${cli.session.invalid.msg}")
	private String sessionInvalidMessage;

	@Value("${cli.session.inactivity.seconds}")
	private Integer inactivityInterval;

	@Value("#{'${cli.authorization.points}'.split(',')}")
	private List<String> authorizationPoints;

	@Value("#{'${cli.business.points}'.split(',')}")
	private List<String> businessPoints;

	@Value("#{'${cli.session.points}'.split(',')}")
	private List<String> sessionPoints;

	private DateTimeFormatter formatter;

	@PostConstruct
	private void init() {
		this.formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss:SSS");
		Collections.sort(businessPoints);
		Collections.sort(authorizationPoints);
		Collections.sort(sessionPoints);
	}

	private Boolean shouldInterceptRequest(List<String> uriList, HttpServletRequest request) {
		Boolean intercept = false;
		Integer start = 0;
		Integer end = uriList.size() - 1;
		String key = request.getServletPath();
		while (start <= end) {
			Integer mid = (start + end) / 2;
			String uri = uriList.get(mid);
			Integer comparison = uri.compareTo(key);
			RequestMatcher formalUri = new AntPathRequestMatcher(uri);
			if (formalUri.matches(request)) {
				intercept = true;
				break;
			} else if (comparison < 0) {
				start = mid + 1;
			} else if (comparison > 0) {
				end = mid - 1;
			}
		}
		return intercept;
	}

	private Pair<Boolean, String> validateCLIAuthorizationRequest(HttpServletRequest request) {
		Boolean forward = true;
		String reason = "";
		if (shouldInterceptRequest(authorizationPoints, request)) {
			Principal principal = request.getUserPrincipal();
			if (principal == null) {// user accessed from CLI, from non session context
				UserLogin userLogin = BasicAuthenticationHandler.checkAuth(request);
				if (userLogin == null) {// user logged in from CLI without credentials
					String token = request.getHeader(ServiceConstants.X_AUTH_TOKEN);
					if (!StringUtils.hasText(token)) {// no user credentials provided
						forward = false;
						LOG.debug("Unprocessable authentication mechanism, returning 422");
						reason = "Unprocessable authentication mechanism - Missing User Login Details in request or incorrect Authentication Header";
					} else {// user authorization credentials token present for authentication
						HttpSession session = request.getSession();
						session.setAttribute(ServiceConstants.TOKEN_PRESENT, true);
						session.setAttribute(ServiceConstants.X_AUTH_TOKEN, token);
						LOG.info("Authorization token present in request header");
					}
				} else {// user logged from CLI with credentials
					LOG.info("User:{} authentication credentials present in request header", userLogin.getUserName());
					HttpSession session = request.getSession();
					session.setAttribute(ServiceConstants.USER_LOGIN, userLogin);
					session.setAttribute(ServiceConstants.TOKEN_PRESENT, false);
				}
			} else {// user accessed from browser, from a session context
				UserLogin userLogin = new UserLogin();
				userLogin.setUserName(principal.getName());
				HttpSession session = request.getSession();
				session.setAttribute(ServiceConstants.USER_LOGIN, userLogin);
				session.setAttribute(ServiceConstants.TOKEN_PRESENT, false);
				LOG.info("User:{} from browser session", userLogin.getUserName());
			}
		} else {
			forward = false;
		}
		Pair<Boolean, String> status = Pair.of(forward, reason);
		return status;
	}

	private Pair<Boolean, String> validateCLIBusinessRequest(HttpServletRequest request) {
		Boolean forward = true;
		String reason = "";
		if (shouldInterceptRequest(businessPoints, request)) {
			String token = request.getHeader(ServiceConstants.X_AUTH_TOKEN);
			if (StringUtils.hasText(token)) {
				LOG.info("Client -> Server Token present in header: {}", token);
				HttpSession session = request.getSession();
				session.setAttribute(ServiceConstants.X_AUTH_TOKEN, token);
			} else {
				reason = "Missing token for authentication!";
				forward = false;
			}
		}
		Pair<Boolean, String> status = Pair.of(forward, reason);
		return status;
	}

	private Pair<Boolean, String> validateCLISessionRequest(HttpServletRequest request) {
		Boolean forward = true;
		String reason = "";
		if (shouldInterceptRequest(sessionPoints, request)) {
			String sessionClient = request.getHeader(ServiceConstants.SESSION_ID);
			HttpSession session = request.getSession();
			String sessionServer = session.getId();
			sessionServer = request.getCookies()[0].getName();
			LOG.debug("SESSIONID from client: {} and server: {}", sessionClient, sessionServer);
			if (sessionClient.equals(sessionServer)) {
				LocalDateTime lastAccessed = LocalDateTime.ofInstant(Instant.ofEpochMilli(session.getLastAccessedTime()),
						ZoneId.systemDefault());
				LocalDateTime now = LocalDateTime.now();
				Long idleSinceLastAccessSeconds = lastAccessed.until(now, ChronoUnit.SECONDS);
				//Long idleSinceLastAccess = lastAccessed.until(now, ChronoUnit.MILLIS);
				if(idleSinceLastAccessSeconds >= inactivityInterval) {
					reason = sessionExpiredMessage;
					forward = false;
					session.invalidate();
					LOG.debug("Session invalidated as it has expired due to being idle for: {} {} since last request at: {}", 
							idleSinceLastAccessSeconds, ChronoUnit.SECONDS, lastAccessed.format(formatter));
				} else {
					LocalDateTime created = LocalDateTime.ofInstant(Instant.ofEpochMilli(session.getCreationTime()),
							ZoneId.systemDefault());
					LOG.debug("Session is valid from creation time: {} with last request at: {}", created, lastAccessed.format(formatter));
				}
			} else {
				forward = false;
				reason = sessionInvalidMessage;
				session.invalidate();
				LOG.debug("Session invalidated as client SESSIONID validation failed against the one on server: {}", sessionInvalidMessage);
			}
		}
		Pair<Boolean, String> status = Pair.of(forward, reason);
		return status;
	}

	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
			throws Exception {
		// TODO Auto-generated method stub
		Pair<Boolean, String> handled = validateCLIAuthorizationRequest(request);
		if (!handled.getFirst()) {
			if (StringUtils.hasText(handled.getSecond())) {// false, "<error message>"
				// authorization point validated
				sendResponse(response, HttpStatus.UNPROCESSABLE_ENTITY, handled.getSecond());
			} else {// false, ""
				// not authorization end point
				handled = validateCLIBusinessRequest(request);
				if (!handled.getFirst()) {// false, "<error message>"
					if (StringUtils.hasText(handled.getSecond())) {
						// business point validated
						sendResponse(response, HttpStatus.UNPROCESSABLE_ENTITY, handled.getSecond());
					} // else business point validated by default
				} else {
					handled = validateCLISessionRequest(request);
					if(!handled.getFirst()) {// false, session expired or not recognized
						if (StringUtils.hasText(handled.getSecond())) {
							// session based business point validated
							sendResponse(response, HttpStatus.REQUEST_TIMEOUT, handled.getSecond());
						} 
					} else {
						// session based business end point
						handled = Pair.of(true, "");
						String actualSessionId = request.getHeader(ServiceConstants.SESSION_ID);
						HttpSession session = request.getSession();
						String userName = null; 
						if(request.getRequestURI().contains("validate")) {
							userName = (String) session.getAttribute(ServiceConstants.USERNAME);
						}else {
							String arr[] = request.getParameterValues(ServiceConstants.USERNAME);
							if(null != arr) {
								userName = (String) request.getParameterValues(ServiceConstants.USERNAME)[0];
							}
						}
						
						String formalSessionId = session.getId();
						LOG.info("actualSessionId: {}, userName: {}, formalSessionId: {}", actualSessionId, userName,
								formalSessionId);
					}
				}
			}
		} // else authorization point validated by default
		LOG.info("Forward to request path: {} {}", request.getServletPath(), handled.getFirst());
		return handled.getFirst();
	}
	
	private void sendResponse(HttpServletResponse response, HttpStatus responseStatus, String message) throws IOException {
		LOG.info("Request could not be processed: {}", message);
		response.setContentType(MediaType.APPLICATION_JSON_VALUE);
		ResponseVO vo = new ResponseVO();
		//HttpStatus.UNPROCESSABLE_ENTITY.name()
		vo.setStatus(responseStatus.name());
		vo.setValue(message);
		String body = mapper.writeValueAsString(vo);
		LOG.info("Error response: {}", body);
		response.setStatus(responseStatus.value());
		PrintWriter pw = response.getWriter();
		pw.write(body);
		pw.flush();
		response.flushBuffer();
	}

}
