package com.cadence.cumulus.service.impl;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import com.cadence.cumulus.common.AppMessages;
import com.cadence.cumulus.common.Constants;
import com.cadence.cumulus.common.CumulusException;
import com.cadence.cumulus.common.ServiceConstants;
import com.cadence.cumulus.integration.IntegrationException;
import com.cadence.cumulus.integration.nfs.service.NFSIntegrationService;
import com.cadence.cumulus.integration.opendj.service.OpenDJIntegrationService;
import com.cadence.cumulus.integration.opendj.vo.ContactInformation;
import com.cadence.cumulus.integration.opendj.vo.Name;
import com.cadence.cumulus.integration.opendj.vo.UserMountInformation;
import com.cadence.cumulus.integration.unifiedregistration.service.URIntegrationService;
import com.cadence.cumulus.integration.unifiedregistration.vo.UserAuthDetail;
import com.cadence.cumulus.model.CumulusUser;
import com.cadence.cumulus.model.Permission;
import com.cadence.cumulus.model.Role;
import com.cadence.cumulus.model.RoleDetail;
import com.cadence.cumulus.model.RolePermission;
import com.cadence.cumulus.model.RolePermissionId;
import com.cadence.cumulus.model.User;
import com.cadence.cumulus.model.UserRole;
import com.cadence.cumulus.model.UserRoleId;
import com.cadence.cumulus.service.CumulusUserService;
import com.cadence.cumulus.service.EmailService;
import com.cadence.cumulus.service.RolePermissionService;
import com.cadence.cumulus.service.UserManagementService;

/**
 * User Management Service Implementation for User Management API's basically
 * for CRUD operations
 *
 * @author Zabeersultan.Farook
 *
 */
@Service
@Transactional(propagation = Propagation.REQUIRED, rollbackFor = { Exception.class })
public class UserManagementServiceImpl implements UserManagementService {

	private static final Logger LOG = LoggerFactory.getLogger(UserManagementServiceImpl.class);

	@Autowired
	private CumulusUserService userService;

	@Autowired
	private RolePermissionService rolePermissionService;

	@Autowired
	private URIntegrationService urIntegrationService;

	@Autowired
	private OpenDJIntegrationService openDJIntegrationService;

	@Autowired
	private NFSIntegrationService nfsIntegrationService;

    @Autowired
    private EmailService emailService;

    @Value("${opendj.enabled}")
	private boolean isOpenDJEnabled;

    @Value("${opendj.user.notification.enabled}")
	private boolean isUserNotificationEnabled;

    @Value("${cumulus.user.created.template}")
    private String USER_CREATED_TEMPLATE;

    @Value("${cumulus.user.created.subject}")
    private String USER_CREATED_SUBJECT;

    @Value("${cumulus.url}")
    private String CUMULUS_URL;

    @Value("${cadence.logo}")
    private String CADENCE_LOGO;

    @Value("${opendj.users.base.homeDir}")
    private String BASE_HOME_DIR;

    @Value("${opendj.users.login.shell}")
    private String LOGIN_SHELL;

    @Value("${opendj.users.groupId}")
    private Long GROUP_ID;

    @Value("${opendj.users.prefixId}")
    private Long USERID_PREFIX;

    @Value("${opendj.users.default.password}")
    private String USERS_DEFAULT_PASSWORD;

    @Value("${opendj.users.auto.fileserver.path}")
    private String USERS_NFS_MOUNT_PATH;

    @Value("${opendj.users.auto.mount.mapname}")
    private String USERS_NIS_MAPNAME;

	@Value("${opendj.users.homeDirectoryFolder:/home/cmls/.tokens/}")
	private String HOME_DIRECTORY_SCRIPT_FILE_FOLDER_LOCATION;

	//TODO: Shambhu Remove the default value for above property after first build tested as Cycle Cloud properties may not be available in first build..

    private String IN_ACTIVE_USER = "Inactive";
    
    private String CADENCE_DOMAIN = "@cadence.com";

	@Override
	public UserAuthDetail getUserFromUnifiedRegistration(String emailID) throws CumulusException {
		// Check if user exists already in Cumulus
		User cumulusUser = userService.getUserByEmail(emailID);
		if (cumulusUser != null && "Active".equals(cumulusUser.getStatus())) {
			// user already present in Cumulus
			// throw new CumulusException("Cadence® Cloud Orchestrator user " + emailID + " already exists.");
			throw new CumulusException(String.format(AppMessages.USER_REGISTRATION_NOT_FOUND_DB.getMessage(), emailID)
					+ String.format(Constants.ERROR_CODE, AppMessages.USER_REGISTRATION_NOT_FOUND_DB.getCode()));
		}
		UserAuthDetail userAuthDetail = new UserAuthDetail();
		try {
			User user = urIntegrationService.getUserDetails(emailID);
			// if user not found from UR , throw exception
			if (user == null) {
				// String errorMsg = "User " + emailID + " not found, Please contact support.";
				String errorMsg = String.format(AppMessages.USER_REGISTRATION_USER_NOT_FOUND.getMessage(), emailID)
						+ String.format(Constants.ERROR_CODE, AppMessages.USER_REGISTRATION_USER_NOT_FOUND.getCode());
				if (emailID.endsWith(CADENCE_DOMAIN)) {
					throw new CumulusException("Cadence "+errorMsg);
				} else {
					throw new CumulusException(errorMsg);
				}
			}
			userAuthDetail.setUser(user);
			// set default values for admin, permission list and role list
			// as this user is not yet present in Cumulus
			userAuthDetail.setAdmin(false);
			userAuthDetail.setPermissions(new ArrayList<>());
			userAuthDetail.setRoles(new ArrayList<>());
		} catch (IntegrationException ie) {
			LOG.error("Exception while invoking UR integration API", ie);
			throw new CumulusException(ie.getMessage());
		}
		return userAuthDetail;
	}

	@Override
	public List<UserAuthDetail> getCumulusUsers(String firstName, String lastName, String emailID, String roleID)
			throws CumulusException {
		List<UserAuthDetail> userAuthDetailList = new ArrayList<>();
		List<User> userList = userService.searchUser(firstName, lastName, emailID, roleID);

		for (User user : userList) {
			if (!IN_ACTIVE_USER.equals(user.getStatus())) {
			    userAuthDetailList.add(getUserAuthDetail(user));
			}

		}

		return userAuthDetailList;

	}

	private UserAuthDetail getUserAuthDetail(User user) {
		// instead of sending a nested User object to the client,
		// we are sending a flatter UserAuthDetail object
		UserAuthDetail userAuthDetail = new UserAuthDetail();
		userAuthDetail.setUser(user);
		// set roles & permissions

		// add all Role object into a List and set it on UserAuthDetail object
		List<Role> roleList = new ArrayList<>();
		List<Permission> permissionList = new ArrayList<>();
		Set<Permission> uniquePermissions = new HashSet<>();

		for (UserRole userRole : user.getUserRoleSet()) {
			Role role = userRole.getUserRoleId().getRole();
			roleList.add(role);
			// set admin flag if user has admin role
			if (ServiceConstants.ADMIN_ROLE.equalsIgnoreCase(role.getName())) {
				userAuthDetail.setAdmin(true);
			}
			for (RolePermission rp : role.getRolePermissionSet()) {
				uniquePermissions.add(rp.getPermission());
			}

			// nullify the permission set within the role object
			// role.setPermissions(null);

		}
		userAuthDetail.setRoles(roleList);
		// nullify the role set in User object
		// userAuthDetail.getUser().setUserRoleSet(null);

		userAuthDetail.setRoles(roleList);
		permissionList.addAll(uniquePermissions);
		userAuthDetail.setPermissions(permissionList);
		return userAuthDetail;
	}

	private boolean isEmpty(String str) {
		return StringUtils.isEmpty(StringUtils.trimWhitespace(str));
	}

	private boolean isValidEmail(String emailID) {
		String regex = "^(.+)@(.+)$";

		Pattern pattern = Pattern.compile(regex);

		Matcher matcher = pattern.matcher(emailID);
		return matcher.matches();

	}

	@Override
	public UserAuthDetail addNewUser(UserAuthDetail userAuthDetail) throws CumulusException {
		boolean isNewUser = true;
		boolean updateStatus = true;

		// validation
		if (userAuthDetail == null || !valid(userAuthDetail.getUser()) || userAuthDetail.getRoles() == null
				|| userAuthDetail.getRoles().isEmpty()) {
			throw new CumulusException("Mandatory fields missing");
		}

		// User user = userAuthDetail.getUser();
		User cumulusUser = userService.getUserByEmail(userAuthDetail.getUser().getEmail());
		if (cumulusUser != null) {
			if ("Active".equals(cumulusUser.getStatus())) {
				throw new CumulusException("User already exists in the Cadence® Cloud Orchestrator database.");
			}
			LOG.info(userAuthDetail.getUser().getEmail()+" user found in cumulus and updating related details.");
			isNewUser = false;
		} else {
			cumulusUser = userAuthDetail.getUser();
		}

		cumulusUser.setDepartment(userAuthDetail.getUser().getDepartment());
		cumulusUser.setProject(userAuthDetail.getUser().getProject());
		// cumulusUser.setTempPasswordStatus(0l); // User using temporary password.

		// validate email id
		if (!isValidEmail(cumulusUser.getEmail())) {
			// throw new CumulusException("Email not valid");
			throw new CumulusException(AppMessages.USER_REGISTRATION_INVALID_EMAIL.getMessage()
					+ String.format(Constants.ERROR_CODE, AppMessages.USER_REGISTRATION_INVALID_EMAIL.getCode()));
		}

		setupUser(cumulusUser, userAuthDetail.getRoles(), isNewUser, updateStatus);

		User user = null;
		try {

			user = userService.saveUser(cumulusUser);
			//Add User to the Configured group in Okta
			// oktaIntegrationService.addUserToCumulusGroup(user.getEmail());

			LOG.info("isOpenDJEnabled flag value from property file : "+isOpenDJEnabled);
			if (isOpenDJEnabled) {
				com.cadence.cumulus.integration.opendj.vo.User opendjUser = null;
				try {
					opendjUser = openDJIntegrationService.findUser(user.getUserId());
				} catch (Exception e) {
					LOG.debug(user.getUserId()+" user not found in opendj: "+e.getMessage());
				}


				if (opendjUser == null) {
					LOG.info("Calling opendj to add user : "+isOpenDJEnabled);
					com.cadence.cumulus.integration.opendj.vo.User testUser =
							new com.cadence.cumulus.integration.opendj.vo.User();

					String userId = user.getUserId();
					testUser.set_id(userId);
					testUser.setDisplayName(user.getUsername());
					testUser.setContactInformation(new ContactInformation(user.getEmail(), user.getPhone()));
					testUser.setName(new Name(user.getFirstName(), user.getLastName()));
					testUser.setUserPassword(USERS_DEFAULT_PASSWORD);
					testUser.setUidNumber(USERID_PREFIX + user.getId());
					testUser.setLoginShell(LOGIN_SHELL);
					testUser.setHomeDirectory(BASE_HOME_DIR + "/" + userId);
					testUser.setGidNumber(GROUP_ID);

					LOG.debug("OpenDJ user object populated : " + testUser);

					openDJIntegrationService.addUser(testUser);



					// TODO : call NFS server to create home directory by passing username & groupname request.
					// Capture NFS server name & full home directory path from response object.
					// nfsIntegrationService.createHomedir(testUser);

					LOG.debug("Creating home directory placeholder file for user folder creation");
					boolean isFileCreated=createHomeDirectoryFile(HOME_DIRECTORY_SCRIPT_FILE_FOLDER_LOCATION,user.getUsername(),GROUP_ID,USERID_PREFIX + user.getId());
					LOG.debug("Home directory file created.. " + isFileCreated);

					UserMountInformation mountInformation = new UserMountInformation();
					mountInformation.set_id(userId);
					mountInformation.setAutomountkey(userId);
					mountInformation.setCn(userId);
					mountInformation.setNismapname(USERS_NIS_MAPNAME);
					String automountInformation = USERS_NFS_MOUNT_PATH + "/" + userId;
					mountInformation.setAutomountinformation(automountInformation);
					mountInformation.setNismapentry(automountInformation);
					openDJIntegrationService.autoMount(mountInformation);
				} else {
					// TODO : Need to handle if user attributes required to update in opendj.
					/*UpdatePassword[] updatePassword = null;
					openDJIntegrationService.updateUser(updatePassword, opendjUser);*/
				}
			}
		} catch (Exception e) {
			LOG.error("Exception while adding new user", e);
			throw new CumulusException(AppMessages.USER_REGISTRATION_CREATE_ERROR.getMessage()
					+ String.format(Constants.ERROR_CODE, AppMessages.USER_REGISTRATION_CREATE_ERROR.getCode()));
		}

		return getUserAuthDetail(user);
	}

	private void setupUser(User user, List<Role> roles, boolean isNewUser, boolean updateStatus) {

		CumulusUser cumulusUser = (CumulusUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		Long createdById = cumulusUser.getId();

		user.setLastModifiedBy(createdById);
		user.setLastModifiedOn(new Date());

		if (isNewUser) {
			user.setCreatedOn(new Date());
			user.setCreatedBy(createdById);
		}

		if (updateStatus) {
			user.setStatus("Active");
		} else {
			user.setStatus("Inactive");
		}
		// extract the text before @ and treat it as user name
		String userName = user.getEmail().split("@")[0];
		user.setUsername(userName);

		// set the user role
		Set<Role> roleSet = new HashSet<>();
		roleSet.addAll(roles);
		Set<UserRole> userRoleSet = new HashSet<>();
		for (Role r : roleSet) {
			UserRole userRole = new UserRole();
			UserRoleId userRoleId = new UserRoleId();
			userRoleId.setUser(user);
			// reload the Role object
			Role role = rolePermissionService.findRole(r.getId());
			userRoleId.setRole(role);
			userRole.setCreatedBy(createdById);
			userRole.setCreatedOn(new Date());
			userRole.setLastModifiedOn(new Date());
			userRole.setLastModifiedBy(createdById);
			userRole.setUserRoleId(userRoleId);
			userRoleSet.add(userRole);

		}
		user.setUserRoleSet(userRoleSet);

	}

	private void setupRole(Role role, List<Permission> permissions, boolean isNewRole) {

		CumulusUser cumulusUser = (CumulusUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		Long createdById = cumulusUser.getId();

		role.setLastModifiedBy(createdById);
		role.setLastModifiedOn(new Date());

		if (isNewRole) {
			role.setCreatedOn(new Date());
			role.setCreatedBy(createdById);
		}

		// set the RolePermission
		Set<Permission> permissionSet = new HashSet<>();
		permissionSet.addAll(permissions);
		List<RolePermission> rolePermissionSet = new LinkedList<>();
		for (Permission p : permissionSet) {
			RolePermission rolePermission = new RolePermission();
			RolePermissionId rolePermissionId = new RolePermissionId();
			rolePermissionId.setRole(role);
			rolePermissionId.setPermission(p);
			rolePermission.setCreatedBy(createdById);
			rolePermission.setCreatedOn(new Date());
			rolePermission.setLastModifiedOn(new Date());
			rolePermission.setLastModifiedBy(createdById);
			rolePermission.setRolePermissionId(rolePermissionId);
			rolePermissionSet.add(rolePermission);

		}
		if (role.getRolePermissionSet() != null) {
			role.getRolePermissionSet().clear();
			role.getRolePermissionSet().addAll(rolePermissionSet);
		} else {
			role.setRolePermissionSet(rolePermissionSet);
		}

	}

	private boolean valid(User user) {
		return user != null && !isEmpty(user.getFirstName()) && !isEmpty(user.getLastName())
				&& !isEmpty(user.getEmail());
	}

	@Override
	public UserAuthDetail updateUser(UserAuthDetail userAuthDetail, boolean updateStatus) throws CumulusException {
		// validation
		if (userAuthDetail == null || !valid(userAuthDetail.getUser()) || userAuthDetail.getRoles() == null
				|| userAuthDetail.getRoles().isEmpty()) {
			throw new CumulusException("Mandatory fields missing");
		}

		// reload User object and set department and project
		User user = userService.getUserByEmail(userAuthDetail.getUser().getEmail());
		// only project name, department and user roles are expected to be
		// updated
		// set department and project
		user.setDepartment(userAuthDetail.getUser().getDepartment());
		user.setProject(userAuthDetail.getUser().getProject());

		setupUser(user, userAuthDetail.getRoles(), false, updateStatus);

		try {

			user = userService.saveUser(user);

		} catch (Exception e) {
			LOG.error("Exception while updating user", e);
			throw new CumulusException("Exception while updating user");
		}

		return getUserAuthDetail(user);

	}

	@Override
	public UserAuthDetail updateUser(String emailId, String status) throws CumulusException {
		User user = userService.getUserByEmail(emailId);

		user.setStatus(status);
		try {
			user = userService.saveUser(user);
		} catch (Exception e) {
			LOG.error("Exception while updating user status", e);
			// throw new CumulusException("Exception while updating user status");
			throw new CumulusException(AppMessages.USER_DELETE_FAILED_IN_DB.getMessage()
					+ String.format(Constants.ERROR_CODE, AppMessages.USER_DELETE_FAILED_IN_DB.getCode()));
		}

		return getUserAuthDetail(user);
	}

	@Override
	public Iterable<Role> getAvailableRoles() throws CumulusException {
		Iterable<Role> roleIterable = rolePermissionService.findAllRole();

		return roleIterable;
	}

	@Override
	public List<RoleDetail> getAvailableRoleDetails() throws CumulusException {
		List<RoleDetail> roleDetailList = new ArrayList<>();
		Iterable<Role> roleList = rolePermissionService.findAllRole();
		// convert to RoleDetail object
		for (Role role : roleList) {
			roleDetailList.add(getRoleDetail(role));
		}
		return roleDetailList;
	}

	private RoleDetail getRoleDetail(Role role) {
		RoleDetail roleDetail = new RoleDetail();
		// copy the direct attributes using BeanUtils.copyProperties
		BeanUtils.copyProperties(role, roleDetail);
		// roleDetail.setId(role.getId());
		// roleDetail.setName(role.getName());
		// roleDetail.setDescription(role.getDescription());
		// roleDetail.setCreatedBy(role.getCreatedBy());
		// roleDetail.setCreatedOn(role.getCreatedOn());
		// roleDetail.setLastModifiedBy(role.getLastModifiedBy());
		// roleDetail.setLastModifiedOn(role.getLastModifiedOn());
		List<Permission> permissionList = new ArrayList<>();
		for (RolePermission rolePermission : role.getRolePermissionSet()) {
			permissionList.add(rolePermission.getPermission());
		}
		roleDetail.setPermission(permissionList);
		return roleDetail;
	}

	@Override
	public List<Permission> getAvailablePermissions() throws CumulusException {
		return rolePermissionService.findAllPermission();
	}

	@Override
	public void removeUser(String emailId, String contactId) throws CumulusException {

		User user = userService.getUserByEmail(emailId);

		userService.deleteUser(user);

		// TODO remove user from Cumulus through UR - API not supported yet

	}

	@Override
	public RoleDetail addNewRole(RoleDetail roleDetail) throws CumulusException {
		// validation
		if (roleDetail == null || isEmpty(roleDetail.getName()) || isEmpty(roleDetail.getDescription())
				|| roleDetail.getPermission() == null || roleDetail.getPermission().isEmpty()) {
			throw new CumulusException("Mandatory fields missing");
		}
		// check for duplicate role name
		if (!rolePermissionService.findRoleByName(roleDetail.getName()).isEmpty()) {
			throw new CumulusException("Cannot add role.A role with the same name already exists");
		}
		Role role = new Role();
		BeanUtils.copyProperties(roleDetail, role);
		// initialize the rolePermissionSet
		role.setRolePermissionSet(null);
		setupRole(role, roleDetail.getPermission(), true);

		try {

			role = rolePermissionService.saveRole(role);

		} catch (Exception e) {
			LOG.error("Exception while adding new role", e);
			throw new CumulusException("Exception while adding new role");
		}

		return getRoleDetail(role);
	}

	@Override
	public void removeRole(String id) throws CumulusException {
		// check if there are users assigned to this role, if so throw
		// validation message
		if (!userService.searchUser(null, null, null, id).isEmpty()) {
			// this role is being assigned to one or more users
			LOG.error("Cannot delete role as it is assigned to one or more users");
			throw new CumulusException("Cannot delete role as it is assigned to one or more users");
		}
		rolePermissionService.deleteRoleById(Long.parseLong(id));

	}

	@Override
	public RoleDetail updateRole(RoleDetail roleDetail) throws CumulusException {
		// validation
		if (roleDetail == null || isEmpty(roleDetail.getName()) || isEmpty(roleDetail.getDescription())
				|| roleDetail.getPermission() == null || roleDetail.getPermission().isEmpty()) {
			throw new CumulusException("Mandatory fields missing");
		}
		// check for duplicate role name. if there are roles with same name and if that role is
		//not having the same id as that of the role being updated, then the user has chosen a
		//new role name and which is already existing
		List<Role> rolesWithSameName = rolePermissionService.findRoleByName(roleDetail.getName());
		if (!rolesWithSameName.isEmpty() && !roleDetail.getId().equals(rolesWithSameName.get(0).getId())) {
			throw new CumulusException("Cannot update role.A role with the same name already exists");
		}
		// reload Role object and set name and description
		Role role = rolePermissionService.findRole(roleDetail.getId());

		role.setName(roleDetail.getName());
		role.setDescription(roleDetail.getDescription());

		setupRole(role, roleDetail.getPermission(), false);

		try {

			role = rolePermissionService.saveRole(role);

		} catch (Exception e) {
			LOG.error("Exception while updating role", e);
			throw new CumulusException("Exception while updating role");
		}

		return getRoleDetail(role);
	}

	private boolean createHomeDirectoryFile(String folderLocation, final String userName, final Long groupID, final Long userID) {

		LOG.debug("createHomeDirectoryFile entered.. " + " folderLocation=["+folderLocation+"], username=["+userName+"], groupID=["+groupID);

		String homeDirectoryFileName = folderLocation + File.separator + userName;

		try {

			File file = new File(homeDirectoryFileName);

			FileWriter fw = new FileWriter(file);

			fw.write("CUSER=" + userName);
			fw.write(System.lineSeparator());
			fw.write("CGID=" + groupID);
			fw.write(System.lineSeparator());
			fw.write("CUID=" + userID);
			fw.write(System.lineSeparator());

			fw.close();
		} catch (IOException ie) {
			System.out.println("Error in createHomeDirectoryFile.. " + ie);
			ie.printStackTrace();
			return false;
		}

		LOG.debug("createHomeDirectoryFile - file created successfully");

		return true;
	}
}
