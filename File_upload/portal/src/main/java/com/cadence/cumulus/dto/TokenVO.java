package com.cadence.cumulus.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@JsonInclude(Include.NON_NULL)
public class TokenVO {

	private String tokenVal;
	private Boolean expired;
	private Long expiryDate;
	
	public Boolean getExpired() {
		return expired;
	}
	public void setExpired(Boolean expired) {
		this.expired = expired;
	}
	public Long getExpiryDate() {
		return expiryDate;
	}
	public void setExpiryDate(Long expiryDate) {
		this.expiryDate = expiryDate;
	}
	@Override
	public String toString() {
		return "{tokenVal=" + tokenVal + ", expired=" + expired + ", expiryDate=" + expiryDate + "}";
	}
	public String getTokenVal() {
		return tokenVal;
	}
	public void setTokenVal(String tokenVal) {
		this.tokenVal = tokenVal;
	}
	
	
	
}
