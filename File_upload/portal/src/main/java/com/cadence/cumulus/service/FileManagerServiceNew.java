package com.cadence.cumulus.service;

import com.cadence.cumulus.model.FileOperationRequest;
import com.cadence.cumulus.model.Result;
import com.fasterxml.jackson.core.JsonProcessingException;
import org.apache.commons.io.FileUtils;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import javax.servlet.ServletException;
import java.io.*;
import java.lang.ProcessBuilder.Redirect;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * @author chandrakant
 * This class replaces the old FileManagerService class. While the old implementation
 * relied on java.io and java.nio API to perform file operations. This became a problem
 * when we had to run the agent as a non-root user vide CMLS-464. So we had to implement
 * all filesystem operations using linux shell commands which are fired as a new process
 * and its results are parsed. While this has not been stress tested, a round of stress
 * testing and regression testing needs to be done to figure out what happens when we end
 * up firing so many processes from our agent app.
 */

@Service
public class FileManagerServiceNew {
    private static final Logger log = LoggerFactory.getLogger(FileManagerServiceNew.class);

    @Value("${date.format:yyyy-MM-dd hh:mm:ss}")
    private String DATE_FORMAT;

    @Value("${user.home.base:/home/}")
    private String USER_HOME_BASE;

/*    @Value("${superuser.switch.command:su}")
    private String superUserSwitchCommand;

    @Value("${superuser.switch.option:-}")
    private String superUserSwitchOption;*/
    @Value("${superuser.switch.command:sudo}")
    private String superUserSwitchCommand;

    @Value("${superuser.switch.option:-u}")
    private String superUserSwitchOption;

    @Value("#{${tools.path}}")
    private Map<String, String> toolsPath;

    private String platform;

    private static final String DEFAULT_USER = "cmls";

    public static final String WINDOWS_OS = "windows";
    public static final String LINUX_OS = "linux";
    public static final String UNIX_OS = "unix";
    public static final String MAC_OS = "macos";

    public FileManagerServiceNew() {
        /*
		 * As of now we are not utilizing the platform details.
		 * In future if we have to execute this service on non-Linux
		 * OS then we need to utilize this.
		 */
        String osName = System.getProperty("os.name");
        if (osName.toLowerCase().indexOf("win") >= 0) {
            platform = WINDOWS_OS;
        } else if (osName.toLowerCase().indexOf("mac") >= 0) {
            platform = MAC_OS;
        } else if (osName.indexOf("nix") >= 0 || osName.indexOf("aix") > 0) {
            platform = UNIX_OS;
        } else if (osName.indexOf("nux") >= 0) {
            platform = LINUX_OS;
        } else {
            throw new RuntimeException("Unsupported platform.");
        }
        log.debug("Platform: " + platform);
    }

    /**
     * @author chandrakant
     * <p>
     * This class encapsulates the result of running a shell command.
     * The output property stores the output of the command
     * and returnValue property stores the exit value.
     */
    static class ProcessResult {
        private String output;
        private Integer returnValue;

        public String getOutput() {
            return output;
        }

        public void setOutput(String output) {
            this.output = output;
        }

        public Integer getReturnValue() {
            return returnValue;
        }

        public void setReturnValue(Integer returnValue) {
            this.returnValue = returnValue;
        }

        public ProcessResult(String output, Integer returnValue) {
            this.output = output;
            this.returnValue = returnValue;
        }
    }

    /**
     * This method runs the shell command passed through an array of strings as parameters
     *
     * @param command - The command that we want to run. If the command we want to run
     *                has command line arguments then each argument goes in as an entry int this array.
     */
    private ProcessResult runCommand(String... command) throws IOException, InterruptedException {
        String tmpScriptName = "/tmp/" + UUID.randomUUID().toString() + ".sh";
        File f = new File(tmpScriptName);
        FileOutputStream fout = new FileOutputStream(f);
        fout.write(("#!/bin/bash\nset -e\n" + command[command.length - 1]).getBytes());
        fout.close();
        f.setExecutable(true, false);
        log.debug("Command: {}", String.join(" :: ", command));
        command[command.length - 1] = tmpScriptName;
        log.debug("RUNNING: {}", String.join(" :: ", command));
        ProcessBuilder processBuilder = new ProcessBuilder(command).redirectErrorStream(true)
                .inheritIO().redirectOutput(Redirect.PIPE);
        Process process = processBuilder.start();
        StringBuilder outputBuilder = new StringBuilder();
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(process.getInputStream()));
        String line = null;
        try {
            while ((line = bufferedReader.readLine()) != null) {
                outputBuilder.append(line + "\n");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        int returnValue = process.waitFor();
        f.delete();
        return new ProcessResult(outputBuilder.toString(), returnValue);

    }

    /**
     * Method to get 'n' characters from the beginning of a file. This Method is
     * primarily used to get the shebang of a script. Shebang is the first line of a file
     * starting with #!. This method is designed to get the first 'n' characters to figure
     * out if we can actually get shebang from this file
     *
     * @param username  - The user who owns the file
     * @param path      - Path to the file
     * @param charCount - Number of characters from the beginning
     * @return The first n characters from the file
     * @throws IOException
     * @throws InterruptedException
     */
    private String fromBeg(String username, String path, int charCount) throws IOException, InterruptedException {
        log.debug("head -c " + charCount + " " + path);
        if (username == null) {
            username = DEFAULT_USER;
        }
 /*       ProcessResult result = runCommand(new String[]{superUserSwitchCommand, superUserSwitchOption, username, "-c",
                "head -c " + charCount + " \"" + path + "\""});*/
        ProcessResult result = runCommand(new String[]{superUserSwitchCommand, superUserSwitchOption, username,
                "head -c " + charCount + " \"" + path + "\""});
        return result.output;
    }

    /**
     * Method to get top 'n' lines from a file. This Method is primarily used to get
     * the shebang line from a script file.
     *
     * @param username  - The user who owns the file
     * @param path      - The path to the file
     * @param lineCount - Number of lines from the beginning
     * @return The top n lines from the file
     * @throws IOException
     * @throws InterruptedException
     */
    private String topLines(String username, String path, int lineCount) throws IOException, InterruptedException {
        log.debug("head -n " + lineCount + " " + path);
        if (username == null) {
            username = DEFAULT_USER;
        }
     /*   ProcessResult result = runCommand(new String[]{superUserSwitchCommand, superUserSwitchOption, username, "-c",
                "head -n " + lineCount + " \"" + path + "\""});*/
        ProcessResult result = runCommand(new String[]{superUserSwitchCommand, superUserSwitchOption, username,
                "head -n " + lineCount + " \"" + path + "\""});
        return result.output;
    }

    /**
     * Method to create a directory for a given user
     *
     * @param username - The directory gets created in the home directory of this user
     * @param path     - Path to the directory that would be created
     * @throws IOException
     * @throws InterruptedException
     */
    public void mkdir(String username, String path) throws IOException, InterruptedException {
        log.debug("mkdir -pv " + path);
        if (username == null) {
            username = DEFAULT_USER;
        }
       // runCommand(new String[]{superUserSwitchCommand, superUserSwitchOption, username, "-c", "mkdir -pv \"" + path + "\""});
        runCommand(new String[]{superUserSwitchCommand, superUserSwitchOption, username, "mkdir -pv \"" + path + "\""});
    }

    private String removeMultipleSpaces(String path){
    	String []destArr = path.split(" ");
    	path = "";
		for(int i = 0 ; i< destArr.length ; i++){
			if(i ==0 ){
				path = path + destArr[i];
			}else if(!"".equals(destArr[i])){
				destArr[i] = destArr[i].trim();
				path = path + " " + destArr[i];
			}
		}
		return path;
    }
    /**
     * Get the unix permissions for the file. The permission is returned as an octal number.
     *
     * @param username - The owner of the file
     * @param path     - Path to the file whose permissions are needed
     * @return - The permissions of the file as an octal number
     * @throws IOException
     * @throws InterruptedException
     */
    private String getPermissions(String username, String path) throws IOException, InterruptedException {
        log.debug("stat -c %a " + path);
        if (username == null) {
            username = DEFAULT_USER;
        }
       /*  ProcessResult result = runCommand(
                new String[]{superUserSwitchCommand, superUserSwitchOption, username, "-c", "stat -c %a \"" + path + "\""}); */
		ProcessResult result = runCommand(
                new String[]{superUserSwitchCommand, superUserSwitchOption, username, "stat -c %a \"" + path + "\""});
        
        return result.output;
    }

    /**
     * Change the permissions on a file in unix/linux filesystems.
     *
     * @param username    - The owner of the file
     * @param path        - Path to the file
     * @param permissions - Permissions that need to be set for the file/directory.
     * @param recursive
     * @throws IOException
     * @throws InterruptedException
     */
    public void chmod(String username, String path, String permissions, boolean recursive)
            throws IOException, InterruptedException {
        if (recursive) {
            log.debug("chmod -R " + permissions + " " + path);
            runCommand(new String[]{superUserSwitchCommand, superUserSwitchOption, username,
                    "chmod -R " + permissions + " \"" + path + "\""});
        } else {
            log.debug("chmod " + permissions + " " + path);
            runCommand(new String[]{superUserSwitchCommand, superUserSwitchOption, username,
                    "chmod " + permissions + " \"" + path + "\""});
        }
    }

    /**
     * Change the ownership of a file.
     *
     * @param username - The user who would own the file
     * @param path     - Path to the file
     * @throws IOException
     * @throws InterruptedException
     */
    public void chown(String username, String path) throws IOException, InterruptedException {
        log.debug("chown -R " + username + " " + path);
        if (username == null) {
            username = DEFAULT_USER;
        }
        runCommand(new String[]{superUserSwitchCommand, superUserSwitchOption, username,
                "chown -R " + username + " \"" + path + "\""});
    }

    /**
     * Copy a file from source to the destination within linux/unix filesystem.
     *
     * @param username    - The user who owns the file
     * @param sourcePath  - The file that needs to be copied over.
     * @param destination - The location where the file needs to be copied over.
     * @throws IOException
     * @throws InterruptedException
     */
    private void cp(String username, String sourcePath, String destination) throws IOException, InterruptedException {
        log.debug("cp -r " + sourcePath + " " + destination);
        if (username == null) {
            username = DEFAULT_USER;
        }
        runCommand(new String[]{superUserSwitchCommand, superUserSwitchOption, username,
                "cp -r \"" + sourcePath + "\" \"" + destination + "\""});
    }

    /**
     * Move a file from one location to other within linux/unix filesystem.
     *
     * @param username    - The user who owns the file.
     * @param source      - The file that needs to be moved.
     * @param destination - The destination where the file needs to be moved.
     * @throws IOException
     * @throws InterruptedException
     */
    private void mv(String username, String source, String destination) throws IOException, InterruptedException {
        log.debug("mv " + source + " " + destination);
        if (username == null) {
            username = DEFAULT_USER;
        }
        runCommand(new String[]{superUserSwitchCommand, superUserSwitchOption, username,
                "mv \"" + source + "\" \"" + destination + "\""});
    }

    /**
     * Remove a file from unix/linux filesystem
     *
     * @param username - The user who owns the file
     * @param fileName - The file that needs to be removed.
     * @throws IOException
     * @throws InterruptedException
     */
    private void rm(String username, String fileName) throws IOException, InterruptedException {
        log.debug("rm -rf " + fileName);
        if (username == null) {
            username = DEFAULT_USER;
        }
        runCommand(new String[]{superUserSwitchCommand, superUserSwitchOption, username, "rm -rf \"" + fileName + "\""});
    }

    /**
     * Fetch the contents of a file.
     *
     * @param username - The owner of the file.
     * @param filename - The file whose contents are needed.
     * @return - The contents of the file returned as a string.
     * @throws IOException
     * @throws InterruptedException
     */
    private String cat(String username, String filename) throws IOException, InterruptedException {
		/*
		 * TODO: Need to check if this method would return the contents of a file
		 * which has an encoding which is not utf-8 and for binary files.
		 */
        log.debug("cat " + filename);
        if (username == null) {
            username = DEFAULT_USER;
        }
        ProcessResult processResult = runCommand(
                new String[]{superUserSwitchCommand, superUserSwitchOption, username, "cat \"" + filename + "\""});
        return processResult.output;
    }

    /**
     * Get the contents of a directory or the details of a file in unix/linux filesystem.
     *
     * @param username - The user who owns the file/directory.
     * @param path     - Path to the file/directory whose details or contents(in case of directory)
     *                 is sought.
     * @return In case of a directory a list of string arrays is returned where each string array
     * represents the details of a file. In case of a file the list would contain a single string array
     * which would represent the details of that file. Each string array contains the output of ls -l
     * in linux. Essentially what we do here is do an ls -l and extract the columns from the output
     * and put them in a string array.
     * @throws IOException
     * @throws InterruptedException
     */
    public List<String[]> ls(String username, String path) throws IOException, InterruptedException {
        log.debug("ls -l --time-style=long-iso " + path);
        if (username == null) {
            username = DEFAULT_USER;
        }
        String[] commands = null;
		/*
		 * We are using --time-style=long-iso with ls so that the time that is
		 * returned is easily parsable by java.
		 */
        if (username != null) {
            commands = new String[]{superUserSwitchCommand, superUserSwitchOption, username,
                    "ls -l --time-style=full-iso \"" + path + "\" | tr -s ' '"};
        } else {
            commands = new String[]{"ls", "-l", "--time-style=full-iso", path, "|", "tr", "-s", "' '"};
        }
        log.debug("Commands String : " + commands);
        ProcessResult processResult = runCommand(commands);
        String[] lines = processResult.output.split("\n");
        List<String[]> files = new ArrayList<String[]>();
        for (String line : lines) {
            String[] parts = line.split(" ");
			/*
			 * Executable files are displayed with a * suffixed to the name, so removing it.
			 */
            if (parts[parts.length - 1].endsWith("*")) {
                parts[parts.length - 1] = parts[parts.length - 1].substring(0, parts[parts.length - 1].length() - 1);
            }
			/*
			 * The first line in the output of ls -l needs to be ignored.
			 */
            if (!parts[0].equals("total") && line != null && line.length() != 0) { // Ignore the first line
                files.add(parts);
            }
        }
        return files;
    }

    private List<String[]> lsUnmodified(String username, String path) throws IOException, InterruptedException {
        log.debug("lsUnmodified: ls -l --time-style=long-iso " + path);
        if (username == null) {
            username = DEFAULT_USER;
        }
        String[] commands = null;
		/*
		 * We are using --time-style=long-iso with ls so that the time that is
		 * returned is easily parsable by java.
		 */
        if (username != null) {
            commands = new String[]{superUserSwitchCommand, superUserSwitchOption, username,
                    "ls -l --time-style=long-iso \"" + path + "\""};
        } else {
            commands = new String[]{"ls", "-l", "--time-style=long-iso", path};
        }
        ProcessResult processResult = runCommand(commands);
        String[] lines = processResult.output.split("\n");
        List<String[]> files = new ArrayList<String[]>();
        for (String line : lines) {
            String[] parts = line.split(" ");
			/*
			 * Executable files are displayed with a * suffixed to the name, so removing it.
			 */
            if (parts[parts.length - 1].endsWith("*")) {
                parts[parts.length - 1] = parts[parts.length - 1].substring(0, parts[parts.length - 1].length() - 1);
            }
			/*
			 * The first line in the output of ls -l needs to be ignored.
			 */
            if (!parts[0].equals("total") && line != null && line.length() != 0) { // Ignore the first line
                files.add(parts);
            }
        }
        return files;
    }

    /**
     * Returns if the path passed belongs to a directory or not
     *
     * @param username - Owner of the file
     * @param path     - Path to the file
     * @return - true if the path represents a directory, false otherwise
     * @throws IOException
     * @throws InterruptedException
     */
    public boolean isDirectory(String username, String path) throws IOException, InterruptedException {
        log.debug("file " + path);
        if (username == null) {
            username = DEFAULT_USER;
        }
        ProcessResult processResult = runCommand(
                new String[]{superUserSwitchCommand, superUserSwitchOption, username, "file \"" + path + "\""});
        return processResult.output.trim().endsWith("directory");
    }

    /**
     * Check if the path represents a symlink to a directory
     *
     * @param username
     * @param path
     * @return true if the path represents symlink to a directory
     * @throws IOException
     * @throws InterruptedException
     */
    public boolean isSymlinkToDirectory(String username, String path) throws IOException, InterruptedException {
        log.debug("file " + path);
        if (username == null) {
            username = DEFAULT_USER;
        }
        ProcessResult processResult = runCommand(
                new String[]{superUserSwitchCommand, superUserSwitchOption, username, "readlink \"" + path + "\""});
        if (processResult.output.trim().length() != 0) {
            return isDirectory(username, processResult.output.trim());
        } else {
            return false;
        }
    }

    /**
     * Returns if a path is valid and whether the path represents a valid file/directory.
     *
     * @param username - The owner of the file
     * @param path     - The path to the file/directory
     * @return true if the path belongs to a valid file/directory and false otherwise.
     * @throws IOException
     * @throws InterruptedException
     */
    public boolean exists(String username, String path) throws IOException, InterruptedException {
        log.debug("ls -l " + path);
        if (username == null) {
            username = DEFAULT_USER;
        }
        ProcessResult processResult = runCommand(
                new String[]{superUserSwitchCommand, superUserSwitchOption, username, "ls -l \"" + path + "\""});
        return processResult.returnValue == 0;
    }

    /**
     * Create an empty file
     *
     * @param username - The owner of the file
     * @param path     - The path to the file that would be cretaed
     * @throws IOException
     * @throws InterruptedException
     */
    public void touchFile(String username, String path) throws IOException, InterruptedException {
        log.debug("touch " + path);
        if (username == null) {
            username = DEFAULT_USER;
        }
        runCommand(new String[]{superUserSwitchCommand, superUserSwitchOption, username, "touch \"" + path + "\""});
    }

    /**
     * Create a file with initial contents.
     *
     * @param username - Owner of the file
     * @param path     - Path to the file
     * @param contents - Contents to be written to the file.
     * @throws IOException
     * @throws InterruptedException
     */
    public void createFile(String username, String path, String contents) throws IOException, InterruptedException {
        String eof = UUID.randomUUID().toString();
        log.debug("tee " + path + " << \"" + eof + "\"\n" + contents + "\n" + eof);
        if (username == null) {
            username = DEFAULT_USER;
        }
        runCommand(new String[]{superUserSwitchCommand, superUserSwitchOption, username,
                "tee \"" + path + "\" << \"" + eof + "\"\n" + contents + "\n" + eof});
    }

    /**
     * Append contents to a file
     *
     * @param username - Owner of the file
     * @param path     - Path to the file
     * @param content  - Contents that need to be appended to the file
     * @throws IOException
     * @throws InterruptedException
     */
    public void appendToFile(String username, String path, String content) throws IOException, InterruptedException {
        String eof = UUID.randomUUID().toString();
        log.debug("tee -a " + path + " << \"" + eof + "\"\n" + content + "\n" + eof);
        if (username == null) {
            username = DEFAULT_USER;
        }
        runCommand(new String[]{superUserSwitchCommand, superUserSwitchOption, username,
                "tee -a \"" + path + "\" << \"" + eof + "\"\n" + content + "\n" + eof});
    }

    /**
     * Create a directory
     *
     * @param request - Details of the directory to be created
     * @return Result of the operation
     */
    public Result addFolder(FileOperationRequest request) {
        try {
        	String path = removeMultipleSpaces(request.getNewPath());
        	log.debug("Creating directory for: {}", path);
            String username = request.getRequestId();
            String directoryPath = USER_HOME_BASE + request.getRequestId() + File.separator + path;
            mkdir(request.getRequestId(), directoryPath);
            chown(directoryPath, username);
            String permissions = getPermissions(request.getRequestId(), USER_HOME_BASE + username);
            chmod(request.getRequestId(), directoryPath, permissions, true);
            return new Result(true, null);
        } catch (Exception e) {
            e.printStackTrace();
            return new Result(false, e.getMessage());
        }
    }

    /**
     * Compress a file. This method uses zip to compress the file. tar can be used
     * for better compression.
     *
     * @param request - Details of the directory to be created
     * @return Result of the operation
     */
    public Result compress(FileOperationRequest request) {
        try {
            String username = request.getRequestId();
            String destination = request.getDestination();
            String compressedFilename = request.getCompressedFilename();
            DateFormat dateFormat = new SimpleDateFormat("yyyyMMddHHmm");
            Date date = new Date();
            compressedFilename += "_" + dateFormat.format(date);
            String homeDirectory = USER_HOME_BASE + request.getRequestId();
            destination = homeDirectory + File.separator + destination + File.separator + compressedFilename;
            List<String> items = request.getItems();
            for (int i = 0; i < items.size(); i++) {
                String item = items.get(i);
                // Doing this because multiple separators are appearing which is not desirable and results in buggy zips
                item = item.replace(File.separator + File.separator, File.separator);
                item = item.replaceAll(" ", "\\\\ ");
                items.set(i, homeDirectory + File.separator + item);
            }

            destination = destination.replaceAll(" ", "\\\\ ");
            
            ProcessResult processResult = runCommand(new String[]{superUserSwitchCommand, superUserSwitchOption,
                    username, "zip -r " + destination + " " + String.join(" ", items)});
            log.debug("Zip command {} , {} " , destination,String.join(" ", items));
            if (processResult.returnValue != 0) {
                log.error(processResult.output);
                return new Result(false, processResult.output);
            } else {
                chown(destination, username);
                chmod(request.getRequestId(), destination, getPermissions(request.getRequestId(), homeDirectory), true);
                return new Result(true, null);
            }
        } catch (Exception ex) {
            log.error(ex.getMessage());
            return new Result(false, ex.getMessage());
        }
    }

    /**
     * Copy file
     *
     * @param request - Details of the file to be copied
     * @return Result of the operation
     */
    public Result copy(FileOperationRequest request) {
        List<String> items = request.getItems();
        String homeFolder = USER_HOME_BASE + request.getRequestId();
        for (String item : items) {
            String sourceFile = homeFolder + File.separator + item;
            String destination = homeFolder + File.separator + request.getNewPath();
          //Logic is added to take care new name in Copy
			boolean singleFile = !StringUtils.isEmpty(request.getSingleFilename());
			if (singleFile) {
			destination = homeFolder + File.separator + request.getNewPath() + File.separator + request.getSingleFilename() ;
			}
			log.debug("destination is {}",destination);
            try {
                cp(request.getRequestId(), sourceFile, destination);
                chown(request.getRequestId(), destination + File.separator + item);
                chmod(request.getRequestId(), destination + File.separator + item,
                        getPermissions(request.getRequestId(), sourceFile), true);
            } catch (Exception e) {
                log.error(e.getMessage());
                return new Result(false, e.getMessage());
            }
        }
        return new Result(true, null);
    }

    /**
     * Move file
     *
     * @param request - Details of the file to be moved.
     * @return Result of the operation
     */
    public Result move(FileOperationRequest request) {
        List<String> items = request.getItems();
        String homeFolder = USER_HOME_BASE + request.getRequestId();
        String target = homeFolder + File.separator + request.getNewPath();
        for (String item : items) {
            String fileName = homeFolder + File.separator + item;
            try {
                mv(request.getRequestId(), fileName, target);
            } catch (Exception e) {
                log.error(e.getMessage());
                return new Result(false, e.getMessage());
            }
        }
        return new Result(true, null);
    }

    /**
     * Change permissions of a file
     *
     * @param request - Details of the file whose permissions need to be changed.
     * @return Result of the operation
     */
    public Result changePermissions(FileOperationRequest request) throws ServletException {
        String homeFolder = USER_HOME_BASE + request.getRequestId();
        String path = homeFolder + File.separator + request.getPath();
        String permsCode = request.getPermsCode();
        boolean recursive = request.isRecursive();

        try {
            chmod(request.getRequestId(), path, permsCode, recursive);
            return new Result(true, null);
        } catch (Exception ex) {
            log.error(ex.getMessage());
            return new Result(false, ex.getMessage());
        }
    }

    /**
     * Delete file/directory(we are using rm -r to do this)
     *
     * @param request - Details of the file to be removed.
     * @return Result of the operation
     */
    public Result delete(FileOperationRequest request) {
        List<String> items = request.getItems();
        String homeDirectory = USER_HOME_BASE + request.getRequestId();
        for (String item : items) {
            String fileName = homeDirectory + File.separator + item;
            try {
                rm(request.getRequestId(), fileName);
            } catch (Exception ex) {
                log.error(ex.getMessage());
                return new Result(false, ex.getMessage());
            }
        }
        return new Result(true, null);
    }

    /**
     * Return the contents of a file.
     *
     * @param request - Details of the file whose contents are sought.
     * @return Result of the operation
     */
    public Result editFile(FileOperationRequest request) throws ServletException {
        try {
            String homeFolder = USER_HOME_BASE + request.getRequestId();
            String path = homeFolder + File.separator + request.getPath();
            log.debug("editFile path: {}", path);

            String content = cat(request.getRequestId(), path);

            return new Result(true, content);
        } catch (Exception e) {
            log.error("editFile", e);
            return new Result(false, e.getMessage());
        }
    }

    /**
     * Return the contents of a file
     *
     * @param request - Details of the file whose contents are needed.
     * @return Result of the operation
     */
    public String fileContents(FileOperationRequest request) throws ServletException {
        String homeFolder = USER_HOME_BASE + request.getRequestId();
        String fileName = homeFolder + File.separator + request.getItem();
        try {
            return cat(request.getRequestId(), fileName);
        } catch (Exception ex) {
            log.error(ex.getMessage());
            return null;
        }
    }

    /**
     * Return the contents of a file
     *
     * @param request - Details of the file whose contents are needed.
     * @return Result of the operation
     */
    public String getFileContents(FileOperationRequest request) throws ServletException {
        try {
            String homeFolder = USER_HOME_BASE + request.getRequestId();
            String path = homeFolder + File.separator + request.getPath();
            log.debug("editFile path: {}", path);

            String content = cat(request.getRequestId(), path);

            return content;
        } catch (Exception e) {
            log.error("editFile", e);
            return null;
        }
    }

    /**
     * Save a file
     *
     * @param request - Details of the file that needs to be saved.
     * @return Result of the operation
     */
    public Result saveFile(FileOperationRequest request) throws ServletException {
        try {
            String homeFolder = USER_HOME_BASE + request.getRequestId();
            String path = request.getPath();
            String content = request.getContent();

            log.debug("saveFile path: {} content: isNotBlank {}, size {}", path, !StringUtils.isEmpty(content),
                    content != null ? content.length() : 0);

            String filename = UUID.randomUUID().toString();
			/*
			 * TODO: This code is not thread safe.
			 */
            File tmpFile = new File("/tmp/" + filename);
            FileUtils.writeStringToFile(tmpFile, content);
            String dest = USER_HOME_BASE + request.getRequestId() + File.separator + path;
            mv(request.getRequestId(), tmpFile.getAbsolutePath(), dest);
            chown(dest, request.getRequestId());
            return new Result(true, null);
        } catch (Exception e) {
            log.error("saveFile", e);
            return new Result(false, e.getMessage());
        }
    }

    /**
     * Extract a zip file.
     *
     * @param request Details of the zip operation
     * @return
     * @throws ServletException
     */
    public Result extract(FileOperationRequest request) throws ServletException {
        try {
            String path = USER_HOME_BASE + request.getRequestId() + File.separator + request.getPath(); // "/public_html/compressed.zip"
            String destination = USER_HOME_BASE + request.getRequestId() + File.separator + request.getDestination(); // "/public_html/extracted-files"
            String sourceFile = null; // request.getSourceFile(); //
            // /public_html/compressed.zip"

            // FIXME parameters are right? the doc so...
            log.debug("extract path: {} destination: {} sourceFile: {}", path, destination, sourceFile);

            // TODO
            return new Result(false, "not implemented");
        } catch (Exception e) {
            log.error("extract", e);
            return new Result(false, e.getMessage());
        }
    }

	/*
	 * chasingh CMLS-464 : File Agent should run as "cmls" user
	 */

    /**
     * List the contents of a directory.
     *
     * @param request
     * @return
     */
    public JSONObject list(FileOperationRequest request) {
        String path = request.getPath();
        List<JSONObject> resultList = new ArrayList<JSONObject>();
        try {
			/*
			 * In case its a symlink to a directory ls should return the contents of the
			 * directory its pointing to. Instead it returns details of the symlink.
			 * So we append / to the end of the symlink name. That lists the contents
			 * of the directory linked by the symlink. DO THIS ONLY IF ITS SYMLINK TO
			 * DIRECTORY
			 */
            long startTime = System.currentTimeMillis();
            log.debug("start of list() method : " + startTime);
            if (isSymlinkToDirectory(request.getRequestId(),
                    USER_HOME_BASE + request.getRequestId() + File.separator + path)) {
                path = path + "/";
            }
            log.debug("Symlink to dir {}", isSymlinkToDirectory(request.getRequestId(), USER_HOME_BASE + request.getRequestId() + File.separator + path));
            List<String[]> files = ls(request.getRequestId(),
                    USER_HOME_BASE + request.getRequestId() + File.separator + path);
            // Did this because some directories have setuid and setgid bit set.
            // If wrong user we would not have permissions to read the contents.
            // The output in this case would be all mangled. e.g. /home/neethan/shared/parthad in aw03
           // ProcessResult result = runCommand("su", request.getRequestId(), "-c", "cd " + USER_HOME_BASE + request.getRequestId() + File.separator + path);
            ProcessResult result = runCommand("sudo","-u", request.getRequestId(), "cd " + USER_HOME_BASE + request.getRequestId() + File.separator + path);
            log.debug("Checking if dont have permissions: {}", result.getOutput().toLowerCase());
            if (result.getOutput().toLowerCase().contains("permission denied")) {
                JSONObject returnResultList = new JSONObject();
                // Need to change this in case we want a permission denied to be shown at ui. Now
                // we are just telling that there's nothing in here.
                returnResultList.put("error", "Permission Denied");
                returnResultList.put("result", resultList);
                returnResultList.put("homeDir", USER_HOME_BASE + request.getRequestId());

                return returnResultList;
            }
            // If we do not have permissions on the directory..
            if (files.size() == 1 && String.join(" ", files.get(0)).endsWith("Permission denied")) {

                JSONObject returnResultList = new JSONObject();
                // Need to change this in case we want a permission denied to be shown at ui. Now
                // we are just telling that there's nothing in here.
                returnResultList.put("result", resultList);
                returnResultList.put("homeDir", USER_HOME_BASE + request.getRequestId());

                return returnResultList;
            }
            for (String[] file : files) {
                JSONObject el = new JSONObject();
                String filename = "";
                String targetFilename = "";
                int arrowIndex = -1;
                //log.debug("no of components {}", file.length);
                if (file.length == 9) {
                    filename = file[file.length - 1];
                } else if (file.length > 9) {
                    if (file[0].startsWith("l")) {
                        filename = "";
                        for (int i = 8; i < file.length; i++) {
                            if (!file[i].equals("->")) {
                                filename += file[i] + " ";
                            } else {
                                arrowIndex = i;
                                break;
                            }
                        }
                        for (int i = arrowIndex + 1; i < file.length; i++) {
                            targetFilename += file[i] + " ";
                        }
                    } else {
                        for (int i = 8; i < file.length; i++) {
                            filename += file[i] + " ";
                        }
                    }
                }
                filename = filename.trim();
                targetFilename = targetFilename.trim();
                if (file[0].startsWith("d") && file[0].endsWith("/")) {
                    // Remove the trailing / in directory names
                    filename = filename.substring(0, filename.length() - 1);
                }
                el.put("name", filename);
                String rights = file[0];
                // To comply with the old rights' format, lets remove the first bit.
                if (rights.startsWith("d") || rights.startsWith("-") || rights.startsWith("l")) {
                    rights = rights.substring(1);
                }
                el.put("rights", rights);
                String date = file[5] + " " + file[6];
                date = date.substring(0, date.lastIndexOf('.'));
                el.put("date", date);
                el.put("size", file[4]);
                el.put("type", file[0].startsWith("d") ? "dir" : "file");
                if (file[0].startsWith("l")) {
                    // This is a symlink. In case it points to a directory lets update type as dir
                    // Lets also use the 3rd last token as the file name and not the last token
                    //filename = file[file.length - 3];
                    // el.put("name", filename);
                    log.debug("Found a symlink. Let's see if its for a dir");
                    log.debug("Is Directory? {} {}", targetFilename, isDirectory(request.getRequestId(), targetFilename));
                    if (isDirectory(request.getRequestId(), targetFilename)) {
                        // This symlink points to a directory
                        el.put("type", "dir");
                    } else {
                        el.put("type", "file");
                    }
                }
                el.put("symlink", file[0].startsWith("l"));
                if (!el.get("type").equals("dir") && !file[0].startsWith("l")) {
                    String first2Chars = fromBeg(request.getRequestId(), USER_HOME_BASE + request.getRequestId()
                            + File.separator + path + File.separator + el.get("name"), 2).trim();
                    if (first2Chars.equals("#!")) {
                        el.put("shebang",
                                topLines(request.getRequestId(), USER_HOME_BASE + request.getRequestId()
                                        + File.separator + path + File.separator + el.get("name"), 1).trim()
                                        .substring(2));
                    }
                    if (!el.keySet().contains("shebang")) {
                        el.put("shebang", "");
                    }
                }
                resultList.add(el);
            }
            JSONObject returnResultList = new JSONObject();
            returnResultList.put("result", resultList);
            returnResultList.put("homeDir", USER_HOME_BASE + request.getRequestId());
            log.debug("End of list() method : " + (System.currentTimeMillis() - startTime));

            return returnResultList;
        } catch (Exception ex) {
            ex.printStackTrace();
            log.error("list files ", ex.getMessage());
        }
        return null;
    }

    /**
     * Validate if a path is valid. Used for validating paths of tools.
     *
     * @param username - Owner of the file(tool)
     * @param toolName - Name of the file(tool)
     * @param exePath  - Path at which the tool's bin directory is present.
     * @return true if found false otherwise
     * @throws ServletException
     */
    public boolean validatePath(String username, String toolName, String exePath) throws ServletException {
        try {
            if (exists(username, exePath) && isDirectory(username, exePath)) {
                return exists(username, exePath + File.separator + "bin" + File.separator + toolName);
            }
        } catch (Exception e) {
            log.error("list", e);
            e.printStackTrace();
        }
        return false;
    }

    /**
     * Get tool versions. Several directories containing multiple versions of tools
     * are present in the tools directory. The names of these directories correspond to the versions.
     * This method essentially traverses through these directories and checks if they
     * contain a directory named bin which in turn contains the tool.
     *
     * @param username - User who has permissions on the directories/files
     * @return - A JSON object containing the tools and their versions.
     * @throws ServletException
     * @throws JsonProcessingException
     */
    public JSONObject listToolsAndVersions(String username) throws ServletException, JsonProcessingException {
        try {
            List<JSONObject> tools = new ArrayList<JSONObject>();
            JSONObject result = new JSONObject();

            for (Map.Entry<String, String> toolEntry : toolsPath.entrySet()) {
                ArrayList<String> toolVersions = new ArrayList<String>();
                JSONObject toolInfo = new JSONObject();
                toolInfo.put("name", toolEntry.getKey());
                toolInfo.put("baseDir", toolEntry.getValue());
                log.debug("toolentry {}", toolEntry.getValue());
                if (exists(username, toolEntry.getValue()) && isDirectory(username, toolEntry.getValue())) {
                    log.debug("ls unmodified.. firing.");
                    List<String[]> files = lsUnmodified(username, toolEntry.getValue());
                    log.debug("ls count: {}", files.size());
                    for (String[] file : files) {
                        String dirname = null;
                        if (file[0].startsWith("d")) {
                            dirname = file[file.length - 1];
                        } else if (file[0].startsWith("l") && isDirectory(
                                username, toolEntry.getValue() + File.separator + file[file.length - 1])) {
                            // Its a symlink to a directory
                            dirname = file[file.length - 3];
                        }
                        if (dirname != null && exists(username, toolEntry.getValue() + File.separator + dirname
                                + File.separator + "bin" + File.separator + toolEntry.getKey())) {
                            toolVersions.add(dirname);
                        }
                    }
                    toolInfo.put("versions", toolVersions);
                }
                tools.add(toolInfo);
            }
            result.put("tools", tools);
            return result;
        } catch (Exception e) {
            log.error("list", e);
            return null;
        }
    }

    /**
     * Rename a file
     *
     * @param request - Details of the operation
     * @return
     * @throws ServletException
     */
    public Result rename(FileOperationRequest request) throws ServletException {
        try {
            String path = USER_HOME_BASE + request.getRequestId() + File.separator + request.getItem();
            String newpath = USER_HOME_BASE + request.getRequestId() + File.separator + request.getNewItemPath();

            log.debug("rename from: {} to: {} for {}", path, newpath, request.getUserName());

            mv(request.getRequestId(), path, newpath);
            chown(newpath, request.getRequestId());
            return new Result(true, null);
        } catch (Exception e) {
            log.error("rename", e);
            return new Result(false, e.getMessage());
        }
    }

    /**
     * Get the details of a file as returned through ls -l.
     *
     * @param request - This object contains the path to the file.
     * @return
     */
    public List<String[]> getFileDetails(FileOperationRequest request) {
        try {
            String path = request.getItem();
            return ls(request.getUserName(), USER_HOME_BASE + request.getRequestId() + File.separator + path);
        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
            return null;
        }
    }
}

