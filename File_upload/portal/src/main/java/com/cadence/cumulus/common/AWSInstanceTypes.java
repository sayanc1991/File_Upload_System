/**
 * 
 */
package com.cadence.cumulus.common;

/**
 * @author ramsudheer
 *
 */
public enum AWSInstanceTypes {
	
	AWS("AWS");
	
	private final String instaceType;

	private AWSInstanceTypes(String instaceType) {
		this.instaceType = instaceType;
	}

	/**
	 * @return the jobStatus
	 */
	public final String getInstaceType() {
		return instaceType;
	}

	public static AWSInstanceTypes getInstanceType(String instaceType) {
		for (AWSInstanceTypes ait : AWSInstanceTypes.values()) {
			if (ait.getInstaceType().equals(instaceType))
				return ait;
		}
		return null;
	}
}
