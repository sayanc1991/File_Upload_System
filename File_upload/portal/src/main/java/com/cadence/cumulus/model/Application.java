package com.cadence.cumulus.model;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class Application {
	
	/*
	 * This class will be a model class in future and will hold the configuration values passed 
	 * from Cycle at the time of chamber provisioning, such as cumulus id.
	 */
	
	private List<Plugin> appPluginList ;

	public List<Plugin> getAppPluginList() {
		return appPluginList;
	}

	public void setAppPluginList(List<Plugin> appPluginList) {
		this.appPluginList = appPluginList;
	}


}
