/**
 * 
 */
package com.cadence.cumulus.repository.mysql;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.cadence.cumulus.model.States;

/**
 * @author deepak
 *
 */
public interface StatesRepository extends JpaRepository<States, Long> {

	public List<States> findByCountryCode(String countryCode);

	public States findByStateCode(String stateCode);
}
