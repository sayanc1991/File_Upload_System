package com.cadence.cumulus.controller;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Base64;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.servlet.http.HttpServletRequest;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.CookieStore;
import org.apache.http.client.config.CookieSpecs;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.impl.client.BasicCookieStore;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.cadence.cumulus.common.FastXErrors;
import com.cadence.cumulus.common.LDAPUNPWStore;

@Controller
public class FastXController {
    private static final Logger log = LoggerFactory.getLogger(FastXController.class);

    private static final String REASON_KEY = "reason";
   
    @Value("${xclient.fastx_url}")
    private String FASTX_URL;

    @Value("${xclient.redir_fastx_url}")
    private String REDIR_FASTX_URL;

    
	private final String LOGIN_URL = "api/login";
	private final String LIST_URL = "api/list/";
	private final String CREATE_SESSION_URL = "api/start/";
	private final String CONNECT_URL = "api/connect/";
	private final String RDP_URL = "xrdp#";
	private CloseableHttpClient client;
	
	private int width = 0, height = 0;
	
	@SuppressWarnings("deprecation")
    @RequestMapping("/xclient/startgdm")
	@PreAuthorize("hasAuthority('X Client Access')")
	public String startgdm(HttpServletRequest request) {
		try { 
			width = Integer.parseInt(request.getParameter("width"));
			height = Integer.parseInt(request.getParameter("height"));
			if(width < 300) width = 1024;
			if(height < 300) height = 768;
		} catch(Exception e) {
			log.info("Not getting view port width and height from client, setting to 1024x768.");
		}
		
		
		//reconstruct unpw for ssh login
		String userNameFull = (String)request.getSession().getAttribute("username");
		log.debug("UserName from session is : " + userNameFull);
		String[] parts = userNameFull.split("@");
		String userName = parts[0];
		String password = LDAPUNPWStore.getInstance().getPassForUser(userName);
		if(password == null) {
			logAndSetErrorOnRequest(request, FastXErrors.FASTX_SERVER_PASSWORD_IS_NULL);
			return "error.html";
		}
		
		CookieStore httpCookieStore = new BasicCookieStore();
		RequestConfig globalConfig = RequestConfig.custom()
		        .setCookieSpec(CookieSpecs.BROWSER_COMPATIBILITY)
		        .build();
		HttpClientBuilder builder = HttpClientBuilder.create().setDefaultCookieStore(httpCookieStore);
		client = builder.setDefaultRequestConfig(globalConfig).build();
		
		//client = HttpClientBuilder.create().build();
		
		String loginResponseJson = "";
		try {
			loginResponseJson = fastXLogin(userName, password);
		} catch (ClientProtocolException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		JSONObject obj = null;
		try {
			obj = new JSONObject(loginResponseJson);
		}
		catch (JSONException ex) {
			logAndSetErrorOnRequest(request, FastXErrors.FASTX_SERVER_INVALID_RESPONSE);
			return "error.html";
		}
		if (obj != null && isErrorObject(obj)) {
			JSONObject error = obj.getJSONObject("error");
			int errorCode = error.getInt("code");
			logAndSetErrorOnRequest(request, errorCode);
			return "error.html";
		}
		
		if (obj != null) {
			String stage = obj.getJSONObject("result").getString("stage");
			String sessionToken = obj.getJSONObject("result").getString("token");
			log.debug("Stage result of login" + stage);
			log.debug("Session Token is : " + sessionToken);
			
			String sessionKey = findGDMSessionKey(sessionToken);
			if(sessionKey == null) {
				createGDMSession(sessionToken);
				sessionKey = findGDMSessionKey(sessionToken);
			}
			
			String connectJSON = "";
			try {
				String url = FASTX_URL + CONNECT_URL + sessionToken;
				log.debug("Posting to url : " + url);
				connectJSON = getFastXURL(url, sessionKey);
				if (connectJSON == null) {
					logAndSetErrorOnRequest(request, FastXErrors.FASTX_INVALID_XSERVER_SESSION);
					return "error.html";
				}
			} catch (ClientProtocolException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			
			//DANGER
			String encoded = new String(Base64.getEncoder().encode(connectJSON.getBytes()));
			log.debug("Connect json is : " + connectJSON);
			log.debug("Encoded json is : " + encoded);
			String redirectUrl = "redirect:" + REDIR_FASTX_URL+RDP_URL+encoded;
			log.debug("Redirect url is : " + redirectUrl);
			return redirectUrl;
		}
		else {
			logAndSetErrorOnRequest(request, FastXErrors.FASTX_COULD_NOT_START_XCLIENT);
			return "error.html";
		}

	}
	
	private String fastXLogin(String userName, String password) throws ClientProtocolException, IOException {
		//Login
		HashMap<String, String> params = new HashMap<String, String>();
		params.put("username", userName);
		params.put("password", password);
		String result = postRequest(FASTX_URL+LOGIN_URL, params);
		log.debug(result);
		return result;
	}
	
	private String getFastXURL(String url, String sessionID) throws ClientProtocolException, IOException {
		log.debug("Posting to url : " + url);
		log.debug("Using session id : " + sessionID);
		
		HashMap<String, String> params = new HashMap<String, String>();
		params.put("id", sessionID);
		
		String result = postRequest(url, params);
		
		JSONObject res = new JSONObject(result);
		log.debug("Result is : " + result);

		JSONObject obj = null;
		if (res.has("result")) {
			obj = res.getJSONObject("result");
			return obj.toString();
		}
		else {
			return null;
		}
	}
	
	private String createGDMSession(String sessionToken) {
		log.debug("In createGDMSession, using url : " + FASTX_URL+CREATE_SESSION_URL+sessionToken);
		String createSessionsResponse = "";
		HashMap<String, String> params = new HashMap<String, String>();
		params.put("command", "gnome-session");
		String geometry = width+"x"+height;
		log.debug("Using geometry - " + geometry);
		params.put("geometry", geometry);
		params.put("name", "gnome-session");
		try {
			createSessionsResponse = postRequest(FASTX_URL+CREATE_SESSION_URL+sessionToken, params);
		} catch (IOException e) {
			log.error("Exception occured while creating GDM session.");
			e.printStackTrace();
		}
		log.debug("Response from create session - " + createSessionsResponse);
		return createSessionsResponse;
	}
	
	private String findGDMSessionKey(String sessionToken) {
		String listSessionsResponse = "";
		try {
			listSessionsResponse = getRequest(FASTX_URL+LIST_URL+sessionToken, null);
		} catch (ClientProtocolException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (URISyntaxException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		log.debug("List session response - " + listSessionsResponse);
		
		JSONObject obj = new JSONObject(listSessionsResponse);
		if(obj.has("result")) {
			JSONArray resultArray = obj.getJSONArray("result");
			JSONObject objArrMember;
			
			String sessionKey = "";
			String command = "";
	
			for (int i = 0; i < resultArray.length(); i++) {
				objArrMember = resultArray.getJSONObject(i);
	    		sessionKey = objArrMember.getString("id");
	    		command = objArrMember.getString("command");
	    		if("gnome-session".equalsIgnoreCase(command)) {
	    			log.debug("Results are : " + sessionKey + " : " + command);
	    			return sessionKey;
	    		}
			}
	
			//no gdm sessions, need to create one
			log.debug("Dont have gdm sessions, need to create one!");
			return null;
		}else {
			log.error("JSONObject 'result' not found");
			return "error.html";
		}
		//return createGDMSession(sessionToken);
	}
	
	private String postRequest(String url, HashMap<String, String> postParams) throws ClientProtocolException, IOException {
		log.debug("\n New Post Request with url " + url);
		//CloseableHttpClient client = HttpClientBuilder.create().build();
//		DefaultHttpClient client = new DefaultHttpClient();
//		client.getParams().setParameter(ClientPNames.COOKIE_POLICY, CookiePolicy.BROWSER_COMPATIBILITY);;
		HttpPost post = new HttpPost(url);
		List<NameValuePair> urlParameters = new ArrayList<NameValuePair>();
		if(postParams != null) {
			Iterator<Entry<String, String>> it = postParams.entrySet().iterator();
		    while (it.hasNext()) {
		        Map.Entry<String, String> pair = (Map.Entry<String, String>)it.next();
		        urlParameters.add(new BasicNameValuePair(pair.getKey(), pair.getValue()));
		        it.remove();
		    }
		}

		post.setEntity(new UrlEncodedFormEntity(urlParameters));

		HttpResponse response = client.execute(post);
		
		log.debug("Response Code : "
		                + response.getStatusLine().getStatusCode());
//		CookieStore cookieStore = client.getCookieStore();
//		List<Cookie> cookies = cookieStore.getCookies();
//		for(Cookie c : cookies) {
//			System.out.println("Cookie : " + c.toString());
//		}
		return EntityUtils.toString(response.getEntity());
	}
	
	private String getRequest(String url, HashMap<String, String> getParams) throws URISyntaxException, ClientProtocolException, IOException {
		log.debug("\n New Get Request with url - " + url);
		
		java.net.URI uri = null;
		
		
		//CloseableHttpClient client = HttpClientBuilder.create().build();
		//DefaultHttpClient client = new DefaultHttpClient();
		//client.getParams().setParameter(ClientPNames.COOKIE_POLICY, CookiePolicy.BROWSER_COMPATIBILITY);
		HttpGet get = new HttpGet(url);
		if(getParams != null) {
			Iterator<Entry<String, String>> it = getParams.entrySet().iterator();
		    while (it.hasNext()) {
		        Map.Entry<String, String> pair = (Map.Entry<String, String>)it.next();
		        uri = new URIBuilder(get.getURI()).addParameter(pair.getKey(),
		        		pair.getValue()).build();
		        it.remove();
		    }
		    ((HttpRequestBase) get).setURI(uri);
		}
		
	    HttpResponse response = client.execute(get);
	    log.debug("Response Code : "
                + response.getStatusLine().getStatusCode());
//		CookieStore cookieStore = client.getCookieStore();
//		List<Cookie> cookies = cookieStore.getCookies();
//		for(Cookie c : cookies) {
//			System.out.println("Cookie : " + c.toString());
//		}

		return EntityUtils.toString(response.getEntity());
	}

	private boolean isErrorObject(JSONObject obj) {
		try {
			obj.getJSONObject("error");
			return true;
		} catch (Exception e) {
			log.info("Not error Object");
			return false;
		}
		
	}
	
	private void logAndSetErrorOnRequest(HttpServletRequest request, Integer errorCode) {
		request.setAttribute(REASON_KEY, "Error: " + errorCode);
		log.error(FastXErrors.ERROR_MESSAGES.get(errorCode));
	}

}
