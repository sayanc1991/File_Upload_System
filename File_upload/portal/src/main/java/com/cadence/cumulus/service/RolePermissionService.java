package com.cadence.cumulus.service;

import java.util.List;

import com.cadence.cumulus.model.Permission;
import com.cadence.cumulus.model.Role;

/**
 * 
 * @author shirish
 *
 */
public interface RolePermissionService {

	/**
	 * 
	 * @return
	 */
	public Iterable<Role> findAllRole();

	/**
	 * 
	 * @return
	 */
	public List<Permission> findAllPermission();
	
		
	public void deleteRoleById(Long id);
	
	public Role findRole(Long id);
	
	public Role saveRole(Role role);
	
	public List<Role> findRoleByName(String name);
	

}
