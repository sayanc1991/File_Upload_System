package com.cadence.cumulus.cache;

/**
 * Enum which has the list of object types which can be cached
 * @author zabeer
 *
 */
public enum CacheObjectType {

	UR_TOKEN_DATA,OKTA_GROUP_ID, SESSION_ID
}
