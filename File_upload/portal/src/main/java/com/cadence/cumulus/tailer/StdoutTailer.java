package com.cadence.cumulus.tailer;

import com.cadence.cumulus.model.AgentCommand;
import com.cadence.cumulus.model.AgentResponse;
import org.apache.commons.io.input.Tailer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.BlockingQueue;

public class StdoutTailer extends CumulusTailerListener {
    private static final Logger log = LoggerFactory.getLogger(StdoutTailer.class);

    public StdoutTailer(BlockingQueue<AgentResponse> queue, AgentCommand command) {
        super(queue, command);
    }

    @Override
    public void handle(String line) {
        //log.debug("StdoutTailer - handle event invoked");
       // log.debug("StdoutTailer - handle event {}", line);
        AgentResponse agentResponse=new AgentResponse(command.getRequestId(), command.getUserName(), "jobs-stdout", line);
        log.debug("StdoutTailer - handle event - Adding blocking queue.. AgentResponse Command - " + agentResponse.getCommand() );
        queue.add(agentResponse);
    }

    @Override
    public void handle(Exception ex) {
        log.debug("StdoutTailer - Error", ex);
    }

    @Override
    public void init(Tailer tailer) {
        log.debug("StdoutTailer - Init done...");
    }

    @Override
    public void fileNotFound() {
        log.debug("StdoutTailer - File not found...");
    }

    @Override
    public void fileRotated() {
        log.debug("StdoutTailer - File rotated...");
    }
}
