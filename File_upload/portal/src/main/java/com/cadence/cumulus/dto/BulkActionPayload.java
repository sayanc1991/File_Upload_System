package com.cadence.cumulus.dto;

import java.util.List;

public class BulkActionPayload extends ActionPayload {

	private List<String> sourcePaths;
	private String destinationPath;
	private String singleFileName;
	public List<String> getSourcePaths() {
		return sourcePaths;
	}
	public void setSourcePaths(List<String> sourcePaths) {
		this.sourcePaths = sourcePaths;
	}
	public String getDestinationPath() {
		return destinationPath;
	}
	public void setDestinationPath(String destinationPath) {
		this.destinationPath = destinationPath;
	}
	public String getSingleFileName() {
		return singleFileName;
	}
	public void setSingleFileName(String singleFileName) {
		this.singleFileName = singleFileName;
	}
}
