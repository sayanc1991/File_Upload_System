package com.cadence.cumulus.integration.unifiedregistration.vo;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class UpdateUserVO {

	private String loginid;
	private String appcode;
	private String usertype;
	private UpdateUserContactInfoVO con;

	public String getLoginid() {
		return loginid;
	}
	public void setLoginid(String loginid) {
		this.loginid = loginid;
	}
	public String getAppcode() {
		return appcode;
	}
	public void setAppcode(String appcode) {
		this.appcode = appcode;
	}
	public String getUsertype() {
		return usertype;
	}
	public void setUsertype(String usertype) {
		this.usertype = usertype;
	}
	public UpdateUserContactInfoVO getCon() {
		return con;
	}
	public void setCon(UpdateUserContactInfoVO con) {
		this.con = con;
	}
	
	@Override
	public String toString() {
		return "UpdateUserVO [loginid=" + loginid + ", appcode=" + appcode + ", usertype=" + usertype + ", con=" + con
				+ "]";
	}
}