package com.cadence.cumulus.service;

import java.util.Map;

import com.cadence.cumulus.model.MachineInfo;

public interface InfraQueueService {

	public Map<String,MachineInfo> fetchMachineInfo();
	
}
