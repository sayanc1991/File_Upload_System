package com.cadence.cumulus.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "Preference")
public class Preference {
	
	@Id
    @GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="Id")
	private String id;
	
	private String category;
	
	private String event;
	
	private String deliveryMechanism;

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public String getEvent() {
		return event;
	}

	public void setEvent(String event) {
		this.event = event;
	}

	public String getDeliveryMechanism() {
		return deliveryMechanism;
	}

	public void setDeliveryMechanism(String deliveryMechanism) {
		this.deliveryMechanism = deliveryMechanism;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}
	

}
