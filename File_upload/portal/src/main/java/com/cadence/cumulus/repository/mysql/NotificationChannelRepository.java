package com.cadence.cumulus.repository.mysql;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.cadence.cumulus.model.NotificationChannel;

@Repository
public interface NotificationChannelRepository extends JpaRepository<NotificationChannel, Long> {

}
