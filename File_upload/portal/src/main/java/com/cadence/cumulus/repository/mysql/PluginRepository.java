package com.cadence.cumulus.repository.mysql;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.cadence.cumulus.model.Plugin;

@Repository
public interface PluginRepository extends JpaRepository<Plugin, Long> {
	
	List<Plugin> findByEnabled(char enabled);

}
