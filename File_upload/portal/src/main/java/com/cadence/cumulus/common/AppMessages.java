/**
 * 
 */
package com.cadence.cumulus.common;

/**
 * @author deepakma
 *
 */
public enum AppMessages {
	
	USER_LOGIN_NOT_FOUND(101, "User not found."),
	USER_LOGIN_AUTH_FAIL(102, "Authentication failed"),
	USER_REGISTRATION_DOMAIN_NOT_FOUND(103,"User domain not registered with Cadence® Cloud Orchestrator."),
	USER_REGISTRATION_NOT_FOUND_DB(104,"Cadence® Cloud Orchestrator user %s already exists."),
	USER_REGISTRATION_USER_NOT_FOUND(105,"User %s not found, Please contact support."),
	// USER_REGISTRATION_FIELDS_EMPTY(108,"Please enter user %s number."),
	USER_REGISTRATION_CREATE_ERROR(106,"Creating user in UR failed, Please try again."),
	USER_REGISTRATION_UPDATE_ERROR(107,"Re-adding user to group failed, Please try again."),
	USER_REGISTRATION_INVALID_EMAIL(108,"Email not valid"),
	USER_DELETE_FAILED_IN_UR(109,"Removing user group failed, Please try again."),
	USER_DELETE_FAILED_IN_DB(110,"Exception while updating user status"),
	USER_RESET_SECURITY_QA_FIELDS_EMPTY(111,"User password / security question / security answer should not be empty."),
	USER_RESET_SECURITY_QA_INVALID_PASSWORD(112,"User password / security question / security answer must not have same values."),
	USER_RESET_SECURITY_QA_INVALID_ANSWER(113,"User answer should not match its Username."),
	USER_RESET_SECURITY_QA_NOT_FOUND(114,"User %s not found in Unified Registration"),
	USER_RESET_SECURITY_QA_FAIL(115,"Updating user security question/answer failed, Please try again."),
	USER_CHANGE_PASSWORD_FIELDS_EMPTY(116,"User password should not be empty."),
	USER_CHANGE_PASSWORD_INVALID(117,"User new password and confirm password should be same."),
	USER_CHANGE_PASSWORD_INVALID_VALUE(118,"User password should not contain a white space character."),
	USER_CHANGE_PASSWORD_NOT_FOUND(119,"User %s not found in Unified Registration"),
	USER_CHANGE_PASSWORD_FAIL(120,"Updating user password failed, Please try again."),

	USER_REGISTRATION_FOUND_UR(130,"User already registered in Cadence.com"),
	USER_REGISTRATION_NOT_FOUND_UR(131,"Please enter the user information for registration."),
	USER_REGISTRATION_CREATE_SUCCESS(132,"New user created successfully."),
	USER_REGISTRATION_UPDATE_SUCCESS(133,"User updated successfully."),
	USER_DELETE_SUCCESS(134,"User successfully deleted."),
	USER_SEARCH_NOT_FOUND(135,"No records found"),
	USER_RESET_SECURITY_QA_SUCCESS(136,"User security Q&A changed successfully."),
	USER_CHANGE_PASSWORD_SUCCESS(137,"User password changed successfully."),
	USER_LOGIN_ERROR(143, "There was a problem during login request."),
	USER_REGISTRATION_CREATE_INTERNAL_USER(146,"You do not have permission to create internal cadence user. Please contact support.");
	
	private final int code;
	private final String message;

	private AppMessages(int code, String message) {
		this.code = code;
		this.message = message;
	}

	/**
	 * @return the Error code
	 */
	public int getCode() {
		return this.code;
	}

	/**
	 * @return the Error message
	 */
	public String getMessage() {
		return this.message;
	}
}