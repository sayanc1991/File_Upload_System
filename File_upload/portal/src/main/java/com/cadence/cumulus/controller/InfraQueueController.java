package com.cadence.cumulus.controller;

import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.cadence.cumulus.model.MachineInfo;
import com.cadence.cumulus.service.InfraQueueService;

@RestController
public class InfraQueueController {

	private static final Logger log = LoggerFactory.getLogger(InfraQueueController.class);
	
	@Autowired
	private InfraQueueService infraQueueService;
	
	@GetMapping(value = "/instances")
    public Map<String,MachineInfo> getInfraQueues(){
	 log.debug("Inside getInfraQueues");
	 Map<String,MachineInfo> info = new HashMap<String, MachineInfo>();
	 info = infraQueueService.fetchMachineInfo();
	 log.debug("Inside getInfraQueues " + info);
		return info;
	}
}
