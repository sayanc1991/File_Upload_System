package com.cadence.cumulus.tailer;

import com.cadence.cumulus.model.AgentCommand;
import com.cadence.cumulus.model.AgentResponse;
import org.apache.commons.io.input.TailerListenerAdapter;

import java.util.concurrent.BlockingQueue;

public abstract class CumulusTailerListener extends TailerListenerAdapter {
    protected BlockingQueue<AgentResponse> queue;

    protected final AgentCommand command;

    public CumulusTailerListener(BlockingQueue<AgentResponse> queue, AgentCommand command) {
        this.queue = queue;
        this.command = command;
    }
}
