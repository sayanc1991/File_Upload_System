package com.cadence.cumulus.tailer;

import com.cadence.cumulus.events.app.AppEventsPublisher;
import com.cadence.cumulus.model.AgentCommand;
import com.cadence.cumulus.model.AgentResponse;
import org.apache.commons.io.input.Tailer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.concurrent.BlockingQueue;

public class EventsTailer extends CumulusTailerListener {
    private static final Logger log = LoggerFactory.getLogger(EventsTailer.class);

    @Autowired
    private AppEventsPublisher publisher;

    public EventsTailer(BlockingQueue<AgentResponse> queue, AgentCommand command) {
        super(queue, command);
    }

    @Override
    public void handle(String line) {
        //log.debug("EventsTailer - handle event");
        log.debug("EventsTailer - handle event - {}", line);
        AgentResponse agentResponse=new AgentResponse(command.getRequestId(), command.getUserName(), "jobs-events", line);
        log.debug("EventsTailer - handle event - AgentResponse object -- adding to blocking queue - command " + agentResponse.getCommand());
        log.debug("EventsTailer - agentResponse " + agentResponse);
        //publisher.publishEvent(agentResponse);
        queue.add(agentResponse);
    }

    @Override
    public void handle(Exception ex) {
        log.debug("EventsTailer - Error", ex);
    }

    @Override
    public void init(Tailer tailer) {
        log.debug("EventsTailer - Init done...");
    }

    @Override
    public void fileNotFound() {
        log.debug("EventsTailer - File not found...");
    }

    @Override
    public void fileRotated() {
        log.debug("EventsTailer - File rotated...");
    }
}
