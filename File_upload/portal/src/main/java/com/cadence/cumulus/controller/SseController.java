package com.cadence.cumulus.controller;

import com.cadence.cumulus.events.api.APIEventsPublisher;
import com.cadence.cumulus.events.app.AppEventsListener;
import com.cadence.cumulus.handler.AgentHandler;
import com.cadence.cumulus.model.CumulusUser;
import com.cadence.cumulus.service.StatusPublisherService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.mvc.method.annotation.SseEmitter;
import rx.RxReactiveStreams;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@RestController
@RequestMapping("/events")
public class SseController {
    private static final Logger log = LoggerFactory.getLogger(SseController.class);

    @Autowired
    private AgentHandler handler;


    //Shambhu: Adding event publisher to send commands to for execution
    @Autowired
    private AppEventsListener appEventsListener;


    @GetMapping(produces = "text/event-stream")
    @ResponseStatus(HttpStatus.OK)
    public SseEmitter getEvents(HttpSession session, HttpServletResponse response) throws IOException {


        log.debug("SseController - getEvents() called.. ");

        CumulusUser user = (CumulusUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        String userName = user.getUsername().split("@")[0];

        log.debug("Adding global event handler for {}@{}", session.getId(), userName);

        String sessionPublisherKey = session.getId() + ".publisher.global.events";
        StatusPublisherService oldPublisher = (StatusPublisherService) session.getAttribute(sessionPublisherKey);
        if (oldPublisher != null) {
            log.debug("Found old publisher. Killing it.");
            oldPublisher.setAlive(false);
        }

        //TODO: Shambhu : Check - refactored this code for notification management
        //final StatusPublisherService publisher = handler.getGlobalEventsPublisher(userName, session.getId());
        final StatusPublisherService publisher = appEventsListener.getGlobalEventsPublisher(userName, session.getId());

        SseEmitter emitter = new SseEmitter();
        emitter.onCompletion(() -> {
            log.debug("onCompletion.. Global Events Emitter Completed");
            publisher.setAlive(false);
            session.removeAttribute(sessionPublisherKey);
        });

        emitter.onTimeout(() -> {
            log.debug("onTimeout... Global Events Emitter Timeout");
            publisher.setAlive(false);
            session.removeAttribute(sessionPublisherKey);
        });

        RxReactiveStreams.toObservable(publisher).subscribe(str -> {
            try {
                log.debug("Sending global event: {}", str);
                emitter.send(str);
            } catch (Exception e) {
                log.debug("Exception emitting global events");
                log.debug("Global Events Exception: ", e);
                log.debug("Killing global events publisher");
                publisher.setAlive(false);
                emitter.completeWithError(e);
            }
        }, error -> {
            log.debug("Caught an error while sending global events");
            log.debug("Error: ", error);
            emitter.completeWithError(error);
        }, emitter::complete);

        session.setAttribute(sessionPublisherKey, publisher);
        return emitter;
    }
}
