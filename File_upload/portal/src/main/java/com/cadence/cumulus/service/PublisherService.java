package com.cadence.cumulus.service;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.TimeUnit;

import org.reactivestreams.Publisher;
import org.reactivestreams.Subscriber;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.task.TaskExecutor;

import com.cadence.cumulus.model.AgentResponse;

public class PublisherService implements Publisher<String> {
    private static final Logger log = LoggerFactory.getLogger(PublisherService.class);

    private TaskExecutor executor;

    private String type;

    private String stopString;

    private BlockingQueue<AgentResponse> queue;

    private boolean alive = true;

    public PublisherService(TaskExecutor executor, String type, BlockingQueue<AgentResponse> queue) {
        this(executor, type, null, queue);
    }
    
    public PublisherService(TaskExecutor executor, String type, String stopString, BlockingQueue<AgentResponse> queue) {
        this.executor = executor;
        this.type = type;
        this.stopString = stopString;
        this.queue = queue;
    }

    @Override
    public void subscribe(Subscriber<? super String> subscriber) {
        executor.execute(() -> {
            try {
                while (true) {
                    if (queue == null) {
                        log.debug("No queue available, breaking out");
                        subscriber.onComplete();
                        break;
                    }

                    AgentResponse agentResponse = queue.poll(50000, TimeUnit.MILLISECONDS);

                    if (!alive) {
                        log.debug("{} Not alive anymore. Breaking", type);
                        subscriber.onComplete();
                        break;
                    }

                    if (agentResponse == null) {
                        log.debug("No {} for 50 seconds, sending ping", type);
                        subscriber.onNext("ping");
                    } else {
                        log.debug("Got response from {} queue: {}", type, agentResponse.toString());
                        try {
                            subscriber.onNext(agentResponse.getData());
                        } catch (Exception e) {
                            log.debug("PublisherService caught exception");
                            subscriber.onError(e);
                        }

                        if (stopString != null) {
                            if (agentResponse.getData().contains(stopString)) {
                                log.debug("{} Completed, breaking out", type);
                                subscriber.onComplete();
                                break;
                            }
                        }
                    }
                }
            } catch (InterruptedException e) {
                log.error(type + " Interrupted: ", e);
            }
        });
    }

    public boolean isAlive() {
        return alive;
    }

    public void setAlive(boolean alive) {
        this.alive = alive;
        this.queue = null;
    }
}
