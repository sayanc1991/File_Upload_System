package com.cadence.cumulus.service;

import javax.servlet.http.HttpServletResponse;

import org.json.JSONObject;
import org.springframework.stereotype.Service;

@Service
public interface FileManagerService {

    public JSONObject list(String path);

    public JSONObject rename(JSONObject params);

    public JSONObject copy(JSONObject params);

    public JSONObject move(JSONObject params);

    public JSONObject delete(JSONObject params);

    public JSONObject editFile(JSONObject params);

    public JSONObject addFolder(JSONObject params);

    public void downloadFiles(JSONObject params, HttpServletResponse response);

    public void downloadMultipleFiles(JSONObject params, HttpServletResponse response);

    public JSONObject compress(JSONObject params);
}
