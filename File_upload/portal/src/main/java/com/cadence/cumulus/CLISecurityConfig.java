package com.cadence.cumulus;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ContentNegotiationConfigurer;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

import com.cadence.cumulus.controller.api.handler.TokenRequestFilter;

@Configuration
public class CLISecurityConfig extends WebMvcConfigurerAdapter {
	
	@Autowired
    private TokenRequestFilter apiHandler;

	@Override
	public void addInterceptors(InterceptorRegistry registry) {
		registry.addInterceptor(apiHandler).addPathPatterns("/api/fm/**").addPathPatterns("/api/token/**").addPathPatterns("/api/download/**").addPathPatterns("/api/listfiles/**");
	}
	
	@Override
	public void configureContentNegotiation(ContentNegotiationConfigurer configurer) {
		// TODO Auto-generated method stub
		// disabled auto response type determination by spring
		// below settings forces spring to use content type
		// as specified in request header Accept
		configurer.favorParameter(false);
		configurer.favorPathExtension(false);
	}
	
}
