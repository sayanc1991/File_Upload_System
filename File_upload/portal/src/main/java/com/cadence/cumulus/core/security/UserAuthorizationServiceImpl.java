package com.cadence.cumulus.core.security;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.cadence.cumulus.model.CumulusGrantedAuthority;
import com.cadence.cumulus.model.RolePermission;
import com.cadence.cumulus.model.User;
import com.cadence.cumulus.model.UserRole;
import com.cadence.cumulus.service.CumulusUserService;

/**
 * This class is responsible to get users Granted Authorities.
 * 
 * @author shirish
 *
 */
@Service
@Transactional(propagation = Propagation.REQUIRED, rollbackFor = { Exception.class })
public class UserAuthorizationServiceImpl {

	/*
	 * Logger
	 */
	private static final Logger log = LoggerFactory.getLogger(UserAuthorizationServiceImpl.class);

	@Autowired
	private CumulusUserService userService;

	/**
	 * Gets the User permissions and Permission exceptions and returns the final
	 * Granted Authorities
	 * 
	 * @param userId
	 * @return
	 */
	@Transactional
	public List<GrantedAuthority> getUserAuthorityList(final String userId) {

		log.debug("Logged in user is : {}", userId);
		User user = userService.getUserByEmail(userId);

		return getUserAuthorityList(user);

	}

	@Transactional
	public List<GrantedAuthority> getUserAuthorityList(final User user) {
		long startTime = System.currentTimeMillis();
		List<GrantedAuthority> auths = new ArrayList<>();

		if (user == null) {
			return auths;
		}

		log.debug("Logged in user ID : {}", user.getEmail());
		Set<UserRole> userRoleSet = user.getUserRoleSet();

		for (UserRole r : userRoleSet) {
			for (RolePermission rp : r.getUserRoleId().getRole().getRolePermissionSet()) {
				//auths.add(new SimpleGrantedAuthority(rp.getPermission().getName()));
				// CMLS-658 to get both permission id and permission name for further check in CLI REST APIs regarding permission
				auths.add(new CumulusGrantedAuthority(rp.getPermission().getId(), rp.getPermission().getName()));
			}
		}

		log.debug("Elapsed Time: {}", System.currentTimeMillis() - startTime);
		log.debug("Logged in user Permissions are: {}", auths);

		return auths;

	}

	/**
	 * Checks if the user has the permission in the granted authorities list.
	 * 
	 * @param permission
	 * @param authList
	 * @return
	 */
	public boolean hasAuthority(String permission, List<GrantedAuthority> authList) {
		for (GrantedAuthority grantedAuthority : authList) {
			if (permission.equals(grantedAuthority.getAuthority())) {
				return true;
			}
		}
		return false;
	}

	public boolean hasAuthority(String permission, Collection<? extends GrantedAuthority> authList) {
		for (GrantedAuthority grantedAuthority : authList) {
			if (permission.equals(grantedAuthority.getAuthority())) {
				return true;
			}
		}
		return false;
	}

}
