package com.cadence.cumulus.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@JsonInclude(content = Include.NON_NULL)
public class DestinationValidation {

	private String folderName;
	private Boolean validationResult;
	private String validationError;
	public String getFolderName() {
		return folderName;
	}
	public void setFolderName(String folderName) {
		this.folderName = folderName;
	}
	public Boolean getValidationResult() {
		return validationResult;
	}
	public void setValidationResult(Boolean validationResult) {
		this.validationResult = validationResult;
	}
	public String getValidationError() {
		return validationError;
	}
	public void setValidationError(String validationError) {
		this.validationError = validationError;
	}
	@Override
	public String toString() {
		return "{folderName=" + folderName + ", validationResult=" + validationResult + ", validationError="
				+ validationError + "}";
	}
	
	
}
