package com.cadence.cumulus.service.impl;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.cadence.cumulus.common.CumulusException;
import com.cadence.cumulus.model.Countries;
import com.cadence.cumulus.repository.mysql.CountriesRepository;
import com.cadence.cumulus.service.CountriesService;

@Component
@Transactional(propagation = Propagation.REQUIRED, rollbackFor = { Exception.class })
public class CountriesServiceImpl implements CountriesService {

    /**
     * Logger
     */
    private static final Logger log = LoggerFactory.getLogger(CountriesServiceImpl.class);

    @Autowired
    private CountriesRepository countriesRepository;

    @Override
    public List<Countries> getAllCountries() throws CumulusException {
        log.debug("Retrieving all Countries.");
        return countriesRepository.findAll();
    }

	@Override
	public Countries getByCountryCode(String countryCode) throws CumulusException {
		log.debug("Retrieving country for country code: "+countryCode);
        return countriesRepository.findByCountryCode(countryCode);
	}
}