package com.cadence.cumulus.handler;

import static com.twilio.Twilio.init;
import static com.twilio.Twilio.setRestClient;
import static com.twilio.rest.api.v2010.account.Message.creator;

import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import com.cadence.cumulus.CumulusTwilioHttpClient;
import com.cadence.cumulus.ProxyConfig;
import com.cadence.cumulus.model.CumulusUserNotificationSettings;
import com.cadence.cumulus.model.JobRun;
import com.cadence.cumulus.model.User;
import com.cadence.cumulus.model.liberate.events.LiberateStatus;
import com.cadence.cumulus.service.CumulusUserService;
import com.cadence.cumulus.service.EmailService;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.twilio.http.TwilioRestClient;
import com.twilio.type.PhoneNumber;

@Component
@Transactional(propagation = Propagation.REQUIRED, rollbackFor = { Exception.class })
public class NotificationEventHandler implements InitializingBean {
    private static final Logger log = LoggerFactory.getLogger(NotificationEventHandler.class);

    @Value("${cumulus.url}")
    private String CUMULUS_URL;

    @Value("${cumulus.job.started.subject}")
    private String JOB_STARTED_SUBJECT;

    @Value("${cumulus.job.completed.subject}")
    private String JOB_COMPLETED_SUBJECT;

    @Value("${cumulus.job.step.failed.subject}")
    private String JOB_STEP_FAILED_SUBJECT;

    @Value("${cumulus.job.step.cancelled.subject}")
    private String JOB_CANCELLED_SUBJECT;
    
    @Value("${twilio.account.sid}")
    private String twilioAccountSid;

    @Value("${twilio.auth.token}")
    private String twilioAuthToken;

    @Value("${twilio.from.phone}")
    private String twilioFromPhone;

    @Autowired
    private ProxyConfig proxyConfig;

    @Autowired
    private CumulusUserService userService;

    @Autowired
    private EmailService emailService;

    private final String JOB_STARTED_TEMPLATE = "mail/jobstarted.html";
    private final String JOB_COMPLETED_TEMPLATE = "mail/jobcompleted.html";
    private final String JOB_STEP_FAILED_TEMPLATE = "mail/jobstepfailed.html";
    private final String JOB_CANCELLED_TEMPLATE = "mail/jobcancelled.html";

    @EventListener
    public void handleLiberateEvent(JobRun event) {
        log.debug("Handling liberate event for notification");

        User user = userService.getUserById(event.getCreatedBy());
        String email = user.getEmail();
        CumulusUserNotificationSettings settings = userService.getNotificationSettings(user);

        try {
            log.debug("User settings: {}", new ObjectMapper().writeValueAsString(settings));
        } catch (Exception e) {
        }

        LiberateStatus eventType = LiberateStatus.valueOf(event.getStatus());
        if (settings.canNotifyEmail(eventType)) {
            log.debug("Notifying {}, via email about {} event", email, event.getStatus());
            Map<String, String> params = null;
            String template = null;
            String subject = null;

            switch (eventType) {
            case started:
                params = new HashMap<>();
                params.put("firstName", user.getFirstName());
                params.put("jobName", event.getJobName());
                params.put("url", CUMULUS_URL);

                template = JOB_STARTED_TEMPLATE;
                subject = JOB_STARTED_SUBJECT;
                break;
            case completed:
                params = new HashMap<>();
                params.put("firstName", user.getFirstName());
                params.put("jobName", event.getJobName());
                params.put("url", CUMULUS_URL);

                template = JOB_COMPLETED_TEMPLATE;
                subject = JOB_COMPLETED_SUBJECT;
                break;
            case failed:
            	log.debug("Sending email with failed message");
                params = new HashMap<>();
                params.put("firstName", user.getFirstName());
                params.put("jobName", event.getJobName());
                params.put("url", CUMULUS_URL);

                template = JOB_STEP_FAILED_TEMPLATE;
                subject = JOB_STEP_FAILED_SUBJECT;
                break;
            case cancelled:
            	params = new HashMap<>();
                params.put("firstName", user.getFirstName());
                params.put("jobName", event.getJobName());
                params.put("url", CUMULUS_URL);

                template = JOB_CANCELLED_TEMPLATE;
                subject = JOB_CANCELLED_SUBJECT;
                break;
            }

            if (!CollectionUtils.isEmpty(params)) {
                // emailService.sendEmail(template, params, settings.getEmail(), subject);
            	emailService.sendEmail(null, params, settings.getEmail(), subject+event.getJobName());
            }
        }
        if (settings.canNotifySms(eventType)) {
            log.debug("Notifying {} on {}, via sms about {} event", settings.getFirstName(), settings.getPhone(), event.getStatus());
            String message = null;
            switch (eventType) {
            case started:
                message = event.getJobName() + " has started.";
                break;
            case completed:
                message = event.getJobName() + " has completed.";
                break;
            case failed:
                message = event.getJobName() + " has failed.";
                break;
            case cancelled:
            	message = event.getJobName() + " has been cancelled.";
                break;
            }
			if (!StringUtils.isEmpty(message)) {
				try {
					creator(new PhoneNumber(settings.getPhone()), new PhoneNumber(twilioFromPhone), message).create();
				} catch (Exception ex) {
					log.error("Error while sending SMS: {}", ex);
				}
			}
        }
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        init(twilioAccountSid, twilioAuthToken);

        if (proxyConfig.isEnabled()) {
            setRestClient(new TwilioRestClient.Builder(twilioAccountSid, twilioAuthToken).httpClient(new CumulusTwilioHttpClient(proxyConfig.getHost(), proxyConfig.getPort())).build());
        }
    }
}
