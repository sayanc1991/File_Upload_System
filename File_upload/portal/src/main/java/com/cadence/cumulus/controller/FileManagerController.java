/**
 *
 */
package com.cadence.cumulus.controller;

import com.cadence.cumulus.common.CumulusException;
import com.cadence.cumulus.common.ServiceException;
import com.cadence.cumulus.events.api.APIEventsPublisher;
import com.cadence.cumulus.model.*;
import com.cadence.cumulus.service.PermissionService;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.ServletContextAware;
import org.springframework.web.context.request.async.DeferredResult;
import org.springframework.web.context.request.async.DeferredResult.DeferredResultHandler;
import org.springframework.web.multipart.MultipartFile;

import javax.activation.MimetypesFileTypeMap;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;
import java.io.*;
import java.lang.ProcessBuilder.Redirect;
import java.math.BigInteger;
import java.security.Principal;
import java.util.*;

import static java.lang.Math.toIntExact;
import static org.springframework.http.MediaType.TEXT_PLAIN_VALUE;

/**
 * @author ramsudheer
 */
@RestController
@RequestMapping(value = "/fm")
public class FileManagerController implements ServletContextAware {
    /**
     * Logger
     */
    private static final Logger log = LoggerFactory.getLogger(FileManagerController.class);

    private static String TMP_DIRECTORY = "/tmp/";// "c:/tmp/"; // "/tmp/";

    private static String UPLOAD_FILE_EXT = ".uploading";

    @Value("${user.home.base}")
    private String USER_HOME_BASE;


    //Shambhu: Adding event publisher to send commands to for execution
    @Autowired
    private APIEventsPublisher publisher;

    @Autowired
    ServletContext context;

    @Autowired
    private PermissionService permissionService;

    private ServletContext servletContext;

    // private final static String appPath = "C:/Uploads/uploadFiles";
    // private final static String appPath = "/opt/cmls/uploads";

    @Value("${user.file.upload.folder}")
    private String fileUploadFolder;

    // private final static String workPath = appPath + "/work";

    private Map<String, String> fileUploadStatus = new HashMap<>();

    /**
     * End point to fetch all files & folders
     *
     * @param response
     * @return
     * @throws CumulusException
     * @throws ServletException
     * @throws IOException
     */
    @PostMapping(value = "/listUrl")
    public @ResponseBody
    Object listFiles(Principal principal, @RequestBody FileOperationRequest request,
                     HttpServletResponse httpResponse) throws CumulusException, ServletException, IOException {
        log.debug("Retrieving all files & folders");


        String userName = principal.getName().split("@")[0];

        boolean agentCommand = true;

        CumulusUser user = (CumulusUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        request.setRequestId(user.getLoginName());

        AgentCommand command = new AgentCommand();
        command.setCommand("file-man");
        command.setFileOperationRequest(request);
        command.setRequestId(UUID.randomUUID().toString());

        DeferredResult<String> result = new DeferredResult<>();
        DeferredResult<AgentResponse> response = new DeferredResult<>();
        switch (request.getAction()) {
            case "list":
                response.setResultHandler(new DeferredResultHandler() {
                    @Override
                    public void handleResult(Object res) {
                        AgentResponse agentResponse = (AgentResponse) res;
                        String listResult = agentResponse.getData();
                        result.setResult(listResult);
                        log.debug("FileManagerController - Result set in the Result Handler");
                    }
                });
                break;
            case "getContent":
                agentCommand = false;
                log.debug("Getting file content...");

                InputStream input = null;
                BufferedOutputStream output = null;
                try {
                    String fileName = USER_HOME_BASE + userName + File.separator + request.getItem();
                    if (exists(userName, fileName)) {
                        List<String> commands = new ArrayList<>();
                        commands.add("sudo");
                        commands.add("-u");
                        commands.add(userName);
                        commands.add("cat \"" + fileName + "\"");

					/*
                     * Create the executable script to do the job
					 */

                        String tmpScriptName = TMP_DIRECTORY + UUID.randomUUID().toString() + ".sh";
                        File f = new File(tmpScriptName);
                        FileOutputStream fout = new FileOutputStream(f);
                        fout.write(("#!/bin/bash\nset -e\n" + commands.get(commands.size() - 1)).getBytes());
                        fout.close();
                        f.setExecutable(true, false);
                        commands.set(commands.size() - 1, tmpScriptName);

                        ProcessBuilder builder = new ProcessBuilder(commands).redirectErrorStream(true).inheritIO()
                                .redirectOutput(Redirect.PIPE);
                        Process p = builder.start();
                        input = p.getInputStream();
                        httpResponse.setHeader("Content-Type", getMimeType(userName, fileName));
                        httpResponse.setHeader("Content-Length", getLength(userName, fileName));
                        httpResponse.setHeader("Content-Disposition", "inline; filename=\"" + request.getItem() + "\"");

                        httpResponse.addHeader("Access-Control-Allow-Origin", "*");
                        httpResponse.addHeader("Access-Control-Allow-Methods", "GET,POST");
                        httpResponse.addHeader("Access-Control-Allow-Headers",
                                "Origin, X-Requested-With, Content-Type, Accept");

                        output = new BufferedOutputStream(httpResponse.getOutputStream());
                        byte[] buffer = new byte[8192];
                        for (int length = 0; (length = input.read(buffer)) > 0; ) {
                            output.write(buffer, 0, length);
                        }
                        p.waitFor();
                        // Delete the script
                        f.delete();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    log.debug("Error: {}", e.getMessage());
                    httpResponse.sendError(HttpStatus.INTERNAL_SERVER_ERROR.value(), "Unable to read file...");
                } finally {
                    if (output != null) {
                        try {
                            output.close();
                        } catch (IOException logOrIgnore) {
                            log.debug("Error: {}", logOrIgnore);
                        }
                    }
                    if (input != null) {
                        try {
                            input.close();
                        } catch (IOException logOrIgnore) {
                            log.debug("Error: {}", logOrIgnore);
                        }
                    }
                }
                break;
            case "savefile":
                response.setResultHandler(new DeferredResultHandler() {
                    @Override
                    public void handleResult(Object res) {
                        AgentResponse agentResponse = (AgentResponse) res;
                        result.setResult(agentResponse.getData());
                    }
                });
                break;
        }

        if (agentCommand) {
            //handler.sendCommand(command, response);
            log.debug("Publishing event...");
            publisher.pubhishEvent(command, response);
            log.debug("Returning result..");
            return result;
        }

        return result;
    }

    /**
     * End point to rename the file
     *
     * @param params
     * @return
     * @throws IOException
     * @throws CumulusException
     * @throws ServletException
     */
    @PostMapping(value = "/renameUrl")
    public @ResponseBody
    DeferredResult<String> rename(@RequestBody FileOperationRequest request,
                                  HttpServletResponse response) throws IOException {
        log.debug("Renaming the file");


        CumulusUser user = (CumulusUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        request.setRequestId(user.getLoginName());

        AgentCommand command = new AgentCommand();
        command.setCommand("file-man");
        command.setFileOperationRequest(request);
        command.setRequestId(UUID.randomUUID().toString());

        return manageResult(command);
    }

    /**
     * End point to copy the files
     *
     * @param params
     * @return
     * @throws CumulusException
     * @throws IOException
     * @throws ServiceException
     */
    @PostMapping(value = "/copyUrl")
    public @ResponseBody
    DeferredResult<String> copy(@RequestBody FileOperationRequest request,
                                HttpServletResponse response) throws IOException {
        log.debug("Copying the files");


        CumulusUser user = (CumulusUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        request.setRequestId(user.getLoginName());

        AgentCommand command = new AgentCommand();
        command.setCommand("file-man");
        command.setFileOperationRequest(request);
        command.setRequestId(UUID.randomUUID().toString());

        return manageResult(command);
    }

    /**
     * End point to move the files
     *
     * @param params
     * @return
     * @throws CumulusException
     * @throws IOException
     * @throws ServiceException
     */
    @PostMapping(value = "/moveUrl")
    public @ResponseBody
    DeferredResult<String> move(@RequestBody FileOperationRequest request,
                                HttpServletResponse response) throws IOException {
        log.debug("Moving the files");


        CumulusUser user = (CumulusUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        request.setRequestId(user.getLoginName());

        AgentCommand command = new AgentCommand();
        command.setCommand("file-man");
        command.setFileOperationRequest(request);
        command.setRequestId(UUID.randomUUID().toString());

        return manageResult(command);
    }

    /**
     * End point to delete the files
     *
     * @param params
     * @return
     * @throws IOException
     * @throws CumulusException
     * @throws ServiceException
     */
    @PostMapping(value = "/removeUrl")
    public @ResponseBody
    DeferredResult<String> delete(@RequestBody FileOperationRequest request,
                                  HttpServletResponse response) throws IOException {
        log.debug("Deleting the files");


        CumulusUser user = (CumulusUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        request.setRequestId(user.getLoginName());

        AgentCommand command = new AgentCommand();
        command.setCommand("file-man");
        command.setFileOperationRequest(request);
        command.setRequestId(UUID.randomUUID().toString());

        return manageResult(command);
    }

    /**
     * End point to compress the files
     *
     * @param params
     * @return
     * @throws IOException
     * @throws CumulusException
     * @throws ServletException
     */
    @PostMapping(value = "/compressUrl")
    public @ResponseBody
    DeferredResult<String> compress(@RequestBody FileOperationRequest request,
                                    HttpServletResponse response) throws IOException {
        log.debug("Compressing the files");


        CumulusUser user = (CumulusUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        request.setRequestId(user.getLoginName());

        AgentCommand command = new AgentCommand();
        command.setCommand("file-man");
        command.setFileOperationRequest(request);
        command.setRequestId(UUID.randomUUID().toString());

        return manageResult(command);
    }

    @PostMapping("createFolderUrl")
    public @ResponseBody
    DeferredResult<String> createFolder(@RequestBody FileOperationRequest request,
                                        HttpServletResponse response) throws IOException {

        CumulusUser user = (CumulusUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        request.setRequestId(user.getLoginName());

        AgentCommand command = new AgentCommand();
        command.setRequestId(UUID.randomUUID().toString());
        command.setCommand("file-man");
        command.setFileOperationRequest(request);

        return manageResult(command);
    }

    @GetMapping("downloadFileUrl")
    public void downloadFile(Principal principal, @RequestParam String action, @RequestParam String path,
                             HttpServletResponse response) {
        InputStream input = null;
        BufferedOutputStream output = null;

        try {
            String userName = principal.getName().split("@")[0];
            String strFile = USER_HOME_BASE + userName + File.separator + path;

            log.debug("Request to {} {} for {}", action, strFile, userName);

            File file = new File(strFile);

            if (!exists(userName, strFile)) {
                log.debug("File {} doesn't exist.", strFile);

                response.setContentType(TEXT_PLAIN_VALUE);
                response.setStatus(HttpStatus.NOT_FOUND.value());

                return;
            }
            if (!canRead(userName, strFile)) {
                log.debug("Cannot read {}.", strFile);

                response.setContentType(TEXT_PLAIN_VALUE);
                response.setStatus(HttpStatus.FORBIDDEN.value());

                return;
            }

            MimetypesFileTypeMap mimeTypesMap = new MimetypesFileTypeMap();
            // This code tries figuring out the mime type from the file name.
            // Some files were opening up in the browser rather than showing a download dialog.
            // This was because scripts without shebang would be treated as normal text files
            // if we try reading the mime type from file contents(which is the right way to do it)
            // So rather than using the getMimeType() method we are using the following:
            String mimeTypeFromName = mimeTypesMap.getContentType(path);
            if (TEXT_PLAIN_VALUE.equals(mimeTypeFromName)) {
                mimeTypeFromName = MediaType.APPLICATION_OCTET_STREAM_VALUE;
            }
            response.setHeader("Content-Type", mimeTypeFromName);
            // response.setHeader("Content-Type", getMimeType(userName, strFile));
            response.setHeader("Content-Length", getLength(userName, strFile));
            response.setHeader("Content-Disposition", "inline; filename=\"" + file.getName() + "\"");
            response.addHeader("Access-Control-Allow-Origin", "*");
            response.addHeader("Access-Control-Allow-Methods", "GET,POST");
            response.addHeader("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");

            log.debug("Starting download...");

            List<String> commands = new ArrayList<String>();
            commands.add("sudo");
            commands.add("-u");
            commands.add(userName);
            commands.add("cat \"" + strFile + "\"");

			/*
             * Create the executable script to do the job
			 */

            String tmpScriptName = TMP_DIRECTORY + UUID.randomUUID().toString() + ".sh";
            File f = new File(tmpScriptName);
            FileOutputStream fout = new FileOutputStream(f);
            fout.write(("#!/bin/bash\nset -e\n" + commands.get(commands.size() - 1)).getBytes());
            fout.close();
            f.setExecutable(true, false);
            commands.set(commands.size() - 1, tmpScriptName);

            ProcessBuilder builder = new ProcessBuilder(commands).redirectErrorStream(true).inheritIO().redirectOutput(Redirect.PIPE);
            Process process = builder.start();
            input = process.getInputStream();
            output = new BufferedOutputStream(response.getOutputStream());

            byte[] buffer = new byte[8192];
            for (int length = 0; (length = input.read(buffer)) > 0; ) {
                output.write(buffer, 0, length);
            }
            output.flush();
            process.waitFor();
            f.delete();

            log.debug("Download of {} complete", strFile);
        } catch (Throwable t) {
            log.error("Error in sending download: ", t);
        } finally {
            if (output != null) {
                try {
                    output.close();
                } catch (IOException logOrIgnore) {
                }
            }
            if (input != null) {
                try {
                    input.close();
                } catch (IOException logOrIgnore) {
                }
            }
        }
    }

    @GetMapping("downloadMultipleUrl")
    public void downloadMultiple(Principal principal, @RequestParam String action,
                                 @RequestParam(value = "items[]") String[] items, @RequestParam String toFilename,
                                 HttpServletResponse response) {
        String userName = principal.getName().split("@")[0];

        String homeFolder = USER_HOME_BASE + userName;
        log.debug("downloadMultipleUri");
        try {
            List<String> commands = new ArrayList<String>();
            commands.add("sudo");
            commands.add("-u");
            commands.add(userName);
            String fileList = "";
            for (String item : items) {
                fileList = fileList + "\"" + homeFolder + File.separator + item + "\" ";
            }
            // Doing this because multiple separators are appearing which is not desirable and results in buggy zips
            fileList = fileList.replace(File.separator + File.separator, File.separator);
          
            commands.add("zip -r \"" +homeFolder + File.separator + toFilename  + "\" " + fileList.trim());

			
			/*
			 * Create the executable script to do the job
			 */

            String tmpScriptName = TMP_DIRECTORY + UUID.randomUUID().toString() + ".sh";
            File f = new File(tmpScriptName);
            FileOutputStream fout = new FileOutputStream(f);
            fout.write(("#!/bin/bash\nset -e\n" + commands.get(commands.size() - 1)).getBytes());
            fout.close();
            f.setExecutable(true, false);
            commands.set(commands.size() - 1, tmpScriptName);
            ProcessBuilder builder = new ProcessBuilder(commands).redirectErrorStream(true).inheritIO().redirectOutput(Redirect.PIPE);
            Process process = builder.start();
            process.waitFor();
            downloadFile(principal, "downloadMultipleUrl", toFilename, response);

            // Delete the zip file we just created

            commands.clear();
            commands.add("sudo");
            commands.add("-u");
            commands.add(userName);
            commands.add("/bin/bash");
            commands.add("-c");
            commands.add("rm -rf " + homeFolder + File.separator + toFilename);

            builder = new ProcessBuilder(commands).redirectErrorStream(true).redirectOutput(Redirect.PIPE).inheritIO();
            process = builder.start();
            process.waitFor();
            f.delete();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    @GetMapping(value = "uploadUrl")
    public int uploadFile(Principal principal, @RequestParam("file") String files,
                          HttpServletResponse response) {
        int size;
        String userName = principal.getName().split("@")[0];
        String userHome = USER_HOME_BASE + userName;
        files = removeMultipleSpaces(files);
        String finalDestination = userHome + "/" + files;
        //String finalDestination =  "/tmp/my_temp/" +files;
        File temp = new File(finalDestination);
        log.debug("Temp File Exists" + temp.exists());
        if (temp.exists()) {
            log.debug("inside if" + temp.length());
            log.debug("inside if" + toIntExact(temp.length()));
            size = toIntExact(temp.length());
        } else {
            log.debug("inseide else");
            size = 0;
        }


        log.debug("*******************INSIDE GET" + files + "destination is " + finalDestination);
        return size;
    }

    @PostMapping(value = "uploadUrl", consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    public Result uploadFile(@RequestParam("file") MultipartFile[] files, @RequestParam String destination,
                             Principal principal, HttpServletResponse response) throws IOException {
        String userName = principal.getName().split("@")[0];
        String userHome = USER_HOME_BASE + userName;
        String finalDestination = userHome + destination;
        //	String finalDestination =  "/tmp/my_temp";
        log.debug("Request to upload {} files to {}", files.length, finalDestination);

        Arrays.stream(files).forEach(file -> {
            log.debug("Uploading file : {}", file.getOriginalFilename());
            log.debug("Uploading file : {}", file.getName());
            InputStream is = null;
            OutputStream bufferStream = null;
            String bufferFile = null;
            try {
                log.debug("1");
                String targetFileName = finalDestination + File.separator + file.getOriginalFilename();
                targetFileName = removeMultipleSpaces(targetFileName);
                String tmpFilename = UUID.randomUUID().toString();
                bufferFile = TMP_DIRECTORY + tmpFilename;
                log.debug("12");
                is = file.getInputStream();

                byte[] buffer = new byte[1000000];
                log.debug("13");
                while (true) {
                    int bytesRead = is.read(buffer);
                    if (bytesRead <= 0) {
                        log.debug("14dd");
                        break;
                    } else {
                        log.debug("14");
                        bufferStream = new FileOutputStream(bufferFile);
                        bufferStream.write(buffer, 0, bytesRead);
                        bufferStream.close();
                        appendFile(userName, bufferFile, targetFileName);
                    }
                }
            } catch (Exception ex) {
                log.debug("Unable to upload {}", file.getOriginalFilename());
                log.error("Error:", ex);
                try {
                    response.sendError(HttpStatus.INTERNAL_SERVER_ERROR.value(), "write error");
                } catch (IOException e) {
                    log.error("Error sending response:", e);
                }
            } finally {
                try {
                    is.close();
                } catch (IOException e) {
                    log.error("Could not close the Multipart file's input stream. Reason: {}", e.getMessage());
                }
                try {
                    bufferStream.close();
                } catch (IOException e) {
                    log.error("Could not close the tmp file's output stream. Reason: {}", e.getMessage());
                }
                try {
                    new File(bufferFile).delete();
                } catch (Exception e) {
                    log.error("Could not delete tmp file used for uploading file. Reason: {}", e.getMessage());
                }
            }
        });

        log.debug("All files uploaded");

        Result r = new Result();
        r.setSuccess(true);
        r.setError(null);

        return r;
    }

    @GetMapping("processUploadFile")
    public void processUploadFile(HttpServletRequest request, Principal principal,
                                  HttpServletResponse response) throws Exception {
        String userName = principal.getName().split("@")[0];
        String fileName = request.getParameter("filename");
        fileName = removeMultipleSpaces(fileName);
        String outputDir = request.getParameter("destination");
        outputDir = removeMultipleSpaces(outputDir);
        String action = request.getParameter("action");
        String sourcePath = outputDir + "/" + fileName + UPLOAD_FILE_EXT;
        String targetPath = outputDir + "/" + fileName;

        if (action.equalsIgnoreCase("rename")) {
            mvByUser(userName, sourcePath, targetPath);
        }
        if (action.equalsIgnoreCase("delete")) {
            rmdirByUser(userName, sourcePath);
        }
        log.debug("SourcePath=" + sourcePath);
        //Double Check If file exists or not and retry 3 times
        for (int i = 0; i < 3; i++) {
            if (exists(userName, sourcePath)) {
                log.debug("Inside Exist of File" + sourcePath + "Retry=" + i);
                rmdirByUser(userName, sourcePath);
            } else {
                log.debug("In Break of Retry=" + i + "for File" + sourcePath);
                break;
            }
        }
    }

    @GetMapping("fileInfo")
    public @ResponseBody
    DeferredResult<String> fileInfo(HttpServletRequest request, Principal principal,
                                    HttpServletResponse response) throws Exception {
        String userName = principal.getName().split("@")[0];
        // gets absolute path of the web application
        for (Enumeration<String> e = request.getParameterNames(); e.hasMoreElements(); ) {
            String parameter = (String) e.nextElement();
            if (!"data".equals(parameter)) {
                log.debug("name=" + parameter + ";value=" + request.getParameter(parameter));
            }
        }

        log.debug("\n Destinatiopn is " + request.getParameter("destination"));
        String fileName = request.getParameter("filename");
        fileName = removeMultipleSpaces(fileName);
        String destination = request.getParameter("destination");
        destination = removeMultipleSpaces(destination);
        File outputFile = null;
        outputFile = new File(destination + "/" + fileName);
        BigInteger size;
        size = new BigInteger(uploadExists(userName, outputFile.getAbsolutePath()));

        File uploadFile = null;
        uploadFile = new File(destination + "/" + fileName + UPLOAD_FILE_EXT);
        BigInteger uploadSize;
        uploadSize = new BigInteger(uploadExists(userName, uploadFile.getAbsolutePath()));

        //check Moidified Time For Already Exists
        boolean clientModifiedDateCheck = true;
        boolean fileExists = false;
        Long lastModifiedDate = null;
        Boolean outputLastModifiedSame = false;
        try {
            lastModifiedDate = Long.parseLong(request.getParameter("lastModified"));
        } catch (NumberFormatException e) {
            clientModifiedDateCheck = false;
        }
        if (clientModifiedDateCheck && exists(userName, outputFile.getAbsolutePath())) {
            String lastModified = whenLastModified(userName, outputFile.getAbsolutePath());
            log.debug("Last modified time for file " + outputFile.getAbsolutePath() + " is " + lastModified);
            long lastMotifiedInSec = 0;
            try {
                lastMotifiedInSec = Long.parseLong(lastModified);
            } catch (NumberFormatException e) {
                log.debug("Failed to parse last modified time for - " + lastModified);
            }
            log.debug("Last modified time for file " + outputFile.getAbsolutePath() + " after parsing " + lastMotifiedInSec + "lastModifiedDate" + lastModifiedDate);
            if (!(lastMotifiedInSec * 1000 < lastModifiedDate)) {
                outputLastModifiedSame = true;
            }

        }
        log.debug("UploadFileSize" + uploadSize + "OutputFileSize" + size);
        JSONObject fileJson = new JSONObject();
        fileJson.put("outputFile", outputFile.getName());
        fileJson.put("outputFileSize", size);
        fileJson.put("uploadFile", uploadFile.getName());
        fileJson.put("uploadFileSize", uploadSize);
        fileJson.put("outputLastModifiedSame", outputLastModifiedSame);
        response.setContentType("text/plain");
        response.setCharacterEncoding("UTF-8");
        response.getWriter().write(fileJson.toString());
        return null;
    }

    @GetMapping("validateDestination")
    public @ResponseBody
    DeferredResult<String> validateDestination(HttpServletRequest request, Principal principal,
                                               HttpServletResponse response) throws Exception {

        String userName = principal.getName().split("@")[0];
        for (Enumeration<String> e = request.getParameterNames(); e.hasMoreElements(); ) {
            String parameter = (String) e.nextElement();
            if (!"data".equals(parameter)) {
                log.debug("Validate Destination folder: name=" + parameter + ";value=" + request.getParameter(parameter));
            }
        }
        final String destination = removeMultipleSpaces(request.getParameter("destination"));
        validateDestinationFolder(request, response, destination, userName);
        return null;
    }


    @GetMapping("saveFile")
    public @ResponseBody
    DeferredResult<String> saveFile1(HttpServletRequest request, Principal principal,
                                     HttpServletResponse response) throws Exception {

        log.debug("In save get");
        return null;
    }

    @PostMapping("saveFile")
    public @ResponseBody
    DeferredResult<String> saveFile(HttpServletRequest request, Principal principal,
                                    HttpServletResponse response) throws Exception {
        // gets absolute path of the web application
        String userName = principal.getName().split("@")[0];
        log.debug("\n user is" + userName);
        for (Enumeration<String> e = request.getParameterNames(); e.hasMoreElements(); ) {
            String parameter = (String) e.nextElement();
            if (!"data".equals(parameter)) {
                log.debug("Save File: name=" + parameter + ";value=" + request.getParameter(parameter));
            }
        }
        if ("saveFile".equals(request.getParameter("call"))) {
            String filename = request.getParameter("filename");
            filename = removeMultipleSpaces(filename);
            final String destination = removeMultipleSpaces(request.getParameter("destination"));
            int size = Integer.parseInt(request.getParameter("size"));
            BigInteger totalSize = new BigInteger(request.getParameter("totalSize"));
            BigInteger lastSize = new BigInteger(request.getParameter("lastSize"));
            String destinationPath = request.getParameter("destination");
            destinationPath = (destinationPath == null || destinationPath.trim().length() == 0 ?
                    USER_HOME_BASE + userName : destinationPath);
            log.debug("Save File Destination" + destinationPath);
            File uploadFile = null;
            uploadFile = new File(destination + "/" + filename + UPLOAD_FILE_EXT);
            BigInteger uploadSize;
            uploadSize = new BigInteger(uploadExists(userName, uploadFile.getAbsolutePath()));
            BigInteger zeroSize = new BigInteger(String.valueOf(0));
            log.debug("IN SAVE FILE last size=" + lastSize + "uploadSize" + uploadSize + "zerosize" + zeroSize);
            if (uploadSize.equals(zeroSize) && !(lastSize.equals(zeroSize))) {
                log.debug("Do nothing as File is cancelled");
            } else {
                log.debug("Inside else in savefile");
                String data = getBody(request, userName, filename, size, destinationPath, uploadSize, totalSize);
            }

        }

        return null;
    }


    private String getBody(HttpServletRequest request, String userName,
                           String filename, int size, String outputDir, BigInteger uploadSize, BigInteger totalSize) throws Exception {

        String body = null;
        StringBuilder stringBuilder = new StringBuilder();
        BufferedReader bufferedReader = null;

        outputDir = removeMultipleSpaces(outputDir);
        OutputStream out = null;

        InputStream filecontent = null;
        try {
            filecontent = request.getInputStream();

            int read = 0;
            byte[] bytes = IOUtils.toByteArray(filecontent, size);
            log.debug("size=" + bytes.length);
            String tmpFilename = UUID.randomUUID().toString();
            File bufferFile = new File(TMP_DIRECTORY + tmpFilename);
            FileUtils.writeByteArrayToFile(bufferFile, bytes);
            try {
                File file = new File(outputDir + "/" + filename);
                String parentPath = file.getAbsoluteFile().getParent();
                parentPath = removeMultipleSpaces(parentPath);
                log.debug("Parent Path" + parentPath);
                if (!(exists(userName, parentPath))) {
                    log.debug("Creating Path" + parentPath);
                    mkdirByUser(userName, parentPath);

                }
                log.debug("*************UploadSize" + uploadSize + "TotalSize" + totalSize + "For" + filename);
                BigInteger zeroSize = new BigInteger(String.valueOf(0));
                if (!(uploadSize.equals(totalSize)) || (totalSize.equals(zeroSize))) {
                    appendFile(userName, bufferFile.getAbsolutePath(), outputDir + "/" + filename + UPLOAD_FILE_EXT);
                }

            } catch (Exception ex) {
                log.error("Unable to Merge .Error:", ex);
                log.error("Unable to Merge File:", bufferFile);
            } finally {
                try {
                    log.debug("Deleting File=" + bufferFile);
                    FileUtils.deleteQuietly(bufferFile);

                } catch (Exception e) {
                    log.error("Could not delete tmp file used for uploading file. Reason: {}", e.getMessage());
                }
            }


        } catch (FileNotFoundException fne) {
            log.debug("You either did not specify a file to upload or are "
                    + "trying to upload a file to a protected or nonexistent " + "location.");
            log.debug("<br/> ERROR: " + fne.getMessage());

        } finally {
            if (out != null) {
                out.close();
            }
            if (filecontent != null) {
                filecontent.close();
            }
        }

        return body;
    }

    private String removeMultipleSpaces(String path){
    	String []destArr = path.split(" ");
    	path = "";
		for(int i = 0 ; i< destArr.length ; i++){
			if(i ==0 ){
				path = path + destArr[i];
			}else if(!"".equals(destArr[i])){
				destArr[i] = destArr[i].trim();
				path = path + " " + destArr[i];
			}
			
		}
		return path;
    }

    /**
     * Extracts file name from HTTP header content-disposition
     */
    private String extractFileName(Part part) {
        String contentDisp = part.getHeader("content-disposition");
        String[] items = contentDisp.split(";");
        for (String s : items) {
            if (s.trim().startsWith("filename")) {
                return s.substring(s.indexOf("=") + 2, s.length() - 1);
            }
        }
        return "";
    }


    private void validateDestinationFolder(HttpServletRequest request, HttpServletResponse response, String destination, String userName)
            throws Exception {

        JSONObject folderValidation = new JSONObject();
        folderValidation.put("folderName", destination);
        boolean validationSuccess = true;
        if (!isItFolder(userName, destination)) {
            folderValidation.put("validationResult", "Failed");
            folderValidation.put("validationError", "Error: Invalid path");
            validationSuccess = false;
        }
        if (validationSuccess && (!canWrite(userName, destination))) {
            folderValidation.put("validationResult", "Failed");
            folderValidation.put("validationError", "Destination folder does not have write permission for the user.");
        }

        response.setContentType("text/plain");
        response.setCharacterEncoding("UTF-8");
        response.getWriter().write(folderValidation.toString());
    }


    private DeferredResult<String> manageResult(AgentCommand command) throws IOException {
        return manageResult(command, null);
    }


    private DeferredResult<String> manageResult(AgentCommand command, DeferredResultHandler resultHandler)
            throws IOException {
        DeferredResult<String> result = new DeferredResult<>();
        DeferredResult<AgentResponse> response = new DeferredResult<>();

        if (resultHandler == null) {
            response.setResultHandler(new DeferredResultHandler() {
                @Override
                public void handleResult(Object res) {
                    AgentResponse agentResponse = (AgentResponse) res;
                    log.debug("Received result back in controller handler.. " + agentResponse.getData());
                    result.setResult(agentResponse.getData().toLowerCase());
                }
            });
        } else {
            response.setResultHandler(resultHandler);
        }

        //handler.sendCommand(command, response);

        publisher.pubhishEvent(command, response);

        return result;

    }

    @Override
    public void setServletContext(ServletContext servletContext) {
        this.servletContext = servletContext;
    }

    /*
     * Class to capture the result of calling an external command.
     * exitCode is the exit code of the external process
     * output is rge output of the external process
     */
    public static class ProcessResult {
        private int exitCode;
        private String output;

        public ProcessResult(int exitCode, String output) {
            this.exitCode = exitCode;
            this.output = output;
        }

        public int getExitCode() {
            return exitCode;
        }

        public void setExitCode(int exitCode) {
            this.exitCode = exitCode;
        }

        public String getOutput() {
            return output;
        }

        public void setOutput(String output) {
            this.output = output;
        }
    }

    /*
     * Run external commands passed as an array of strings and return the output and
     * exit code for the external command.
     * We are running the commands by creating a script because filenames with space
     * are not treated well. Enclosing the filenames in quotes is also not helping
     * as ProcessBuilder takes care of that only if we pass the entire name with space
     * as an element of the array. We cant do that because we are using su and the
     * param after -c should be a single string with the whole command that needs to
     * be executed.
     */
    private static ProcessResult runExternalCommand(String... commands) throws Exception {
        String tmpScriptName = TMP_DIRECTORY + UUID.randomUUID().toString() + ".sh";
        File f = new File(tmpScriptName);
        FileOutputStream fout = new FileOutputStream(f);
        fout.write(("#!/bin/bash\nset -e\n" + commands[commands.length - 1]).getBytes());
        fout.close();
        f.setExecutable(true, false);
        commands[commands.length - 1] = tmpScriptName;
        ProcessBuilder builder = new ProcessBuilder(commands).redirectErrorStream(true).inheritIO().redirectOutput(Redirect.PIPE);
        Process p = builder.start();
        String line = null;
        String output = "";
        BufferedReader reader = new BufferedReader(new InputStreamReader(p.getInputStream()));
        while ((line = reader.readLine()) != null) {
            output += line;
        }
        reader.close();
        int exitCode = p.waitFor();
        f.delete();
        log.debug("Ran executable : {}", f.getAbsolutePath());
        return new ProcessResult(exitCode, output);
    }

    /*
     * Check if a path exists and is valid.
     */
    private boolean exists(String username, String filePath) throws Exception {
        //File work = new File(filePath);
        //return work.exists();
        log.debug("Inside exists********" + filePath);
        ProcessResult processResult = runExternalCommand("sudo", "-u", username, "ls -l \"" + filePath + "\"");
        log.debug("Exists = ls -l " + filePath + "; exitcode=" + processResult.getExitCode());
        return processResult.getExitCode() == 0;
    }

    /*
 * Check if a path exists and is valid.
 */
    private String uploadExists(String username, String filePath) throws Exception {
        ProcessResult processResult = runExternalCommand("sudo", "-u", username, "ls -l \"" + filePath + "\"" + " | cut -d \" \" -f5");
        String output = processResult.getOutput();
        String size = String.valueOf(0);
        log.debug("Output Code For " + filePath + "is =" + processResult.getExitCode() + "And Result =" + processResult.getOutput());
        if (output.matches("\\d*")) {
            size = output.trim();
            log.debug("Size is" + size);
        }
        return size;
    }

    /*
     * Get the mime type of the file.
     */
    private String getMimeType(String username, String filePath) throws Exception {
        ProcessResult processResult = runExternalCommand("sudo", "-u", username, "file --mime-type -b \"" + filePath + "\"");
        log.debug("file --mime-type -b " + filePath + ";output=" + processResult.getOutput());
        return processResult.getOutput();
    }

    /*
     * Get the length of the file in bytes.
     */
    private String getLength(String username, String filePath) throws Exception {
        //ProcessResult processResult = runExternalCommand("su", "-", username, "-c", "du -b \"" + filePath + "\"");
        ProcessResult processResult = runExternalCommand("sudo", "-u", username, "stat -c '%s' \"" + filePath + "\"");
        //String len=processResult.getOutput().split("\\s+")[0].trim();
        //ProcessResult processResult = runExternalCommand("su", "-", username, "-c", "ls -l \"" + filePath + " | cut -d \" \" -f5"+ "\"");
        String len = processResult.getOutput().trim();
        log.debug("Length is" + len);
        return len;
    }

    /*
     * Does the user have read permissions on the file.
     */
    private boolean canRead(String username, String filePath) throws Exception {
        // File fileToCheck = new File(filePath);
        // return fileToCheck.exists() ? true : false;
        ///*
        ProcessResult processResult = runExternalCommand("sudo", "-u", username, "[ -r \"" + filePath + "\" ]");
        log.debug("[ -r " + filePath + " ]" + "; exitcode=" + processResult.getExitCode());
        return processResult.getExitCode() == 0;
        //*/
    }

    private boolean canWrite(String username, String filePath) throws Exception {
        //File fileToCheck = new File(filePath);
        //return fileToCheck.exists() ? true : false;
        ///*
        ProcessResult processResult = runExternalCommand("sudo", "-u", username, "[ -w \"" + filePath + "\" ]");
        log.debug("user:" + username + ":" + "[ -w " + filePath + " ]" + "; exitcode=" + processResult.getExitCode());
        return processResult.getExitCode() == 0;
        //*/
    }

    private boolean isItFolder(String username, String filePath) throws Exception {
        //File fileToCheck = new File(filePath);
        //return fileToCheck.exists() ? true : false;
        ///*
        ProcessResult processResult = runExternalCommand("sudo", "-u", username, "[ -d \"" + filePath + "\" ]");
        log.debug("user:" + username + ":" + "[ -d " + filePath + " ]" + "; exitcode=" + processResult.getExitCode());
        return processResult.getExitCode() == 0;
        //*/
    }

    private static String findWorkDir(String dir) {
        String[] dirComponents = dir.split("/");
        dirComponents[dirComponents.length - 1] = "." + dirComponents[dirComponents.length - 1] + "_dir";
        return String.join("/", dirComponents);
    }

    /*
     * Append contents of one file at the end of other
     */
    private int appendFile(String username, String sourcePath, String targetPath) throws Exception {
		/*
		File sourceFile = new File (sourcePath);
		File targetFile = new File (targetPath);
		FileUtils.writeByteArrayToFile(targetFile, FileUtils.readFileToByteArray(sourceFile), true);
		return 0;
		*/
        ///*
        ProcessResult processResult = runExternalCommand("sudo", "-u", username, "cat \"" + sourcePath + "\" >> \"" + targetPath + "\"");
        log.debug("user:" + username + ":" + "Appending " + sourcePath + " to " + targetPath + "; exitcode=" + processResult.getExitCode() + ";output->" + processResult.getOutput());
        return processResult.getExitCode();
        //*/
    }

    private static int copyFileTo(String username, String sourcePath, String targetPath, String dirName) throws Exception {
        // mkdirByUser(username, dirName);
		/*
		File targetDir = new File(dirName);
		if (targetDir.exists() && targetDir.isDirectory()) {
			FileUtils.copyFile(new File(sourcePath), new File(dirName + "/" +  targetPath));
		}
		return 0;
		*/
        ///*
        ProcessResult processResult = runExternalCommand("sudo", "-u", username, "cp \"" + sourcePath + "\" \"" + dirName + "/" + targetPath + "\"");
        log.debug("user:" + username + ":" + "copyFileTo : copying " + sourcePath + " to " + targetPath + " to dir " + dirName + "; exitcode=" + processResult.getExitCode());
        return processResult.getExitCode();
        //*/
    }

    /*
     * Create a file of 0 length
     */
    private static int mkdirByUser(String username, String path) throws Exception {
        //FileUtils.forceMkdir(new File(path));
        //return 0;
        ///*
        ProcessResult processResult = runExternalCommand("sudo", "-u", username, "mkdir -p \"" + path + "\"");
        log.debug("user:" + username + ":" + "creating directory " + path + "; exitcode=" + processResult.getExitCode());
        return processResult.getExitCode();
        //*/
    }

    private static int rmdirByUser(String username, String path) throws Exception {
        //FileUtils.deleteQuietly(new File(path));
        //return 0;
        ///*
        log.debug("Inside rmdirByUser" + path);
        ProcessResult processResult = runExternalCommand("sudo", "-u", username, "rm -rf \"" + path + "\"");
        log.debug("RMDIR = user:" + username + ":" + "removed directory " + path + "; exitcode=" + processResult.getExitCode());
        return processResult.getExitCode();
        //*/
    }

    private static int mvByUser(String userName, String sourcePath, String targetPath) throws Exception {
        ProcessResult processResult = runExternalCommand("sudo", "-u", userName, "mv \"" + sourcePath + "\" \"" + targetPath + "\"");
        log.debug("user:" + userName + ":" + "Moving :  " + sourcePath + " to " + targetPath + "exitcode=" + processResult.getExitCode());
        return processResult.getExitCode();
    }

    private int countAsUser(String username, String workdir) throws Exception {
        int fileCount = 0;
        //File work = new File(workdir);
        //fileCount = work.listFiles().length;
        ///*
        ProcessResult processResult = runExternalCommand("sudo", "-u", username, "ls -1 \"" + workdir + "\"" + "|wc -l");
        try {
            fileCount = Integer.parseInt(processResult.getOutput().trim());
        } catch (NumberFormatException e) {
            log.debug("Error: {}", e.getMessage());
        }
        //*/
        log.debug("user:" + username + ":" + "checking filecount in " + workdir + "; result=" + fileCount);
        return fileCount;
    }

    private String getFileList(String username, String workdir) throws Exception {
		/*
		File work = new File(workdir);
		if (work.isDirectory()) {
			return String.join(" ", work.list());
		}
		return "";
		*/
        ///*
        ProcessResult processResult = runExternalCommand("sudo", "-u", username, "ls \"" + workdir + "\"|sed -e 's/$/,/g'");
        log.debug("user:" + username + ":" + "checking filelist now in " + workdir + "; output=" + processResult.getOutput().trim());
        return processResult.getOutput().trim();
        //*/
    }

    private String whenLastModified(String username, String filePath) throws Exception {
        //File sourceFile = new File (filePath);
        //return sourceFile.exists() ? Long.toString(sourceFile.lastModified()) : "0";
        ///*
        ProcessResult processResult = runExternalCommand("sudo", "-u", username, "stat -c %Y \"" + filePath + "\"");
        log.debug("user:" + username + ":" + "checking when " + filePath + " last modified; result=" + processResult.getExitCode());
        return processResult.getExitCode() == 0 ? processResult.getOutput().trim() : "0";
        //*/
    }
}
