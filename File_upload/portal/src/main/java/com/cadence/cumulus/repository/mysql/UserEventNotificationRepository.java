package com.cadence.cumulus.repository.mysql;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.cadence.cumulus.model.UserEventNotification;
import com.cadence.cumulus.model.UserEventNotificationId;

@Repository
public interface UserEventNotificationRepository extends JpaRepository<UserEventNotification, UserEventNotificationId> {
	
	public List<UserEventNotification> findByUserEventNotificationId_User_Email(String email);
	
}