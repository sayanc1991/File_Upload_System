package com.cadence.cumulus.dto;

import java.util.StringJoiner;

import org.springframework.util.StringUtils;

public class TokenPayload {

	private String sub;
	private String aud;
	public String getSub() {
		return sub;
	}
	public void setSub(String sub) {
		this.sub = sub;
	}
	public String getAud() {
		return aud;
	}
	public void setAud(String aud) {
		this.aud = aud;
	}
	@Override
	public String toString() {
		StringJoiner joiner = new StringJoiner("&");
		joiner = joiner.add(StringUtils.hasText(sub) ? "sub=" + sub : "");
		joiner = joiner.add(StringUtils.hasText(aud) ? "aud=" + aud : "");
		return joiner.toString();
	}
	
	
}
