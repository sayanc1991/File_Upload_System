package com.cadence.cumulus.integration.opendj.service;

import com.cadence.cumulus.integration.IntegrationException;
import com.cadence.cumulus.integration.opendj.vo.UpdatePassword;
import com.cadence.cumulus.integration.opendj.vo.User;
import com.cadence.cumulus.integration.opendj.vo.UserMountInformation;

public interface OpenDJIntegrationService {
	
	/**
	 * Adds the Cumulus user in OpenDJ Directory Service.
	 * 
	 * 
	 * @param User user of the cumulus user (usually email id) ) to be added in OpenDJ.
	 * @throws IntegrationException Exception if any while accessing OpenDJ.
	 */
	public void addUser(User user) throws IntegrationException;

	/**
	 * Adds cumulus user auto mount information in OpenDJ Directory Service.
	 * 
	 * 
	 * @param mountInformation user of the cumulus user to be added in OpenDJ.
	 * @throws IntegrationException Exception if any while accessing OpenDJ.
	 */
	public void autoMount(UserMountInformation mountInformation) throws IntegrationException;

	/**
	 * Finds user information from OpenDJ Directory Service.
	 * 
	 * 
	 * @param username to be fetch from OpenDJ.
	 * @throws IntegrationException Exception if any while accessing OpenDJ.
	 */
	public User findUser(String username) throws IntegrationException;

	/**
	 * updates cumulus user information in OpenDJ Directory Service.
	 * 
	 * 
	 * @param UpdatePassword[] updatePassword of the cumulus user (usually password,etc) to be added in OpenDJ.
	 * @param User user of the cumulus user (usually email id) to be added in OpenDJ.
	 * @throws IntegrationException Exception if any while accessing OpenDJ.
	 */
	public void updateUser(UpdatePassword[] updatePassword, User user) throws IntegrationException;

}
