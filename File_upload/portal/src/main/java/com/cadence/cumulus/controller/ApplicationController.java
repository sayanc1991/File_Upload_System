package com.cadence.cumulus.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.cadence.cumulus.common.CumulusException;
import com.cadence.cumulus.model.Application;
import com.cadence.cumulus.model.Plugin;
import com.cadence.cumulus.service.ApplicationService;

/**
 * 
 * @author shirish
 *
 */

@RestController
@RequestMapping(value = "/application")
public class ApplicationController {
	
	@Autowired
	private ApplicationService applicationService;
	
	/**
	 * Logger
	 */
	private static final Logger log = LoggerFactory.getLogger(ApplicationController.class);
	
	@GetMapping(value = "/profile")
    public @ResponseBody Application getApplicationProfile() throws CumulusException {
        log.debug("Fetching application profile");
        return applicationService.getApplicationProfile();
    }
	
	@GetMapping(value = "/activeplugin")
    public @ResponseBody List<Plugin> getActivePlugin() throws CumulusException {
        log.debug("Fetching user profile");
        return applicationService.getActivePlugin();
    }
	
	@GetMapping(value = "/plugin")
    public @ResponseBody List<Plugin> getPlugins() throws CumulusException {
        log.debug("Fetching user profile");
        return applicationService.getPlugin();
    }


	

}
