package com.cadence.cumulus.controller;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.http.HttpHost;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.conn.DefaultProxyRoutePlanner;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.cadence.cumulus.ProxyConfig;
import com.cadence.cumulus.common.AppMessages;
import com.cadence.cumulus.common.Constants;
import com.cadence.cumulus.common.LDAPUNPWStore;
import com.cadence.cumulus.integration.IntegrationException;
import com.cadence.cumulus.integration.opendj.service.OpenDJIntegrationService;
import com.cadence.cumulus.integration.opendj.vo.UpdatePassword;
import com.cadence.cumulus.integration.opendj.vo.User;
import com.cadence.cumulus.model.ResponseVO;
import com.cadence.cumulus.service.CumulusUserService;

@Controller
public class HomeController {
    private static final Logger log = LoggerFactory.getLogger(HomeController.class);

    @Autowired
    private ProxyConfig proxyConfig;

    @Value("${redirect.app.url}")
    private String appRedirectUrl;

    @Value("${redirect.base.url}")
    private String redirectUrlBase;
    
    @Value("${cumulus.product}")
    private String productName;

    @Value("${opendj.enabled}")
    private boolean isOpenDJEnabled;

    @Value("${okta.auth.url}")
    private String oktaUrl;

    @Value("${okta.auth.http.header.authorization}")
    private String oktaHttpAuthorization;

    private String appUrl = "&redirectUrl=";

	@Autowired
	private OpenDJIntegrationService openDJIntegrationService;

	@Autowired
	private CumulusUserService userService;

    @RequestMapping(value = "/login", method = RequestMethod.GET)
    public String login() {
        log.debug("Rec'd login page request");
        return "login.html";
    }

    @RequestMapping(value = "/login", method = RequestMethod.POST)
    public ModelAndView doLogin(@RequestParam("username") String username, 
    		@RequestParam("password") String passwd, HttpServletRequest request) throws Exception {
        log.debug("Start authentication /login called :  " + username);

        JSONObject jsonRequest = new JSONObject();
        jsonRequest.put("username", username);
        jsonRequest.put("password", passwd);

    	com.cadence.cumulus.model.User cumulusUser = null;
    	if (username.contains("@")) {
    		cumulusUser = userService.getUserByEmail(username);
    	} else {
    		cumulusUser = userService.getUserByUserName(username);
    	}

    	if (cumulusUser == null) {
    		Map<String, String> model = new HashMap<>();
			model.put("errorMsg", String.format(AppMessages.USER_LOGIN_NOT_FOUND.getMessage(), username)
					+ String.format(Constants.ERROR_CODE, AppMessages.USER_LOGIN_NOT_FOUND.getCode()));
					// + " [Error Code: " + AppMessages.USER_LOGIN_NOT_FOUND.getCode() + "]");
            return new ModelAndView("login.html", model);
    	}

        log.debug("OKTA Auth URL: " + oktaUrl);
        log.debug("OKTA Http Authorization : " + oktaHttpAuthorization);

        HttpPost httpPost = new HttpPost(oktaUrl);
        httpPost.addHeader("content-type", "application/json");
        httpPost.addHeader("Authorization", oktaHttpAuthorization);
        httpPost.setEntity(new StringEntity(jsonRequest.toString()));

        CloseableHttpClient httpClient = null;
        if (proxyConfig.isEnabled()) {
            httpClient = HttpClients.custom().setRoutePlanner(new DefaultProxyRoutePlanner((new HttpHost(proxyConfig.getHost(), proxyConfig.getPort())))).build();
        } else {
            httpClient = HttpClients.createDefault();
        }

        CloseableHttpResponse httpResponse = httpClient.execute(httpPost);

        log.debug("Auth Service Response " + httpResponse);

        int status = httpResponse.getStatusLine().getStatusCode();
        log.debug("Auth Response Status: {}", status);

        BufferedReader reader = new BufferedReader(new InputStreamReader(httpResponse.getEntity().getContent(), "UTF-8"));
        String oktaResponse = reader.readLine();

        JSONObject jObject  = new JSONObject(oktaResponse);
        log.debug("Okta Response: {}", oktaResponse);

        if (status == 200) {
        	log.debug("isOpenDJEnabled: "+isOpenDJEnabled);

            if (username.contains("@")) {
                log.debug("Removing @ from the username");
                log.debug("username - before trim: " + username);
                username = username.split("@")[0];
                log.debug("Username after trim : {}", username);
            }

            log.debug("Storing in-memory for FastX/Terminal auth..");
            // store unpw for LDAP/SSH AUTH
            LDAPUNPWStore.getInstance().storeUnpw(username, passwd);


            // Call to update user password in opendj after user authentication successful.
        	if (isOpenDJEnabled) {
            	User user = null;
            	try {
            		user = openDJIntegrationService.findUser(username);
            		log.info("Successfully obtained user from OpenDJ : "+user.toString());
            	} catch (IntegrationException ie) {
            		log.debug("User " + username + " not found in opendj server.");
            		log.debug(ie.getMessage());
            		if (ie.getMessage().contains("Unauthorized")) {
            			Map<String, String> model = new HashMap<>();
            			model.put("errorMsg", AppMessages.USER_LOGIN_ERROR.getMessage()
            					+ String.format(Constants.ERROR_CODE, AppMessages.USER_LOGIN_ERROR.getCode()));
                        return new ModelAndView("login.html", model);
            		}
        		}
            	
    	        if (user != null) {
    	        	UpdatePassword[] updatePassword = new UpdatePassword[1];
    	        	updatePassword[0] = new UpdatePassword();
    	        	updatePassword[0].setOperation("replace");
    	        	updatePassword[0].setField("userPassword");
    	        	updatePassword[0].setValue(passwd);
    	        	try {
	    	        	openDJIntegrationService.updateUser(updatePassword, user);
	    	        	log.info("Successfully updated user password in OpenDJ.");
	    	        } catch (IntegrationException ie) {
	            		log.debug(username + " password not able to update in opendj server.");
	            		log.debug(ie.getMessage());
	    	        }
    	        }
            }
        	if (oktaResponse != null) {
                if (oktaResponse.startsWith("[")) {
                    oktaResponse = oktaResponse.substring(1, oktaResponse.length());
                }
            }

            JSONObject jsonResult = new JSONObject(oktaResponse);
            String token = null;
            try {
            	token = jsonResult.getString("cookieToken");
            } catch (JSONException je) {
            	log.debug("Exception while parsing okta response: "+je.getMessage());
            	Map<String, String> model = new HashMap<>();
    			model.put("errorMsg", AppMessages.USER_LOGIN_ERROR.getMessage()
    					+ String.format(Constants.ERROR_CODE, AppMessages.USER_LOGIN_ERROR.getCode()));
                return new ModelAndView("login.html", model);
            }
            log.debug("Okta cookietoken is: {}", token);

            log.debug("User authentication successful..");

            log.debug("redirecting to: {}", redirectUrlBase + token + appUrl + appRedirectUrl);

            return new ModelAndView("redirect:" + redirectUrlBase + token + appUrl + appRedirectUrl);
        }

        log.debug("User authentication FAILED..");

        Map<String, String> model = new HashMap<>();
		model.put("errorMsg", jObject.getString("errorSummary")
				+ String.format(Constants.ERROR_CODE, AppMessages.USER_LOGIN_AUTH_FAIL.getCode()));

        return new ModelAndView("login.html", model);
    }
    
	@GetMapping("/productname" )
    public ResponseEntity<ResponseVO> getProductName(){
    	ResponseVO responseVO = new ResponseVO();
    	if(null != productName && !"".equals(productName)){
    		responseVO.setStatus("success");
    		responseVO.setValue(productName.toLowerCase());
    		return ResponseEntity.ok(responseVO);
    	}
    	return ResponseEntity.ok(responseVO);
    }

}
