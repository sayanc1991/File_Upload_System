package com.cadence.cumulus.core.security;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.saml.SAMLCredential;
import org.springframework.security.saml.userdetails.SAMLUserDetailsService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.cadence.cumulus.common.ServiceException;
import com.cadence.cumulus.model.CumulusUser;
import com.cadence.cumulus.model.CumulusUserNotificationSettings;
import com.cadence.cumulus.model.User;
import com.cadence.cumulus.service.CumulusUserService;
import com.cadence.cumulus.service.PermissionService;
import com.cadence.cumulus.utility.FileSystemUtil;

@Service
@Transactional(propagation = Propagation.REQUIRED, rollbackFor = { Exception.class })
public class SAMLUserDetailsServiceImpl implements SAMLUserDetailsService {
    private static final Logger log = LoggerFactory.getLogger(SAMLUserDetailsServiceImpl.class);

    @Value("${user.home.base}")
    private String userHomeBase; 

    @Value("${user.shared.folder}")
    private String sharedFoler;

    /*
     * CMLS-994 - shared directory is not created when we run this app as non root user >> 
    @Autowired
    private PermissionService permissionService;
    * <<
    */

    //  CMLS-994 
    @Autowired
	private FileSystemUtil fsUtil;
    
    @Autowired
    private CumulusUserService userService;
    
    @Autowired
    private UserAuthorizationServiceImpl userAuthorizationServiceImpl;


    @Override
    public Object loadUserBySAML(SAMLCredential credential) throws UsernameNotFoundException {
        String userName = credential.getNameID().getValue();
        String firstName = credential.getAttributeAsString("firstName");
        String lastName = credential.getAttributeAsString("lastName");

        log.debug("Logged in user is: {}", userName);
        log.debug("SAML token extracted: first name:" + firstName + " lastName " + lastName) ;

        User user  = userService.getUserByEmail(userName);

        log.debug("user record from local database " + user);

        if (user==null){
            log.debug("Unable to login. Please contact your administrator to request access.");
            throw new UsernameNotFoundException("Unable to login. Please contact your administrator to request access.");
        }

        log.debug("REtrived in user is: {}", user.getId());
        CumulusUserNotificationSettings settings = userService.getNotificationSettings(user);
        if (settings == null) {
            settings = new CumulusUserNotificationSettings();
            settings.setEmail(userName);
            settings.setFirstName(firstName);
            settings.setLastName(lastName);
            settings = userService.saveDefaultNotificationSettings(user);
        }

        String loginName = userName.split("@")[0];
        String strLink = userHomeBase + loginName + "/shared";

        // CMLS-1074: Updating cumulus DB with user's firstname & lastname.
        if (!firstName.equalsIgnoreCase(user.getFirstName()) || !lastName.equalsIgnoreCase(user.getLastName())) {
        	user.setFirstName(firstName);
        	user.setLastName(lastName);
        	userService.saveUser(user);
        }

        //File shared = new File(sharedFoler);

        
        /*
         * CMLS-994 - Below logic won't work when portal application runs as non-root user. Accessing files directly using Java File will give permission denied error. 
        File linkFolder = new File(strLink);
        Path link = Paths.get(strLink);

        if (!linkFolder.exists()) {
            Path target = Paths.get(sharedFoler);

            try {
                Files.createSymbolicLink(link, target);
                permissionService.setOwnershipInfo(strLink, loginName);
            } catch (IOException e) {
                log.error("Unable to create symlink. Will try on next login");
            } catch (ServiceException e) {
                log.error("Unable to set ownership.");
            }
        }*/
        
        // CMLS-994 create link for shared folder using sudo ln -s
        try {
			fsUtil.createSymbolicLink(loginName, sharedFoler, strLink);
		} catch (Exception e) {
			  log.error("Unable to create symlink. Will try on next login",e);
		}

        return new CumulusUser(user.getId(),userName, firstName, lastName, settings, true, true, true, true, userAuthorizationServiceImpl.getUserAuthorityList(userName));
    }
}
