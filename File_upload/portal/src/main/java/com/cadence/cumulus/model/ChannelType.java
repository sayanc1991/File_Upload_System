package com.cadence.cumulus.model;

public enum ChannelType {

	SMS, Email, Browser;

}
