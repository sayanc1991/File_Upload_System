package com.cadence.cumulus.cache;

import java.io.Serializable;

/**
 * Object which can be used as a key to store the cache candidates.
 * CacheKey is typically made of the CacheObjectType and CacheObjectID
 * CacheObjectID will be used only when there are multiple instances of the same object needs to be cached,
 * otherwise it will default to CacheObjectType itself
 * @author zabeer
 *
 */
public class CacheKey implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private CacheObjectType cacheObjectType;
	private String cacheObjectID;

	// UR Token Data is maintained at app level and there is always one such object instance (UR Token)
	//to be cached, so no need to setCacheObjecctID.
	public static final CacheKey UR_TOKEN_KEY = new CacheKey(CacheObjectType.UR_TOKEN_DATA);

	public CacheKey(CacheObjectType cacheObjectType, String cacheObjectID) {
		super();
		this.cacheObjectType = cacheObjectType;
		this.cacheObjectID = cacheObjectID;
	}

	public CacheKey(CacheObjectType cacheObjectType) {
		super();
		this.cacheObjectType = cacheObjectType;
		// default object id to cacheobjecttype itself
		this.cacheObjectID = cacheObjectType.name();
	}

	public CacheObjectType getCacheObjectType() {
		return cacheObjectType;
	}

	public void setCacheObjectType(CacheObjectType cacheObjectType) {
		this.cacheObjectType = cacheObjectType;
	}

	public String getCacheObjectID() {
		return cacheObjectID;
	}

	public void setCacheObjectID(String cacheObjectID) {
		this.cacheObjectID = cacheObjectID;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((cacheObjectID == null) ? 0 : cacheObjectID.hashCode());
		result = prime * result + ((cacheObjectType == null) ? 0 : cacheObjectType.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CacheKey other = (CacheKey) obj;
		if (cacheObjectID == null) {
			if (other.cacheObjectID != null)
				return false;
		} else if (!cacheObjectID.equals(other.cacheObjectID))
			return false;
		if (cacheObjectType != other.cacheObjectType)
			return false;
		return true;
	}

}
