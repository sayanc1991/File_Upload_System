/**
 * 
 */
package com.cadence.cumulus.model;

import javax.persistence.AssociationOverride;
import javax.persistence.AssociationOverrides;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;

/**
 * 
 * @author shirish
 *
 */

@JsonInclude(JsonInclude.Include.NON_EMPTY)
@Entity
@Table(name = "UserRole")
@AssociationOverrides({ @AssociationOverride(name = "userRoleId.user", joinColumns = @JoinColumn(name = "UserId")),
		@AssociationOverride(name = "userRoleId.role", joinColumns = @JoinColumn(name = "RoleId")) })
public class UserRole extends BaseEntity {

	private UserRoleId userRoleId = new UserRoleId();
	private Long createdBy;

	@EmbeddedId
	public UserRoleId getUserRoleId() {
		return userRoleId;
	}

	public void setUserRoleId(UserRoleId userRoleId) {
		this.userRoleId = userRoleId;
	}

	@Transient
	@JsonIgnore
	public User getUser() {
		return userRoleId.getUser();
	}

	public void setUser(User user) {
		userRoleId.setUser(user);
	}

	@Transient
	@JsonIgnore
	public Role getRole() {
		return userRoleId.getRole();
	}

	public void setRole(Role role) {
		userRoleId.setRole(role);
	}

	public Long getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(Long createdBy) {
		this.createdBy = createdBy;
	}

	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (o == null || getClass() != o.getClass())
			return false;

		UserRole that = (UserRole) o;

		if (getUserRoleId() != null ? !getUserRoleId().equals(that.getUserRoleId()) : that.getUserRoleId() != null)
			return false;

		return true;
	}

	public int hashCode() {
		return (getUserRoleId() != null ? getUserRoleId().hashCode() : 0);
	}

}
