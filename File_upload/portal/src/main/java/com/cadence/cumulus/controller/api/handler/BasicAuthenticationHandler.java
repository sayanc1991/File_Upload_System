package com.cadence.cumulus.controller.api.handler;

import java.util.Base64;

import javax.servlet.http.HttpServletRequest;

import com.cadence.cumulus.model.UserLogin;

public class BasicAuthenticationHandler {
	

	public static UserLogin checkAuth(HttpServletRequest request) {

		String authHeaderVal = request.getHeader("AuthorizationHeader");
		UserLogin userLogin = null;
		if (authHeaderVal != null) {
			String[] headerArr = authHeaderVal.split(" ");
			if (headerArr != null && headerArr.length > 1) {
				String encCredentials = headerArr[1];
				Base64.Decoder decoder = Base64.getDecoder();
				String decodedCredentials = new String(
						decoder.decode(encCredentials));
				String[] decodedCredentialsArr = decodedCredentials.split(":");
				if (decodedCredentialsArr != null
						&& decodedCredentialsArr.length == 2) {
					userLogin = new UserLogin();
					userLogin.setUserName(decodedCredentialsArr[0]);
					userLogin.setPassword(decodedCredentialsArr[1]);
				}
			}

		}
		return userLogin;
	}
	
	/*public static void main(String... args) {
		String s = "shirish@cadence.com:MiJ%F1L100";
		
		Base64.Encoder e = Base64.getEncoder();
		System.out.println(e.encodeToString(s.getBytes()));
		
	}*/

}
