package com.cadence.cumulus.repository.mysql;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.cadence.cumulus.model.EventType;

@Repository
public interface EventTypeRepository extends JpaRepository<EventType, Long> {

}
