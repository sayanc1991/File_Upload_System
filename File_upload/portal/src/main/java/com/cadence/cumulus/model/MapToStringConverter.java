package com.cadence.cumulus.model;

import java.util.HashMap;
import java.util.Map;

import javax.persistence.AttributeConverter;

public class MapToStringConverter implements AttributeConverter<Map<String, String>, String>{

	@Override
	public String convertToDatabaseColumn(Map<String, String> attribute) {
		 if(null != attribute && !attribute.isEmpty()){
			 return attribute.toString();
		 }
		 return "";
	}

	@Override
	public Map<String, String> convertToEntityAttribute(String dbData) {
		Map<String, String> map = new HashMap<>();
		String value[];
			if(null != dbData && !"".equals(dbData)){
				dbData = dbData.substring(1, dbData.length()-1);
				value = dbData.split(",");
				for(int i = 0; i< value.length; i++){
					String subVal[] = value[i].trim().split("=");
					map.put(subVal[0], subVal[1]);
				}
			}
		return map;
	}

}
