package com.cadence.cumulus.model;

import java.nio.file.attribute.GroupPrincipal;
import java.nio.file.attribute.UserPrincipal;

/**
 * @author sandeep
 *
 * DTO object for holding resource ownership information
 */
public class ResourceOwnershipInfo {
	private UserPrincipal user;
	private GroupPrincipal group;
	
	public ResourceOwnershipInfo() {
		
	}
	
	public ResourceOwnershipInfo(UserPrincipal user, GroupPrincipal group){
		this.user = user;
		this.group = group;
	}
	
	public UserPrincipal getUser() {
		return user;
	}
	
	public void setUser(UserPrincipal user) {
		this.user = user;
	}
	
	public GroupPrincipal getGroup() {
		return group;
	}
	
	public void setGroup(GroupPrincipal group) {
		this.group = group;
	}
}
