/**
 * 
 */
package com.cadence.cumulus.repository.mysql;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.cadence.cumulus.model.UserRole;
import com.cadence.cumulus.model.UserRoleId;

/**
 * 
 * @author shirish
 *
 */
@Repository
public interface UserRoleRepository extends JpaRepository<UserRole, UserRoleId> {
	
	

	
	
}
