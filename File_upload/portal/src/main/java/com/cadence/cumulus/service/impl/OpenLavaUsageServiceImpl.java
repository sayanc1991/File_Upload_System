package com.cadence.cumulus.service.impl;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cadence.cumulus.model.OpenLavaResourceEntity;
import com.cadence.cumulus.model.OpenLavaResourceVO;
import com.cadence.cumulus.repository.mysql.OpenLavaUsageRepository;
import com.cadence.cumulus.service.OpenLavaUsageService;

/**
 * @author Vikramaditya Singh
 * 
 *
 */
@Service("openLavaUsageService")
public class OpenLavaUsageServiceImpl implements OpenLavaUsageService {

	@Autowired
	private OpenLavaUsageRepository openLavaUsageRepository;
	/*
	 * (non-Javadoc)
	 * 
	 * @see com.cadence.cumulus.service.OpenLavaUsageService#
	 * findUsageDetailsFromRange(java.lang.Long, java.lang.Long)
	 * The method is used to fetch records from 'openLavaUsage' table based on start date and end date
	 */
	@Override
	public List<OpenLavaResourceVO> findUsageDetailsFromRange(Long startTime, Long finishTime) {
		List<OpenLavaResourceVO> listFinal = new ArrayList<OpenLavaResourceVO>();
		OpenLavaResourceVO openLavaResourceVO = null;
		
		List<OpenLavaResourceEntity> list = openLavaUsageRepository.findByStartTimeAfterAndStartTimeBefore(startTime, finishTime);
		Iterator<OpenLavaResourceEntity> it = list.iterator();

		while(it.hasNext()){
			OpenLavaResourceEntity entity = (OpenLavaResourceEntity) it.next();
	
				openLavaResourceVO = new OpenLavaResourceVO();
				if (null != entity.getCoresUsed()) {
					openLavaResourceVO.setCoresUsed(entity.getCoresUsed());
				}
				if (null != entity.getCountOfHosts()) {
					openLavaResourceVO.setCountOfHosts(entity.getCountOfHosts());
				}
				if (null != entity.getExecutionTime()) {
					openLavaResourceVO.setExecutionTime(entity.getExecutionTime());
				}
				if (null != entity.getFinishTime()) {
					openLavaResourceVO.setFinishTime(entity.getFinishTime());
				}
				if (null != entity.getGroup()) {
					openLavaResourceVO.setGroup(entity.getGroup());
				}
				if (null != entity.getJobId()) {
					openLavaResourceVO.setJobId(entity.getJobId());
				}
				if (null != entity.getQueue()) {
					openLavaResourceVO.setQueue(entity.getQueue());
				}
				if (null != entity.getStartTime()) {
					openLavaResourceVO.setStartTime(entity.getStartTime());
				}
				if (null != entity.getUserName()) {
					openLavaResourceVO.setUserName(entity.getUserName());
				}
				listFinal.add(openLavaResourceVO);
		}
		return listFinal;
	}

}
