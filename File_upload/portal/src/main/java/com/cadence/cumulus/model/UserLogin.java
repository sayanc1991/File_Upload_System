package com.cadence.cumulus.model;

public class UserLogin extends BaseEntity {
	private String userName;
	private String password;
	private String mfaToken;

	public String getMfaToken() {
		return mfaToken;
	}

	public void setMfaToken(String mfaToken) {
		this.mfaToken = mfaToken;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
}
