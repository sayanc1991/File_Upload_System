package com.cadence.cumulus.integration.unifiedregistration.vo;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class UpdateUserContactInfoVO {

	private String Id;

	public String getId() {
		return Id;
	}
	public void setId(String id) {
		Id = id;
	}
	
}