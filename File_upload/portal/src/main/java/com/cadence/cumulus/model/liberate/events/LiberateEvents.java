package com.cadence.cumulus.model.liberate.events;

public enum LiberateEvents {
	started("started"),
    progress("progress"),
    failed("failed"),
    completed("completed"),
    disconnect("disconnect"),
	cancelled("cancelled");

    private String value;

    private LiberateEvents(String value) {
        this.value = value;
    }

    public String value() {
        return this.value;
    }
}
