/**
 * 
 */
package com.cadence.cumulus.model;

import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;

/**
 * 
 * @author shirish
 *
 */
@JsonInclude(JsonInclude.Include.NON_EMPTY)
@Entity
@Table(name = "Role")
public class Role extends BaseEntity {

	private Long id;

	private String name;

	private String description;

	private Set<UserRole> userRoleSet;

	private List<RolePermission> rolePermissionSet;

	/**
	 * 
	 * @return name
	 */

	public String getName() {
		return name;
	}

	/**
	 * 
	 * @param name
	 */
	public void setName(String name) {
		this.name = name;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "Id")
	public Long getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@OneToMany(mappedBy = "userRoleId.role", fetch = FetchType.LAZY)
	@JsonIgnore
	public Set<UserRole> getUserRoleSet() {
		return userRoleSet;
	}

	public void setUserRoleSet(Set<UserRole> userRoleSet) {
		this.userRoleSet = userRoleSet;
	}

	@OneToMany(mappedBy = "rolePermissionId.role", cascade = CascadeType.ALL, fetch = FetchType.EAGER, orphanRemoval = true)
	@JsonIgnore
	public List<RolePermission> getRolePermissionSet() {
		return rolePermissionSet;
	}

	public void setRolePermissionSet(List<RolePermission> rolePermissionSet) {
//		if (this.rolePermissionSet != null) {
//			// remove objects which are not present in the passed on set
//			Iterator<RolePermission> it = this.rolePermissionSet.iterator();
//			while (it.hasNext()) {
//				RolePermission rp = it.next();
//				if (!rolePermissionSet.contains(rp)) {
//					it.remove();
//				}
//			}
//			this.rolePermissionSet.addAll(rolePermissionSet);
//		} else {
//			this.rolePermissionSet = rolePermissionSet;
//		}
		this.rolePermissionSet = rolePermissionSet;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Role other = (Role) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

}
