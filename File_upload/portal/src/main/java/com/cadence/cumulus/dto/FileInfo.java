package com.cadence.cumulus.dto;

import java.math.BigInteger;

public class FileInfo {

	private String outputFile, uploadFile;
	private BigInteger outputFileSize, uploadFileSize;
	private Boolean outputLastModifiedSame;
	public String getOutputFile() {
		return outputFile;
	}
	public void setOutputFile(String outputFile) {
		this.outputFile = outputFile;
	}
	public String getUploadFile() {
		return uploadFile;
	}
	public void setUploadFile(String uploadFile) {
		this.uploadFile = uploadFile;
	}
	public BigInteger getOutputFileSize() {
		return outputFileSize;
	}
	public void setOutputFileSize(BigInteger outputFileSize) {
		this.outputFileSize = outputFileSize;
	}
	public BigInteger getUploadFileSize() {
		return uploadFileSize;
	}
	public void setUploadFileSize(BigInteger uploadFileSize) {
		this.uploadFileSize = uploadFileSize;
	}
	public Boolean getOutputLastModifiedSame() {
		return outputLastModifiedSame;
	}
	public void setOutputLastModifiedSame(Boolean outputLastModifiedSame) {
		this.outputLastModifiedSame = outputLastModifiedSame;
	}
	@Override
	public String toString() {
		return "{outputFile : " + outputFile + ", uploadFile : " + uploadFile + ", outputFileSize : " + outputFileSize
				+ ", uploadFileSize : " + uploadFileSize + ", outputLastModifiedSame : " + outputLastModifiedSame + "}";
	}
	
}
