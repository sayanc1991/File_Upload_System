package com.cadence.cumulus.cli.utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.TreeMap;

import org.apache.log4j.Logger;


public class AppUtils  implements AppConfigKeys {

	private static final char WINDOWS_SEPARATOR = '\\';
	private static final char UNIX_SEPARATOR = '/';
	private static final int NOT_FOUND = -1;
	/**
	 * -1 = ctrl + c
	 * 10  = unix line feed on hitting enter
	 * 13  = windows carriage return on hitting enter
	 */
	private static final List<Integer> TERMINATORS = Arrays.asList(-1, 10, 13);
	
	private static Logger LOG = Logger.getLogger(AppUtils.class);
	private static Map<MessageCodes,String> appMessages;
	
	// TODO - spawn two threads from daemon thread to run concurrently
	
	/*static {
		ExecutorService executor = Executors.newFixedThreadPool(2);
		CompletableFuture<Void> loadAppProperties = CompletableFuture.runAsync(() -> {
			String fileName = Constants.APPLICATION_PROPERTIES;
			try {
				Properties appProperties = readPropertiesFile(fileName);
				System.setProperties(appProperties);
				//System.out.println(fileName + " " + Thread.currentThread().getName() + " " + appProperties.size());
			} catch (CLIException e) {
				System.out.println(e.getMessage());
			}
		}, executor);
		CompletableFuture<Void> loadAppMessages = CompletableFuture.runAsync(() -> {
			String fileName = Constants.MESSAGES_PROPERTIES;
			try {
				Properties messageProperties = readPropertiesFile(fileName);
				appMessages = new TreeMap<>();
				for(Object key : messageProperties.keySet()) {
					MessageCodes code = MessageCodes.valueOf(key.toString());
					String value = messageProperties.getProperty(key.toString());
					appMessages.put(code, value);
				}
				//System.out.println(fileName + " " + Thread.currentThread().getName() + " " + appMessages.size());
			} catch (CLIException e) {
				System.out.println(e.getMessage());
			}
		}, executor);
		//Stream.of(loadAppMessages, loadAppProperties).map(CompletableFuture::join);
		CompletableFuture.allOf(loadAppMessages, loadAppProperties).isDone();
		executor.shutdown();
	}*/
	
	public static void init() throws CLIException {
		Properties appProperties = readPropertiesFile(APPLICATION_PROPERTIES);
		Properties systemProperties = System.getProperties();
		// below line is dangerous, don't remove it ever
		appProperties.putAll(systemProperties);
		System.setProperties(appProperties);
		Properties messageProperties = readPropertiesFile(MESSAGES_PROPERTIES);
		appMessages = new TreeMap<>();
		for(Object key : messageProperties.keySet()) {
			MessageCodes code = MessageCodes.get(key.toString());
			String value = messageProperties.getProperty(key.toString());
			appMessages.put(code, value);
		}
	}
	
	/**
	 * Custom reader from stdin to additionally capture ctrl + c pressed during input from stdin
	 * @param prompt for user to make input
	 * @return user input provided from stdin
	 * @throws IOException I/O occurs while reading from stdin
	 */
	public static String readFromConsole(String prompt) throws IOException {
		// TODO Auto-generated method stub
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.print(prompt);
		String input = "";
		int ascii = 0;
		while(!TERMINATORS.contains(ascii = br.read())) {
			char c  = (char) ascii;
			input = input + c;
		}
		if(ascii == -1) {
			System.out.println();
			input = null;
			LOG.info("Application terminated");
		} else {
			LOG.info("User console input: " + input);
			input = input.trim();
			LOG.info("After trime: " + input);
		}
		LOG.info("Last character read: " + ascii);
		return input;
	}
	
	private static Properties readPropertiesFile(String fileName) throws CLIException{
		try {
			ClassLoader classLoader = AppUtils.class.getClassLoader();
			InputStream input = classLoader.getResourceAsStream(fileName);
			Properties properties = new Properties();
			properties.load(input);
			input.close();
			LOG.info(fileName + " loaded successfully");
			return properties;
		} catch (IOException e) {
			System.out.println(e.getMessage());
			LOG.error("Error while reading properties file: " + fileName + " : "  + e);
			MessageCodes code = MessageCodes.CLI_ERROR;
			CLIException clie = DisplayUtility.formatException(code, e);
			LOG.error("Encountered: " + code.getCode());
			throw clie;
		} 
	}

	public static boolean hasLength(String str) {
		return (str != null && str.length() > 0);
	}
	
	public static String separatorsToUnix(final String path) {
		if (path == null || path.indexOf(WINDOWS_SEPARATOR) == NOT_FOUND) {
			return path;
		}
		return path.replace(WINDOWS_SEPARATOR, UNIX_SEPARATOR);
	}
	
	public static String getMessage(MessageCodes key) {
		String msg = appMessages.get(key);
		return msg;
	}

}
