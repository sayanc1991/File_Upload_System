package com.cadence.cumulus.cli.model;

import java.util.StringJoiner;

import com.cadence.cumulus.cli.utils.AppUtils;


public class ChunkUploadRequest {

	private String filename,destination, call;
	private Integer size,lastSize;
	public String getFilename() {
		return filename;
	}
	public void setFilename(String filename) {
		this.filename = filename;
	}
	public String getDestination() {
		return destination;
	}
	public void setDestination(String destination) {
		this.destination = destination;
	}
	public Integer getSize() {
		return size;
	}
	public void setSize(Integer size) {
		this.size = size;
	}
	public Integer getLastSize() {
		return lastSize;
	}
	public void setLastSize(Integer lastSize) {
		this.lastSize = lastSize;
	}
	
	public String getCall() {
		return call;
	}
	public ChunkUploadRequest() {
		super();
		this.call = "saveFile";
	}
	@Override
	public String toString() {
		StringJoiner sj = new StringJoiner("&");
		if(size != null) sj = sj.add("size=" + size);
		if(lastSize != null) sj = sj.add("lastSize=" + lastSize);
		String requestQuery = sj.toString();
		requestQuery = AppUtils.hasLength(requestQuery) ? "?".concat(requestQuery) : requestQuery;
		requestQuery = destination + "/" + filename + requestQuery;
		requestQuery = "/" + requestQuery;
		return requestQuery;
	}
	
	
}
