package com.cadence.cumulus.cli.utils;

import java.util.Map;
import java.util.TreeMap;

public enum MessageCodes {
	
	SUCCESS("", "0"),
	NO_COMMAND("command.none", "1"),
	TOO_MANY_COMMANDS("command.too.many", "7"),
	UNRECOGNIZED_COMMAND("command.not.recognized", "3"), 
	ERROR_UPLOAD_DESTINATION("upload.destination.syntax.error", ""),
	NO_UPLOAD_SOURCES("upload.source.none", "4"), 
	UNDEFINED_UPLOAD_SOURCE("upload.source.not.found", "5"),
	EXHAUSTED_UPLOAD_SOURCE("upload.sources.exhausted", "6"),
	ERROR_UPLOAD_SOURCE("upload.source.read.error", "8"),
	TOO_MANY_OPTIONS("option.too.many", "9"),
	INVALID_OPTIONAL_OPTION("option.optional", "10"),
	REQUIRED_OPTION("option.mandatory", "11"),
	HELP_PROMPT("help.prompt", "12"),
	TOKEN_INVALID("token.invalid", "16"),
	TOKEN_EXPIRED("token.expired", "17"),
	CLI_ERROR("cli.system.error", "18"),
	UPLOAD_FAILED("upload.source.failed", "19"),
	UPLOAD_PROGRESS("upload.source.progress", "20"),
	UPLOAD_RESUME("upload.source.resume", "21"),
	UPLOAD_EXISTS("upload.source.exists", "22"),
	UPLOAD_COMPLETED("upload.source.completed", "23"),
	RETRY("cli.retry", "24"),
	RETRY_WAIT("cli.retry.wait", "25"),
	RETRY_FAILED("cli.retry.failed", "26"),
	SERVER_ERROR("server.system.error", "27"),
	//TOKEN_UNEXPECTED_USER("token.generate.unexpected.user", "30"),
	OPTION_UNEXPECTED_VALUE("option.unexpected.value", "30"),
	
	// CMLS-830
	ENV_VAR_FILE_EMTPY("env.var.file.empty", "13"),
	ENV_VAR_FILE_NOT_FOUND("env.var.file.not.found", "14"),
	ENV_VAR_FILE_NOT_SET("env.var.file.not.set", "15"),
	ENV_VAR_FILE_TOO_BIG("env.var.file.too.big", "28"),
	ENV_VAR_FILE_INACCESSIBLE("env.var.file.inaccessible", "29"),
	
	// CMLS-1056
	INIT_INVALID_TOKEN("init.invalid.token","31"),
	INIT_INVALID_URL("init.invalid.url","32"),
	INIT_FAILED("init.failed","33"),
	INVALID_CONFIG("configuration.invalid","34"),
	INIT_FILE_PERM_FAILED("init.file.permission.failed","35"),
	NOT_FOUND_CONFIG("configuration.not.found","36"),
	HELP_LOAD_ERROR("help.file.access.error","37"),
	INIT_SYNTAX_ERROR("init.syntax.error", "38"),
	CLI_CONNECTION_TIMEOUT("cli.connection.timeout", "39"), 
	INIT_PROMPT("init.prompt","40"),
	UPLOAD_FAILED_READ("upload.failed.no.file","41"),
	UPLOAD_FAILED_ACCESS("upload.failed.no.permission","42"),
	UPLOAD_FAILED_NETWORK("upload.failed.no.network","43"),
	UPLOAD_FAILED_UNKNOWN("upload.failed.unknown","44");
	
	private String value;
	private String code;
	
	private static final Map<String,MessageCodes> enums = new TreeMap<>();
	
	private MessageCodes(String value, String code) {
		this.value = value;
		this.code = code;
	}
	
	public String getValue() {
		return this.value;
	}

	public String getCode() {
		return this.code;
	}
	
	public static MessageCodes get(String key) {
		MessageCodes result = enums.get(key);
		if(result == null) {
			 throw new IllegalArgumentException("Invalid message code: " + key);
		}
		return result;
	}
	
	static {
		for(MessageCodes mc : values()) {
			enums.put(mc.value, mc);
		}
	}
	
}
