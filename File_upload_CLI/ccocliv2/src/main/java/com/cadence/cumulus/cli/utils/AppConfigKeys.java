package com.cadence.cumulus.cli.utils;

public interface AppConfigKeys {

	public static final String CLI_TOKEN_HEADER_NAME = "cli.token.header.name";
	public static final String CLI_SESSION_HEADER_NAME = "cli.session.header.name";
	public static final String CLI_RETRY_INTERVAL_SECONDS = "cli.retry.interval.seconds";
	public static final String CLI_RETRY_ATTEMPTS = "cli.retry.attempts";
	public static final String CLI_UPLOAD_FILE_CHUNK_SIZE = "cli.upload.file.chunk.size";
	public static final String CHAMBER_CONNECTION_TIMEOUT = "chamber.connection.timeout";
	public static final String CHAMBER_RESPONSE_TIMEOUT = "chamber.response.timeout";
	public static final String CHAMBER_REQUEST_TIMEOUT = "chamber.request.timeout";
	public static final String CHAMBER_TOKEN_IN_VARIABLE = "chamber.token.in.variable";
	public static final String CHAMBER_URL_IN_VARIABLE = "chamber.url.in.variable";
	public static final String CCO_CONF_FILE = "cli.configuration.name";
	public static final String CCO_BASE_DIR = "cli.base";
	public static final String UPLOAD_BASE_ENDPOINT = "cli.upload.api.base";
	public static final String UPLOAD_API_VALIDATE = "cli.upload.api.validate";
	public static final String UPLOAD_API_INFO = "cli.upload.api.info";
	public static final String UPLOAD_API_SAVE = "cli.upload.api.save";
	public static final String UPLOAD_API_PROCESS = "cli.upload.api.process";
	public static final String UPLOAD_API_COMPLETE = "cli.upload.api.complete";
	public static final String MESSAGES_PROPERTIES = "messages.properties";
	public static final String APPLICATION_PROPERTIES = "application.properties";
	public static final String UPLOAD_ERROR = "cli.upload.error";
	public static final String URL_FORMAT = "chamber.url.format";
	public static final String ARG_FORMAT = "cli.cmd.arg.format";
	public static final String TOKEN_FORMAT = "chamber.token.format";
	public static final String USERNAME = "chamber.request.username";
	public static final String CONF_FILE_PERM = "cli.configuration.permission";
	public static final String VERSION_FORMAT = "cli.version.format";
	
}
