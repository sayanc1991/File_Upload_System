package com.cadence.cumulus.cli.commands;

import java.util.ArrayList;
import java.util.List;

public class Upload {
	
	
	private String destination;
	private List<String> sources;
	
	public Upload() {
		this.destination = "";
		this.sources = new ArrayList<>();
	}

	public String getDestination() {
		return destination;
	}

	public void setDestination(String destination) {
		this.destination = destination;
	}

	public List<String> getSources() {
		return sources;
	}

	public void setSources(List<String> sources) {
		this.sources = sources;
	}

	@Override
	public String toString() {
		return this.getClass().getSimpleName() + " {destination=" + destination + ", sources=" + sources + "}";
	}

	
}
