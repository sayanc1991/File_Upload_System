package com.cadence.cumulus.cli.service.impl;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigInteger;
import java.net.ConnectException;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.net.URISyntaxException;
import java.net.UnknownHostException;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.NoSuchFileException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;
import java.text.DecimalFormat;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionException;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.conn.ConnectTimeoutException;
import org.apache.http.entity.ByteArrayEntity;
import org.apache.http.util.EntityUtils;
import org.apache.log4j.Logger;

import com.cadence.cumulus.cli.commands.Upload;
import com.cadence.cumulus.cli.dto.ErrorMetadata;
import com.cadence.cumulus.cli.dto.ExceptionMetadata;
import com.cadence.cumulus.cli.model.ChunkUploadRequest;
import com.cadence.cumulus.cli.model.DestinationValidation;
import com.cadence.cumulus.cli.model.FileInfoResponse;
import com.cadence.cumulus.cli.model.PathWrapper;
import com.cadence.cumulus.cli.model.ResponseMetadata;
import com.cadence.cumulus.cli.model.ResponseVO;
import com.cadence.cumulus.cli.model.UploadRequest;
import com.cadence.cumulus.cli.utils.AppConfigKeys;
import com.cadence.cumulus.cli.utils.AppUtils;
import com.cadence.cumulus.cli.utils.CLIException;
import com.cadence.cumulus.cli.utils.DisplayUtility;
import com.cadence.cumulus.cli.utils.MessageCodes;
import com.fasterxml.jackson.databind.ObjectMapper;

public class UploadClientImpl extends RestClientImpl  implements AppConfigKeys {

	private static String instance;
	private static Upload cmdParams;
	private static List<PathWrapper> sourceFiles;
	private static ExecutorService executorService;
	
	private final static Logger LOG = Logger.getLogger(UploadClientImpl.class);

	static {
		instance = System.getProperty(CHAMBER_URL_IN_VARIABLE);
		sourceFiles = new ArrayList<>();
		executorService = Executors.newFixedThreadPool(2);
	}

	private static Boolean isUploadPossible() throws InterruptedException, ExecutionException, IOException {
		Boolean isUploadable = false;
		CompletableFuture<Boolean> validateSources = CompletableFuture.supplyAsync(() -> {
			Integer noOfSources = readSources(cmdParams.getSources());
			Boolean isValid = noOfSources > 0;
			return isValid;
		}, executorService);
		CompletableFuture<Boolean> validateDestination = CompletableFuture.supplyAsync(() -> {
			Boolean isValid = false;
			try {
				DestinationValidation validationResponse = verifyDestinationPath(cmdParams.getDestination());
				if (validationResponse != null) {
					if (validationResponse.getValidationResult()) {
						isValid = true;
					} else {
						LOG.info("Destination: " + cmdParams.getDestination() + " validation failed: "
								+ validationResponse.getValidationError());
						DisplayUtility.displayOutput(validationResponse.getValidationError());
					}
				}
			}catch (UnknownHostException | SocketException | SocketTimeoutException | ConnectTimeoutException e) {
				DisplayUtility.displayOutput(DisplayUtility.formatError(MessageCodes.CLI_CONNECTION_TIMEOUT, System.getProperty(CHAMBER_URL_IN_VARIABLE)));
			}catch (IOException e) {
				LOG.error("Throwing completion exception: ", e);
				throw new CompletionException(e);
			}
			return isValid;
		}, executorService);
		Boolean sourcesValid = validateSources.get();
		if (sourcesValid) {
			isUploadable = validateDestination.get();
		} else {
			validateDestination.cancel(true);
		}
		return isUploadable;
	}

	public static void startUpload(Upload upload) throws CLIException {
		cmdParams = upload;
		String message = "";
		try {
			if (isUploadPossible()) {
				LOG.info("Destination: " + cmdParams.getDestination() + " is present in server and accessible");
				String result = null;
				String outMsg = "";
				for (PathWrapper pw : sourceFiles) {
					if(!"".equals(outMsg)) {
						clearConsole(outMsg);
					}
					String fileName = pw.getFilePath().toFile().getName();
					int i = 0;
					Exception cause = null;
					String reason = "";
					while (i < Integer.getInteger(CLI_RETRY_ATTEMPTS)) {
						try {
							result = upload(pw);
							if (!System.getProperty(UPLOAD_ERROR).equals(result)) {
								DisplayUtility.displayOutput(result);
							}
							cause = null;
							break;
						} catch (IOException e) {
							if(e instanceof NoSuchFileException) {
								reason = AppUtils.getMessage(MessageCodes.UPLOAD_FAILED_READ);
							} else if(e instanceof FileNotFoundException) {
								reason = AppUtils.getMessage(MessageCodes.UPLOAD_FAILED_ACCESS);
							} else if(e instanceof SocketException || e instanceof ConnectException || e instanceof UnknownHostException) {
								reason = AppUtils.getMessage(MessageCodes.UPLOAD_FAILED_NETWORK);
							} else {
								reason = AppUtils.getMessage(MessageCodes.UPLOAD_FAILED_UNKNOWN);
							}
							LOG.info("Retry attempt: " + i + " for :" + reason);
							LOG.error("Error while uploading: " + pw.getFilePath(), e);
							String retyWaitMsg = "";
							if (i > 0) {
								try {
									message = "\r" + String.join("", Collections.nCopies(message.length(), " "));
									LOG.info(message);
									System.out.print("\b");
							        System.out.flush();
							        retyWaitMsg = DisplayUtility.displayRetryWaitOnSameLine(MessageCodes.RETRY, i,MessageCodes.RETRY_WAIT,
											Integer.getInteger(CLI_RETRY_ATTEMPTS));
							        Integer interval = Integer.getInteger(CLI_RETRY_INTERVAL_SECONDS);
									Thread.sleep(interval * 1000);
									LOG.info("Encountered : " + MessageCodes.RETRY_WAIT.getCode());
								} catch (InterruptedException ie) {
									ExceptionMetadata meta = new ExceptionMetadata(MessageCodes.RETRY_FAILED, ie);
									LOG.error("Encountered : " + MessageCodes.RETRY_FAILED.getCode());
									throw new CLIException(DisplayUtility.formatException(meta),
											meta.getMessageCode().getCode());
								}
							}
							clearConsole(retyWaitMsg);
							LOG.info(
									DisplayUtility.formatError(MessageCodes.UPLOAD_FAILED, fileName, e.getMessage()));
							LOG.error("Encountered : " + MessageCodes.UPLOAD_FAILED.getCode());
							DisplayUtility.displayRetry(MessageCodes.RETRY, ++i, Integer.getInteger(CLI_RETRY_ATTEMPTS));
							LOG.info("Encountered : " + MessageCodes.RETRY.getCode());
							Thread.sleep(500);
							LOG.info("Retry attempt: " + i + "  out of: " + Integer.getInteger(CLI_RETRY_ATTEMPTS));
							cause = e;
						}
					}
					if (cause != null) {
						message = "\r" + String.join("", Collections.nCopies(message.length(), " "));
						System.out.write(message.getBytes());
						String dirFilePath = relativizePath(pw); 
						outMsg = DisplayUtility.displayOnSameLine(MessageCodes.RETRY_FAILED, pw.getFilePath());
						Integer interval = Integer.getInteger(CLI_RETRY_INTERVAL_SECONDS);
						Thread.sleep(interval * 1000);
						LOG.info("Retry failed for uploading file: " + pw.getFilePath());
						clearConsole(outMsg);
						//DisplayUtility.displayOutput("Failed to upload " + dirFilePath);
						message = DisplayUtility.formatError(MessageCodes.UPLOAD_FAILED, dirFilePath, reason);
						System.out.println(message);
					}
					if (System.getProperty(UPLOAD_ERROR).equals(result)) {
						break;
					}
				}
				if (!System.getProperty(UPLOAD_ERROR).equals(result)) {
					Boolean uploadStatus = completeUpload();
					if (null != uploadStatus) {
						LOG.info("Upload operation completed for " + sourceFiles.size() + " files successfully: "
								+ uploadStatus);
					}
				}else {
					message = DisplayUtility.formatError(MessageCodes.SERVER_ERROR);
					System.out.println(message);
					LOG.error("Encountered : " + MessageCodes.SERVER_ERROR.getCode());
				}
			}
		} 
		/**
		 * TODO anirban to uncomment the below code later to catch timeout exceptions and render them
		 * appropriately when tapas has finished determining the values 
		 */
		/* catch(UnknownHostException | SocketException | SocketTimeoutException | ConnectTimeoutException e) {
			throw e;
		} */catch (CompletionException | IOException | InterruptedException | ExecutionException e) {
			if(!(e instanceof SocketException || e instanceof ConnectException || e instanceof UnknownHostException)) {
				LOG.error("Error executing upload: ", e);
				Throwable throwable = e.getCause();
				if(null == throwable) {
					throwable = e;
				}
				CLIException clie = DisplayUtility.formatException(MessageCodes.CLI_ERROR, throwable);
				LOG.error("Encountered : " + MessageCodes.CLI_ERROR.getCode());
				throw clie;
			}
		} finally {
			executorService.shutdown();
		}
	}
	
	/*private static String getParentDirPath(String fileOrDirPath) {
	    boolean endsWithSlash = fileOrDirPath.endsWith(File.separator);
	    return fileOrDirPath.substring(0, fileOrDirPath.lastIndexOf(File.separatorChar, 
	            endsWithSlash ? fileOrDirPath.length() - 2 : fileOrDirPath.length() - 1));
	}*/
	
	private static void clearConsole(String msg) {
		for(int j = 0; j< msg.length(); j++) {
			System.out.print("\b \b");
		}
	}

	private static String upload(PathWrapper resource) throws IOException {
		LOG.info("Inside upload...");
		String message = "";
		Long fileSize = Files.size(resource.getFilePath());
		String fileName = relativizePath(resource);
		UploadRequest ur = new UploadRequest();
		ur.setDestination(cmdParams.getDestination());
		ur.setFilename(fileName);
		ur.setLastModified(resource.getFilePath().toFile().lastModified());
		FileInfoResponse info = fileInfo(ur);
		if (info != null) {
			// completely uploaded file (consider timestamp - ASK),
			LOG.info("File to be uploaded: " + resource.getFilePath());
			if (info.getOutputLastModifiedSame()) {
				message = DisplayUtility.formatError(MessageCodes.UPLOAD_EXISTS, fileName);
				LOG.info("Encountered: " + MessageCodes.UPLOAD_EXISTS.getCode() + " : " + fileName);
			} else {
				// comment out below line to opt out of mock file io exception
				// throw new IOException("Mock exception");
				//InputStream is = Files.newInputStream(resource.getFilePath());
				InputStream is = new FileInputStream(resource.getFilePath().toString());
				// DisplayUtility.displayOutput("Size of file to be uploaded: " + fileSize + "
				// bytes");
				Integer bytesRead = 0;
				Long totalBytesRead = 0l;
				byte[] chunk = new byte[Integer.getInteger(CLI_UPLOAD_FILE_CHUNK_SIZE)];
				// new file upload
				// partially uploaded file
				if (info.getUploadFileSize() != BigInteger.ZERO) {
					Long n = info.getUploadFileSize().longValue();
					message = DisplayUtility.displayOnSameLine(MessageCodes.UPLOAD_RESUME, fileName);
					LOG.error("Encountered : " + MessageCodes.UPLOAD_RESUME.getCode());
					DisplayUtility.displayOutput("");
					totalBytesRead = is.skip(n);
					LOG.info("Resuming file: " + resource.getFilePath() + " upload from: {} bytes" + totalBytesRead);
				}
				// begin upload
				LocalDateTime start = LocalDateTime.now();
				boolean isError = false;
				do {
					bytesRead = is.read(chunk);
					totalBytesRead = totalBytesRead + bytesRead;
					LOG.info("Uploading " + resource.getFilePath() + " bytes: " + totalBytesRead);
					ChunkUploadRequest request = new ChunkUploadRequest();
					request.setDestination(ur.getDestination());
					request.setFilename(fileName);
					request.setSize(bytesRead < 0 ? 0 : bytesRead);
					request.setLastSize(bytesRead == fileSize.intValue() ? 0
							: totalBytesRead == fileSize.intValue() ? bytesRead : 0);
					isError = uploadFileChunk(request, new ByteArrayEntity(chunk));
					if (isError) {
						break;
					}
					Double uploadPercentage = (double) totalBytesRead / fileSize * 100;
					String formattedUploadPercentage = formatToTwodecimals(uploadPercentage);
					message = DisplayUtility.displayOnSameLine(MessageCodes.UPLOAD_PROGRESS, fileName,
							uploadPercentage);
					LOG.error("Encountered : " + MessageCodes.UPLOAD_PROGRESS.getCode());
					LOG.info("Uploaded " + resource.getFilePath() + " progress: " + formattedUploadPercentage + " %");
				} while (bytesRead > -1 && totalBytesRead < fileSize);

				if (isError) {
					is.close();
					return System.getProperty(UPLOAD_ERROR);
				}
				LocalDateTime end = LocalDateTime.now();
				Long uploadTimeSeconds = start.until(end, ChronoUnit.SECONDS);
				message = "\r" + String.join("", Collections.nCopies(message.length(), " "));
				System.out.write(message.getBytes());
				ur.setAction("rename");
				isError = processUpload(ur);
				if (isError) {
					is.close();
					return System.getProperty(UPLOAD_ERROR);
				}
				message = DisplayUtility.formatError(MessageCodes.UPLOAD_COMPLETED, fileName);
				LOG.info("Encountered : " + MessageCodes.UPLOAD_COMPLETED.getCode());
				LOG.info("uploadTime: " + uploadTimeSeconds + " " + ChronoUnit.SECONDS + " for file: "
						+ resource.getFilePath());
				is.close();
			}
		} else {
			message = System.getProperty(UPLOAD_ERROR);
		}
		return message;
	}

	private static String formatToTwodecimals(Double value) {
		DecimalFormat df = new DecimalFormat(".##");
		String formattedPercent = df.format(value);
		return formattedPercent;
	}

	private static String relativizePath(PathWrapper resource) {
		String fileName = resource.getFilePath().getFileName().toString();
		if (resource.getParentDir() != null) {
			Path parentDir = resource.getParentDir();
			Path superParentDir = parentDir.getParent();
			Path relativePath = superParentDir != null ? superParentDir.relativize(resource.getFilePath()) : resource.getFilePath();
			fileName = relativePath.toString();
			fileName = AppUtils.separatorsToUnix(fileName);
		}
		return fileName;
	}

	private static Integer readSources(List<String> sources) {
		Integer folders = 0, files = 0;
		for (String src : sources) {
			Path resourcePath = Paths.get(src);
			if (Files.exists(resourcePath)) {
				if (Files.isDirectory(resourcePath)) {
					try {
						Files.walkFileTree(resourcePath, new SimpleFileVisitor<Path>() {
							@Override
							public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
								if (!attrs.isDirectory()) {
									PathWrapper pw = new PathWrapper(file);
									pw.setParentDir(resourcePath);
									sourceFiles.add(pw);
								}
								return FileVisitResult.CONTINUE;
							}
						});
						folders++;
					} catch (IOException e) {
						LOG.error("Error accessing source: " + e);
						System.out.println(DisplayUtility.formatError(MessageCodes.ERROR_UPLOAD_SOURCE, src));
						LOG.error("Encountered while reading source: " + MessageCodes.ERROR_UPLOAD_SOURCE.getCode());
					}
				} else if (Files.isRegularFile(resourcePath)) {
					PathWrapper pw = new PathWrapper(resourcePath);
					sourceFiles.add(pw);
					files++;
				}
			} else {
				System.out.println(DisplayUtility.formatError(MessageCodes.UNDEFINED_UPLOAD_SOURCE, src));
				LOG.error("Encountered: " + MessageCodes.UNDEFINED_UPLOAD_SOURCE.getCode() + " for: " + src);
			}
		}
		LOG.info("Total source files read: " + sourceFiles.size() + ". Individual files: " + files
				+ ", Individual folders: " + folders);
		if (sourceFiles.isEmpty()) {
			DisplayUtility.displayOutput(DisplayUtility.formatError(MessageCodes.EXHAUSTED_UPLOAD_SOURCE));
			LOG.info("Encountered before exiting readSources: " + MessageCodes.EXHAUSTED_UPLOAD_SOURCE.getCode());
		}
		LOG.info("Exiting readSources");
		return sourceFiles.size();
	}

	private static DestinationValidation verifyDestinationPath(String destinationPath) throws IOException {

		DestinationValidation validation = null;
		ResponseVO responseVO = null;
		String uri = instance + System.getProperty(UPLOAD_BASE_ENDPOINT) + System.getProperty(UPLOAD_API_VALIDATE)
				+ (AppUtils.hasLength(destinationPath) ? "/" + destinationPath : "");
		LOG.info("URI: " + uri);
		Object responseObj = null;
		try {
			responseObj = get(uri, null, DestinationValidation.class);
		} catch (URISyntaxException e) {
			LOG.error("Error in URI syntax : ", e);
			ErrorMetadata ef = new ErrorMetadata(MessageCodes.UNRECOGNIZED_COMMAND, e.getMessage());
			System.out.println(DisplayUtility.formatErrorWithHelp(ef));
		}
		HttpResponse response = null;
		if (null != responseObj && responseObj instanceof HttpResponse) {
			response = (HttpResponse) responseObj;

			HttpEntity entity = response.getEntity();
			int status = response.getStatusLine().getStatusCode();
			if (status == HttpStatus.SC_NON_AUTHORITATIVE_INFORMATION) {
				responseVO = new ObjectMapper().readValue(EntityUtils.toString(entity), ResponseVO.class);
				validation = new DestinationValidation();
				validation.setValidationError(responseVO.getValue());
				validation.setValidationResult(false);
			} else {
				validation = new ObjectMapper().readValue(EntityUtils.toString(entity), DestinationValidation.class);
			}
		}
		return validation;

	}

	private static FileInfoResponse fileInfo(UploadRequest request) throws ClientProtocolException, IOException {
		FileInfoResponse info = null;
		String uri = instance + System.getProperty(UPLOAD_BASE_ENDPOINT) + System.getProperty(UPLOAD_API_INFO) + request.toString();
		Object responseObj = null;
		try {
			responseObj = get(uri, null, FileInfoResponse.class);
		} catch (URISyntaxException e) {
			ErrorMetadata ef = new ErrorMetadata(MessageCodes.UNRECOGNIZED_COMMAND, e.getMessage());
			DisplayUtility.formatErrorWithHelp(ef);
		}
		HttpResponse response = null;
		if (null != responseObj && responseObj instanceof HttpResponse) {
			response = (HttpResponse) responseObj;

			HttpEntity entity = response.getEntity();
			info = new ObjectMapper().readValue(EntityUtils.toString(entity), FileInfoResponse.class);
		}
		return info;
	}

	private static Boolean uploadFileChunk(ChunkUploadRequest request, ByteArrayEntity payload) throws IOException {
		String uri = instance + System.getProperty(UPLOAD_BASE_ENDPOINT) + System.getProperty(UPLOAD_API_SAVE) + request.toString();
		HttpResponse responseObj = null;
		try {
			responseObj = (HttpResponse) post(uri, payload, Void.class);
		} catch (URISyntaxException e) {
			ErrorMetadata ef = new ErrorMetadata(MessageCodes.UNRECOGNIZED_COMMAND, e.getMessage());
			DisplayUtility.formatErrorWithHelp(ef);
		}
		ResponseMetadata metadata = null;
		if (null != responseObj) {
			metadata = formatResponse(responseObj);

			if (metadata.getError()) {
				DisplayUtility.displayOutput(metadata.getMessage());
			}
		} else {
			metadata = new ResponseMetadata();
			metadata.setError(true);
		}
		return metadata.getError();
	}

	private static ResponseMetadata formatResponse(HttpResponse response) {
		ResponseMetadata metadata = new ResponseMetadata();
		Boolean error = isErrorResponse(response);
		metadata.setError(error);
		LOG.info("Processable response from server to client: {}" + metadata);
		return metadata;
	}

	private static Boolean isErrorResponse(HttpResponse response) {
		int statusCode = response.getStatusLine().getStatusCode();
		Boolean isError = (statusCode >= 200 && statusCode <= 300) ? false : true;
		LOG.info("Response is erronous: {}" + isError);
		return isError;
	}

	private static Boolean processUpload(UploadRequest request) throws ClientProtocolException, IOException {
		String uri = instance + System.getProperty(UPLOAD_BASE_ENDPOINT) + System.getProperty(UPLOAD_API_PROCESS) + request.toString();
		Object responseObj = null;
		try {
			responseObj = get(uri, null, Void.class);
		} catch (URISyntaxException e) {
			ErrorMetadata ef = new ErrorMetadata(MessageCodes.UNRECOGNIZED_COMMAND, e.getMessage());
			DisplayUtility.formatErrorWithHelp(ef);
		}
		ResponseMetadata metadata = null;
		HttpResponse response = null;
		if (null != responseObj && responseObj instanceof HttpResponse) {
			response = (HttpResponse) responseObj;

			HttpEntity entity = response.getEntity();
			if (null != entity) {
				metadata = new ObjectMapper().readValue(EntityUtils.toString(entity), ResponseMetadata.class);
			} else {
				metadata = new ResponseMetadata();
				metadata.setError(false);
			}
			if (metadata.getError()) {
				DisplayUtility.displayOutput(metadata.getMessage());
			}
		} else {
			metadata = new ResponseMetadata();
			metadata.setError(true);
		}
		return metadata.getError();
	}

	private static Boolean completeUpload() throws ClientProtocolException, IOException {
		Boolean status = null;
		String uri = instance + System.getProperty(UPLOAD_BASE_ENDPOINT) + System.getProperty(UPLOAD_API_COMPLETE);
		Object responseObj = null;
		try {
			responseObj = get(uri, null, Boolean.class);
		} catch (URISyntaxException e) {
			ErrorMetadata ef = new ErrorMetadata(MessageCodes.UNRECOGNIZED_COMMAND, e.getMessage());
			DisplayUtility.formatErrorWithHelp(ef);
		}
		HttpResponse response = null;
		if (null != responseObj && responseObj instanceof HttpResponse) {
			response = (HttpResponse) responseObj;

			ResponseMetadata metadata = formatResponse(response);
			if (metadata.getError()) {
				status = Boolean.valueOf(metadata.getMessage());
			} else {
				status = (Boolean) metadata.getBody();
			}
		}
		return status;
	}

}
