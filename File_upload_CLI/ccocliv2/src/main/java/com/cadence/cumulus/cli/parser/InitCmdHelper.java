package com.cadence.cumulus.cli.parser;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.log4j.Logger;

import com.cadence.cumulus.cli.CLIConfiguration;
import com.cadence.cumulus.cli.commands.Init;
import com.cadence.cumulus.cli.dto.ErrorMetadata;
import com.cadence.cumulus.cli.utils.AppConfigKeys;
import com.cadence.cumulus.cli.utils.DisplayUtility;
import com.cadence.cumulus.cli.utils.MessageCodes;
import com.cadence.cumulus.cli.utils.AppUtils;
import com.cadence.cumulus.cli.utils.CLIException;

public class InitCmdHelper implements AppConfigKeys {
	
	private static final Logger LOG = Logger.getLogger(InitCmdHelper.class);
	private static final String TOKEN_SWITCH = "t";
	private static final String URL_SWITCH = "u";
	private static Map<String, List<String>> args;
	
	// TODO use message key token.generate.unexpected.user
	
	private static String validate() {
		// TODO Auto-generated method stub
		ErrorMetadata ef = new ErrorMetadata(MessageCodes.OPTION_UNEXPECTED_VALUE);
		List<String> options = new ArrayList<>(args.keySet());
		if(options.isEmpty()) {
			ef.setValid(true);
		} else if(options.size() > 1) {
			ef.setMessageCode(MessageCodes.INIT_SYNTAX_ERROR);
			ef.setPlaceHolders(args.keySet());
		} else {
			String option = options.get(0);
			LOG.info("Validating command option received: " + option);
			switch (option) {
			case TOKEN_SWITCH:
				validateArgumentValueFor(TOKEN_SWITCH, ef);
				break;
			case URL_SWITCH:
				validateArgumentValueFor(URL_SWITCH, ef);
				break;
			default:
				Set<String> argKeys = args.keySet();
				List<String> allowedKeys = Arrays.asList(TOKEN_SWITCH, URL_SWITCH);
				argKeys.removeAll(allowedKeys);
				ef = new ErrorMetadata(MessageCodes.TOO_MANY_OPTIONS, argKeys);
				LOG.info("Unrecognized option values: " + argKeys);
				LOG.error("Encountered " + ef.getMessageCode().getCode());
				break;
			}
		}
		String message = DisplayUtility.formatErrorWithInit(ef);
		return message;
	}
	
	private static void validateArgumentValueFor(String key, ErrorMetadata ef) {
		List<String> values = args.get(key);
		if (values != null) {
			ef.setPlaceHolders(key);
			LOG.info("Multiple " + key + " entries specified: " + values);
		} else {
			try {
				CLIConfiguration.load();
				ef.setValid(true);
				LOG.info(key + " value to be set: " + values);
			} catch (CLIException e) {
				ef.setMessageCode(e.getCode());
			}
		}
	}
	
	public static Init convert(Map<String, List<String>> args) {
		// TODO Auto-generated method stub
		Init payload = null;
		InitCmdHelper.args = args;
		String message = validate();
		if(AppUtils.hasLength(message)) {
			System.out.println(message);
			LOG.info("Init syntax validation failed: " + message);
		} else {
			payload = new Init();
			for (String option : args.keySet()) {
				switch (option) {
				case TOKEN_SWITCH:
					payload.setToken(true);
					break;
				case URL_SWITCH:
					payload.setUrl(true);
					break;
				}
			}
			LOG.info("Init to be executed for switches: " + args.keySet());
		}
		return payload;
	}

}
