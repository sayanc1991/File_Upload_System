package com.cadence.cumulus.cli;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URISyntaxException;

import org.apache.log4j.Logger;

import com.cadence.cumulus.cli.commands.Help;
import com.cadence.cumulus.cli.commands.Init;
import com.cadence.cumulus.cli.commands.Upload;
import com.cadence.cumulus.cli.commands.Version;
import com.cadence.cumulus.cli.dto.ApplicationCommand;
import com.cadence.cumulus.cli.parser.CmdHelper;
import com.cadence.cumulus.cli.parser.HelpCmdHelper;
import com.cadence.cumulus.cli.parser.InitCmdHelper;
import com.cadence.cumulus.cli.parser.UploadCmdHelper;
import com.cadence.cumulus.cli.parser.VersionCmdHelper;
import com.cadence.cumulus.cli.service.impl.RestClientImpl;
import com.cadence.cumulus.cli.service.impl.UploadClientImpl;
import com.cadence.cumulus.cli.utils.AppConfigKeys;
import com.cadence.cumulus.cli.utils.AppUtils;
import com.cadence.cumulus.cli.utils.CLIException;
import com.cadence.cumulus.cli.utils.CommandNames;
import com.cadence.cumulus.cli.utils.DisplayUtility;
import com.cadence.cumulus.cli.utils.MessageCodes;

public class CLIApplication implements AppConfigKeys{
	private final static Logger LOG = Logger.getLogger(CLIApplication.class);

	private ApplicationCommand appCmd;

	public CLIApplication(ApplicationCommand appCmd) {
		this.appCmd = appCmd;
	}

	public static void main(String[] args) {
		try {
			AppUtils.init();
			ApplicationCommand appCmd = CmdHelper.parse(args);
			// check if returned command is secure or not
			CommandNames cmd = appCmd.getCmd();
			CLIApplication cliApp = new CLIApplication(appCmd);
			if (cmd.isSecure()) {
				cliApp.executeSecureCmd();
			} else {
				cliApp.executeNormalCmd();
			}
		} catch (CLIException e) {
			System.out.println(e.getMessage());
		} /*catch (IOException e) {
			LOG.error("Unexpected error);
			DisplayUtility.displayOutput(DisplayUtility.formatError(MessageCodes.SERVER_ERROR));
		}*/
	}

	private void executeNormalCmd() throws CLIException {
		switch (appCmd.getCmd()) {
		case HELP:
			displayHelp();
			break;
		case TOKEN:
			configureSystem();
			break;
		case VERSION:
			displayVersion();
			break;
		default:
			// no commands found
			break;
		}
	}

	private void executeSecureCmd() throws CLIException {
		// TODO - spawn two threads from daemon thread to run concurrently
		/*
		 * ExecutorService executorService = Executors.newFixedThreadPool(2);
		 * CompletableFuture<Boolean> loadConfiguration =
		 * CompletableFuture.supplyAsync(() -> { Boolean loaded = false; try {
		 * AppConfig.load(); loaded = true; } catch (CLIException e) { throw new
		 * CompletionException(e); } return loaded; }, executorService);
		 * executorService.shutdown();
		 */
		CLIConfiguration.load();
		RestClientImpl.init();
		switch (appCmd.getCmd()) {
		case UPLOAD:
			executeUpload();
			break;
		default:
			// no commands found
			break;
		}
		RestClientImpl.destroy();
	}

	private void displayHelp() throws CLIException {
		// TODO Auto-generated method stub
		try {
			Help help = HelpCmdHelper.convert(appCmd.getArguments());
			if (help != null) {
				InputStream is = this.getClass().getClassLoader().getResourceAsStream(help.getManualName());
				BufferedReader br = new BufferedReader(new InputStreamReader(is));
				String line = "";
				while((line = br.readLine()) != null) {
					System.out.println(line);
				}
				is.close();
			} // else help validation failed
		} catch (IOException | URISyntaxException e) {
			LOG.error("Error loading manual content: {}", e);
			CLIException clie = DisplayUtility.formatException(MessageCodes.HELP_LOAD_ERROR, e);
			LOG.error("Encountered: " + MessageCodes.CLI_ERROR.getCode() + " : " + clie);
			throw clie;
		}
	}
		
	private void displayVersion() {
		Version version = VersionCmdHelper.convert(appCmd.getArguments());
		if (version != null) {
			Integer buildNumber = Integer.getInteger(version.getBuildKey());
			//String buildNumber = System.getProperty(version.getBuildKey());
			String versionNumber = System.getProperty(version.getValue());
			String template = System.getProperty(VERSION_FORMAT);
			String output = String.format(template, versionNumber, buildNumber);
			System.out.println(output);
		} // else version validation failed
	}

	private void executeUpload() throws CLIException {
		Upload upload = UploadCmdHelper.convert(appCmd.getArguments());
		if(upload != null) {
			CLIConfiguration.load();
			UploadClientImpl.startUpload(upload);
		}
	}

	private String readToken() throws IOException {
		LOG.info("Reading token from user");
		String token = AppUtils.readFromConsole("Enter your token: ");
		if (token == null) {
			token = "";
		} else if (!token.matches(System.getProperty(TOKEN_FORMAT))) {
			String message = AppUtils.getMessage(MessageCodes.INIT_INVALID_TOKEN);
			System.out.write(message.getBytes());
			System.out.println();
			token = "";
			LOG.info("Token is invalid against: " + System.getProperty(TOKEN_FORMAT));
		} else {
			LOG.info("Token is valid");
		}
		return token;
	}

	private String readURL() throws IOException {
		LOG.info("Reading base URL from user");
		String url = AppUtils.readFromConsole("Enter base URL: ");
		if (url == null) {
			url = "";
		} else if (!url.matches(System.getProperty(URL_FORMAT))) {
			String message = AppUtils.getMessage(MessageCodes.INIT_INVALID_URL);
			System.out.write(message.getBytes());
			System.out.println();
			LOG.info("Chamber base URL is invalid against: " + System.getProperty(URL_FORMAT));
			url = "";
		} else {
			LOG.info("Chamber base URL is valid");
		}
		return url;
	}	

	private void configureSystem() throws CLIException {
		Init init = InitCmdHelper.convert(appCmd.getArguments());
		if (init != null) {
			LOG.info("Init command syntax is valid");
			String token = "", url = "";
			try {
				if ((init.isToken() && init.isUrl()) || (!init.isToken() && !init.isUrl())) {
					LOG.info("Init command to be executed for both token and URL");
					token = readToken();
					if (AppUtils.hasLength(token)) {
						url = readURL();
						if (AppUtils.hasLength(url)) 
							CLIConfiguration.setAll(token, url);
					}
				} else if (init.isToken() && !init.isUrl()) {
					LOG.info("Init command to be executed for only token");
					token = readToken();
					if (AppUtils.hasLength(token)) 
						CLIConfiguration.setToken(token);
				} else if (!init.isToken() && init.isUrl()) {
					LOG.info("Init command to be executed for only URL");
					url = readURL();
					if (AppUtils.hasLength(url)) 
						CLIConfiguration.setURL(url);
				}
			} catch (IOException e) {
				// TODO Auto-generated catch block
				LOG.error("Error reading CLI configuration from console: {}", e);
				CLIException clie = DisplayUtility.formatException(MessageCodes.INIT_FAILED, e);
				LOG.error("Encountered: " + MessageCodes.INIT_FAILED.getCode() + " : " + e);
				throw clie;
			}
		}

	}

}
