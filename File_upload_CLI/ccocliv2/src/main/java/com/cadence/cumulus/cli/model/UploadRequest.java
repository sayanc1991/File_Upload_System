package com.cadence.cumulus.cli.model;

import java.util.StringJoiner;

import com.cadence.cumulus.cli.utils.AppUtils;


public class UploadRequest {

	private String filename, destination, action;
	private Long lastModified;

	public String getFilename() {
		return filename;
	}

	public void setFilename(String filename) {
		this.filename = filename;
	}

	public String getDestination() {
		return destination;
	}

	public void setDestination(String destination) {
		this.destination = destination;
	}

	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}

	@Override
	public String toString() {
		StringJoiner sj = new StringJoiner("&");
		if(lastModified != null) sj = sj.add("lastModified=" + lastModified);
		String requestQuery = sj.toString();
		requestQuery = AppUtils.hasLength(requestQuery) ? "?".concat(requestQuery) : requestQuery;
		requestQuery = destination + "/" + filename + requestQuery;
		if(AppUtils.hasLength(action)) requestQuery = action + "/" + requestQuery;
		requestQuery = "/" + requestQuery;
		return requestQuery;
	}

	public Long getLastModified() {
		return lastModified;
	}

	public void setLastModified(Long lastModified) {
		this.lastModified = lastModified;
	}
	
}
