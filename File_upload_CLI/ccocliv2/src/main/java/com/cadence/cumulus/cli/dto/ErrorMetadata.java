package com.cadence.cumulus.cli.dto;

import java.util.Arrays;

import com.cadence.cumulus.cli.utils.MessageCodes;

public class ErrorMetadata {
	
	protected Boolean valid;
	protected Object[] placeHolders;
	protected MessageCodes messageCode;
	
	
	public MessageCodes getMessageCode() {
		return messageCode;
	}
	public void setMessageCode(MessageCodes messageCode) {
		this.messageCode = messageCode;
	}
	public ErrorMetadata(MessageCodes code, Boolean valid) {
		this.messageCode = code;
		this.valid = valid;
		this.placeHolders = new Object[] {"%s"};
	}
	public ErrorMetadata(MessageCodes code) {
		this.messageCode = code;
		this.valid = false;
		this.placeHolders = new Object[] {"%s"};
	}
	public ErrorMetadata(MessageCodes code, Object...placeHolders) {
		this.messageCode = code;
		this.valid = false;
		this.placeHolders = placeHolders;
	}
	public Object[] getPlaceHolders() {
		return placeHolders;
	}
	public void setPlaceHolders(Object...placeHolders) {
		this.placeHolders = placeHolders;
	}
	public Boolean isValid() {
		return valid;
	}
	public void setValid(Boolean valid) {
		this.valid = valid;
	}
	@Override
	public String toString() {
		return "{valid=" + valid + ", placeHolders=" + Arrays.toString(placeHolders) + ", code="
				+ messageCode + "}";
	}
	

}
