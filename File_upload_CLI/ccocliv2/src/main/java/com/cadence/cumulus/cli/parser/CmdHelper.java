package com.cadence.cumulus.cli.parser;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import org.apache.log4j.Logger;

import com.cadence.cumulus.cli.dto.ApplicationCommand;
import com.cadence.cumulus.cli.dto.ErrorMetadata;
import com.cadence.cumulus.cli.utils.AppConfigKeys;
import com.cadence.cumulus.cli.utils.CommandNames;
import com.cadence.cumulus.cli.utils.DisplayUtility;
import com.cadence.cumulus.cli.utils.MessageCodes;
import com.cadence.cumulus.cli.utils.AppUtils;

public class CmdHelper implements AppConfigKeys {
	
	private static final Logger LOG = Logger.getLogger(CmdHelper.class);
	
	private static CommandNames cmd;
	private static List<CommandNames> commands;
	private static ErrorMetadata meta;
	
	static {
		commands = new ArrayList<>(Arrays.asList(CommandNames.values()));
	}

	private static String validate(List<String> probableCommands) {
		// TODO Auto-generated method stub
		LOG.info("Validating command received: " + probableCommands);
		if(probableCommands.size() == 0) {
			meta = new ErrorMetadata(MessageCodes.NO_COMMAND);
			LOG.error("Encountered: " + meta.getMessageCode().getCode());
		} else if(probableCommands.size() > 1) {
			meta = new ErrorMetadata(MessageCodes.TOO_MANY_COMMANDS, probableCommands);
			LOG.error("Encountered: " + meta.getMessageCode().getCode());
		} else if(probableCommands.size() == 1) {
			String cmdName = probableCommands.get(0);
			if(!isCommandDefined(cmdName)) {
				meta = new ErrorMetadata(MessageCodes.UNRECOGNIZED_COMMAND, cmdName);
				LOG.error("Encountered: " + meta.getMessageCode().getCode());
			} else {
				LOG.info("Command: " + cmdName + " syntax pre-validation successful");
			}
		}
		String message = DisplayUtility.formatErrorWithHelp(meta);
		return message;
	}
	
	private static Comparator<CommandNames> cmdCompare = new Comparator<CommandNames>() {
		
		@Override
		public int compare(CommandNames o1, CommandNames o2) {
			// TODO Auto-generated method stub
			String c1 = o1.getValue();
			String c2 = o2.getValue();
			return c1.compareTo(c2);
		}
	};

	private static Boolean isCommandDefined(String key) {
		Boolean defined = false;
		Integer start = 0;
		Integer end = commands.size() - 1;
		while(start <= end) {
			Integer mid = (start + end) / 2;
			CommandNames value = commands.get(mid);
			Integer comparison = key.compareTo(value.getValue());
			if(comparison == 0) {
				cmd = commands.get(mid);
				LOG.info("Recognized command: " + cmd);
				defined = true;
				break;
			} else if(comparison > 0) {
				start = mid + 1;
			} else if(comparison < 0) {
				end = mid - 1;
			}
		}
		return defined;
	}
	
	private static String extractCmdSwitch(String arg) {
		int start = arg.lastIndexOf("-");
		String key = arg.substring(start + 1);
		return key;
	}
	
	public static ApplicationCommand parse(String[] args) {
		ApplicationCommand appCmd = new ApplicationCommand();
		Collections.sort(commands, cmdCompare);
		String argRegex = System.getProperty(ARG_FORMAT);
		List<String> commands = new ArrayList<>();
		Map<String, List<String>> arguments = new TreeMap<>();
		// parse passed command line arguments
		for (int i = 0; i < args.length; i++) {
			if(args[i].matches(argRegex)) {
				String[] arr = args[i].split("=");
				String key = extractCmdSwitch(arr[0]);
				if(arr.length == 1) {
					if(args[i].contains("=")) {
						arguments.put(key, new ArrayList<>());
					} else {
						arguments.put(key, null);
					}
				} else {
					List<String> value = arguments.get(key);
					if (value == null) {
						value = new ArrayList<>();
					}
					value.add(arr[1]);
					arguments.put(key, value);
				}
			} else {
				commands.add(args[i]);
			}
		}
		String message = validate(commands);
		if(!AppUtils.hasLength(message)) {
			appCmd.setCmd(cmd);
			appCmd.setArguments(arguments);
		} else {
			System.out.println(message);
		}
		return appCmd;
	}
	
}
