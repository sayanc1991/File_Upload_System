package com.cadence.cumulus.cli.utils;

public class CLIException extends Exception {

	/**
	 * @author anirban
	 * 
	 */
	private static final long serialVersionUID = -1952030072829015340L;
	
	private Integer exitCode;
	private MessageCodes code;

	public CLIException(MessageCodes code) {
		super();
		this.code = code;
		this.exitCode = Integer.parseInt(code.getCode());
	}

	public CLIException(String message, String exitCode) {
		super(message);
		this.exitCode = AppUtils.hasLength(exitCode) ? Integer.parseInt(exitCode) : 0;
	}
	
	public int getExitCode() {
		return this.exitCode;
	}

	public MessageCodes getCode() {
		return code;
	}

	public CLIException(String message, MessageCodes code) {
		super(message);
		this.code = code;
		this.exitCode = Integer.parseInt(code.getCode());
	}
	
	
	
}
