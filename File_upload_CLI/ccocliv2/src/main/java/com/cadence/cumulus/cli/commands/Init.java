package com.cadence.cumulus.cli.commands;

public class Init {
	
	private Boolean token;
	private Boolean url;
	public Boolean isToken() {
		return token;
	}
	public void setToken(Boolean token) {
		this.token = token;
	}
	public Boolean isUrl() {
		return url;
	}
	public void setUrl(Boolean url) {
		this.url = url;
	}
	@Override
	public String toString() {
		return this.getClass().getSimpleName() + " {token=" + token + ", url=" + url + "}";
	}
	public Init() {
		super();
		this.token = false;
		this.url = false;
	}
	
	
}
