package com.cadence.cumulus.cli.utils;

public enum CommandNames {

	TOKEN("init"),
	UPLOAD("upload", true),
	HELP("help"),
	VERSION("version"), 
	_NONE("null");
	
	private String value;
	private Boolean secured;
	
	private CommandNames(String value) {
		this.value = value;
		this.secured = false;
	}
	
	private CommandNames(String value, Boolean secured) {
		this.value = value;
		this.secured = secured;
	}
	
	public String getValue() {
		return this.value;
	}
	
	public Boolean isSecure() {
		return this.secured;
	}
}
