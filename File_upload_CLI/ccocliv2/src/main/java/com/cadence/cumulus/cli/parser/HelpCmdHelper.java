package com.cadence.cumulus.cli.parser;

import java.net.URISyntaxException;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.log4j.Logger;

import com.cadence.cumulus.cli.commands.Help;
import com.cadence.cumulus.cli.dto.ErrorMetadata;
import com.cadence.cumulus.cli.utils.DisplayUtility;
import com.cadence.cumulus.cli.utils.MessageCodes;
import com.cadence.cumulus.cli.utils.AppUtils;

public class HelpCmdHelper  {
	
	private static final Logger LOG = Logger.getLogger(HelpCmdHelper.class);

	private static Map<String, List<String>> args;
	
	private static String validate() {
		// TODO Auto-generated method stub
		Set<String> options = args.keySet();
		ErrorMetadata meta = null;
		if (!options.isEmpty()) {
			meta = new ErrorMetadata(MessageCodes.TOO_MANY_OPTIONS, options);
		}
		String message = DisplayUtility.formatErrorWithHelp(meta);
		return message;
	}

	public static Help convert(Map<String, List<String>> args) throws URISyntaxException {
		// TODO Auto-generated method stub
		Help payload = null;
		HelpCmdHelper.args = args;
		String message = validate();
		if(AppUtils.hasLength(message)) {
			System.out.println(message);
			LOG.info("Help syntax validation failed: " +  message);
		} else {
			payload = new Help();
			LOG.info("Help manual to be loaded: " + payload.getManualName());
		}
		return payload;
	}

}
