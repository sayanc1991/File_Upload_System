package com.cadence.cumulus.cli.parser;

import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.log4j.Logger;

import com.cadence.cumulus.cli.commands.Version;
import com.cadence.cumulus.cli.dto.ErrorMetadata;
import com.cadence.cumulus.cli.utils.DisplayUtility;
import com.cadence.cumulus.cli.utils.MessageCodes;
import com.cadence.cumulus.cli.utils.AppUtils;

public class VersionCmdHelper  {
	
	private static final Logger LOG = Logger.getLogger(VersionCmdHelper.class);
	
	private static Map<String, List<String>> args;
	
	private static String validate() {
		// TODO Auto-generated method stub
		Set<String> options = args.keySet();
		ErrorMetadata meta = null;
		if (!options.isEmpty()) {
			meta = new ErrorMetadata(MessageCodes.TOO_MANY_OPTIONS, options);
		}
		String message = DisplayUtility.formatErrorWithHelp(meta);
		return message;
	}

	public static Version convert(Map<String, List<String>> args) {
		// TODO Auto-generated method stub
		VersionCmdHelper.args = args;
		Version payload = null;
		String message = validate();
		if(AppUtils.hasLength(message)) {
			System.out.println(message);
			LOG.info("Version syntax validation failed: " + message);
		} else {
			payload = new Version();
			LOG.info("Version number loaded");
		}
		return payload;
	}

}
