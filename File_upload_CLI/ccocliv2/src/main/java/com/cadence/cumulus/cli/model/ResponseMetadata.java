package com.cadence.cumulus.cli.model;

public class ResponseMetadata {
	
	// response body as pojo
	private Object body;
	private Boolean error;
	// error message as string value
	private String message;
	public Object getBody() {
		return body;
	}
	public void setBody(Object body) {
		this.body = body;
	}
	public Boolean getError() {
		return error;
	}
	public void setError(Boolean error) {
		this.error = error;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	@Override
	public String toString() {
		return "{body=" + body + ", error=" + error + ", message=" + message + "}";
	}

}
