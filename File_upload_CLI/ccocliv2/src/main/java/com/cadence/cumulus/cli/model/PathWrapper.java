package com.cadence.cumulus.cli.model;

import java.nio.file.Path;

public class PathWrapper {
	
	private Path filePath;
	private Path parentDir;
	public Path getFilePath() {
		return filePath;
	}
	public void setFilePath(Path filePath) {
		this.filePath = filePath;
	}
	public Path getParentDir() {
		return parentDir;
	}
	public void setParentDir(Path parentDir) {
		this.parentDir = parentDir;
	}
	public PathWrapper(Path filePath) {
		super();
		this.filePath = filePath;
		this.parentDir = null;
	}
	@Override
	public String toString() {
		return "{filePath=" + filePath + ", parentDir=" + parentDir + "}";
	}

}
