package com.cadence.cumulus.cli;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.ProcessBuilder.Redirect;
import java.nio.file.Files;
import java.nio.file.NoSuchFileException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.UUID;

import org.apache.log4j.Logger;

import com.cadence.cumulus.cli.dto.ErrorMetadata;
import com.cadence.cumulus.cli.dto.ProcessResult;
import com.cadence.cumulus.cli.model.ConfigModel;
import com.cadence.cumulus.cli.utils.AppConfigKeys;
import com.cadence.cumulus.cli.utils.AppUtils;
import com.cadence.cumulus.cli.utils.CLIException;
import com.cadence.cumulus.cli.utils.DisplayUtility;
import com.cadence.cumulus.cli.utils.MessageCodes;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;

public class CLIConfiguration implements AppConfigKeys{
	
	private final static Logger LOG = Logger.getLogger(CLIConfiguration.class);
	
	private static ObjectMapper mapper;
	private static String userHome;
	
	static {
		mapper = new ObjectMapper();
		userHome = System.getProperty("user.home");
	}
	
	public static void load() throws CLIException {
		try {
			String base = System.getProperty(CCO_BASE_DIR);
			String fileName = System.getProperty(CCO_CONF_FILE);
			Path confFilePath = Paths.get(userHome, base, fileName);
			byte[] content = Files.readAllBytes(confFilePath);
			ConfigModel config = mapper.readValue(content, ConfigModel.class);
			if(AppUtils.hasLength(config.getToken()) && AppUtils.hasLength(config.getUrl())) {
				System.setProperty(CHAMBER_TOKEN_IN_VARIABLE, config.getToken());
				System.setProperty(CHAMBER_URL_IN_VARIABLE, config.getUrl());
			} else {
				MessageCodes code = MessageCodes.INVALID_CONFIG;
				String message = DisplayUtility.formatError(code);
				LOG.error("Encountered: " + code.getCode());
				throw new CLIException(message, code.getCode());
			}
		} catch (IOException e) {
			CLIException clie = wrapException(e);
			throw clie;
		}
	}
	
	public static void setToken(String token) throws CLIException {
		LOG.info("Setting CLI Configuration parameter: Token");
		String url = System.getProperty(CHAMBER_URL_IN_VARIABLE);
		ConfigModel config = new ConfigModel();
		config.setToken(token);
		config.setUrl(url);
		setHelper(config);
		System.out.println("Token updated!");
	}
	
	public static void setURL(String url) throws CLIException {
		LOG.info("Setting CLI Configuration parameter: URL");
		String token = System.getProperty(CHAMBER_TOKEN_IN_VARIABLE);
		ConfigModel config = new ConfigModel();
		config.setToken(token);
		config.setUrl(url);
		setHelper(config);
		System.out.println("Chamber base URL updated!");
	}
	
	public static void setAll(String token, String url) throws CLIException {
		LOG.info("Setting all CLI Configuration parameters");
		ConfigModel config = new ConfigModel(token, url);
		setHelper(config);
		System.out.println("CLI configured. You can upload files now.");
	}
	
	private static void setHelper(ConfigModel config) throws CLIException {
		try {
			ObjectWriter ow = mapper.writerWithDefaultPrettyPrinter();
			byte[] content = ow.writeValueAsBytes(config);
			String base = System.getProperty(CCO_BASE_DIR);
			String fileName = System.getProperty(CCO_CONF_FILE);
			Path filePath = Paths.get(userHome, base, fileName);
			LOG.info("Writing CLI Configuration parameters to file: " + filePath);
			filePath = Files.write(filePath, content);
			String osName = System.getProperty("os.name");
			if (null != osName && !osName.startsWith("Windows")) {
				LOG.info("OS is: " + osName + " hence permission needs to be set for file: " + fileName);
				String chmod[] = {"chmod " + Integer.getInteger(CONF_FILE_PERM) + " \"" + filePath + "\""};
				runCommand(chmod);
				LOG.info("Script executed successfully!");
			}
		} catch (InterruptedException e) {
			LOG.error("Error changing CLI configuration permission: ", e);
			MessageCodes code = MessageCodes.INIT_FILE_PERM_FAILED;
			LOG.error("Encountered: " + code.getCode());
			/*String message = DisplayUtility.formatError(code);
			throw new CLIException(message, code.getCode());*/
		} catch (IOException e) {
			CLIException clie = wrapException(e);
			throw clie;
		}
	}
	
	private static ProcessResult runCommand(String... command) throws IOException, InterruptedException {
	        String tmpScriptName = "/tmp/" + UUID.randomUUID().toString() + ".sh";
	        File f = new File(tmpScriptName);
	        FileOutputStream fout = new FileOutputStream(f);
	        String scriptContents = "#!/bin/bash\nset -e\n" + command[command.length - 1];
	        LOG.info("Script contents: " + scriptContents);
	        fout.write(scriptContents.getBytes());
	        fout.close();
	        f.setExecutable(true, false);
	        command[command.length - 1] = tmpScriptName;
	        LOG.info("Executing script: " + tmpScriptName);
	        ProcessBuilder processBuilder = new ProcessBuilder(command).redirectErrorStream(true)
	                .inheritIO().redirectOutput(Redirect.PIPE);
	        Process process = processBuilder.start();
	        StringBuilder outputBuilder = new StringBuilder();
	        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(process.getInputStream()));
	        String line = null;
	        while ((line = bufferedReader.readLine()) != null) {
                outputBuilder.append(line + "\n");
            }
	        int returnValue = process.waitFor();
	        f.delete();
	        return new ProcessResult(outputBuilder.toString(), returnValue);

	    }
	
	private static CLIException wrapException(IOException e) {
		LOG.error("Error loading CLI configuration", e);
		CLIException clie = null;
		MessageCodes code = MessageCodes.SUCCESS;
		if(e instanceof JsonMappingException || e instanceof JsonParseException) {
			code = MessageCodes.INVALID_CONFIG;
			String message = DisplayUtility.formatErrorWithInit(new ErrorMetadata(code));
			clie = new CLIException(message, code);
		} else if(e instanceof FileNotFoundException || e instanceof NoSuchFileException) {
			code = MessageCodes.NOT_FOUND_CONFIG;
			String message = DisplayUtility.formatErrorWithInit(new ErrorMetadata(code));
			clie = new CLIException(message, code);
		} else {
			code = MessageCodes.CLI_ERROR;
			clie = DisplayUtility.formatException(code, e);
		}
		LOG.error("Encountered: " + code.getCode());
		return clie;
	}

}
