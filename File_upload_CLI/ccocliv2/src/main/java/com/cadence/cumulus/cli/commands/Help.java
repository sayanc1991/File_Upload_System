package com.cadence.cumulus.cli.commands;

public class Help {

	private String manualName;

	@Override
	public String toString() {
		return this.getClass().getSimpleName() + " {manualName=" + manualName + "}";
	}

	public Help() {
		super();
		this.manualName = "manual.txt";
	}

	public String getManualName() {
		return this.manualName;
	}

	
}
