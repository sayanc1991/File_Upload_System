package com.cadence.cumulus.cli.model;


public class DestinationValidation {

	private Boolean validationResult;
	private String folderName;
	private String validationError;
	public String getFolderName() {
		return folderName;
	}
	public void setFolderName(String folderName) {
		this.folderName = folderName;
	}
	public Boolean getValidationResult() {
		return validationResult;
	}
	public void setValidationResult(Boolean validationResult) {
		this.validationResult = validationResult;
	}
	public String getValidationError() {
		return validationError;
	}
	public void setValidationError(String validationError) {
		this.validationError = validationError;
	}
	@Override
	public String toString() {
		return "{folderName=" + folderName + ", validationResult=" + validationResult + ", validationError="
				+ validationError + "}";
	}
	
}