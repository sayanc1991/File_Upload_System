package com.cadence.cumulus.cli.parser;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.log4j.Logger;

import com.cadence.cumulus.cli.commands.Upload;
import com.cadence.cumulus.cli.dto.ErrorMetadata;
import com.cadence.cumulus.cli.utils.DisplayUtility;
import com.cadence.cumulus.cli.utils.MessageCodes;
import com.cadence.cumulus.cli.utils.AppUtils;

public class UploadCmdHelper{

	private static final Logger LOG = Logger.getLogger(UploadCmdHelper.class);
	private static final String DESTINATION_SWITCH = "d";
	private static final String SOURCE_SWITCH = "f";
	
	private static Map<String, List<String>> args;

	public static String validate() {
		// TODO Auto-generated method stub
		ErrorMetadata ef = new ErrorMetadata(MessageCodes.NO_UPLOAD_SOURCES, SOURCE_SWITCH);
		LOG.info("Options received: " + args.keySet());
		for (String option : args.keySet()) {
			List<String> values = args.get(option);
			LOG.info("Validating command option received: " + option);
			switch (option) {
			case SOURCE_SWITCH:
				if (values == null || values.size() == 0) {
					LOG.info("No upload sources specified");
				} else {
					ef.setValid(true);
					LOG.info("Option value, sources: " + values);
				}
				break;
			case DESTINATION_SWITCH:
				ef = new ErrorMetadata(MessageCodes.ERROR_UPLOAD_DESTINATION, DESTINATION_SWITCH);
				if (values == null || values.size() == 0)
					LOG.info("No upload destination specified: {}" + values);
				else if (values.size() > 1)
					LOG.info("Multiple upload destination specified: " + values);
				else {
					ef.setValid(true);
					LOG.info("Option value, destination: {}" + values);
				}
				break;
			default:
				Set<String> argKeys = args.keySet();
				List<String> allowedKeys = Arrays.asList(DESTINATION_SWITCH, SOURCE_SWITCH);
				argKeys.removeAll(allowedKeys);
				ef = new ErrorMetadata(MessageCodes.TOO_MANY_OPTIONS, argKeys);
				LOG.info("Unrecognized option values: " + argKeys);
				LOG.error("Encountered " + ef.getMessageCode().getCode());
				break;
			}

			if (!ef.isValid())
				break;
		}
		String message = DisplayUtility.formatErrorWithHelp(ef);
		return message;
	}

	public static Upload convert(Map<String, List<String>> source) {
		// TODO Auto-generated method stub
		args = source;
		String message = validate();
		Upload payload = null;
		if (!AppUtils.hasLength(message)) {
			payload = new Upload();
			for (String option : source.keySet()) {
				List<String> values = source.get(option);
				switch (option) {
				case DESTINATION_SWITCH:
					String destination = values.get(0);
					String urlDestination = AppUtils.separatorsToUnix(destination);
					payload.setDestination(urlDestination);
					break;
				case SOURCE_SWITCH:
					payload.setSources(values);
					break;
				}
			}
			LOG.info("Upload command parsed successfully");
		} else {
			System.out.println(message);
		}
		return payload;
	}

}
