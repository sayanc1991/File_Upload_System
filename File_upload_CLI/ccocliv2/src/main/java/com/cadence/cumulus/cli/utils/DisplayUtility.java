package com.cadence.cumulus.cli.utils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Formatter;
import java.util.List;

import org.apache.log4j.Logger;

import com.cadence.cumulus.cli.dto.ErrorMetadata;
import com.cadence.cumulus.cli.dto.ExceptionMetadata;

public class DisplayUtility {
	
	private final static Logger LOG = Logger.getLogger(DisplayUtility.class);
	
	private static StringBuilder sb;
	private static Formatter fmt;
	
	static {
		sb = new StringBuilder();
		fmt = new Formatter(sb);
	}
	
	public static String formatErrorWithHelp(ErrorMetadata meta) {
		String message = "";
		if(meta != null && !meta.isValid()) {
			LOG.info("Formatting with help for error code: " + meta.getMessageCode());
			List<Object> list = new ArrayList<>(Arrays.asList(meta.getPlaceHolders()));
			list.add("%s");
			Object[] placeHolders = list.toArray(new Object[list.size()]);
			meta.setPlaceHolders(placeHolders);
			message = formatError(meta);
			MessageCodes code = MessageCodes.HELP_PROMPT;
			String helpPrompt = AppUtils.getMessage(code);
			message = String.format(message, helpPrompt);
		}
		return message;
	}
	
	public static String formatErrorWithInit(ErrorMetadata meta) {
		String message = "";
		if(meta != null && !meta.isValid()) {
			LOG.info("Formatting with init for error code: " + meta.getMessageCode());
			List<Object> list = new ArrayList<>(Arrays.asList(meta.getPlaceHolders()));
			list.add("%s");
			Object[] placeHolders = list.toArray(new Object[list.size()]);
			meta.setPlaceHolders(placeHolders);
			message = formatError(meta);
			MessageCodes code = MessageCodes.INIT_PROMPT;
			String helpPrompt = AppUtils.getMessage(code);
			message = String.format(message, helpPrompt);
		}
		return message;
	}
	
	public static CLIException formatException(MessageCodes codes, Throwable cause) {
		String template = AppUtils.getMessage(codes);
		String message = String.format(template, cause.getMessage());
		CLIException e = new CLIException(message, codes);
		return e;
	}
	
	public static String formatException(ExceptionMetadata meta) {
		fmt.format(AppUtils.getMessage(meta.getMessageCode()), meta.getPlaceHolders());
		String message = sb.toString();
		fmt.flush();
		sb.setLength(0);
		String cliError = AppUtils.getMessage(MessageCodes.CLI_ERROR);
		message = String.format(cliError, meta.getCauseName(), meta.getCause(), message);
		return message;
	}
	
	public static String formatError(ErrorMetadata meta) {
		String message = "";
		if(meta != null && !meta.isValid()) {
			fmt.format(AppUtils.getMessage(meta.getMessageCode()), meta.getPlaceHolders());
			message = sb.toString();
			fmt.flush();
			sb.setLength(0);
		}
		return message;
	}
	
	public static String formatError(MessageCodes code, Object...placeHolders) {
		fmt.format(AppUtils.getMessage(code), placeHolders);
		String message = sb.toString();
		fmt.flush();
		sb.setLength(0);
		return message;
	}
	
	public static String displayOnSameLine(MessageCodes code, Object...placeHolders) throws IOException {
		ErrorMetadata meta = new ErrorMetadata(code);
		meta.setPlaceHolders(placeHolders);
		String message = formatError(meta);
		System.out.write(message.getBytes());
		return message;
	}
	
	public static String displayRetryOnSameLine(MessageCodes code, Object...placeHolders) throws IOException {
		ErrorMetadata meta = new ErrorMetadata(code);
		meta.setPlaceHolders(placeHolders);
		String message = formatError(meta);
		System.out.println(message);
		return message;
	}
	
	public static String displayRetry(MessageCodes code, Object...placeHolders) throws IOException {
		ErrorMetadata meta = new ErrorMetadata(code);
		meta.setPlaceHolders(placeHolders);
		String message = formatError(meta);
		System.out.write(message.getBytes());
		return message;
	}
	
	public static String displayRetryWaitOnSameLine(MessageCodes retry, int count, MessageCodes retryWait, int retryAttempts) throws IOException {
		String messageOne = fetchMessage(retry, count, retryAttempts);
		String messageTwo = fetchMessage(retryWait, retryAttempts);
		messageTwo =  messageTwo.split("\\r")[1];
		String finalVal = messageOne + " " + messageTwo;
		System.out.write(finalVal.getBytes());
		return finalVal;
	}
	
	public static String fetchMessage(MessageCodes codes, Object...placeHolders) {
		ErrorMetadata meta = new ErrorMetadata(codes);
		meta.setPlaceHolders(placeHolders);
		String message = formatError(meta);
		
		return message;
	}
	
	public static void displayOutput(Object message) {
		if(null != message) {
			System.out.println(message);
		}
	}
	
}
