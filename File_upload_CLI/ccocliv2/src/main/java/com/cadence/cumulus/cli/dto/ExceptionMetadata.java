package com.cadence.cumulus.cli.dto;

import java.util.Arrays;

import com.cadence.cumulus.cli.utils.MessageCodes;

public class ExceptionMetadata extends ErrorMetadata{
	public ExceptionMetadata(MessageCodes code, Throwable e) {
		super(code);
		this.e = e;
	}
	private Throwable e;
	@Override
	public String toString() {
		return "{e=" + e + ", valid=" + valid + ", placeHolders=" + Arrays.toString(placeHolders) + ", code=" + messageCode.getCode()
				+ "}";
	}
	public Throwable getException() {
		return e;
	}
	public String getCauseName() {
		return this.e.getClass().getSimpleName();
	}
	public String getCause() {
		return this.e.getMessage();
	}
	
}
