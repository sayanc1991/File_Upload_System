package com.cadence.cumulus.cli.service.impl;

import java.io.IOException;
import java.net.CookieHandler;
import java.net.CookieManager;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.net.UnknownHostException;

import org.apache.http.Header;
import org.apache.http.HttpHeaders;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.conn.ConnectTimeoutException;
import org.apache.http.conn.ssl.NoopHostnameVerifier;
import org.apache.http.entity.ByteArrayEntity;
import org.apache.http.entity.ContentType;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.protocol.HTTP;
import org.apache.log4j.Logger;

import com.cadence.cumulus.cli.utils.AppConfigKeys;
import com.cadence.cumulus.cli.utils.DisplayUtility;
import com.cadence.cumulus.cli.utils.AppUtils;

public class RestClientImpl  implements AppConfigKeys {

	private static CloseableHttpClient httpClient;
	private static String userName;

	private final static Logger LOG = Logger.getLogger(RestClientImpl.class);

	public static void init() {
		userName = System.getProperty("user.name");
		RequestConfig config = RequestConfig.custom().setConnectTimeout(Integer.getInteger(CHAMBER_CONNECTION_TIMEOUT))
				.setConnectionRequestTimeout(Integer.getInteger(CHAMBER_REQUEST_TIMEOUT))
				.setSocketTimeout(Integer.getInteger(CHAMBER_RESPONSE_TIMEOUT)).build();
		httpClient = HttpClients.custom().setSSLHostnameVerifier(new NoopHostnameVerifier())
				.setDefaultRequestConfig(config).build();
	}

	protected static Object get(String endPoint, Object requestBody, Class<?> responseType)
			throws ClientProtocolException, IOException, URISyntaxException {
		CookieHandler.setDefault(new CookieManager());
		URL url = new URL(endPoint);
		String nullFragment = null;
		URI uri = new URI(url.getProtocol(), url.getHost(), url.getPath(), url.getQuery(), nullFragment);
		URIBuilder builder = new URIBuilder(uri);
		builder.setParameter("userName", userName);
		URI mod = builder.build();
		HttpGet httpget = new HttpGet(mod);
		httpget.addHeader(System.getProperty(CLI_TOKEN_HEADER_NAME), System.getProperty(CHAMBER_TOKEN_IN_VARIABLE));
		String sessionId = System.getProperty(CLI_SESSION_HEADER_NAME);
		// System.out.println("sessionId :" + sessionId);
		if (AppUtils.hasLength(sessionId)) {
			httpget.addHeader(System.getProperty(CLI_SESSION_HEADER_NAME), sessionId);
		}
		httpget.addHeader(HttpHeaders.ACCEPT, ContentType.APPLICATION_JSON.toString());
		httpget.addHeader(HttpHeaders.CONTENT_TYPE, ContentType.APPLICATION_JSON.toString());
		httpget.setHeader(HttpHeaders.CONNECTION, HTTP.CONN_KEEP_ALIVE);
		if (null != sessionId) {
			if (null != userName && !"".equals(userName)) {
				httpget.addHeader(System.getProperty(USERNAME), userName);
			}
			httpget.setHeader("Cookie", sessionId);
		}
		HttpResponse response = null;
		try {
			response = httpClient.execute(httpget);
		} catch (UnknownHostException | SocketException | SocketTimeoutException | ConnectTimeoutException e) {
			LOG.error("Exception occurred: ",  e);
			throw e;
		}catch(IOException e){
			String message = "";
			if(null != e.getCause()) {
				message = e.getCause().getMessage();
			}else {
				message = e.getMessage();
			}
			LOG.error("Could not resolve hostname: ", e);
			DisplayUtility.displayOutput("Could not resolve hostname: " + message);
		} 
		if (null != response) {
			int status = response.getStatusLine().getStatusCode();
			if (status >= 200 && status < 300) {
				Header[] header = response.getHeaders(System.getProperty(CLI_SESSION_HEADER_NAME));
				if (header.length > 0) {
					sessionId = header[0].getValue();
					LOG.info("Established session from server: " + sessionId);
					if (AppUtils.hasLength(sessionId)) {
						String key = System.getProperty(CLI_SESSION_HEADER_NAME);
						System.setProperty(key, sessionId);
					}
				}

				header = response.getHeaders(System.getProperty(USERNAME));
				if (header.length > 0) {
					String userName = header[0].getValue();
					if (AppUtils.hasLength(userName)) {
						RestClientImpl.userName = userName;
						//RestClientImpl.userName = "vikramadityasingh";
					}
				}

				return response;
			} else {
				throw new ClientProtocolException("Unexpected response status: " + status);
			}
		}
		return response;
	}

	protected static Object post(String endPoint, Object requestBody, Class<?> responseType)
			throws IOException, URISyntaxException {
		URL url = new URL(endPoint);
		String nullFragment = null;
		URI uri = new URI(url.getProtocol(), url.getHost(), url.getPath(), url.getQuery(), nullFragment);
		URIBuilder builder = new URIBuilder(uri);
		builder.setParameter("userName", userName);
		URI mod = builder.build();
		HttpResponse response = null;
		HttpPost httppost = new HttpPost(mod);
		httppost.addHeader(System.getProperty(CLI_TOKEN_HEADER_NAME), System.getProperty(CHAMBER_TOKEN_IN_VARIABLE));
		httppost.setHeader(HttpHeaders.CONNECTION, HTTP.CONN_KEEP_ALIVE);
		String sessionId = System.getProperty(CLI_SESSION_HEADER_NAME);
		if (AppUtils.hasLength(sessionId)) {
			httppost.addHeader(System.getProperty(CLI_SESSION_HEADER_NAME), sessionId);
		}
		/*if (null != sessionId) {
			httppost.setHeader("Cookie", sessionId);
		}*/
		ByteArrayEntity entity = (ByteArrayEntity) requestBody;
		httppost.setEntity(entity);
		httppost.addHeader(HttpHeaders.ACCEPT, ContentType.APPLICATION_JSON.toString());
		httppost.addHeader(HttpHeaders.CONTENT_TYPE, ContentType.APPLICATION_JSON.toString());
		// System.out.println("Executing request: " + httppost.getRequestLine());
		try {
			response = httpClient.execute(httppost);

			return response;
		} catch (UnknownHostException | SocketException | SocketTimeoutException | ConnectTimeoutException e) {
			LOG.error("Exception occurred: ", e);
			throw e;
		} catch(IOException e){
			String message = "";
			if(null != e.getCause()) {
				message = e.getCause().getMessage();
			}else {
				message = e.getMessage();
			}
			LOG.error("Could not resolve hostname: ", e);
			DisplayUtility.displayOutput("");
			DisplayUtility.displayOutput("Could not resolve hostname: " + message);
		} 
			// response.close();
		return response;
	}

	public static void destroy() {
		// TODO Auto-generated method stub
		try {
			if (httpClient != null){ 
				httpClient.close();
			}
			System.exit(0);
		} catch (IOException e) {
			LOG.error("Exception during http client processing: ", e);
		}
	}

}
