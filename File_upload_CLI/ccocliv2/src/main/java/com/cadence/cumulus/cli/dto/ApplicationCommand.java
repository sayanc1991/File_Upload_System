package com.cadence.cumulus.cli.dto;

import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import com.cadence.cumulus.cli.utils.CommandNames;

public class ApplicationCommand {

	private CommandNames cmd;
	private Map<String,List<String>> arguments;
	
	public Map<String, List<String>> getArguments() {
		return arguments;
	}
	public void setArguments(Map<String, List<String>> arguments) {
		this.arguments = arguments;
	}
	public CommandNames getCmd() {
		return cmd;
	}
	public void setCmd(CommandNames cmd) {
		this.cmd = cmd;
	}
	@Override
	public String toString() {
		return "{cmd=" + cmd + ", arguments=" + arguments + "}";
	}
	public ApplicationCommand() {
		this.cmd = CommandNames._NONE;
		this.arguments = new TreeMap<>();
	}
	
	
	
}
