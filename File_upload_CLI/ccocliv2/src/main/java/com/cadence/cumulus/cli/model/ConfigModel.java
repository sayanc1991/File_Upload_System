package com.cadence.cumulus.cli.model;

public class ConfigModel {

	private String activeConfigName;
	private String token;
	private String url;
	
	public String getToken() {
		return token;
	}
	public void setToken(String token) {
		this.token = token;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public String getActiveConfigName() {
		return activeConfigName;
	}
	public void setActiveConfigName(String activeConfigName) {
		this.activeConfigName = activeConfigName;
	}
	public ConfigModel() {
		this.activeConfigName = "default";
		this.token = "";
		this.url = "";
	}
	public ConfigModel(String token, String url) {
		super();
		this.activeConfigName = "default";
		this.token = token;
		this.url = url;
	}
	
	
}
