package com.cadence.cumulus.cli.commands;


public class Version  {

	private String buildKey;
	private String value;
	
	public Version() {
		super();
		this.buildKey = "cli.build.number";
		this.value = "cli.project.version";
	}

	public String getBuildKey() {
		return this.buildKey;
	}

	public String getValue() {
		return this.value;
	}
	
	
}
