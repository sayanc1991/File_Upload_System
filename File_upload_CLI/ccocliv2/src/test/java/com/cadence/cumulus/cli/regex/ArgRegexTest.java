package com.cadence.cumulus.cli.regex;

import java.util.ArrayList;
import java.util.List;

public class ArgRegexTest {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String regex = "^--[a-zA-Z]+=?[\\S\\s]*$";
		List<String> data = new ArrayList<>();
		data.add("--f=C:\\testData");
		data.add("--f=C:\\testData\\ with space");
		data.add("-f");
		data.add("--f=");
		data.add("--f");
		for(String d : data) {
			System.out.println(d + " matched: " + d.matches(regex));
		}
	}

}
