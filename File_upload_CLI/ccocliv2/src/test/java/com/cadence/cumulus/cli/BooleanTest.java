package com.cadence.cumulus.cli;

public class BooleanTest {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String d1 = "true";
		String d2 = "false";
		String d3 = "k";
		String d4 = "9";
		System.out.println(Boolean.parseBoolean(d1));
		System.out.println(Boolean.parseBoolean(d2));
		System.out.println(Boolean.parseBoolean(d3));
		System.out.println(Boolean.parseBoolean(d4));
		System.out.println(Boolean.valueOf(d1));
		System.out.println(Boolean.valueOf(d2));
		System.out.println(Boolean.valueOf(d3));
		System.out.println(Boolean.valueOf(d4));
		System.out.println(String.format("%-30.5s", "Hello World"));
	}

}
