package com.cadence.cumulus.cli.regex;

import java.util.ArrayList;
import java.util.List;

import com.cadence.cumulus.cli.utils.AppConfigKeys;

public class TokenRegexTest {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String regex = AppConfigKeys.TOKEN_FORMAT;
		List<String> data = new ArrayList<>();
		data.add("Em+6hquhRwFxtveseAhDVprrASkAKnm1xr8YYIAPg0w=");
		data.add("");
		data.add("Em+6hquhRwFxtveseAhDVprrASkAKnm1xr8YYIAPg0w");
		data.add("Em+6hquhRwFxtveseAhDVprrASkAKnm1xr8YYIAPg0=");
		data.add("Em+6hquhRwFxtveseAhDVprrASkAKnm1xr8YYIAPg0w=l");
		for(String d : data) {
			System.out.println(d + " matched: " + d.matches(regex));
		}
	}

}
