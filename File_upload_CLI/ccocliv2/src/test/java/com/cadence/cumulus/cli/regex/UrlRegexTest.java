package com.cadence.cumulus.cli.regex;

import java.util.ArrayList;
import java.util.List;

public class UrlRegexTest {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String regex = "^(https?):\\/\\/((([a-zA-Z0-9]+-*)*[a-zA-Z0-9])\\.?)*(([a-zA-Z0-9]+-*)*[a-zA-Z0-9])";
		List<String> data = new ArrayList<>();
		data.add("https://test-201.cadencecloud.net");
		data.add("");
		data.add("https://cco-301.cadencecloud.net");
		data.add("www.google.com");
		data.add("instagram.com");
		data.add("http://www.instagram.com");
		data.add("https://www.google.com");
		for(String d : data) {
			System.out.println(d + " matched: " + d.matches(regex));
		}
	}

}
