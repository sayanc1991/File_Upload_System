package com.cadence.cumulus.cli.upload;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

public class FilePathTest {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		List<String> src = new ArrayList<>();
		src.add("bin");
		src.add("f");
		src.add("pom.xml");
		src.add("cli.log");
		for(String p : src) {
			Path fp = Paths.get(p);
			Boolean e = Files.exists(fp);
			System.out.println(p + " " + e);
		}
	}

}
