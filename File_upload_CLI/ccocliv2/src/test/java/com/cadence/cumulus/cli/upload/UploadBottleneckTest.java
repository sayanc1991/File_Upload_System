package com.cadence.cumulus.cli.upload;

import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.util.EntityUtils;

import com.cadence.cumulus.cli.CLIConfiguration;
import com.cadence.cumulus.cli.commands.Upload;
import com.cadence.cumulus.cli.dto.ApplicationCommand;
import com.cadence.cumulus.cli.dto.ErrorMetadata;
import com.cadence.cumulus.cli.model.DestinationValidation;
import com.cadence.cumulus.cli.model.PathWrapper;
import com.cadence.cumulus.cli.model.ResponseVO;
import com.cadence.cumulus.cli.parser.CmdHelper;
import com.cadence.cumulus.cli.parser.UploadCmdHelper;
import com.cadence.cumulus.cli.service.impl.RestClientImpl;
import com.cadence.cumulus.cli.service.impl.UploadClientImpl;
import com.cadence.cumulus.cli.utils.AppConfigKeys;
import com.cadence.cumulus.cli.utils.CLIException;
import com.cadence.cumulus.cli.utils.DisplayUtility;
import com.cadence.cumulus.cli.utils.MessageCodes;
import com.cadence.cumulus.cli.utils.AppUtils;
import com.fasterxml.jackson.databind.ObjectMapper;

public class UploadBottleneckTest extends RestClientImpl implements AppConfigKeys{
	
	private static String cmdName = "upload";
	
	private static Map<String, String[]> testData;
	
	private ExecutorService executorService;
	private String testCaseName;
	
	public UploadBottleneckTest(String testCaseName) {
		super();
		this.testCaseName = testCaseName;
		this.executorService = Executors.newFixedThreadPool(2);
	}

	static {
		testData = new LinkedHashMap<>();
		
		testData.put("validFiles", new String[] {"--f=C:\\testData\\x.txt", "--f=C:\\testData\\y.txt"});
		testData.put("invalidFiles", new String[] {"--f=C:\\testData\\te.txt", "--f=C:\\testData\\wew.txt"});
		testData.put("validAndInvalidFiles", new String [] {"--f=C:\\testData\\x.txt", "--f=C:\\testData\\wew.txt"});
		
		testData.put("validFilesAndInvalidDestination", new String[] {"--f=C:\\testData\\x.txt", "--f=C:\\testData\\y.txt", "--d=xyz"});
		testData.put("invalidFilesAndInvalidDestination", new String[] {"--f=C:\\testData\\te.txt", "--f=C:\\testData\\wew.txt", "--d=xyz"});
		testData.put("validAndInvalidFilesAndInvalidDestination", new String [] {"--f=C:\\testData\\x.txt", "--f=C:\\testData\\wew.txt", "--d=xyz"});
		
		testData.put("validFolders", new String [] {"--f=C:\\testData\\a", "--f=C:\\testData\\0f"});
		testData.put("invalidFolders", new String [] {"--f=C:\\testData\\saf", "--f=C:\\testData\\dm"});
		testData.put("validAndInvalidFolders", new String [] {"--f=C:\\testData\\a", "--f=C:\\testData\\dm"});
		
		testData.put("validFoldersAndInvalidDestination", new String [] {"--f=C:\\testData\\a", "--f=C:\\testData\\0f", "--d=xyz"});
		testData.put("invalidFoldersAndInvalidDestination", new String [] {"--f=C:\\testData\\saf", "--f=C:\\testData\\dm", "--d=xyz"});
		testData.put("validAndInvalidFoldersAndInvalidDestination", new String [] {"--f=C:\\testData\\a", "--f=C:\\testData\\dm", "--d=xyz"});
		
		testData.put("validFilesAndFolders", new String [] {"--f=C:\\testData\\x.txt", "--f=C:\\testData\\0f"});
		testData.put("invalidFilesAndFolders", new String [] {"--f=C:\\testData\\te.txt", "--f=C:\\testData\\dm"});
		testData.put("validFilesAndInvalidFolders", new String [] {"--f=C:\\testData\\x.txt", "--f=C:\\testData\\dm"});
		testData.put("invalidFilesAndValidFolders", new String [] {"--f=C:\\testData\\te.txt", "--f=C:\\testData\\0f"});
		
		testData.put("validFilesAndFoldersAndInvalidDestination", new String [] {"--f=C:\\testData\\x.txt", "--f=C:\\testData\\0f", "--d=xyz"});
		testData.put("invalidFilesAndFoldersAndInvalidDestination", new String [] {"--f=C:\\testData\\te.txt", "--f=C:\\testData\\dm", "--d=xyz"});
		testData.put("validFilesAndInvalidFoldersAndInvalidDestination", new String [] {"--f=C:\\testData\\x.txt", "--f=C:\\testData\\dm", "--d=xyz"});
		testData.put("invalidFilesAndValidFoldersAndInvalidDestination", new String [] {"--f=C:\\testData\\te.txt", "--f=C:\\testData\\0f", "--d=xyz"});
	}

	public static void main(String[] args) throws IOException, InterruptedException, ExecutionException, CLIException {
		// TODO Auto-generated method stub
		RestClientImpl.init();
		for(String key : testData.keySet()) {
			String[] value = testData.get(key);
			args = new String[value.length + 1];
			args[0] = cmdName;
			System.arraycopy(value, 0, args, 1, value.length);
			ApplicationCommand appCmd = CmdHelper.parse(args);
			Upload upload = UploadCmdHelper.convert(appCmd.getArguments());
			if(upload != null) {
				CLIConfiguration.load();
				UploadClientImpl.startUpload(upload);
			}
		}
		RestClientImpl.destroy();
	}
	
	public void execute(Upload cmdParams) throws InterruptedException, ExecutionException {
		System.out.println("Object: " + this.hashCode() + " -> " + this.testCaseName);
		
		CompletableFuture<Boolean> validateSources = CompletableFuture.supplyAsync(() -> {
			List<PathWrapper> result = readSources(cmdParams.getSources());
			Boolean isValid = result.size() > 0;
			return isValid;
		}, executorService);
		
		CompletableFuture<Boolean> validateDestination = CompletableFuture.supplyAsync(() -> {
			Boolean isValid = false;
			DestinationValidation validationResponse = verifyDestinationPath(cmdParams.getDestination());
			if (validationResponse != null) {
				isValid = validationResponse.getValidationResult();
			}
			return isValid;
		}, executorService);

		Boolean sourcesValid  = validateSources.get();
		if(sourcesValid) {
			Boolean canUpload = validateDestination.get();
			System.err.println("Object: " + this.hashCode() + " -> Upload possible: " + canUpload);
		} else {
			Boolean aborted = validateDestination.cancel(true);
			System.err.println("Object: " + this.hashCode() + " -> Upload possible: " + !aborted);
		}
		
        executorService.shutdown();
	}

	private List<PathWrapper> readSources(List<String> sources) {
		List<PathWrapper> sourceFiles = new ArrayList<>();
		Integer folders = 0, files = 0;
		for (String src : sources) {
			Path resourcePath = Paths.get(src);
			if (Files.exists(resourcePath)) {
				if (Files.isDirectory(resourcePath)) {
					try {
						Files.walkFileTree(resourcePath, new SimpleFileVisitor<Path>() {
							@Override
							public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
								if (!attrs.isDirectory()) {
									PathWrapper pw = new PathWrapper(file);
									pw.setParentDir(resourcePath);
									sourceFiles.add(pw);
								}
								return FileVisitResult.CONTINUE;
							}
						});
						folders++;
					} catch (IOException e) {
						DisplayUtility.displayOutput(DisplayUtility.formatError(MessageCodes.ERROR_UPLOAD_SOURCE, src));
					}
				} else if (Files.isRegularFile(resourcePath)) {
					PathWrapper pw = new PathWrapper(resourcePath);
					sourceFiles.add(pw);
					files++;
				}
			} else {
				//DisplayUtility.displayOutput(DisplayUtility.formatError(MessageCodes.UNDEFINED_UPLOAD_SOURCE, src));
			}
		}
		if (sourceFiles.isEmpty()) {
			//DisplayUtility.displayOutput(DisplayUtility.formatError(MessageCodes.EXHAUSTED_UPLOAD_SOURCE));
		}
		return sourceFiles;
	}
	
	private DestinationValidation verifyDestinationPath(String destinationPath) {
		String instance = System.getProperty(CHAMBER_URL_IN_VARIABLE);
		DestinationValidation validation = null;
		ResponseVO responseVO = null;
		String uri = instance + UPLOAD_BASE_ENDPOINT + UPLOAD_API_VALIDATE
				+ (AppUtils.hasLength(destinationPath) ? "/" + destinationPath : "");
		try {
			Object responseObj = null;
			try {
				responseObj = get(uri, null, DestinationValidation.class);
			} catch (URISyntaxException e) {
				ErrorMetadata ef = new ErrorMetadata(MessageCodes.UNRECOGNIZED_COMMAND, e.getMessage());
				DisplayUtility.formatErrorWithHelp(ef);
			}
			HttpResponse response = null;
			if (null != responseObj && responseObj instanceof HttpResponse) {
				response = (HttpResponse) responseObj;

				HttpEntity entity = response.getEntity();
				int status = response.getStatusLine().getStatusCode();
				if (status == HttpStatus.SC_NON_AUTHORITATIVE_INFORMATION) {
					responseVO = new ObjectMapper().readValue(EntityUtils.toString(entity), ResponseVO.class);
					validation = new DestinationValidation();
					validation.setValidationError(responseVO.getValue());
					validation.setValidationResult(false);
				} else {
					validation = new ObjectMapper().readValue(EntityUtils.toString(entity),
							DestinationValidation.class);
				}
			}
		} catch (IOException e) {
			System.out.println(e.getMessage());
			e.printStackTrace();
		}
		return validation;

	}

	
	
}
